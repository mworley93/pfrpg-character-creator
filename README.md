# Pathfinder RPG Character Creator #

## Description ##
This program is designed to help you create a level one character using the Pathfinder rules system.  Have fun, and thank you for using our program!

## Copyright ##
This project was made possible due to Paizo's Open Game License, which states that the game mechanics of the Pathfinder Roleplaying Game are Open Game Content.  All artwork shown during use is copyright to Paizo Publishing LLC.

## Installation ##
A Windows exe is provided for download in the Downloads tab of this repository.

## Version ##
1.0.0.0

## Screenshots ##

![screen1.PNG](https://bitbucket.org/repo/49yXM5/images/2042694829-screen1.PNG)
![screen2.PNG](https://bitbucket.org/repo/49yXM5/images/424928392-screen2.PNG)
![example.PNG](https://bitbucket.org/repo/49yXM5/images/852967538-example.PNG)

## Developer Notes ##
To generate the final executable:

1.) In Netbeans, clean and rebuild the project so that the jar in the dist folder is up-to-date.  
2.) On the Files tab in the Project window, right click on build.xml.  Run Target-> Other Targets -> package-for-store.  This bundles the 
	pdf-box jar with the CharacterCreator.jar.  
3.) In launch4j, open the launch_config.xml file in the CharacterCreator project file.  This loads the settings for the executable.  
4.) At the top left in launch4j, click "Build Wrapper" (the cog).  Now you should be able to find PFRPG Character Creator.exe in the 
	store folder in the CharacterCreator project.