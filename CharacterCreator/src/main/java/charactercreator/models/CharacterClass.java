/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import java.util.EnumMap;

/**
 *
 * @author Brandon and Megan!
 */

// This class is all about defining the basic information that each CharacterClass will have. In the end, this will be split up
// as not every class needs the same information, this class only contains the info -every class has-.
// In addition, classes will not be inputted manually in this program, but rather a function will be made to allow
// an XML file to be read in to populate any given class with the appropriate information.

public class CharacterClass {
    
    // These are the variables that every single class in the game will use
    private String className;
    private int hitDie;
    private int skillPoints;
    private String[] classFeatures = new String[10];
    private String[] classFeats = new String[10];
    
    private int startingGold;
    private int baseAttackBonus;
    private int fortitude;
    private int reflex;
    private int will;
    private int numberOfFeats;
    private int bonusCombatFeats;
    
    private int bonusSkillPoints;
    
    private EnumMap<Skill, Boolean> classSkills = new EnumMap<>(Skill.class);
    
    public void setClassSkills(EnumMap<Skill, Boolean> newClassSkills) {
        classSkills = newClassSkills;
    }
    
    public Boolean getClassSkill(Skill skill) {
        return classSkills.get(skill);
    }
    
    public EnumMap<Skill, Boolean> getClassSkillsMap() {
        return classSkills;
    }
    
    public void addClassFeats(String[] newFeats) {
        classFeats = newFeats;
    }
    
    public String[] getClassFeats() {
        return classFeats;
    }
    
    public void setClassName(String newClassName) {
        className = newClassName;
    }
    
    public String getClassName() {
        return className;
    }
    
    public void setStartingGold(int newGold) {
        startingGold = newGold;
    }
    
    public int getStartingGold() {
        return startingGold;
    }
    
    public void setHitDie(int newHitDie) {
        hitDie = newHitDie;
    }
    
    public int getHitDie() {
        return hitDie;
    }
    
    public void setSkillPoints(int newSkillPoints) {
        skillPoints = newSkillPoints;
    }
    
    public int getSkillPoints() {
        return skillPoints;
    }
    
    public void setClassFeatures(String[] newClassFeatures) {
        classFeatures = newClassFeatures;
    }
    
    public String[] getClassFeatures() {
        return classFeatures;
    }
    
    public void setBaseAttackBonus(int newBaseAttackBonus) {
        baseAttackBonus = newBaseAttackBonus;
    }
    
    public int getBaseAttackBonus(){
        return baseAttackBonus;
    }
    
    public void setFortitude(int newFortitude) {
        fortitude = newFortitude;
    }
    
    public int getFortitude() {
        return fortitude;
    }
    
    public void setReflex(int newReflex) {
        reflex = newReflex;
    }
    
    public int getReflex(){
        return reflex;
    }
    
    public void setWill(int newWill) {
        will = newWill;
    }
    
    public int getWill() {
        return will;
    }
    
    public void setNumberOfFeats(int newNumberOfFeats){
        numberOfFeats = newNumberOfFeats;
    }
    
    public int getNumberOfFeats(){
        return numberOfFeats;
    }
    
    public void setBonusCombatFeats(int newBonusCombatFeats) {
        bonusCombatFeats = newBonusCombatFeats;
    }
    
    public int getBonusCombatFeats(){
        return bonusCombatFeats;
    }

    
    public int getBonusSkillPoints() {
        return bonusSkillPoints;
    }
}
