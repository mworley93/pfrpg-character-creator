/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import java.lang.String;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Brandon also Megan! she is cool!
 */

// This class defines the data that any given race will contain. A BaseCharacter will then
// hold on to one of this object for reference in regards to anything related to race.

public class CharacterRace {
    
    private String raceName;
    private String size;
    private int speed;
    private String[] raceFeatures = new String[100];
    private int numRaceFeatures;
    private String[] shortRaceFeatures = new String[100];
    private String[] languages = new String[100];
    private int bonusSkillPoints;
    private int bonusFeatPoints;
    
    public void setRaceName(String newRaceName) {
        raceName = newRaceName;
    }
    
    public void cleanFeatures() {
        raceFeatures = clean(raceFeatures);
    }
    
    public void cleanLanguages() {
        languages = clean(languages);
    }
    
    public static String[] clean(final String[] v) {
        List<String> list = new ArrayList<String>(Arrays.asList(v));
        list.removeAll(Collections.singleton(null));
        return list.toArray(new String[list.size()]);
    }
    
    
    public String getRaceName() {
        return raceName;
    }
    
    public void setSize(String newSize) {
        size = newSize;
    }
    
    public String getSize() {
        return size;
    }
    
    public void setSpeed(int newSpeed){
        speed = newSpeed;
    }
    
    public int getSpeed() {
        return speed;
    }
    
    public void setShortRaceFeatures(String[] newFeatures) {
        shortRaceFeatures = newFeatures;
    }
    
    public String[] getShortRaceFeatures() {
        return shortRaceFeatures;
    }
    
    public void setRaceFeatures(String[] newRaceFeatures) {
        raceFeatures = newRaceFeatures;
    }
    
    public String[] getRaceFeatures() {
        return raceFeatures;
    }
    
    public void setLanguages(String[] newLanguages) {
        languages = newLanguages;
    }
    
    public String[] getLanguages() {
        return languages;
    }
    
    private void setBonusFeatPoints(int newBonusFeatPoints) {
        bonusFeatPoints = newBonusFeatPoints;
    }
    
    public int getBonusFeatPoints() {
        return bonusFeatPoints;
    }
    
    private void setBonusSkillPoints(int newBonusSkillPoints) {
        bonusSkillPoints = newBonusSkillPoints;
    }
    
    public int getBonusSkillPoints() {
        return bonusSkillPoints;
    }
    
    public void calculateBonusPoints() {
        calculateBonusFeatPoints();
        calculateBonusSkillPoints();
    }
    
    private void calculateBonusFeatPoints() {
        if(this.getRaceName().matches("Human")) {
            setBonusFeatPoints(1);
        }
        else {
            setBonusFeatPoints(0);
        }
    }
    
    private void calculateBonusSkillPoints() {
        if(this.getRaceName().matches("Human")) {
            setBonusSkillPoints(1);
        }
        else {
            setBonusSkillPoints(0);
        }
    }
    
    public void calculateNumFeatures() {
        int counter = 0;
        for (String s : this.getRaceFeatures()) {
            if (s != null) {
                counter++;
            }
        }
        this.numRaceFeatures = counter;
    }
    
    public int getNumClassFeatures() {
        return numRaceFeatures;
    }
}
