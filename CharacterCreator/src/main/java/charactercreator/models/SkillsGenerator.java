/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

/**
 *
 * @author Brandon plus, dude, Megan is the hardcorest programmer ALIVE
 */

import charactercreator.models.AbilityScoreGenerator.Adjustment;
import java.util.EnumMap;
import java.util.Iterator;

// This class is all about generating  a skill set for a level one Pathfinder character. It does this using EnumMap, and then a
// point buy system. This is then assigned to a character.
public class SkillsGenerator {
    
    // The EnumMap used for saving all the scores
    EnumMap<Skill, Integer> totalSkillMap = new EnumMap<>(Skill.class);
    
    // The EnumMap used to temporarily store the scores as Strings for the -insert Megan panel here-
    EnumMap<Skill, String> stringSkillMap = new EnumMap<>(Skill.class);
    
    EnumMap<Skill, Integer> abilityModSkillMap = new EnumMap<>(Skill.class);
    EnumMap<Skill, Integer> classSkillMap = new EnumMap<>(Skill.class);
    EnumMap<Skill, Integer> rankSkillMap = new EnumMap<>(Skill.class);
    EnumMap<Skill, Integer> miscSkillMap = new EnumMap<>(Skill.class);
    
    // These are the points to be used in the point-buy system
    int totalPoints;
    int points;
    
    public void changePoints(Skill changedSkill, Adjustment adjustment) {
        if(adjustment == Adjustment.UP) {
            points--;
            rankSkillMap.put(changedSkill, (rankSkillMap.get(changedSkill)) + 1);
        }
        
        if(adjustment == Adjustment.DOWN){
            points++;
            rankSkillMap.put(changedSkill, (rankSkillMap.get(changedSkill)) - 1);
        }
        
    }
    
    public void setTotalPoints(int newTotalPoints) {
        totalPoints = newTotalPoints;
    }
    
    public void setSkillString(Skill skillToSet, String value) {
        stringSkillMap.put(skillToSet, value);
    }
    
    public int getPoints() {
        return points;
    }
    
    public void calculatePoints(BaseCharacter baseCharacter) {
        int intModPoints = baseCharacter.getAbilityScoreModifier(AbilityScore.INT);
        if (intModPoints < 0) {
            intModPoints = 0;
        }
        int runningTotal = baseCharacter.getCharacterClass().getSkillPoints() + baseCharacter.getCharacterRace().getBonusSkillPoints() + 
                intModPoints;
        points = runningTotal;
    }
    
    public int getScore(Skill skillToReturn) {
        return totalSkillMap.get(skillToReturn);
    }
    
    public String getScoreString(Skill skillToReturn) {
        return stringSkillMap.get(skillToReturn);
    }
    
    // Resets each skill using an Iterator and sets points back to totalPoints
    public void resetPointsAndSkills() {
        Iterator<Skill> enumKeySet = rankSkillMap.keySet().iterator();
        while(enumKeySet.hasNext()) {
            Skill currentSkill = enumKeySet.next();
            rankSkillMap.put(currentSkill, 0);
        }
        points = totalPoints;
    }
    
    public void resetScoreStrings() {
        Iterator<Skill> enumKeySet = stringSkillMap.keySet().iterator();
        while(enumKeySet.hasNext()) {
            Skill currentSkill = enumKeySet.next();
            stringSkillMap.put(currentSkill, "");
        }
    }
    
    public void convertSkillStrings() {
        Iterator<Skill> enumKeySet = totalSkillMap.keySet().iterator();
        while(enumKeySet.hasNext()) {
            Skill currentSkill = enumKeySet.next();
            totalSkillMap.put(currentSkill, Integer.parseInt(stringSkillMap.get(currentSkill)));
        }
    }
    
    public void convertSkillInts() {
        Iterator<Skill> enumKeySet = stringSkillMap.keySet().iterator();
        while(enumKeySet.hasNext()) {
            Skill currentSkill = enumKeySet.next();
            stringSkillMap.put(currentSkill, Integer.toString(totalSkillMap.get(currentSkill)));
        }
    }
    
    public void calculateTotalSkillMap() {
        int tempTotal = 0;
        Iterator<Skill> skillIterator = totalSkillMap.keySet().iterator();
        while(skillIterator.hasNext()) {
            Skill currentSkill = skillIterator.next();
            tempTotal = rankSkillMap.get(currentSkill) + abilityModSkillMap.get(currentSkill) + classSkillMap.get(currentSkill) + miscSkillMap.get(currentSkill);
            totalSkillMap.put(currentSkill, tempTotal);
        }
    }
    
    public EnumMap<Skill, Integer> getTotalSkillMap() {
        return totalSkillMap;
    }
    
    public EnumMap<Skill, Integer> getRankSkillMap() {
        return rankSkillMap;
    }
    
    public EnumMap<Skill, Integer> getMiscSkillMap() {
        return miscSkillMap;
    }
    
    public void calculateAllSkillMaps(BaseCharacter character) {
        
        calculateClassSkillMap(character);
        calculateMiscSkillMap(character);
        calculateModifierMap(character);
        calculateTotalSkillMap();
        
    }
    
    public Integer getTotalSkill(Skill skill) {
        return totalSkillMap.get(skill);
    }
    
    public void calculateMiscSkillMap(BaseCharacter character) {
        miscSkillMap = character.getMiscSkillMap();
    }
    
    public Integer getMiscSkill(Skill skill) {
        return miscSkillMap.get(skill);
    }
    
    
    public void calculateClassSkillMap(BaseCharacter character) {
        Iterator<Skill> skillIterator = classSkillMap.keySet().iterator();
        while(skillIterator.hasNext()) {
            Skill currentSkill = skillIterator.next();
            if(character.getCharacterClass().getClassSkillsMap().get(currentSkill) && this.rankSkillMap.get(currentSkill) >= 1) {
                classSkillMap.put(currentSkill, 3);
            }
            else {
                classSkillMap.put(currentSkill, 0);
            }
        }
    }
    
    public Integer getClassSkill(Skill skill) {
        return classSkillMap.get(skill);
    }
        
        public void calculateModifierMap(BaseCharacter character) {
            abilityModSkillMap.put(Skill.ACROBATICS, character.getAbilityScoreModifier(AbilityScore.DEX));
            abilityModSkillMap.put(Skill.APPRAISE, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.BLUFF, character.getAbilityScoreModifier(AbilityScore.CHA));
            abilityModSkillMap.put(Skill.CLIMB, character.getAbilityScoreModifier(AbilityScore.STR));
            abilityModSkillMap.put(Skill.CRAFT, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.DIPLOMACY, character.getAbilityScoreModifier(AbilityScore.CHA));
            abilityModSkillMap.put(Skill.DISABLEDEVICE, character.getAbilityScoreModifier(AbilityScore.DEX));
            abilityModSkillMap.put(Skill.DISGUISE, character.getAbilityScoreModifier(AbilityScore.CHA));
            abilityModSkillMap.put(Skill.ESCAPEARTIST, character.getAbilityScoreModifier(AbilityScore.DEX));
            abilityModSkillMap.put(Skill.FLY, character.getAbilityScoreModifier(AbilityScore.DEX));
            abilityModSkillMap.put(Skill.HANDLEANIMAL, character.getAbilityScoreModifier(AbilityScore.CHA));
            abilityModSkillMap.put(Skill.HEAL, character.getAbilityScoreModifier(AbilityScore.WIS));
            abilityModSkillMap.put(Skill.INTIMIDATE, character.getAbilityScoreModifier(AbilityScore.CHA));
            abilityModSkillMap.put(Skill.KNOWLEDGEARCANA, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGEDUNGEONEERING, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGEENGINEERING, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGEGEOGRAPHY, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGEHISTORY, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGELOCAL, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGENATURE, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGENOBILITY, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGEPLANES, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.KNOWLEDGERELIGION, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.LINGUISTICS, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.PERCEPTION, character.getAbilityScoreModifier(AbilityScore.WIS));
            abilityModSkillMap.put(Skill.PERFORM, character.getAbilityScoreModifier(AbilityScore.CHA));
            abilityModSkillMap.put(Skill.PROFESSION, character.getAbilityScoreModifier(AbilityScore.WIS));
            abilityModSkillMap.put(Skill.RIDE, character.getAbilityScoreModifier(AbilityScore.DEX));
            abilityModSkillMap.put(Skill.SENSEMOTIVE, character.getAbilityScoreModifier(AbilityScore.WIS));
            abilityModSkillMap.put(Skill.SLEIGHTOFHAND, character.getAbilityScoreModifier(AbilityScore.DEX));
            abilityModSkillMap.put(Skill.SPELLCRAFT, character.getAbilityScoreModifier(AbilityScore.INT));
            abilityModSkillMap.put(Skill.STEALTH, character.getAbilityScoreModifier(AbilityScore.DEX));
            abilityModSkillMap.put(Skill.SURVIVAL, character.getAbilityScoreModifier(AbilityScore.WIS));
            abilityModSkillMap.put(Skill.SWIM, character.getAbilityScoreModifier(AbilityScore.STR));
            abilityModSkillMap.put(Skill.USEMAGICDEVICE, character.getAbilityScoreModifier(AbilityScore.CHA));            
        }  
        
        public Integer getAbilityModSkill(Skill skill) {
            return abilityModSkillMap.get(skill);
        }   

    
    public SkillsGenerator() {
        resetSkillMap(totalSkillMap);
        resetSkillMap(rankSkillMap);
        resetSkillMap(abilityModSkillMap);
        resetSkillMap(classSkillMap);
        resetSkillMap(miscSkillMap);

        stringSkillMap.put(Skill.ACROBATICS , "");
        stringSkillMap.put(Skill.APPRAISE , "");  
        stringSkillMap.put(Skill.BLUFF , "");
        stringSkillMap.put(Skill.CLIMB , "");
        stringSkillMap.put(Skill.CRAFT , "");
        stringSkillMap.put(Skill.DIPLOMACY , "");  
        stringSkillMap.put(Skill.DISABLEDEVICE , "");
        stringSkillMap.put(Skill.DISGUISE , "");  
        stringSkillMap.put(Skill.ESCAPEARTIST , "");
        stringSkillMap.put(Skill.FLY , "");  
        stringSkillMap.put(Skill.HANDLEANIMAL , "");
        stringSkillMap.put(Skill.HEAL , "");  
        stringSkillMap.put(Skill.INTIMIDATE , "");
        stringSkillMap.put(Skill.KNOWLEDGEARCANA , "");  
        stringSkillMap.put(Skill.KNOWLEDGEDUNGEONEERING , "");
        stringSkillMap.put(Skill.KNOWLEDGEENGINEERING , "");
        stringSkillMap.put(Skill.KNOWLEDGEGEOGRAPHY , "");
        stringSkillMap.put(Skill.KNOWLEDGEHISTORY , "");  
        stringSkillMap.put(Skill.KNOWLEDGELOCAL , "");
        stringSkillMap.put(Skill.KNOWLEDGENATURE , "");  
        stringSkillMap.put(Skill.KNOWLEDGENOBILITY , "");
        stringSkillMap.put(Skill.KNOWLEDGEPLANES , "");  
        stringSkillMap.put(Skill.KNOWLEDGERELIGION , "");
        stringSkillMap.put(Skill.LINGUISTICS , ""); 
        stringSkillMap.put(Skill.PERCEPTION , "");
        stringSkillMap.put(Skill.PERFORM , "");  
        stringSkillMap.put(Skill.PROFESSION , "");
        stringSkillMap.put(Skill.RIDE , "");
        stringSkillMap.put(Skill.SENSEMOTIVE , "");
        stringSkillMap.put(Skill.SLEIGHTOFHAND , "");  
        stringSkillMap.put(Skill.SPELLCRAFT , "");
        stringSkillMap.put(Skill.STEALTH , "");  
        stringSkillMap.put(Skill.SURVIVAL , "");
        stringSkillMap.put(Skill.SWIM , "");  
        stringSkillMap.put(Skill.USEMAGICDEVICE , "");
 
    }
    
        private void resetSkillMap(EnumMap<Skill, Integer> skillMap) {
            skillMap.put(Skill.ACROBATICS, 0);
            skillMap.put(Skill.APPRAISE, 0);
            skillMap.put(Skill.BLUFF, 0);
            skillMap.put(Skill.CLIMB, 0);
            skillMap.put(Skill.CRAFT, 0);
            skillMap.put(Skill.DIPLOMACY, 0);
            skillMap.put(Skill.DISABLEDEVICE, 0);
            skillMap.put(Skill.DISGUISE, 0);
            skillMap.put(Skill.ESCAPEARTIST, 0);
            skillMap.put(Skill.FLY, 0);
            skillMap.put(Skill.HANDLEANIMAL, 0);
            skillMap.put(Skill.HEAL, 0);
            skillMap.put(Skill.INTIMIDATE, 0);
            skillMap.put(Skill.KNOWLEDGEARCANA, 0);
            skillMap.put(Skill.KNOWLEDGEDUNGEONEERING, 0);
            skillMap.put(Skill.KNOWLEDGEENGINEERING, 0);
            skillMap.put(Skill.KNOWLEDGEGEOGRAPHY, 0);
            skillMap.put(Skill.KNOWLEDGEHISTORY, 0);
            skillMap.put(Skill.KNOWLEDGELOCAL, 0);
            skillMap.put(Skill.KNOWLEDGENATURE, 0);
            skillMap.put(Skill.KNOWLEDGENOBILITY, 0);
            skillMap.put(Skill.KNOWLEDGEPLANES, 0);
            skillMap.put(Skill.KNOWLEDGERELIGION, 0);
            skillMap.put(Skill.LINGUISTICS, 0);
            skillMap.put(Skill.PERCEPTION, 0);
            skillMap.put(Skill.PERFORM, 0);
            skillMap.put(Skill.PROFESSION, 0);
            skillMap.put(Skill.RIDE, 0);
            skillMap.put(Skill.SENSEMOTIVE, 0);
            skillMap.put(Skill.SLEIGHTOFHAND, 0);
            skillMap.put(Skill.SPELLCRAFT, 0);
            skillMap.put(Skill.STEALTH, 0);
            skillMap.put(Skill.SURVIVAL, 0);
            skillMap.put(Skill.SWIM, 0);
            skillMap.put(Skill.USEMAGICDEVICE, 0);
        }   

}
