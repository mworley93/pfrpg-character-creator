/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import charactercreator.helpers.XMLReader;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Brandon not to mention Mergoogler
 */

// This class is used to hold onto a selection of feats, check if the BaseCharacter provided fulfills their prerequisites, and allows the user to add feats to their character.

public class FeatGenerator {
    
    ArrayList<Feat> featList = new ArrayList<>(); // Contains every feat.
    ArrayList<Feat> filteredFeatList = new ArrayList<>(); // Holds all of the feats that the character is currently eligible for.
    ArrayList<Feat> charFeats = new ArrayList<>(); // Keeps track of which feats are non-removable.
    ArrayList<Feat> selectedFeats = new ArrayList<>(); // Holds all of the character's feats, selected and non-removable.
    ArrayList<Feat> combatFeatList = new ArrayList<>(); // Holds all the combat feats to select from.
    ArrayList<Feat> selectedCombatFeats = new ArrayList<>(); // Holds all the character's selected combat feats.
    ArrayList<Feat> monkFeats = new ArrayList<>(); // Holds all monk feats to select from.
    ArrayList<Feat> selectedMonkFeats = new ArrayList<>(); // Holds all the character's selected monk feats.
    
    int numFeatsRemaining;
    int numCombatFeatsRemaining;
    int numMonkFeatsRemaining;
    

    
    public void addSelectedFeat(Feat feat) {
        if (!selectedFeats.contains(feat)) {
            selectedFeats.add(feat);
        }
        numFeatsRemaining--;
    }
    
    public void removeSelectedFeat(Feat feat) {   
        selectedFeats.remove(feat);
        numFeatsRemaining++;
    }
    
    public void addSelectedCombatFeat(Feat feat) {
        if (!selectedCombatFeats.contains(feat)) {
            selectedCombatFeats.add(feat);
        }
        numCombatFeatsRemaining--;
    }
    
    public void removeSelectedCombatFeat(Feat feat) {   
        selectedCombatFeats.remove(feat);
        numCombatFeatsRemaining++;
    }    
    
    public void addSelectedMonkFeat(Feat feat) {
        if (!selectedMonkFeats.contains(feat)) {
            selectedMonkFeats.add(feat);
        }
        numMonkFeatsRemaining--;
    }
    
    public void removeSelectedMonkFeat(Feat feat) {   
        selectedMonkFeats.remove(feat);
        numMonkFeatsRemaining++;
    }       
    
    /**
     * Checks which feats are inherent for the current character and adds them to the charFeats list.
     * This feat list should NOT change.
     * @param baseCharacter The character to check.
     */
    public void identifyCharacterFeats(BaseCharacter baseCharacter) {
        if (baseCharacter != null) {
            Iterator<Feat> iter = featList.iterator();
            while (iter.hasNext()) {
                Feat feat = iter.next();
                if(baseCharacter.checkForFeat(feat.getFeatName())) {
                     charFeats.add(feat);
                     selectedFeats.add(feat);
                }
                
            }
        }
    }
    
    /*
     * This method checks if a feat is a prerequisite to another 
     * selected feat.  This needs to be used to determine if the 
     * user is trying to remove a feat that precedes another if they, 
     * have selected both, which should not be allowed.
     */
    public boolean isPrerequisiteToSelected(Feat feat) {
        ArrayList<Feat> successors = new ArrayList<Feat>();
        
        // Iterate through all of the feats that have been selected.  If any of them 
        // have the input feat as a prerequisite, then add them to the list.  
        Iterator<Feat> iter = selectedFeats.iterator();
        while (iter.hasNext()) {
            Feat selectedFeat = iter.next();
            if (selectedFeat.getPrerequisite().compareTo(feat.getFeatName()) == 0) {
                successors.add(selectedFeat);
            }
        }
        // If the list is longer than zero, if means than one or more feats than the user 
        // has selected depend on this feat; therefore, it is a prerequisite.
        boolean isPrereq = false;
        if (successors.size() > 0) {
            isPrereq = true;
        }
        return isPrereq;
    }
    
    public void fillCombatFeats() {
        for (Feat feat : featList) {
            if (feat.getIsCombatFeat()) {
                combatFeatList.add(feat);
            }           
        }
    }
    
    public void fillMonkFeats() {
        monkFeats.add(getFeat("Catch Off-Guard"));
        monkFeats.add(getFeat("Combat Reflexes"));
        monkFeats.add(getFeat("Deflect Arrows"));
        monkFeats.add(getFeat("Dodge"));
        monkFeats.add(getFeat("Improved Grapple"));
        monkFeats.add(getFeat("Scorpion Style"));
        monkFeats.add(getFeat("Throw Anything"));
    }
    
    public void fillFeatList(String fileName) {
        XMLReader tempReader = new XMLReader();
        tempReader.readFeats(fileName, this.featList);
    }
    
    // Given a BaseCharacter, this function determines if they are eligible for the feat, and if so, adds the feat to a filtered arraylist.
    public void checkPrerequisites(BaseCharacter characterToCheck) {
        filteredFeatList.clear();
        for (Feat feat : featList) {
            if (feat.checkPrerequisite(characterToCheck) == true) {
                filteredFeatList.add(feat);
            }
        }
    }
    
    // This gets a feat from the featList, if it exists, and returns it.
    
    public Feat getFeat(String featToCheckFor) {
            for (Feat feat : featList) {
                if (feat.getFeatName().equals(featToCheckFor)) {
                    return feat;
                }
            }
        return null;
    }
    
    public ArrayList<Feat> getFeatList() {
        return featList;
    }
    
    public void resetFeatList() {
        this.featList.clear();
    }
    
    public ArrayList<Feat> getFilteredFeatList() {
        return filteredFeatList;
    }
    
    public ArrayList<Feat> getCombatFeatList() {
        return combatFeatList;
    }
    
    public ArrayList<Feat> getMonkFeatList() {
        return monkFeats;
    }
    
    public ArrayList<Feat> getFilteredFeats() {
        return filteredFeatList;
    }
    
    public ArrayList<Feat> getSelectedFeats() {
        return selectedFeats;
    }
    
    public ArrayList<Feat> getSelectedCombatFeats() {
        return selectedCombatFeats;
    }
    
    public ArrayList<Feat> getSelectedMonkFeats() {
        return selectedMonkFeats;
    }
    
    public ArrayList<Feat> getCharacterFeats() {
        return charFeats;
    }
    
    public void setNumAllowedFeats(int num) {
        numFeatsRemaining = num;
    }
    
    public int getNumFeatsRemaining() {
        return numFeatsRemaining;
    }
    
    public void setNumAllowedCombatFeats(int num) {
        numCombatFeatsRemaining = num;
    }
    
    public int getNumCombatFeatsRemaining() {
        return numCombatFeatsRemaining;
    }
    
    public void setNumAllowedMonkFeats(int num) {
        numMonkFeatsRemaining = num;
    }
    
    public int getNumMonkFeatsRemaining() {
        return numMonkFeatsRemaining;
    }
    
    public void resetTemporaryLists() {
        filteredFeatList.clear();
        selectedFeats.clear();
        charFeats.clear();
    }
    
    public void resetCombatLists() {
        selectedCombatFeats.clear();
    }
    
    public void resetMonkLists() {
        selectedMonkFeats.clear();
    }
    
    /**
     * Adds all of the feat names in the given feat array to a String[].
     * @param feats 
     * @return 
     */
    public String[] convertNamesToReadable(ArrayList<Feat> feats) {
        int count = feats.size();
        String[] readableFeatArray = new String[count];
        for (int i = 0; i < count; ++i) {
            readableFeatArray[i] = feats.get(i).getFeatName();
        }
        return readableFeatArray;
    }
    
    // When supplied with a BaseCharacter and the String name of a single feat, this will look through the entire feat database and add that feat to the BaseCharacter.
    public void addFeatToCharacter(BaseCharacter characterToAddTo, String featToCheckFor) {
        for (Feat feat : featList) {
            if ( feat.getFeatName().equals(featToCheckFor)) {
                characterToAddTo.addToFeatList(feat);
            }
        }
    }
    
    // This function is specifically for taking an array of Strings, and adding all of them to a BaseCharacter.
    public void addClassFeatsToBaseCharacter(BaseCharacter characterToAddTo) {
        String[] featStringArray = characterToAddTo.getCharacterClass().getClassFeats();
        
        for (String featName : featStringArray) {
            if(featName == null) {
                return;
            }
            characterToAddTo.addToFeatList(this.getFeat(featName));
        }
    }
}
