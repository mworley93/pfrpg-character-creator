/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

/**
 * This enum represents the different alignments a character can have.  A string representation can 
 * be obtained by calling .getString() on the enum.
 * @author Megan Worley
 */
public enum Alignment {
    LAWFUL_GOOD("Lawful Good"),
    NEUTRAL_GOOD("Neutral Good"),
    CHAOTIC_GOOD("Chaotic Good"),
    LAWFUL_NEUTRAL("Lawful Neutral"),
    NEUTRAL("Neutral"),
    CHAOTIC_NEUTRAL("Chaotic Neutral"),
    LAWFUL_EVIL("Lawful Evil"),
    NEUTRAL_EVIL("Neutral Evil"),
    CHAOTIC_EVIL("Chaotic Evil");
    
    private final String str;
    
    Alignment(String str) {
        this.str = str;
    }
    
    public String getString() {
        return str;
    }
}
