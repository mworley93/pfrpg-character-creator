/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import java.util.EnumMap;
import java.util.Iterator;
/**
 *
 * @author Brandon (and Megan, she assigned this to me!)
 */

// This class is used to hold on to a temporary ability score EnumMap, which can be changed by the user either by rolling dice
// or allocating points using the point-buy system. This EnumMap will then be placed on the character permanently.

public class AbilityScoreGenerator {
    
    // The EnumMap used for saving all the scores.
    EnumMap<AbilityScore, Integer> purchaseAbilityScoreMap = new EnumMap<>(AbilityScore.class);
    
    // The EnumMap used to temporarily store the scores as Strings for the RollPanel.
    EnumMap<AbilityScore, Integer> rollAbilityScoreMap = new EnumMap<>(AbilityScore.class);
 
   
    // This enumeration helps the changePoints function determine whether to raise or lower the passed abilityscore
    public enum Adjustment { UP, DOWN};
    
    // This array will hold the scores rolled so they can be allocated to specific ability scores
    int[] rolledArray = new int[6];
    
    // These are the points to be used in the point-buy system.
    int totalPoints;
    int points;
    
    // These are the associated points costs with each value (1-18, user should not be able to reach any of the 0's)
    int[] pointsValue = { 0, 0, 0, 0, 0, 0, -4, -2, -1, 0, 1, 2, 3, 5, 7, 10, 13, 17 };
    
    // These are the points costs mapped in a way so that a table can be constructed out of the information.
    private Object[][] pointsTableData = { {7, -4}, {8, -2}, {9, -1}, {10, 0}, {11, 1}, {12, 2}, {13, 3}, {14, 5}, {15, 7}, {16, 10},
        {17, 13}, {18, 17} };
  
    
    // This method fills up the rolled array by rolling groups of four d6's, and adds their totals, while subtracting the lowest value each time.
    
    public void fillRolledArray() {
        int lowest;
        int runningTotal;
        int currentRoll;
        
        for (int i = 0; i<6; i++) {
            runningTotal = 0;
            lowest = 6;
            
            for (int j = 0; j<4; j++) {
                // generate d6 value
                currentRoll = (int)(Math.random()*6) + 1;
                
                // Keep track of lowest number rolled
                if(currentRoll < lowest) {
                    lowest = currentRoll;
                }
                runningTotal += currentRoll;
            }
            // subtract off lowest value and put total into array
            runningTotal -= lowest;
            rolledArray[i] = runningTotal;
        } 
    }
    
    
    public void changePoints(AbilityScore changedScore, Adjustment adjustment) {
        if(adjustment == Adjustment.UP){
            
            // You can't raise a score above 18 using points
            if(purchaseAbilityScoreMap.get(changedScore) > 17) {
                return;
            }
            // First, add the points back to the points array, then increment the Ability score, and subtract the cost of the new value.
            points = points + pointsValue[purchaseAbilityScoreMap.get(changedScore) - 1 ];
            
            // Check to see if points would be negative when incrementing. If so, refund points and return.
            if (points - pointsValue[purchaseAbilityScoreMap.get(changedScore)] < 0) {
                points = points - pointsValue[purchaseAbilityScoreMap.get(changedScore) - 1];
                return;
            }
            
            purchaseAbilityScoreMap.put(changedScore, (purchaseAbilityScoreMap.get(changedScore)) + 1);
            points = points - pointsValue[purchaseAbilityScoreMap.get(changedScore) - 1];
        }
            
        if(adjustment == Adjustment.DOWN){
            // You can't lower a score below 7 using points
            if (purchaseAbilityScoreMap.get(changedScore) < 8) {
                return;
            }
            // Same as before, add the points back to the array, decrement Ability Score, then subtract the cost of the new value
            points = points + pointsValue[purchaseAbilityScoreMap.get(changedScore) - 1];
            purchaseAbilityScoreMap.put(changedScore, (purchaseAbilityScoreMap.get(changedScore)) - 1);
            points = points - pointsValue[purchaseAbilityScoreMap.get(changedScore) - 1];
        }    
    }
    
    public void setPurchaseScore(AbilityScore score, String value) {
        purchaseAbilityScoreMap.put(score, Integer.parseInt(value));
    }
    
    public void setPurchaseScore(AbilityScore score, Integer value) {
        purchaseAbilityScoreMap.put(score, value);
    }
    
    public void setRollScore(AbilityScore score, Integer value) {
        rollAbilityScoreMap.put(score, value);
    }
    
    public void setRollScore(AbilityScore score, String value) {
        if (value.matches("")) {
            rollAbilityScoreMap.put(score, 0);
        }
        else {
            rollAbilityScoreMap.put(score, Integer.parseInt(value));
        }
    }
    
    public int getPoints() {
        return points;
    }
    
    public Object[][] getPointsTableData() {
        return pointsTableData;
    }
    
    public String getPurchaseScore(AbilityScore scoreToReturn) {
        int score = purchaseAbilityScoreMap.get(scoreToReturn);
        return Integer.toString(score);
    }
    
    public String getRollScore(AbilityScore scoreToReturn) {
        int value = rollAbilityScoreMap.get(scoreToReturn);
        if (value == 0) {
            return "";
        }
        else 
            return Integer.toString(value);
    }
    
    /***
     * This function checks the roll array to see if any of the scores are still 0. If they are, return true, because one is empty.
     * If false, then they are all non-zero.
     * @return 
     */
    public Boolean checkRollForEmpty() {
        if(this.rollAbilityScoreMap.get(AbilityScore.CHA) == 0) {
            return true;
        }        
        if(this.rollAbilityScoreMap.get(AbilityScore.CON) == 0) {
            return true;
        }        
        if(this.rollAbilityScoreMap.get(AbilityScore.DEX) == 0) {
            return true;
        }        
        if(this.rollAbilityScoreMap.get(AbilityScore.INT) == 0) {
            return true;
        }        
        if(this.rollAbilityScoreMap.get(AbilityScore.STR) == 0) {
            return true;
        }        
        if(this.rollAbilityScoreMap.get(AbilityScore.WIS) == 0) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public int[] getGeneratedNumbers() {
        return rolledArray;
    }
            
    public void resetPointsAndPurchaseScores() {
        Iterator<AbilityScore> enumKeySet = purchaseAbilityScoreMap.keySet().iterator();
        while(enumKeySet.hasNext()) {
            AbilityScore currentScore = enumKeySet.next();
            purchaseAbilityScoreMap.put(currentScore, 10);
        }
        points = totalPoints;

    }
    
    public void resetRollScores() {
        Iterator<AbilityScore> enumKeySet = rollAbilityScoreMap.keySet().iterator();
        while(enumKeySet.hasNext()) {
            AbilityScore currentScore = enumKeySet.next();
            rollAbilityScoreMap.put(currentScore, 0);
        }
    }
    
    public void setTotalPoints(int total) {
        totalPoints = total;
    }
    
    public EnumMap getRollScores() {
        return rollAbilityScoreMap;
    }
    
    public EnumMap getPurchaseScores() {
        return purchaseAbilityScoreMap;
    }
    
    public AbilityScoreGenerator() {
        purchaseAbilityScoreMap.put(AbilityScore.STR, 10);
        purchaseAbilityScoreMap.put(AbilityScore.DEX, 10);
        purchaseAbilityScoreMap.put(AbilityScore.CON, 10);
        purchaseAbilityScoreMap.put(AbilityScore.INT, 10);
        purchaseAbilityScoreMap.put(AbilityScore.WIS, 10);
        purchaseAbilityScoreMap.put(AbilityScore.CHA, 10);   
        
        rollAbilityScoreMap.put(AbilityScore.STR, 0);
        rollAbilityScoreMap.put(AbilityScore.DEX, 0);
        rollAbilityScoreMap.put(AbilityScore.CON, 0);
        rollAbilityScoreMap.put(AbilityScore.INT, 0);
        rollAbilityScoreMap.put(AbilityScore.WIS, 0);
        rollAbilityScoreMap.put(AbilityScore.CHA, 0);
    }
    
}
