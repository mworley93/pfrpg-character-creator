/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import charactercreator.helpers.PDFCreator;

/**
 * This class acts as a centralized location for all of the data classes in the program.  It was constructed to deal with the issue of multiple controllers needing access to
 * the same model, such as the CardController and ContentPanelController both needing the UIModel, and multiple layers needing the FeatsGenerator, and so on.  It provides a 
 * convenient place to access, initialize, and deallocate the models at will.
 * @author Megan Worley
 */
public class ModelWrapper {
    private UIModel uiModel;
    private BaseCharacter baseCharacter;
    private AbilityScoreGenerator scoreGen;
    private SkillsGenerator skillsGenerator;
    private FeatGenerator featGenerator;
    private EquipmentGenerator equipmentGenerator;
    private AttributeGenerator attributeGenerator;
    private SpellGenerator spellGenerator;
    
    /**
     * Default constructor.
     */
    public ModelWrapper() {
    }
    
    /**
     * Initializes a model held in this ModelWrapper.
     * @param choice The class name of the model to initialize.
     */
    public void initializeModel(String choice) {
        switch (choice) {
            case "UIModel":
                uiModel = new UIModel();
                break;
            case "BaseCharacter":
                baseCharacter = new BaseCharacter();
                break;
            case "AbilityScoreGenerator":
                scoreGen = new AbilityScoreGenerator();
                break;
            case "SkillsGenerator":
                skillsGenerator = new SkillsGenerator();
                break;
            case "FeatGenerator":
                featGenerator = new FeatGenerator();
                break;
            case "EquipmentGenerator":
                equipmentGenerator = new EquipmentGenerator();
                break;
            case "AttributeGenerator":
                attributeGenerator = new AttributeGenerator();
                break;
            case "SpellGenerator":
                spellGenerator = new SpellGenerator();
                break;
        }
    }
    
    /**
     * Sets a model to null so that the garbage collector can clean up its memory footprint.
     * @param choice The class name of the model to deallocate.
     */
    public void deallocateModel(String choice) {
        switch (choice) {
            case "UIModel":
                uiModel = null;
                break;
            case "BaseCharacter":
                baseCharacter = null;
                break;
            case "AbilityScoreGenerator":
                scoreGen = null;
                break;
            case "SkillsGenerator":
                skillsGenerator = null;
                break;
            case "FeatGenerator":
                featGenerator = null;
                break;
            case "EquipmentGenerator":
                equipmentGenerator = null;
                break;
            case "AttributeGenerator":
                attributeGenerator = null;
                break;
            case "SpellGenerator":
                spellGenerator = null;
                break;
        }
    }
    
    public UIModel getUIModel() {
        return uiModel;
    }
    
    public BaseCharacter getBaseCharacter() {
        return baseCharacter;
    }
    
    public AbilityScoreGenerator getAbilityScoreGenerator() {
        return scoreGen;
    }
    
    public SkillsGenerator getSkillsGenerator() {
        return skillsGenerator;
    }
    
    public FeatGenerator getFeatGenerator() {
        return featGenerator;
    }
    
    public EquipmentGenerator getEquipmentGenerator() {
        return equipmentGenerator;
    }
    
    public AttributeGenerator getAttributeGenerator() {
        return attributeGenerator;
    }
    
    public SpellGenerator getSpellGenerator() {
        return spellGenerator;
    }
}
