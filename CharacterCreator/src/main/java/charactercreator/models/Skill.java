/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;


/**
 *
 * @author Brandon and MEGANNNNNNN!
 */

// This defines the many skills in Pathfinder, to be used typically in an EnumMap which will be mapped to the character.

public enum Skill {
    ACROBATICS ("Acrobatics"), 
    APPRAISE ("Appraise"), 
    BLUFF ("Bluff"), 
    CLIMB ("Climb"), 
    CRAFT ("Craft"), 
    DIPLOMACY ("Diplomacy"), 
    DISABLEDEVICE ("Disable Device"), 
    DISGUISE ("Disguise"), 
    ESCAPEARTIST ("Escape Artist"), 
    FLY ("Fly"), 
    HANDLEANIMAL ("Handle Animal"), 
    HEAL ("Heal"), 
    INTIMIDATE ("Intimidate"), 
    KNOWLEDGEARCANA ("Knowledge (Arcana)"), 
    KNOWLEDGEDUNGEONEERING ("Knowledge (Dungeoneering)"), 
    KNOWLEDGEENGINEERING ("Knowledge (Engineering)"), 
    KNOWLEDGEGEOGRAPHY ("Knowledge (Geography)"), 
    KNOWLEDGEHISTORY ("Knowledge (History)"), 
    KNOWLEDGELOCAL ("Knowledge (Local)"), 
    KNOWLEDGENATURE ("Knowledge (Nature)"), 
    KNOWLEDGENOBILITY ("Knowledge (Nobility)"), 
    KNOWLEDGEPLANES ("Knowledge (Planes)"), 
    KNOWLEDGERELIGION ("Knowledge (Religion)"), 
    LINGUISTICS ("Linguistics"), 
    PERCEPTION ("Perception"), 
    PERFORM ("Perform"), 
    PROFESSION ("Profession"), 
    RIDE ("Ride"), 
    SENSEMOTIVE ("Sense Motive"), 
    SLEIGHTOFHAND ("Sleight of Hand"), 
    SPELLCRAFT ("Spellcraft"), 
    STEALTH ("Stealth"), 
    SURVIVAL ("Survival"), 
    SWIM ("Swim"), 
    USEMAGICDEVICE ("Use Magic Device");

    private final String text;
    
    Skill(String text) {
        this.text = text;
    }
    
    public static Skill fromString(String name) {
        if (name != null) {
            for (Skill skill : Skill.values()) {
                if (name.equalsIgnoreCase(skill.text)) {
                    return skill;
                }
            }
        }
        return null;
    }
    
    public String getString() {
        return text;
    }
}
