/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

/**
 *
 * @author Brandon
 */
public class Spell {
    int spellLevel;
    String school;
    String description;
    String longDescription;
    String spellName;
    
    public void setSpellLevel(int newSpellLevel) {
        spellLevel = newSpellLevel;
    }
    
    public int getSpellLevel() {
        return spellLevel;
    }
    
    public void setSchool(String newSchool) {
        school = newSchool;
    }
    
    public String getSchool() {
        return school;
    }
    
    public void setDescription(String newDescription) {
        description = newDescription;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setLongDescription(String newLongDescription) {
        longDescription = newLongDescription;
    }
    
    public String getLongDescription() {
        return longDescription;
    }
    
    public void setSpellName(String newSpellName) {
        spellName = newSpellName;
    }
    
    public String getSpellName() {
        return spellName;
    }
}
