
package charactercreator.models;

import charactercreator.models.equipment.Armor;
import charactercreator.models.equipment.Equipment;
import charactercreator.models.equipment.Weapon;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.ArrayList;

/**
 *
 * @author Megan and Brandon!
 */
 
 /*
	BaseCharacter is the primary class of the Pathfinder Character Creator.
	This class will be instantiated when the user begins the character creation process, and then edited as the user
	uses the GUI's buttons and other inputs. At the end, the character as a whole will be output to the user
	for use in their Pathfinder roleplaying game.
	
	This class will contain all the information that every character will have. Class and race information will be contained within an
	object of that type, which will be a private member of the basecharacter class.
 

 */
 
 
public class BaseCharacter {

	// Private variables
	
	private String name;
	private String gender;
	private String biography;
	private CharacterRace characterRace;
	private CharacterClass characterClass;
	private String playerName;
	private int height;
	private int weight;
	private int age;
        private String size;
	private String alignment;
	private String deity;
	private String[] languages;
        private String bloodline;
        private String arcaneBond;
        private String favoredEnemy;
        private String druidClassFeature;
        private String animalCompanion;
        private String firstDomain;
        private String secondDomain;
        private String arcaneSchool;
        private String forbiddenArcaneSchoolOne;
        private String forbiddenArcaneSchoolTwo;
        private String familiar;
	
	private int speed;
	
	private int fortitude;
	private int reflex;
	private int will;
	
	private int hitPoints;
        private int baseAttackBonus;
	private int cmb;
	private int cmd;
	
        private int skillPointsPerLevel;
        
	private int initiative;
	
	EnumMap<AbilityScore, Integer> baseAbilityScoreMap = new EnumMap<>(AbilityScore.class);
	EnumMap<AbilityScore, Integer> finalAbilityScoreMap = new EnumMap<>(AbilityScore.class);
	EnumMap<AbilityScore, Integer> abilityScoreModifierMap = new EnumMap<>(AbilityScore.class);
        EnumMap<AbilityScore, Integer> raceAbilityScoreMap = new EnumMap<>(AbilityScore.class);
	
	EnumMap<Skill, Integer> baseSkillMap = new EnumMap<>(Skill.class);
        EnumMap<Skill, Integer> rankSkillMap = new EnumMap<>(Skill.class);
        EnumMap<Skill, Integer> miscSkillMap = new EnumMap<>(Skill.class);
        EnumMap<Skill, Integer> finalSkillMap = new EnumMap<>(Skill.class);

	private ArrayList<Feat> featList = new ArrayList<>();
        private ArrayList<Weapon> weaponsList = new ArrayList<>();
        private ArrayList<Armor> armorList = new ArrayList<>();
        private ArrayList<Equipment> equipmentList = new ArrayList<>();
        private ArrayList<Spell> spellList = new ArrayList<>();
	
	public BaseCharacter() {
            this.initializeAllAbilityScoreMaps();
            this.initializeSkillMaps();
        }
	// Methods
	
	// Get and set methods
        
        public void setSpellList(ArrayList<Spell> newSpellList) {
            spellList = newSpellList;
        }
        
        public ArrayList<Spell> getSpellList() {
            return spellList;
        }
        
        public void printSpellList() {
            for (Spell spell : spellList) {
                System.out.println(spell.spellName);
            }
        }        
        
        public void addToFeatList(Feat featToAdd) {
            featList.add(featToAdd);
        }
        
        public void removeFromFeatList(Feat feat) {
            featList.remove(feat);
        }
        
        public void removeFeatsFromFeatList(ArrayList<Feat> featsToRemove) {
            for (Feat feat : featsToRemove) {
                this.removeFromFeatList(feat);
            }
        }
        
        public ArrayList<Feat> getFeatList() {
            return featList;
        }
        
        public void resetFeatsList() {
            this.featList.clear();
        }
        
        public void removeDuplicateFeats() {
            ArrayList<Feat> tempFeatList = new ArrayList<>();
            for (Feat feat : featList) {
                if (!tempFeatList.contains(feat)) tempFeatList.add(feat);
            }
            featList = tempFeatList;
        }
        
        public ArrayList<Weapon> getWeaponsList() {
            return weaponsList;
        }
        
        public void setWeaponsList(ArrayList<Weapon> newWeaponsList) {
            weaponsList = newWeaponsList;
        }
        
        public void addWeapon(Weapon newWeapon) {
            if(!weaponsList.contains(newWeapon)) {
                weaponsList.add(newWeapon);
            }
        }
        
        public void removeWeapon(Weapon weaponToRemove) {
            weaponsList.remove(weaponToRemove);
        }
        
        public ArrayList<Armor> getArmorList() {
            return armorList;
        }
        
        public void setArmorList(ArrayList<Armor> newArmorList) {
            armorList = newArmorList;
        }
        
        public void addArmor(Armor newArmor) {
            if(!armorList.contains(newArmor)) {
                armorList.add(newArmor);
            }
        }
        
        public void removeArmor(Armor weaponToRemove) {
            armorList.remove(weaponToRemove);
        }        
        
        public void printCurrentFeatList() {
            for (Feat feat : featList) {
                System.out.println(feat.getFeatName() + ": " + feat.getDescription());
            }
        }
        
        public Boolean checkForFeat(String featToCheckFor) {
            for (Feat feat : featList) {
                if (feat.getFeatName().equals(featToCheckFor)) {
                    return true;
                }
            }
            return false;
        }        
        
	public String getName() {
		return name;
	}
	
	public void setName(String newName) {
		name = newName;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String newGender) {
		gender = newGender;
	}
	
	public String getBiography() {
		return biography;
	}
	
	public void setBiography(String newBiography) {
		biography = newBiography;
	}
        
        public String getBloodline() {
            return bloodline;
        }
        
        public void setBloodline(String newBloodline) {
            bloodline = newBloodline;
        }
        
        public String getArcaneSchool() {
            return arcaneSchool;
        }
        
        public void setArcaneSchool(String newArcaneSchool) {
            arcaneSchool = newArcaneSchool;
        }
        
        public String getForbiddenArcaneSchoolOne() {
            return forbiddenArcaneSchoolOne;
        }
        
        public void setForbiddenArcaneSchoolOne(String newSchool) {
            forbiddenArcaneSchoolOne = newSchool;
        }
        
        public String getForbiddenArcaneSchoolTwo() {
            return forbiddenArcaneSchoolTwo;
        }
        
        public void setForbiddenArcaneSchoolTwo(String newSchool) {
            forbiddenArcaneSchoolTwo = newSchool;
        }
        
        public String getFavoredEnemy() {
            return favoredEnemy;
        }
        
        public void setFavoredEnemy(String newFavoredEnemy) {
            favoredEnemy = newFavoredEnemy;
        }
        
        public String getDruidFeature() {
            return druidClassFeature;
        }
        
        public void setDruidFeature(String newDruidClassFeature) {
            druidClassFeature = newDruidClassFeature;
        }
	
        public String getAnimalCompanion() {
            return animalCompanion;
        }
        
        public void setAnimalCompanion(String newAnimalCompanion) {
            animalCompanion = newAnimalCompanion;
        }
        
        public String getFirstDomain() {
            return firstDomain;
        }
        
        public void setFirstDomain(String newFirstDomain) {
            firstDomain = newFirstDomain;
        }
        
        public String getSecondDomain() {
            return secondDomain;
        }
        
        public void setSecondDomain(String newSecondDomain) {
            secondDomain = newSecondDomain;
        }
        
        public String getArcaneBond() {
            return arcaneBond;
        }
        
        public void setArcaneBond(String newArcaneBond) {
            arcaneBond = newArcaneBond;
        }
        
        public String getFamiliar() {
            return familiar;
        }
        
        public void setFamiliar(String newFamiliar) {
            familiar = newFamiliar;
        }
        
	public CharacterRace getCharacterRace() {
		return characterRace;
	}
	
	public void setCharacterRace(CharacterRace newCharacterRace) {
		characterRace = newCharacterRace;
	}
        
	public CharacterClass getCharacterClass() {
		return characterClass;
	}
	
	public void setCharacterClass(CharacterClass newCharacterClass) {
		characterClass = newCharacterClass;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public void setPlayerName(String newPlayerName) {
		playerName = newPlayerName;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int newHeight) {
		height = newHeight;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setWeight(int newWeight) {
		weight = newWeight;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int newAge) {
		age = newAge;
	}
	
	public String getAlignment() {
            return alignment;
	}
	
	public void setAlignment(String newAlignment) {
		alignment = newAlignment;
	}
	
	public String getDeity() {
		return deity;
	}
	
	public void setDeity(String newDeity) {
		deity = newDeity;
	}
        
        public String getSize() {
            return size;
        }
        
        public void setSize(String newSize) {
            size = newSize;
        }
	
	public String[] getLanguages() {
		return languages;
	}
	
	public void setLanguages(String[] newLanguages) {
		languages = newLanguages;
	}
	
	public int getSpeed() {
		return speed;
	}
	
	public void setSpeed(int newSpeed) {
		speed = newSpeed;
	}   

	public int getFortitude() {
		return fortitude;
	}
	
	public void setFortitude(int newFortitude) {
		fortitude = newFortitude;
	}
	
	public int getReflex() {
		return reflex;
	}
	
	public void setReflex(int newReflex) {
		reflex = newReflex;
	}
	
	public int getWill() {
		return will;
	}
	
	public void setWill(int newWill) {
		will = newWill;
	}
	
	public void calculateInitiative() {
            initiative = abilityScoreModifierMap.get(AbilityScore.DEX);
	}
        
	public EnumMap<AbilityScore, Integer> getBaseAbilityScores() {
		return baseAbilityScoreMap;
	}
	
	public void setBaseAbilityScores(EnumMap<AbilityScore, Integer> newAbilityScoreMap) {
		baseAbilityScoreMap.putAll(newAbilityScoreMap);
	}
        
        public void printAbilityScores(EnumMap<AbilityScore, Integer> map) {
            Iterator<AbilityScore> itr = map.keySet().iterator();
            
            while(itr.hasNext()) {
                AbilityScore currentScore = itr.next();
                System.out.println(map.get(currentScore));
            }
        }
        
        public void setRaceAbilityScore(AbilityScore score, int num) {
            this.raceAbilityScoreMap.put(score, num);
        }
        
        public EnumMap<AbilityScore, Integer> getAbilityScoreModifierMap() {
            return abilityScoreModifierMap;
        }        
        
        public void resetAbilityScoreModifiers() {
            this.initializeAbilityScoreMap(abilityScoreModifierMap);
        }
        
        public void resetFinalAbilityScoreMap() {
            this.initializeAbilityScoreMap(finalAbilityScoreMap);
        }
        
        public void resetBaseAbilityScoreMap() {
            this.initializeAbilityScoreMap(baseAbilityScoreMap);
        }
        
        public EnumMap<AbilityScore, Integer> getFinalAbilityScores() {
            return finalAbilityScoreMap;
        }
        
        public void setBaseSkillMap (EnumMap<Skill, Integer> newBaseSkillMap) {
            baseSkillMap = newBaseSkillMap;
        }
        
        public EnumMap<Skill, Integer> getBaseSkillMap() {
            return baseSkillMap;
        }
        
        public void setRankSkillMap (EnumMap<Skill, Integer> newRankSkillMap) {
            rankSkillMap = newRankSkillMap;
        }
        
        public EnumMap<Skill, Integer> getRankSkillMap() {
            return rankSkillMap;
        }
        
        public void setMiscSkillMap(EnumMap<Skill, Integer> newMiscSkillMap) {
            miscSkillMap = newMiscSkillMap;
        }
        
        public EnumMap<Skill, Integer> getMiscSkillMap() {
            return miscSkillMap;
        }
        
        public void setMiscSkill(Skill skill, int value) {
            miscSkillMap.put(skill, value);
        }
        
        public int getMiscSkill(Skill skill) {
            return miscSkillMap.get(skill);
        }
        
        public void setFinalSkillMap (EnumMap<Skill, Integer> newFinalSkillMap) {
            finalSkillMap = newFinalSkillMap;
        }
        
        public EnumMap<Skill, Integer> getFinalSkillMap() {
            return finalSkillMap;
        }     
        
        public void initializeAllAbilityScoreMaps() {
            initializeAbilityScoreMap(this.baseAbilityScoreMap);
            initializeAbilityScoreMap(this.finalAbilityScoreMap);
            initializeAbilityScoreMap(this.abilityScoreModifierMap);
            initializeAbilityScoreMap(this.raceAbilityScoreMap);
        }
        
        public void initializeAbilityScoreMap(EnumMap<AbilityScore, Integer> abilityScoreMap) {
            abilityScoreMap.put(AbilityScore.CHA, 0);
            abilityScoreMap.put(AbilityScore.CON, 0);
            abilityScoreMap.put(AbilityScore.DEX, 0);
            abilityScoreMap.put(AbilityScore.INT, 0);
            abilityScoreMap.put(AbilityScore.STR, 0);
            abilityScoreMap.put(AbilityScore.WIS, 0);
        }
        
        public void setFinalAbilityScores(EnumMap<AbilityScore, Integer> newFinalAbilityScoreMap) {
                finalAbilityScoreMap = newFinalAbilityScoreMap;
        }
	
	// Calculates the Ability Modifiers by iterating over the abilityScoreModifier map and filling in each enumeration
	// with the appropriate value by pulling the equivalent score from the finalAbilityScoreMap and performing the calculation
	// to determine the ability modifier.
	
	public void calculateAbilityModifiers() {
	
		// Iterate over the modifier enumMap, and fill it in with the appropriately calculated score taken from the
		// finalAbilityScore map.
		Iterator<AbilityScore> enumKeySet = abilityScoreModifierMap.keySet().iterator();
		int temporaryAbilityScoreModifier;
                
		while(enumKeySet.hasNext()) {
			AbilityScore currentState = enumKeySet.next();
                        temporaryAbilityScoreModifier = finalAbilityScoreMap.get(currentState) / 2 - 5;
                        
			abilityScoreModifierMap.put(currentState, temporaryAbilityScoreModifier);
		}
	}
        
        public int getAbilityScoreModifier(AbilityScore abilityScoreToGet) {
            return abilityScoreModifierMap.get(abilityScoreToGet);
        }
	
	// Calculations of Final Ability Scores
	
	public void calculateFinalAbilityScores() {
            Iterator<AbilityScore> enumKeySet = finalAbilityScoreMap.keySet().iterator();
            while(enumKeySet.hasNext()) {
                AbilityScore currentScore = enumKeySet.next();
                finalAbilityScoreMap.put(currentScore, baseAbilityScoreMap.get(currentScore) + raceAbilityScoreMap.get(currentScore));
            }
	}
        
        public void setRaceAbilityScoreMap(EnumMap<AbilityScore, Integer> newMap) {
            raceAbilityScoreMap = newMap;
        }
        
        public EnumMap getRaceAbilityScores() {
            return raceAbilityScoreMap;
        }
        
        public void resetRaceAbilityScoreMap() {
            initializeAbilityScoreMap(this.raceAbilityScoreMap);
        }
	
	// Calculation of Fortitude, Reflex, Will
	
	public void calculateFortitude(int classFortitude) {
		fortitude = abilityScoreModifierMap.get(AbilityScore.CON) + this.getCharacterClass().getFortitude();
	}
	
	public void calculateReflex(int classReflex) {
		reflex = abilityScoreModifierMap.get(AbilityScore.DEX) + this.getCharacterClass().getReflex();
	}
	
	public void calculateWill(int classWill) {
		will = abilityScoreModifierMap.get(AbilityScore.WIS) + this.getCharacterClass().getReflex();
	}
	
        public void calculateSkillPointsPerLevel() {
            skillPointsPerLevel = abilityScoreModifierMap.get(AbilityScore.INT) + this.characterClass.getSkillPoints();
        }
        
        public int getSkillPointsPerLevel() {
            return skillPointsPerLevel;
        }
        
        public void initializeSkillMaps() {
            resetSkillMap(miscSkillMap);
            resetSkillMap(rankSkillMap);
            resetSkillMap(baseSkillMap);
            resetSkillMap(finalSkillMap);
        }
        
        public void resetSkillMap(EnumMap<Skill, Integer> skillMap) {
            skillMap.put(Skill.ACROBATICS, 0);
            skillMap.put(Skill.APPRAISE, 0);
            skillMap.put(Skill.BLUFF, 0);
            skillMap.put(Skill.CLIMB, 0);
            skillMap.put(Skill.CRAFT, 0);
            skillMap.put(Skill.DIPLOMACY, 0);
            skillMap.put(Skill.DISABLEDEVICE, 0);
            skillMap.put(Skill.DISGUISE, 0);
            skillMap.put(Skill.ESCAPEARTIST, 0);
            skillMap.put(Skill.FLY, 0);
            skillMap.put(Skill.HANDLEANIMAL, 0);
            skillMap.put(Skill.HEAL, 0);
            skillMap.put(Skill.INTIMIDATE, 0);
            skillMap.put(Skill.KNOWLEDGEARCANA, 0);
            skillMap.put(Skill.KNOWLEDGEDUNGEONEERING, 0);
            skillMap.put(Skill.KNOWLEDGEENGINEERING, 0);
            skillMap.put(Skill.KNOWLEDGEGEOGRAPHY, 0);
            skillMap.put(Skill.KNOWLEDGEHISTORY, 0);
            skillMap.put(Skill.KNOWLEDGELOCAL, 0);
            skillMap.put(Skill.KNOWLEDGENATURE, 0);
            skillMap.put(Skill.KNOWLEDGENOBILITY, 0);
            skillMap.put(Skill.KNOWLEDGEPLANES, 0);
            skillMap.put(Skill.KNOWLEDGERELIGION, 0);
            skillMap.put(Skill.LINGUISTICS, 0);
            skillMap.put(Skill.PERCEPTION, 0);
            skillMap.put(Skill.PERFORM, 0);
            skillMap.put(Skill.PROFESSION, 0);
            skillMap.put(Skill.RIDE, 0);
            skillMap.put(Skill.SENSEMOTIVE, 0);
            skillMap.put(Skill.SLEIGHTOFHAND, 0);
            skillMap.put(Skill.SPELLCRAFT, 0);
            skillMap.put(Skill.STEALTH, 0);
            skillMap.put(Skill.SURVIVAL, 0);
            skillMap.put(Skill.SWIM, 0);
            skillMap.put(Skill.USEMAGICDEVICE, 0);
        }
        
        public void resetAndSetRacialSkillBonuses() {
            resetSkillMap(miscSkillMap);
            setRacialSkillBonuses();
        }
        
        private void setRacialSkillBonuses() {
            // Sure Foot
            if(this.getCharacterRace().getRaceName().matches("Halfling")) {
                setMiscSkill(Skill.ACROBATICS, 2);
                setMiscSkill(Skill.CLIMB, 2);
            }
            
            // Keen Senses
            if(this.getCharacterRace().getRaceName().matches("Elf") ||
                    this.getCharacterRace().getRaceName().matches("Half-elf") ||
                            this.getCharacterRace().getRaceName().matches("Halfling")) {
                setMiscSkill(Skill.PERCEPTION, 2);
            }
            
            // Intimidating
            if(this.getCharacterRace().getRaceName().matches("Half-orc")) {
                setMiscSkill(Skill.INTIMIDATE, 2);
            }
        }
}
