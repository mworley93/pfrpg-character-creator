/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import charactercreator.helpers.Range;
import charactercreator.helpers.XMLReader;
import java.util.HashMap;
import java.util.Random;

/**
 * This class holds information about the character's personality and appearance, used as the model on 
 * the misc panel.
 * @author Megan
 */
public class AttributeGenerator {
    private HashMap<String, Range> raceAgeRanges;
    private HashMap<String, Integer> maleHeightBases;
    private HashMap<String, Integer> maleHeightMods;
    private HashMap<String, Integer> femaleHeightBases;
    private HashMap<String, Integer> femaleHeightMods;
    private HashMap<String, Integer> maleWeightBases;
    private HashMap<String, Integer> maleWeightMods;
    private HashMap<String, Integer> femaleWeightBases;
    private HashMap<String, Integer> femaleWeightMods;
    
    private int age;
    private int height;
    private int weight;
    private String characterName;
    private String playerName;
    private String background;
    private Alignment alignment;
    private Gender gender;
    
    /**
     * Default constructor.
     */
    public AttributeGenerator() {
        readVitalsInformation();
        gender = Gender.MALE;
        alignment = Alignment.LAWFUL_GOOD;
        characterName = "";
        playerName = "";
        background = "";
    }
    
    /**
     * Reads in age, height, and weight statistics as they correspond to race from 
     * an XML file.
     */
    private void readVitalsInformation() {
        XMLReader reader = new XMLReader();
        raceAgeRanges = reader.readAges("/data/vitalsData.xml");
        HashMap[] heights = reader.readHeights("/data/vitalsData.xml");
        maleHeightBases = heights[0];
        maleHeightMods = heights[1];
        femaleHeightBases = heights[2];
        femaleHeightMods = heights[3];
        HashMap[] weights = reader.readWeights("/data/vitalsData.xml");
        maleWeightBases = weights[0];
        maleWeightMods = weights[1];
        femaleWeightBases = weights[2];
        femaleWeightMods = weights[3];
    }
    
    /**
     * Randomizes the character's age.  It will set a result within the adulthood years, so 
     * no negative age results are incurred.
     * @param race 
     */
    public void randomizeAge(String race) {
        Range ageRange = raceAgeRanges.get(race);
        age = ageRange.getRandom();
    }
    
    /**
     * Sets the character's age in years.
     * @param years 
     */
    public void setAge(int years) {
        age = years;
    }
    
    /**
     * Returns the character's age in years.
     * @return 
     */
    public int getAge() {
        return age;
    }
    
    
    /**
     * Randomizes the character's height.
     * @param gender 
     * @param race 
     */
    public void randomizeHeight(Gender gender, String race) {
        int base = 0;
        int mod = 0;
        if (gender == Gender.MALE) {
            base = maleHeightBases.get(race);
            mod = maleHeightMods.get(race);
        }
        else if (gender == Gender.FEMALE) {
            base = femaleHeightBases.get(race);
            mod = femaleHeightMods.get(race);
        }
        Random rand = new Random();
        int rolled = Math.abs(rand.nextInt()) % mod + 1;
        height = (base + 2*rolled);
    }
    
    public void setHeight(int feet, int inches) {
        height = feet * 12 + inches;
    }
    
    /**
     * Returns the character's height in inches.
     * @return 
     */
    public int getHeight() {
        return height;
    }
    

    /**
     * Randomizes the character's weight.
     * @param gender
     * @param race 
     */
    public void randomizeWeight(Gender gender, String race) {
        int base = 0;
        int mod = 0;
        int mult = 0;
        if (gender == Gender.MALE) {
            base = maleWeightBases.get(race);
            mod = maleHeightMods.get(race);
            mult = maleWeightMods.get(race);
        }
        else if (gender == Gender.FEMALE) {
            base = femaleWeightBases.get(race);
            mod = femaleHeightMods.get(race);
            mult = femaleWeightMods.get(race);
        }
        Random rand = new Random();
        int rolled = Math.abs(rand.nextInt()) % mod + 1;
        weight = (base + 2*rolled * mult);
    }
    
    /**
     * Sets the character's weight in pounds.
     * @param lbs 
     */
    public void setWeight(int lbs) {
        weight = lbs;
    }
    
    /**
     * Returns the character's weight in pounds.
     * @return 
     */
    public int getWeight() {
        return weight;
    }
    
    /**
     * Sets the name of the character.
     * @param name 
     */
    public void setCharacterName(String name) {
        characterName = name;
    }
    
    /**
     * Gets the name of the character.
     * @return 
     */
    public String getCharacterName() {
        return characterName;
    }
    
    /**
     * Sets the name of the player creating the character.
     * @param name 
     */
    public void setPlayerName(String name) {
        playerName = name;
    }
    
    /**
     * Gets the name of the player creating the character.
     * @return 
     */
    public String getPlayerName() {
        return playerName;
    }
    
    /**
     * Sets the description of the character's background.
     * @param background 
     */
    public void setBackground(String background) {
        this.background = background;
    }
    
    /**
     * Gets the description of the character's background.
     * @return 
     */
    public String getBackground() {
        return background;
    }
    
    /**
     * Sets this character's alignment.
     * @param alignment 
     */
    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }
    
    public void setAlignment(String alignment) {
        if (alignment.matches(Alignment.LAWFUL_GOOD.getString())) {
            this.alignment = Alignment.LAWFUL_GOOD;
        }
        else if (alignment.matches(Alignment.NEUTRAL_GOOD.getString())) {
            this.alignment = Alignment.NEUTRAL_GOOD;
        }
        else if (alignment.matches(Alignment.CHAOTIC_GOOD.getString())) {
            this.alignment = Alignment.CHAOTIC_GOOD;
        }
        else if (alignment.matches(Alignment.LAWFUL_NEUTRAL.getString())) {
            this.alignment = Alignment.LAWFUL_NEUTRAL;
        }
        else if (alignment.matches(Alignment.NEUTRAL.getString())) {
            this.alignment = Alignment.NEUTRAL;
        }
        else if (alignment.matches(Alignment.CHAOTIC_NEUTRAL.getString())) {
            this.alignment = Alignment.CHAOTIC_NEUTRAL;
        }
        else if (alignment.matches(Alignment.LAWFUL_EVIL.getString())) {
            this.alignment = Alignment.LAWFUL_EVIL;
        }
        else if (alignment.matches(Alignment.NEUTRAL_EVIL.getString())) {
            this.alignment = Alignment.NEUTRAL_EVIL;
        }
        else {
            this.alignment = Alignment.CHAOTIC_EVIL;
        }
    }
    
    /**
     * Returns this character's alignment.
     * @return 
     */
    public Alignment getAlignment() {
        return alignment;
    }
    
    /**
     * Sets this character's gender.
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }
    
    /**
     * Sets this character's gender.
     */
    public void setGender(String gender) {
        if (gender.matches(Gender.MALE.getString())) {
            this.gender = Gender.MALE;
        }
        else {
            this.gender = Gender.FEMALE;
        }
    }
    
    /**
     * Gets this character's gender.
     */
    public Gender getGender() {
        return gender;
    }
}
