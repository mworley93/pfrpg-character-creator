package charactercreator.models;

/**
 * Defines essential ability score types needed by several classes.
 * @author Brandon Sharp, Megan Worley
 */
    public enum AbilityScore{
        STR("STR"), 
        DEX("DEX"), 
        CON("CON"), 
        INT("INT"), 
        WIS("WIS"), 
        CHA("CHA");

        String str;

        AbilityScore(String str) {
            this.str = str;
        }

        public String getString() {
            return str;
        }
    }

