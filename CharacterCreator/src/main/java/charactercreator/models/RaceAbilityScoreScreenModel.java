/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;


/**
 *
 * @author Brandon
 */
public class RaceAbilityScoreScreenModel {
    
    String[][] scoreTable;
    
    public RaceAbilityScoreScreenModel() {
        scoreTable = new String[][]{{"STR", "0", "0", "0", "0"}, {"DEX", "0", "0", "0", "0"}, {"CON", "0", "0", "0", "0"}, {"INT", "0", "0", "0", "0"}, {"WIS", "0", "0", "0", "0"}, {"CHA", "0", "0", "0", "0"} };
        
    }
    
    public void updateTableForCharacter(BaseCharacter basecharacter) {
        updateModifiers(basecharacter);
        updateBase(basecharacter);
        updateRacialBonus(basecharacter);
        updateTotals(basecharacter);        
    }
    
    private void updateModifiers(BaseCharacter basecharacter) {
        scoreTable[0][2] = Integer.toString(basecharacter.getAbilityScoreModifierMap().get(AbilityScore.STR));
        scoreTable[1][2] = Integer.toString(basecharacter.getAbilityScoreModifierMap().get(AbilityScore.DEX));
        scoreTable[2][2] = Integer.toString(basecharacter.getAbilityScoreModifierMap().get(AbilityScore.CON));
        scoreTable[3][2] = Integer.toString(basecharacter.getAbilityScoreModifierMap().get(AbilityScore.INT));
        scoreTable[4][2] = Integer.toString(basecharacter.getAbilityScoreModifierMap().get(AbilityScore.WIS));
        scoreTable[5][2] = Integer.toString(basecharacter.getAbilityScoreModifierMap().get(AbilityScore.CHA));              
    }
    
    private void updateBase(BaseCharacter basecharacter) {
        scoreTable[0][3] = Integer.toString(basecharacter.getBaseAbilityScores().get(AbilityScore.STR));
        scoreTable[1][3] = Integer.toString(basecharacter.getBaseAbilityScores().get(AbilityScore.DEX));
        scoreTable[2][3] = Integer.toString(basecharacter.getBaseAbilityScores().get(AbilityScore.CON));
        scoreTable[3][3] = Integer.toString(basecharacter.getBaseAbilityScores().get(AbilityScore.INT));
        scoreTable[4][3] = Integer.toString(basecharacter.getBaseAbilityScores().get(AbilityScore.WIS));
        scoreTable[5][3] = Integer.toString(basecharacter.getBaseAbilityScores().get(AbilityScore.CHA));
    }
    
    public void updateRacialBonus(BaseCharacter basecharacter) {
        scoreTable[0][4] = Integer.toString(basecharacter.raceAbilityScoreMap.get(AbilityScore.STR));
        scoreTable[1][4] = Integer.toString(basecharacter.raceAbilityScoreMap.get(AbilityScore.DEX));
        scoreTable[2][4] = Integer.toString(basecharacter.raceAbilityScoreMap.get(AbilityScore.CON));
        scoreTable[3][4] = Integer.toString(basecharacter.raceAbilityScoreMap.get(AbilityScore.INT));
        scoreTable[4][4] = Integer.toString(basecharacter.raceAbilityScoreMap.get(AbilityScore.WIS));
        scoreTable[5][4] = Integer.toString(basecharacter.raceAbilityScoreMap.get(AbilityScore.CHA));        
    }
    
    public void updateTotals(BaseCharacter basecharacter) {
        scoreTable[0][1] = Integer.toString(basecharacter.getFinalAbilityScores().get(AbilityScore.STR));
        scoreTable[1][1] = Integer.toString(basecharacter.getFinalAbilityScores().get(AbilityScore.DEX));
        scoreTable[2][1] = Integer.toString(basecharacter.getFinalAbilityScores().get(AbilityScore.CON));
        scoreTable[3][1] = Integer.toString(basecharacter.getFinalAbilityScores().get(AbilityScore.INT));
        scoreTable[4][1] = Integer.toString(basecharacter.getFinalAbilityScores().get(AbilityScore.WIS));
        scoreTable[5][1] = Integer.toString(basecharacter.getFinalAbilityScores().get(AbilityScore.CHA));        
    }
    
    public String[][] getTable() {
        return scoreTable;
    }
    
    public void resetTable() {
        scoreTable = new String[][]{{"STR", "0", "0", "0", "0"}, {"", "0", "0", "0", "0"}, {"CON", "0", "0", "0", "0"}, {"INT", "0", "0", "0", "0"}, {"WIS", "0", "0", "0", "0"}, {"CHA", "0", "0", "0", "0"} };
    }
}
