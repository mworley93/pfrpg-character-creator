/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

/**
 * This enum represents a character's gender.  A string representation can be obtained by calling 
 * .getString() on the enum.
 * @author Megan Worley
 */
public enum Gender {
    FEMALE("Female"),
    MALE("Male");
    
    private final String str; 
    
    Gender(String str) {
        this.str = str;
    }
    
    public String getString() {
        return str;
    }
}
