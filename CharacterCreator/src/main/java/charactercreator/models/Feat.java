/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

/**
 *
 * @author Brandon
 */

// This class is used to store data for Feats the character may select. Most characters will have just one feat, but Monks and Fighters may select an additional feat from a
// smaller list. This class is designed to be filled in from an xml file.

// KEY NOTATION POINTS: Prerequisite is simply a String of the Feat's name, unless it is none.
// String effect is used to apply any effects, the timing of this function call will need to be noted.

public class Feat {
    // String used for case-statements in prerequisite function
    private String featName;
    private String prerequisite;
    private String description;
    private Boolean isCombatFeat;
    
    
    public void setFeatName(String newFeatName) {
        featName = newFeatName;
    }
    
    public String getFeatName() {
        return featName;
    }
    
    public void setPrerequisite(String newPrerequisite) {
        prerequisite = newPrerequisite;
    }
    
    public String getPrerequisite() {
        return prerequisite;
    }
    
    public void setDescription(String newDescription) {
        description = newDescription;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setIsCombatFeat(Boolean newValue) {
        isCombatFeat = newValue;
    }
    
    public Boolean getIsCombatFeat() {
        return isCombatFeat;
    }
    
    
    public Boolean checkPrerequisite(BaseCharacter characterToCheck) {
        
        // If there is no prerequisite, then every character should be able to select the feat.
        if(prerequisite.equals("none")) {
            return true;
        }
        
        if(prerequisite.equals("Channel Energy Feature")) {
            if(characterToCheck.getCharacterClass().getClassName().equals("Cleric")) return true;
        }
        
        if(prerequisite.equals("Arcane Spells")) {
            if(characterToCheck.getCharacterClass().getClassName().equals("Sorcerer") || characterToCheck.getCharacterClass().getClassName().equals("Wizard")) return true;
        }
        
        if(prerequisite.equals("Spell Focus Conjuration")) {
            if(characterToCheck.checkForFeat("Spell Focus")) return true;
        }
        
        if(prerequisite.equals("Combat Expertise Feat")) {
            if(characterToCheck.checkForFeat("Combat Expertise")) return true;
        }

        if(prerequisite.equals("Combat Reflexes Feat")) {
            if(characterToCheck.checkForFeat("Combat Reflexes")) return true;
        }
        
        if(prerequisite.equals("Dodge Feat")) {
            if(characterToCheck.checkForFeat("Dodge")) return true;
        }

        if(prerequisite.equals("Endurance Feat")) {
            if(characterToCheck.checkForFeat("Endurance")) return true;
        }
        
        if(prerequisite.equals("INT13")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.INT) >= 13) return true;
        }

        if(prerequisite.equals("DEX13")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.DEX) >= 13) return true;
        }
        
        if(prerequisite.equals("DEX13 Base Attack Bonus 1")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.DEX) >= 13 && characterToCheck.getCharacterClass().getBaseAttackBonus() >= 1) return true;
        }
        
        if(prerequisite.equals("Base Attack Bonus 1")) {
            if(characterToCheck.getCharacterClass().getBaseAttackBonus() >= 1) return true;
        }     

        if(prerequisite.equals("Bard's Bardic Performance")) {
            if(characterToCheck.getCharacterClass().getClassName().equals("Bard")) return true;
        }
        
        if(prerequisite.equals("Barbarian's Rage")) {
            if(characterToCheck.getCharacterClass().getClassName().equals("Barbarian")) return true;
        }
        
        if(prerequisite.equals("Great Fortitude Feat")) {
            if(characterToCheck.checkForFeat("Great Fortitude")) return true;
        }
        
        if(prerequisite.equals("Light Armor Proficiency")) {
            return characterToCheck.checkForFeat("Light Armor Proficiency");
        }
        
        if(prerequisite.equals("Medium Armor Proficiency")) {
            return characterToCheck.checkForFeat("Medium Armor Proficiency");
        }
        
        if(prerequisite.equals("DEX13 and Improved Unarmed Strike Feat")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.DEX) >= 13 && characterToCheck.checkForFeat("Improved Unarmed Strike")) return true;
        }
        
        if(prerequisite.equals("Improved Unarmed Strike Feat")) {
            if(characterToCheck.checkForFeat("Improved Unarmed Strike")) return true;
        }        
        
        if(prerequisite.equals("Iron Will Feat")) {
            if(characterToCheck.checkForFeat("Iron Will")) return true;
        }
        
        if(prerequisite.equals("Lightning Reflexes Feat")) {
            if(characterToCheck.checkForFeat("Lightning Reflexes")) return true;
        }
        
        if(prerequisite.equals("Ride Skill Rank 1")) {
            if(characterToCheck.getFinalSkillMap().get(Skill.RIDE) >= 1) return true;
        }
        
        if(prerequisite.equals("Mounted Combat Feat")) {
            if(characterToCheck.checkForFeat("Mounted Combat")) return true;
        }
        
        if(prerequisite.equals("Ride-by Attack Feat")) {
            if(characterToCheck.checkForFeat("Ride-By Attack")) return true;
        }
        
        if(prerequisite.equals("Mounted Combat Feat and Improved Bullrush Feat")) {
            if(characterToCheck.checkForFeat("Mounted Combat") && characterToCheck.checkForFeat("Improved Bull Rush")) return true;
        }
        
        if(prerequisite.equals("WIS13 and Druid's Wildshape Class Feature")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.WIS) >= 13 && characterToCheck.getCharacterClass().getClassName().equals("Druid")) return true;
        }
        
        if(prerequisite.equals("DEX15 and Nimble Moves Feat")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.DEX) >= 15 && characterToCheck.checkForFeat("Nimble Moves")) return true;
        }
        
        if(prerequisite.equals("Point-Blank Shot Feat")) {
            if(characterToCheck.checkForFeat("Point-Blank Shot")) return true;
        }
        
        if(prerequisite.equals("DEX13 and Point-Blank Shot Feat")) {
            if(characterToCheck.checkForFeat("Point-Blank Shot") && characterToCheck.getFinalAbilityScores().get(AbilityScore.DEX) >= 13) return true;
        }
        
        if(prerequisite.equals("STR13 and Base Attack Bonus 1")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.STR) >= 13 && characterToCheck.getCharacterClass().getBaseAttackBonus() >= 1) return true;
        }
        
        if(prerequisite.equals("Power Attack Feat")) {
            if(characterToCheck.checkForFeat("Power Attack")) return true;
        }
        
        if(prerequisite.equals("Shield Proficiency")) {
            if(characterToCheck.checkForFeat("Shield Proficiency")) return true;
        }
        
        if(prerequisite.equals("Spell Focus")) {
            if(characterToCheck.checkForFeat("Spell Focus")) return true;
        }
        
        if(prerequisite.equals("Wizard Class")) {
            if(characterToCheck.getCharacterClass().getClassName().equals("Wizard")) return true;
        }
        
        if(prerequisite.equals("Spell Penetration")) {
            if(characterToCheck.checkForFeat("Spell Penetration")) return true;
        }
        
        if(prerequisite.equals("DEX15")) {
            if(characterToCheck.getFinalAbilityScores().get(AbilityScore.DEX) >= 15) return true;
        }
        
        if(prerequisite.equals("Two-Weapon Fighting Feat")) {
            if(characterToCheck.checkForFeat("Two-Weapon Fighting")) return true;
        }
 
        if(prerequisite.equals("DEX15 and Two-Weapon Fighting Feat")) {
            if(characterToCheck.checkForFeat("Two-Weapon Fighting") && characterToCheck.getFinalAbilityScores().get(AbilityScore.DEX) >= 15) return true;
        }
        
        if(prerequisite.equals("Weapon Focus Feat")) {
            if(characterToCheck.checkForFeat("Weapon Focus")) return true;
        }
        
        return false;
        
    }
    
    public void applyEffect() {
        
    }
    
}
