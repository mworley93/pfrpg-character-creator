/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import charactercreator.helpers.XMLReader;
import java.util.HashMap;

/**
 * This class loads and stores misc. data that is presented to the user.  It includes overview text, descriptions for certain game features, how-to information, and 
 * images.
 * @author Megan Worley, Brandon Sharp
 */
public class UIModel {
    
    XMLReader fileReader;
    
    private HashMap<String, String> tutorialMessages;
    private HashMap<String, String> tutorialImages;
    
    private HashMap<String, String> classDescriptions;
    private HashMap<String, String> classFeatures;
    private HashMap<String, String> classImages;
    private HashMap<String, String> classInfo;
    
    private HashMap<String, String> raceDescriptions;
    private HashMap<String, String> raceFeatures;
    private HashMap<String, String> raceImages;
    private HashMap<String, String> raceInfo;
    
    private HashMap<String, String> abilityScoreDescriptions;
    private HashMap<String, String> methodDescriptions;
    
    
    private HashMap<String, String> bloodlineDescriptions;
    private HashMap<String, String> bloodlineFeatures;
    private HashMap<String, String> bloodlineImages;
    
    private HashMap<String, String> favoredEnemyDescriptions;
    private HashMap<String, String> favoredEnemyImages;
    
    private HashMap<String, String> druidBranchDescriptions;
    private HashMap<String, String> druidBranchImages;
    
    private HashMap<String, String> animalCompanionDescriptions;
    private HashMap<String, String> animalCompanionImages;
    
    private HashMap<String, String> druidDomainFeatures;
    private HashMap<String, String> druidDomainImages;
    
    private HashMap<String, String> arcaneBondDescriptions;
    private HashMap<String, String> arcaneBondImages;
    
    private HashMap<String, String> wizardFamiliarDescriptions;
    private HashMap<String, String> wizardFamiliarImages;
    
    private HashMap<String, String> arcaneSchoolDescriptions;
    private HashMap<String, String> arcaneSchoolFeatures;
    private HashMap<String, String> arcaneSchoolImages;
    
    private HashMap<String, String> forbiddenArcaneSchoolDescriptions;
    private HashMap<String, String> forbiddenArcaneSchoolFeatures;
    private HashMap<String, String> forbiddenArcaneSchoolImages;  
    
    private HashMap<String, String> secondForbiddenArcaneSchoolDescriptions;
    private HashMap<String, String> secondForbiddenArcaneSchoolFeatures;
    private HashMap<String, String> secondForbiddenArcaneSchoolImages;      
    
    private HashMap<String, String> clericDomainFeatures;
    private HashMap<String, String> clericDomainImages;   
    
    private HashMap<String, String> secondClericDomainFeatures;
    private HashMap<String, String> secondClericDomainImages;   
    
    private HashMap<String, String> racialAbilitiesImages;
    
    private HashMap<String, String> equipmentImages;
    
    private String currentClassDescription;
    private String currentClassFeature;
    private String currentClassImagePath;
    
    private String currentRaceDescription;
    private String currentRaceFeature;
    private String currentRaceImagePath;
    
    private String abilityScoreOverview;
    private String currentAbilityScoreMethod;
    private String currentMethodDescription;
    private HashMap<String, String> abilityScoreInfo;
    
    private String raceAbilityScoreOverview;
    private String raceAbilityScoreInstructions;
    
    private String currentBloodlineDescription;
    private String currentBloodlineFeature;
    private String currentBloodlineImagePath;
    private String bloodlineOverview;
    private String skillsOverview;
    private String skillsHowTo;
    private HashMap<String, String> skillsInfo;
    
    private String currentFavoredEnemyDescription;
    private String currentFavoredEnemyImagePath;
    private String favoredEnemyOverview;
    
    private String currentDruidBranchDescription;
    private String currentDruidBranchImagePath;
    private String druidBranchOverview;
    
    private String currentAnimalCompanionDescription;
    private String currentAnimalCompanionImagePath;
    private String animalCompanionOverview;
    
    private String currentDruidDomainFeature;
    private String currentDruidDomainImagePath;
    private String druidDomainOverview;
    
    private String currentArcaneBondDescription;
    private String currentArcaneBondImagePath;
    private String arcaneBondOverview;
    
    private String currentWizardFamiliarDescription;
    private String currentWizardFamiliarImagePath;
    private String wizardFamiliarOverview;
    
    private String currentArcaneSchoolDescription;
    private String currentArcaneSchoolFeature;
    private String currentArcaneSchoolImagePath;
    private String arcaneSchoolOverview;
    
    private String currentForbiddenArcaneSchoolDescription;
    private String currentForbiddenArcaneSchoolFeature;
    private String currentForbiddenArcaneSchoolImagePath;
    private String forbiddenArcaneSchoolOverview;   
    
    private String currentSecondForbiddenArcaneSchoolDescription;
    private String currentSecondForbiddenArcaneSchoolFeature;
    private String currentSecondForbiddenArcaneSchoolImagePath;
    private String secondForbiddenArcaneSchoolOverview;    
    
    private String currentClericDomainFeature;
    private String currentClericDomainImagePath;
    private String clericDomainOverview;      
    
    private String currentSecondClericDomainFeature;
    private String currentSecondClericDomainImagePath;
    private String secondClericDomainOverview;      
    
    
    private String featsOverview;
    private String featsImage;
    
    private String combatFeatsOverview;
    private String combatFeatsImage;
    
    private String monkFeatsImage;
    
    private String equipmentOverview;
    private String equipmentHowTo;
    private String currentEquipmentImage;
    private String goldImage;
    private String itemsImage;
    
    private String miscOverview;
    private HashMap<String, String> miscInfo;
    
    private String spellbookOverview;
    private String spellbookHowTo;
    private String spellbookImage;
    
    public UIModel()
    {
        fileReader = new XMLReader();
        loadContent();
    }
    
    /**
     * Responsible for loading all of the needed user interface data from external XML files.
     */
    private void loadContent() {
        tutorialMessages = fileReader.readDescriptiveFile("/data/tutorialUIData.xml", "message", "description");
        tutorialImages = fileReader.readDescriptiveFile("/data/tutorialUIData.xml", "arrow", "image");
        
        classDescriptions = fileReader.readDescriptiveFile("/data/classUIData.xml", "class", "description");
        classFeatures = fileReader.readDescriptiveFile("/data/classUIData.xml", "class", "classfeatures");
        classImages = fileReader.readDescriptiveFile("/data/classUIData.xml", "class", "classimage");
        classInfo = fileReader.readDescriptiveFile("/data/classUIData.xml", "moreinfo", "text");
        
        raceDescriptions = fileReader.readDescriptiveFile("/data/raceUIData.xml", "race", "description");
        raceFeatures = fileReader.readDescriptiveFile("/data/raceUIData.xml", "race", "features");
        raceImages = fileReader.readDescriptiveFile("/data/raceUIData.xml", "race", "image");
        raceInfo = fileReader.readDescriptiveFile("/data/raceUIData.xml", "moreinfo", "text");
        
        abilityScoreOverview = fileReader.readDescriptiveFile("/data/abilityScoreUIData.xml", "overview", "description").get("Overview");
        abilityScoreDescriptions = fileReader.readDescriptiveFile("/data/abilityScoreUIData.xml", "abilityscore", "description");
        methodDescriptions = fileReader.readDescriptiveFile("/data/abilityScoreUIData.xml", "method", "description");
        abilityScoreInfo = fileReader.readDescriptiveFile("/data/abilityScoreUIData.xml", "moreinfo", "text");
        
        raceAbilityScoreOverview = fileReader.readDescriptiveFile("/data/raceAbilityScoreUIData.xml", "info", "description").get("Overview");
        raceAbilityScoreInstructions = fileReader.readDescriptiveFile("/data/raceAbilityScoreUIData.xml", "info", "description").get("Instructions");
        
        bloodlineDescriptions = fileReader.readDescriptiveFile("/data/bloodlineUIData.xml", "bloodline", "description");
        bloodlineFeatures = fileReader.readDescriptiveFile("/data/bloodlineUIData.xml", "bloodline", "features");
        bloodlineImages = fileReader.readDescriptiveFile("/data/bloodlineUIData.xml", "bloodline", "image");
        bloodlineOverview = fileReader.readDescriptiveFile("/data/bloodlineUIData.xml", "overview", "description").get("Overview");
        
        skillsOverview = fileReader.readDescriptiveFile("/data/skillsUIData.xml", "skillinfo", "description").get("Overview");
        skillsHowTo = fileReader.readDescriptiveFile("/data/skillsUIData.xml", "skillinfo", "description").get("How-To");
        skillsInfo = fileReader.readDescriptiveFile("/data/skillsUIData.xml", "moreinfo", "text");
        
        favoredEnemyDescriptions = fileReader.readDescriptiveFile("/data/favoredEnemyUIData.xml", "enemy", "description");
        favoredEnemyImages = fileReader.readDescriptiveFile("/data/favoredEnemyUIData.xml", "enemy", "image");
        favoredEnemyOverview = fileReader.readDescriptiveFile("/data/favoredEnemyUIData.xml", "overview", "description").get("Overview");
        
        druidBranchDescriptions = fileReader.readDescriptiveFile("/data/druidBranchUIData.xml", "feature", "description");
        druidBranchImages = fileReader.readDescriptiveFile("/data/druidBranchUIData.xml", "feature", "image");
        druidBranchOverview = fileReader.readDescriptiveFile("/data/druidBranchUIData.xml", "overview", "description").get("Overview");
        
        animalCompanionDescriptions = fileReader.readDescriptiveFile("/data/animalCompanionUIData.xml", "companion", "description");
        animalCompanionImages = fileReader.readDescriptiveFile("/data/animalCompanionUIData.xml", "companion", "image");
        animalCompanionOverview = fileReader.readDescriptiveFile("/data/animalCompanionUIData.xml", "overview", "description").get("Overview");        
        
        druidDomainFeatures = fileReader.readDescriptiveFile("/data/druidDomainUIData.xml", "domain", "features");
        druidDomainImages = fileReader.readDescriptiveFile("/data/druidDomainUIData.xml", "domain", "image");
        druidDomainOverview = fileReader.readDescriptiveFile("/data/druidDomainUIData.xml", "overview", "description").get("Overview");     
        
        arcaneBondDescriptions = fileReader.readDescriptiveFile("/data/arcaneBondUIData.xml", "feature", "description");
        arcaneBondImages = fileReader.readDescriptiveFile("/data/arcaneBondUIData.xml", "feature", "image");
        arcaneBondOverview = fileReader.readDescriptiveFile("/data/arcaneBondUIData.xml", "overview", "description").get("Overview");
        
        wizardFamiliarDescriptions = fileReader.readDescriptiveFile("/data/wizardFamiliarUIData.xml", "familiar", "description");
        wizardFamiliarImages = fileReader.readDescriptiveFile("/data/wizardFamiliarUIData.xml", "familiar", "image");
        wizardFamiliarOverview = fileReader.readDescriptiveFile("/data/wizardFamiliarUIData.xml", "overview", "description").get("Overview");
        
        arcaneSchoolDescriptions = fileReader.readDescriptiveFile("/data/arcaneSchoolUIData.xml", "school", "description");
        arcaneSchoolFeatures = fileReader.readDescriptiveFile("/data/arcaneSchoolUIData.xml", "school", "features");
        arcaneSchoolImages = fileReader.readDescriptiveFile("/data/arcaneSchoolUIData.xml", "school", "image");
        arcaneSchoolOverview = fileReader.readDescriptiveFile("/data/arcaneSchoolUIData.xml", "overview", "description").get("Overview");
        
        forbiddenArcaneSchoolDescriptions = fileReader.readDescriptiveFile("/data/forbiddenArcaneSchoolUIData.xml", "school", "description");
        forbiddenArcaneSchoolFeatures = fileReader.readDescriptiveFile("/data/forbiddenArcaneSchoolUIData.xml", "school", "features");
        forbiddenArcaneSchoolImages = fileReader.readDescriptiveFile("/data/forbiddenArcaneSchoolUIData.xml", "school", "image");
        forbiddenArcaneSchoolOverview = fileReader.readDescriptiveFile("/data/forbiddenArcaneSchoolUIData.xml", "overview", "description").get("Overview");  
        
        secondForbiddenArcaneSchoolDescriptions = fileReader.readDescriptiveFile("/data/secondForbiddenArcaneSchoolUIData.xml", "school", "description");
        secondForbiddenArcaneSchoolFeatures = fileReader.readDescriptiveFile("/data/secondForbiddenArcaneSchoolUIData.xml", "school", "features");
        secondForbiddenArcaneSchoolImages = fileReader.readDescriptiveFile("/data/secondForbiddenArcaneSchoolUIData.xml", "school", "image");
        secondForbiddenArcaneSchoolOverview = fileReader.readDescriptiveFile("/data/secondForbiddenArcaneSchoolUIData.xml", "overview", "description").get("Overview");     

        clericDomainFeatures = fileReader.readDescriptiveFile("/data/clericDomainUIData.xml", "domain", "feature");
        clericDomainImages = fileReader.readDescriptiveFile("/data/clericDomainUIData.xml", "domain", "image");
        clericDomainOverview = fileReader.readDescriptiveFile("/data/clericDomainUIData.xml", "overview", "description").get("Overview");
        
        secondClericDomainFeatures = fileReader.readDescriptiveFile("/data/secondClericDomainUIData.xml", "domain", "feature");
        secondClericDomainImages = fileReader.readDescriptiveFile("/data/secondClericDomainUIData.xml", "domain", "image");
        secondClericDomainOverview = fileReader.readDescriptiveFile("/data/secondClericDomainUIData.xml", "overview", "description").get("Overview");  
        
        racialAbilitiesImages = fileReader.readDescriptiveFile("/data/raceAbilityScoreUIData.xml", "image", "path");
        
        featsOverview = fileReader.readDescriptiveFile("/data/featsUIData.xml", "overview", "description").get("Overview");
        featsImage = fileReader.readDescriptiveFile("/data/featsUIData.xml", "image", "path").get("Feats");
        
        combatFeatsOverview = fileReader.readDescriptiveFile("/data/featsUIData.xml", "overview", "description").get("OverviewTwo");
        combatFeatsImage = fileReader.readDescriptiveFile("/data/featsUIData.xml", "image", "path").get("Combat");
        monkFeatsImage = fileReader.readDescriptiveFile("/data/featsUIData.xml", "image", "path").get("Monk");
        
        equipmentOverview = fileReader.readDescriptiveFile("/data/equipmentUIData.xml", "overview", "description").get("Overview");
        equipmentHowTo = fileReader.readDescriptiveFile("/data/equipmentUIData.xml", "howto", "description").get("How-To");
        equipmentImages = fileReader.readDescriptiveFile("/data/equipmentUIData.xml", "equipment", "image");
        goldImage = equipmentImages.get("Gold");
        itemsImage = equipmentImages.get("Items");

        miscOverview = fileReader.readDescriptiveFile("/data/miscUIData.xml", "overview", "description").get("Overview");
        miscInfo = fileReader.readDescriptiveFile("/data/miscUIData.xml", "moreinfo", "text");
        
        spellbookOverview = fileReader.readDescriptiveFile("/data/spellbookUIData.xml", "overview", "description").get("Overview");
        spellbookHowTo = fileReader.readDescriptiveFile("/data/spellbookUIData.xml", "howto", "description").get("How-To");
        spellbookImage = fileReader.readDescriptiveFile("/data/spellbookUIData.xml", "image", "path").get("Spellbook");
    }
    
    /**
     * Returns the requested information.
     * @param type The type information needed.
     * @return A string holding the current information pertaining to the requested item.
     */
    public String getCurrentInformation(String type) {
        if (type.compareTo("class") == 0)
            return currentClassDescription;
        else if (type.compareTo("race") == 0)
            return currentRaceDescription;
        else if (type.compareTo("abilityScoreMethod") == 0)
            return currentAbilityScoreMethod;
        else if (type.compareTo("methodDescription") == 0)
            return currentMethodDescription;
        else if (type.compareTo("bloodline") == 0)
            return currentBloodlineDescription;
        else if (type.compareTo("favoredEnemy") == 0)
            return currentFavoredEnemyDescription;
        else if (type.compareTo("druidBranch") == 0)
            return currentDruidBranchDescription;
        else if (type.compareTo("animalCompanion") == 0)
            return currentAnimalCompanionDescription;
        else if (type.compareTo("arcaneBond") == 0)
            return currentArcaneBondDescription;
        else if (type.compareTo("wizardFamiliar") == 0)
            return currentWizardFamiliarDescription;
        else if (type.compareTo("arcaneSchool") == 0)
            return currentArcaneSchoolDescription;
        else if (type.compareTo("forbiddenArcaneSchool") == 0)
            return currentForbiddenArcaneSchoolDescription;
        else if (type.compareTo("secondForbiddenArcaneSchool") == 0)
            return currentSecondForbiddenArcaneSchoolDescription;   
        else
            return "Description unavailable.";
    }
    
    /**
     * Returns the requested list of features.
     * @param type The type of features needed.
     * @return A feature list associated with the given item.
     */
    public String getCurrentFeature(String type) {
        if (type.compareTo("class") == 0)
            return currentClassFeature;
        else if (type.compareTo("race") == 0)
            return currentRaceFeature;
        else if (type.compareTo("bloodline") == 0)
            return currentBloodlineFeature;
        else if (type.compareTo("druidDomain") == 0)
            return currentDruidDomainFeature;
        else if (type.compareTo("arcaneSchool") == 0)
            return currentArcaneSchoolFeature;
        else if (type.compareTo("forbiddenArcaneSchool") == 0)
            return currentForbiddenArcaneSchoolFeature;
        else if (type.compareTo("secondForbiddenArcaneSchool") == 0)
            return currentSecondForbiddenArcaneSchoolFeature;     
        else if (type.compareTo("clericDomain") == 0)
            return currentClericDomainFeature;
        else if (type.compareTo("secondClericDomain") == 0)
            return currentSecondClericDomainFeature;        
        else
            return "Features unavailable.";
    }
    
    /**
     * Returns the requested image.
     * @param type The type of image needed.
     * @return An image associated with the given item.
     */
    public String getCurrentImagePath(String type) {
        if (type.compareTo("class") == 0)
            return currentClassImagePath;
        else if (type.compareTo("race") == 0)
            return currentRaceImagePath;
        else if (type.compareTo("bloodline") == 0)
            return currentBloodlineImagePath;
        else if (type.compareTo("favoredEnemy") == 0)
            return currentFavoredEnemyImagePath;
        else if (type.compareTo("druidBranch") == 0)
            return currentDruidBranchImagePath;
        else if (type.compareTo("animalCompanion") == 0)
            return currentAnimalCompanionImagePath;
        else if (type.compareTo("druidDomain") == 0)
            return currentDruidDomainImagePath;
        else if (type.compareTo("arcaneBond") == 0)
            return currentArcaneBondImagePath;
        else if (type.compareTo("wizardFamiliar") == 0)
            return currentWizardFamiliarImagePath;
        else if (type.compareTo("arcaneSchool") == 0)
            return currentArcaneSchoolImagePath;
        else if (type.compareTo("forbiddenArcaneSchool") == 0)
            return currentForbiddenArcaneSchoolImagePath;
        else if (type.compareTo("secondForbiddenArcaneSchool") == 0)
            return currentSecondForbiddenArcaneSchoolImagePath;     
        else if (type.compareTo("clericDomain") == 0)
            return currentClericDomainImagePath;
        else if (type.compareTo("secondClericDomain") == 0)
            return currentSecondClericDomainImagePath;      
        else if (type.compareTo("equipment") == 0)
            return currentEquipmentImage;
        else
            return "Error: Could not find file.";
    }
    
    public HashMap getClassInfo() {
        return classInfo;
    }
    
    public HashMap getRaceInfo() {
        return raceInfo;
    }
    
    public String getTutorialMessage(String key) {
        return tutorialMessages.get(key);
    }
    
    public String getTutorialImage(String key) {
        return tutorialImages.get(key);
    }
    
    public String getAbilityScoreOverview() {
        return abilityScoreOverview;
    }
    
    public String getAbilityScoreDescription(String key) {
        return abilityScoreDescriptions.get(key);
    }
    
    public HashMap getAbilityScoreInfo() {
        return abilityScoreInfo;
    }
    
    public String getRaceAbilityScoreOverview() {
        return raceAbilityScoreOverview;
    }
    
    public String getRaceAbilityScoreInstructions() {
        return raceAbilityScoreInstructions;
    }
    
    public String getBloodlineOverview() {
        return bloodlineOverview;
    }
    
    public String getSkillsOverview() {
        return skillsOverview;
    }
    
    public String getSkillsHowTo() {
        return skillsHowTo;
    }
    
    public HashMap getSkillsInfo() {
        return skillsInfo;
    }
    
    public String getFavoredEnemyOverview() {
        return favoredEnemyOverview;
    }
    
    public String getDruidBranchOverview() {
        return druidBranchOverview;
    }
    
    public String getAnimalCompanionOverview() {
        return animalCompanionOverview;
    }
    
    public String getDruidDomainOverview() {
        return druidDomainOverview;
    }
    
    public String getArcaneBondOverview() {
        return arcaneBondOverview;
    }
    
    public String getWizardFamiliarOverview() {
        return wizardFamiliarOverview;
    }
    
    public String getArcaneSchoolOverview() {
        return arcaneSchoolOverview;
    }   
    
    public String getClericDomainOverview() {
        return clericDomainOverview;
    }
    
    public String getSecondClericDomainOverview() {
        return secondClericDomainOverview;
    }    
    
    public String getRacialAbilitiesImage(String key) {
        return racialAbilitiesImages.get(key);
    }
    
    public String getFeatsOverview() {
        return featsOverview;
    }
    
    public String getFeatsImage() {
        return featsImage;
    }
    
    public String getCombatFeatsOverview() {
        return combatFeatsOverview;
    }
    
    public String getCombatFeatsImage() {
        return combatFeatsImage;
    }
    
    public String getMonkFeatsImage() {
        return monkFeatsImage;
    }
    
    public String getEquipmentOverview() {
        return equipmentOverview;
    }
    
    public String getEquipmentHowTo() {
        return equipmentHowTo;
    }
    
    public String getGoldImage() {
        return goldImage;
    }
    
    public String getItemsImage() {
        return itemsImage;
    }
    
    public String getMiscOverview() {
        return miscOverview;
    }
    
    public HashMap getMiscInfo() {
        return miscInfo;
    }
    
    public String getSpellbookOverview() {
        return spellbookOverview;
    }
    
    public String getSpellbookHowTo() {
        return spellbookHowTo;
    }
    
    public String getSpellbookImage() {
        return spellbookImage;
    }
    
    /**
     * Sets all of the class items at once to keep them in sync.
     * @param key 
     */
    public void setCurrentClassItems(String key) {
        currentClassDescription = classDescriptions.get(key);
        currentClassFeature = classFeatures.get(key);
        currentClassImagePath = classImages.get(key);
    }
    
    /**
     * Sets all of the race items at once to keep them in sync.
     * @param key 
     */
    public void setCurrentRaceItems(String key) {
        currentRaceDescription = raceDescriptions.get(key);
        currentRaceFeature = raceFeatures.get(key);
        currentRaceImagePath = raceImages.get(key);
    }
    
    /**
     * Sets all of the ability score method items at once to keep them in sync.
     * @param method 
     */
    public void setCurrentAbilityScoreMethod(String method) {
        currentAbilityScoreMethod = method;
        currentMethodDescription = methodDescriptions.get(method);
    }
    
    // Sets all the bloodline items at once to keep in sync
    
    public void setCurrentBloodlineItems(String key) {
        currentBloodlineDescription = bloodlineDescriptions.get(key);
        currentBloodlineFeature = bloodlineFeatures.get(key);
        currentBloodlineImagePath = bloodlineImages.get(key);
    }
    
    public void setCurrentFavoredEnemyItems(String key) {
        currentFavoredEnemyDescription = favoredEnemyDescriptions.get(key);
        currentFavoredEnemyImagePath = favoredEnemyImages.get(key);
    }
    
    public void setCurrentDruidBranchItems(String key) {
        currentDruidBranchDescription = druidBranchDescriptions.get(key);
        currentDruidBranchImagePath = druidBranchImages.get(key);
    }
    
    public void setCurrentAnimalCompanionItems(String key) {
        currentAnimalCompanionDescription = animalCompanionDescriptions.get(key);
        currentAnimalCompanionImagePath = animalCompanionImages.get(key);
    }
    
    public void setCurrentDruidDomainItems(String key) {
        currentDruidDomainFeature = druidDomainFeatures.get(key);
        currentDruidDomainImagePath = druidDomainImages.get(key);
    }
    
    public void setCurrentArcaneBondItems(String key) {
        currentArcaneBondDescription = arcaneBondDescriptions.get(key);
        currentArcaneBondImagePath = arcaneBondImages.get(key);
    }
    
    public void setCurrentWizardFamiliarItems(String key) {
        currentWizardFamiliarDescription = wizardFamiliarDescriptions.get(key);
        currentWizardFamiliarImagePath = wizardFamiliarImages.get(key);
    }
    
    public void setCurrentArcaneSchoolItems(String key) {
        currentArcaneSchoolDescription = arcaneSchoolDescriptions.get(key);
        currentArcaneSchoolFeature = arcaneSchoolFeatures.get(key);
        currentArcaneSchoolImagePath = arcaneSchoolImages.get(key);
    }
    
    public void setCurrentForbiddenArcaneSchoolItems(String key) {
        currentForbiddenArcaneSchoolDescription = forbiddenArcaneSchoolDescriptions.get(key);
        currentForbiddenArcaneSchoolFeature = forbiddenArcaneSchoolFeatures.get(key);
        currentForbiddenArcaneSchoolImagePath = forbiddenArcaneSchoolImages.get(key);
    }    

    public void setCurrentSecondForbiddenArcaneSchoolItems(String key) {
        currentSecondForbiddenArcaneSchoolDescription = secondForbiddenArcaneSchoolDescriptions.get(key);
        currentSecondForbiddenArcaneSchoolFeature = secondForbiddenArcaneSchoolFeatures.get(key);
        currentSecondForbiddenArcaneSchoolImagePath = secondForbiddenArcaneSchoolImages.get(key);
    }     
    
    public void setCurrentClericDomainItems(String key) {
        currentClericDomainFeature = clericDomainFeatures.get(key);
        currentClericDomainImagePath = clericDomainImages.get(key);
    }
    
    public void setCurrentSecondClericDomainItems(String key) {
        currentSecondClericDomainFeature = secondClericDomainFeatures.get(key);
        currentSecondClericDomainImagePath = secondClericDomainImages.get(key);
    }  
    
    public void setCurrentEquipmentItems(String key) {
        currentEquipmentImage = equipmentImages.get(key);
    }
    
}
