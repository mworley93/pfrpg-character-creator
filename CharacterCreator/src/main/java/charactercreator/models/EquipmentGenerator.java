/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;


import charactercreator.helpers.XMLReader;
import charactercreator.models.equipment.Equipment;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author Brandon woooooo Megan
 */

// This class will construct and fill a Linked List full of Equipment.

public class EquipmentGenerator {
    
    // Store items.
    ArrayList<Equipment> equipmentList = new ArrayList<>();
    ArrayList<Equipment> filteredEquipmentList = new ArrayList<>();
    ArrayList<Equipment> armorList = new ArrayList<>();
    ArrayList<Equipment> weaponList = new ArrayList<>();
    
    // Inventory items.
    ArrayList<Equipment> selectedArmor = new ArrayList();
    ArrayList<Equipment> selectedWeapons = new ArrayList();
    int gold;
    
    public ArrayList getEquipmentList() {
        return equipmentList;
    }
    
    public ArrayList getFilteredList() {
        return filteredEquipmentList;
    }
    
    public ArrayList getSelectedArmor() {
        return selectedArmor;
    }
    
    public ArrayList getSelectedWeapons() {
        return selectedWeapons;
    }
    
    public void setGold(int amount) {
        gold = amount;
    }
    
    public int getGold() {
        return gold;
    }
    
    public Equipment getEquipment(String equipmentName) {
        for (Equipment equipment : equipmentList) {
            if (equipment.getName().equals(equipmentName)) {
                return equipment;
            }
        }
        return null;
    }
    
    /**
     * Adds all of the equipment names in the given feat array to a String[].
     * @param equipment 
     * @return A String Array containing the equipment names.
     */
    public String[] convertNamesToReadable(ArrayList<Equipment> equipment) {
        int count = equipment.size();
        String[] readableFeatArray = new String[count];
        for (int i = 0; i < count; ++i) {
            readableFeatArray[i] = equipment.get(i).getName();
        }
        return readableFeatArray;
    }
    
    public void fillEquipmentList(String fileName) {
        XMLReader tempEquipReader = new XMLReader();
        tempEquipReader.readEquipment(fileName, equipmentList);
    }
    
    public void buyEquipment(Equipment item) {
        if (item.getEquipmentType().compareTo("Weapon") == 0) {
            selectedWeapons.add(item);
        }
        else if (item.getEquipmentType().compareTo("Armor") == 0) {
            selectedArmor.add(item);
        }
        gold -= item.getCost();
    }
    
    public void sellEquipment(Equipment item) {
        if (item.getEquipmentType().compareTo("Weapon") == 0) {
            selectedWeapons.remove(item);
        }
        else if (item.getEquipmentType().compareTo("Armor") == 0) {
            selectedArmor.remove(item);
        }
        gold += item.getCost();
    }
    
    public void emptyFilteredList() {
        
        ListIterator itr = equipmentList.listIterator();
        Equipment currentEquipment;
        
        while(itr.hasNext()) {
            currentEquipment = (Equipment) itr.next();
            filteredEquipmentList.remove(currentEquipment);
        }
    }
    
    
    public void emptyEquipmentList() {
        
        ListIterator itr = equipmentList.listIterator();
        Equipment currentEquipment;
        
        while(itr.hasNext()) {
            currentEquipment = (Equipment) itr.next();
            equipmentList.remove(currentEquipment);
        }
    }
    
    public void fillArmorList() {
        for (Equipment equipment : equipmentList) {
            if (equipment.getEquipmentType().matches("Armor")) {
                armorList.add(equipment);
            }
        }
    }
    
    public void fillWeaponlist() {
        for (Equipment equipment : equipmentList) {
            if (equipment.getEquipmentType().matches("Weapon")) {
                weaponList.add(equipment);
            }
        }
    }
    
    public ArrayList<Equipment> getArmorList() {
        return armorList;
    }
    
    public ArrayList<Equipment> getWeaponList() {
        return weaponList;
    }
    
    public void filterEquipmentToMatchProficiencies(BaseCharacter characterToCheck) {
        
        ListIterator itr = equipmentList.listIterator();
        String proficiencyToCheck;
        Equipment currentEquipment;
        
        while(itr.hasNext()) {
            currentEquipment = (Equipment) itr.next();
            proficiencyToCheck = currentEquipment.getProficiencyRequired();
            if(characterToCheck.checkForFeat(proficiencyToCheck)) {
                filteredEquipmentList.add(currentEquipment);
            }
        }
    }
    
    public void printEquipment() {
        for (Equipment equip : equipmentList) {
            System.out.println(equip.getName());
        }        
    }
}
