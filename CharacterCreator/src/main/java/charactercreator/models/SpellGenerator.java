/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models;

import charactercreator.helpers.XMLReader;
import java.util.ArrayList;

/**
 *
 * @author Brandon
 */
public class SpellGenerator {
    ArrayList<Spell> fullSpellList = new ArrayList<>();
    ArrayList<Spell> levelZeroSpellList = new ArrayList<>();
    ArrayList<Spell> levelOneSpellList = new ArrayList<>();
    
    ArrayList<Spell> selectedLevelZeroSpellList = new ArrayList<>();
    ArrayList<Spell> selectedLevelOneSpellList = new ArrayList<>();
    
    int levelZeroSpellsRemaining;
    int levelOneSpellsRemaining;    
    
    int levelZeroSpellLimit;
    int levelOneSpellLimit;
    
    public int getLevelZeroSpellsRemaining() {
        return levelZeroSpellsRemaining;
    }
    
    public int getLevelOneSpellsRemaining() {
        return levelOneSpellsRemaining;
    }
    
    public ArrayList<Spell> getFullSpellList() {
        return fullSpellList;
    }
    
    public ArrayList<Spell> getLevelZeroSpellList() {
        return levelZeroSpellList;
    }
    
    public ArrayList<Spell> getLevelOneSpellList() {
        return levelOneSpellList;
    }
    
    public ArrayList<Spell> getSelectedLevelZeroSpellList() {
        return selectedLevelZeroSpellList;
    }
    
    public ArrayList<Spell> getSelectedLevelOneSpellList() {
        return selectedLevelOneSpellList;
    }
    
    public Spell getSpell(String spellName) {
        for (Spell spell : fullSpellList) {
            if (spell.getSpellName().equals(spellName)) {
                return spell;
            }
        }
        return null;
    }    
    
    public void fillSpellList(String fileName) {
        XMLReader tempSpellReader = new XMLReader();
        tempSpellReader.readSpells(fileName, fullSpellList);
    }    
    
    public void printSpellList() {
        for (Spell spell : fullSpellList) {
            System.out.println(spell.spellName);
        }
    }
    
    public void fillSeparatedSpellLists() {
        for (Spell spell : fullSpellList) {
            if (spell.getSpellLevel() == 0) {
                levelZeroSpellList.add(spell);
            }
            else {
                levelOneSpellList.add(spell);
            }
        }
    }
    
    public void addSpell(Spell spell) {
        if (spell.getSpellLevel() == 0) {
            selectedLevelZeroSpellList.add(spell);
            levelZeroSpellsRemaining--;
        }
        else {
            selectedLevelOneSpellList.add(spell);
            levelOneSpellsRemaining--;
        }
    }
    
    public void removeSpell(Spell spell) {
        if (spell.getSpellLevel() == 0) {
            selectedLevelZeroSpellList.remove(spell);
            levelZeroSpellsRemaining++;
        }
        else {
            selectedLevelOneSpellList.remove(spell);
            levelOneSpellsRemaining++;
        }
    }  
    
    public void setSpellLimit(int spellLimit) {
        levelOneSpellLimit = spellLimit;
    }
    
    public void resetRemainingSpells() {
        levelOneSpellsRemaining = levelOneSpellLimit;
    }
    
    
    /**
     * Adds all of the spell names in the given array to a String[].
     * @param spells 
     * @return A String Array containing the spell names.
     */
    public String[] convertNamesToReadable(ArrayList<Spell> spells) {
        int count = spells.size();
        String[] readableSpellArray = new String[count];
        for (int i = 0; i < count; ++i) {
            readableSpellArray[i] = spells.get(i).getSpellName();
        }
        return readableSpellArray;
    }    
}
