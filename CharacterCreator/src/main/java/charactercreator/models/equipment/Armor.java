/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models.equipment;


/**
 *
 * @author Brandon
 */
public class Armor extends Equipment{
    
    int armorBonus;
    int maxDexBonus;
    int armorCheckPenalty;
    double arcaneFailureChance;
    
    
    public void setArmorBonus(int newArmorBonus) {
        armorBonus = newArmorBonus;
    }
    
    public int getArmorBonus() {
        return armorBonus;
    }
    
    public void setMaxDexBonus(int newMaxDexBonus) {
        maxDexBonus = newMaxDexBonus;
    }
    
    public int getMaxDexBonus() {
        return maxDexBonus;
    }
    
    public void setArmorCheckPenalty(int newArmorCheckPenalty) {
        armorCheckPenalty = newArmorCheckPenalty;
    }
    
    public int getArmorCheckPenalty() {
        return armorCheckPenalty;
    }
    
    public void setArcaneFailureChance(double newArcaneFailureChance) {
        arcaneFailureChance = newArcaneFailureChance;
    }
    
    public double getArcaneFailureChance() {
        return arcaneFailureChance;
    }
    
}
