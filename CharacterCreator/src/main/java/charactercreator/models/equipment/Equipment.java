/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models.equipment;


/**
 *
 * @author Megan first, then Brandon second, this time
 */
public class Equipment {
        
    String equipmentType;
    String equipmentName;
    double goldCost;
    double weight;
    String requiredProficiency;
    String equipmentDescription;
    
    
    public void setEquipmentType(String newEquipmentType) {
        equipmentType = newEquipmentType;
    }
    
    public String getEquipmentType() {
        return equipmentType;
    }  
    
    public void setProficiencyRequired(String newProficiencyRequired) {
        requiredProficiency = newProficiencyRequired;
    }
    
    public String getProficiencyRequired() {
        return requiredProficiency;
    }    
    
    public void setName(String newName) {
        equipmentName = newName;
    }
    
    public String getName() {
        return equipmentName;
    }
    
    public void setEquipmentDescription(String description) {
        equipmentDescription = description;
    }
    
    public String getEquipmentDescription() {
        return equipmentDescription;
    }    
    
    public void setCost(double newCost) {
        goldCost = newCost;
    }
    
    public double getCost() {
        return goldCost;
    }
    
    public void setWeight(double newWeight) {
        weight = newWeight;
    }
    
    public double getWeight() {
        return weight;
    }
}
