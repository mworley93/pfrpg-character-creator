/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.models.equipment;


/**
 *
 * @author I'm dedicating this one to Megan Worley!
 */
public class Weapon extends Equipment {
    String smallDamage;
    String mediumDamage;
    String critical;
    
    int range;
    
    String damageType;

    
    public void setSmallDamage(String newSmallDamage) {
        smallDamage = newSmallDamage;
    }
    
    public String getSmallDamage() {
        return smallDamage;
    }
    
    public void setMediumDamage(String newMediumDamage) {
        mediumDamage = newMediumDamage;
    }
    
    public String getMediumDamage() {
        return mediumDamage;
    }
    
    public void setCritical(String newCritical) {
        critical = newCritical;
    }
    
    public String getCritical() {
        return critical;
    }
    
    public void setRange(int newRange) {
        range = newRange;
    }
    
    public int getRange() {
        return range;
    }
    
    public void setDamageType(String newDamageType) {
        damageType = newDamageType;
    }
    
    public String getDamageType() {
        return damageType;
    }

}
