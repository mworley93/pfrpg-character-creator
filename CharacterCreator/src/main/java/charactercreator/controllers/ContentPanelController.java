package charactercreator.controllers;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import charactercreator.models.UIModel;
import charactercreator.views.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

/**
 * This class declares the action listeners for the content panels and adds them to the view components.  It changes the current data in the user interface model
 * based on the user's action.
 * @author Megan Worley, Brandon Sharp
 */
public class ContentPanelController {
    
    private UIModel uiModel;
    private ContentPanelView contentView;
    
    public ContentPanelController(UIModel uiModel, ContentPanelView contentView) {
        this.uiModel = uiModel;
        this.contentView = contentView;
    }
    
    public void initViewAndModel()  {
        uiModel.setCurrentClassItems("Barbarian");
        uiModel.setCurrentRaceItems("Dwarf");
        uiModel.setCurrentAbilityScoreMethod("Standard");
        uiModel.setCurrentBloodlineItems("Aberrant");
        uiModel.setCurrentFavoredEnemyItems("Aberration");
        uiModel.setCurrentDruidBranchItems("Animal Companion");
        uiModel.setCurrentAnimalCompanionItems("Ape");
        uiModel.setCurrentDruidDomainItems("Air");
        uiModel.setCurrentArcaneBondItems("Bonded Object");
        uiModel.setCurrentWizardFamiliarItems("Bat");
        uiModel.setCurrentArcaneSchoolItems("Abjuration");
        uiModel.setCurrentForbiddenArcaneSchoolItems("Abjuration");
        uiModel.setCurrentSecondForbiddenArcaneSchoolItems("Abjuration");
        uiModel.setCurrentClericDomainItems("Air");
        uiModel.setCurrentSecondClericDomainItems("Air");
        contentView.initView();
        addActionListeners();
    }
    
    private void addActionListeners() {   
        contentView.addClassListener(classContentListener);
        contentView.addRaceListener(raceContentListener);
        contentView.addAbilityScoreListener(abilityScoreListener);
        contentView.addRaceAbilityScoreListener(raceAbilityScoreListener);
        contentView.addBloodlineListener(bloodlineListener);
        contentView.addSkillsListener(skillsListener);
        contentView.addFavoredEnemyListener(favoredEnemyListener);
        contentView.addDruidBranchListener(druidBranchListener);
        contentView.addAnimalCompanionListener(animalCompanionListener);
        contentView.addDruidDomainListener(druidDomainListener);
        contentView.addArcaneBondListener(arcaneBondListener);
        contentView.addWizardFamiliarListener(wizardFamiliarListener);
        contentView.addArcaneSchoolListener(arcaneSchoolListener);
        contentView.addForbiddenArcaneSchoolListener(forbiddenArcaneSchoolListener);
        contentView.addSecondForbiddenArcaneSchoolListener(secondForbiddenArcaneSchoolListener);
        contentView.addClericDomainListener(clericDomainListener);
        contentView.addSecondClericDomainListener(secondClericDomainListener);
        contentView.addFeatsListener(featListener);
        contentView.addCombatFeatsListener(combatFeatListener);
        contentView.addMonkFeatListener(monkFeatListener);
        contentView.addEquipmentListener(equipmentListener);
        contentView.addMiscListener(miscListener);
        contentView.addSpellbookListener(spellbookListener);
    }
    
     /**
     * Adds the implementation for the buttons on the class selection panel.
     */
    private ActionListener classContentListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Class Selection") == 0) {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentClassItems((String)dropDownMenu.getSelectedItem());
                contentView.updateClassView();
            }
            else if (event.getActionCommand().compareTo("More Info - Class") == 0) {
                ((ClassPanel)contentView.getContentPanel("Class Selection Screen")).displayPopup("Classes", (String)uiModel.getClassInfo().get("Classes"));
            }
            else if (event.getActionCommand().compareTo("More Info - Class Features") == 0) {
                ((ClassPanel)contentView.getContentPanel("Class Selection Screen")).displayPopup("Class Features", (String)uiModel.getClassInfo().get("Class Features"));
            }
        }      
    };
    
    /**
     * Adds the implementation for the buttons on the race selection panel.
     */
    private ActionListener raceContentListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Race Selection") == 0) {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentRaceItems((String)dropDownMenu.getSelectedItem());
                contentView.updateRaceView();
            }
            else if (event.getActionCommand().compareTo("More Info - Races") == 0) {
                ((RacePanel)contentView.getContentPanel("Race Selection Screen")).displayPopup("Races", (String)uiModel.getRaceInfo().get("Races"));
            }
            else if (event.getActionCommand().compareTo("More Info - Racial Qualities") == 0) {
                ((RacePanel)contentView.getContentPanel("Race Selection Screen")).displayPopup("Racial Qualities", (String)uiModel.getRaceInfo().get("Racial Qualities"));
            }
        }      
    };
    
    private ActionListener abilityScoreListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Ability Score Method Selection") == 0) {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentAbilityScoreMethod((String)dropDownMenu.getSelectedItem());
                contentView.updateAbilityScoresView();
            }
            else if (event.getActionCommand().compareTo("More Info - Ability Scores") == 0) {
                ((AbilityScorePanel)contentView.getContentPanel("Ability Score Screen")).displayPopup("Ability Score Modifiers", 
                        (String)uiModel.getAbilityScoreInfo().get("Ability Scores"));
            }
        }
    };
    
    private ActionListener raceAbilityScoreListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Race Ability Score Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                contentView.updateRaceAbilityScoresView();
            }
        }
    };    
    
    private ActionListener bloodlineListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Bloodline Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentBloodlineItems((String)dropDownMenu.getSelectedItem());
                contentView.updateBloodlineView();
            }
        }
    };
    
    private ActionListener skillsListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("More Info - Skills") == 0) {
                ((SkillsPanel)contentView.getContentPanel("Skills Selection Screen")).displayPopup("Skills", 
                        (String)uiModel.getSkillsInfo().get("Skills"));
            }
        }
    };
    
    private ActionListener featListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Add Feat") == 0) {
                ((FeatsPanel)contentView.getContentPanel("Feats Selection Screen")).addFeat();
            }
            else if (event.getActionCommand().compareTo("Remove Feat") == 0) {
                ((FeatsPanel)contentView.getContentPanel("Feats Selection Screen")).removeFeat();
            }
            contentView.updateFeatsView();
        }
    };
    
    private ActionListener combatFeatListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Add Feat") == 0) {
                ((CombatFeatsPanel)contentView.getContentPanel("Combat Feats Selection Screen")).addFeat();
            }
            else if (event.getActionCommand().compareTo("Remove Feat") == 0) {
                ((CombatFeatsPanel)contentView.getContentPanel("Combat Feats Selection Screen")).removeFeat();
            }
            contentView.updateCombatFeatsView();
        }
    };    
    
    private ActionListener monkFeatListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Add Feat") == 0) {
                ((MonkFeatPanel)contentView.getContentPanel("Monk Feat Selection Screen")).addFeat();
            }
            else if (event.getActionCommand().compareTo("Remove Feat") == 0) {
                ((MonkFeatPanel)contentView.getContentPanel("Monk Feat Selection Screen")).removeFeat();
            }
            contentView.updateMonkFeatView();
        }
    };       
    
    private ActionListener favoredEnemyListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Favored Enemy Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentFavoredEnemyItems((String)dropDownMenu.getSelectedItem());
                contentView.updateFavoredEnemyView();
            }
        }
    };
    
    private ActionListener druidBranchListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Druid Branch Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentDruidBranchItems((String)dropDownMenu.getSelectedItem());
                contentView.updateDruidBranchView();
            }
        }
    };
    
    private ActionListener animalCompanionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Animal Companion Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentAnimalCompanionItems((String)dropDownMenu.getSelectedItem());
                contentView.updateAnimalCompanionView();
            }
        }
    };
    
    private ActionListener druidDomainListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Druid Domain Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentDruidDomainItems((String)dropDownMenu.getSelectedItem());
                contentView.updateDruidDomainView();
            }
        }
    };    
    
    private ActionListener arcaneBondListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Arcane Bond Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentArcaneBondItems((String)dropDownMenu.getSelectedItem());
                contentView.updateArcaneBondView();
            }
        }
    };
    
    private ActionListener wizardFamiliarListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Wizard Familiar Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentWizardFamiliarItems((String)dropDownMenu.getSelectedItem());
                contentView.updateWizardFamiliarView();
            }
        }
    };
    
    private ActionListener arcaneSchoolListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Arcane School Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentArcaneSchoolItems((String)dropDownMenu.getSelectedItem());
                contentView.updateArcaneSchoolView();
            }
        }
    };  
    
    private ActionListener forbiddenArcaneSchoolListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Forbidden Arcane School Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentForbiddenArcaneSchoolItems((String)dropDownMenu.getSelectedItem());
                contentView.updateForbiddenArcaneSchoolView();
            }
        }
    };      
    
    private ActionListener secondForbiddenArcaneSchoolListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Second Forbidden Arcane School Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentSecondForbiddenArcaneSchoolItems((String)dropDownMenu.getSelectedItem());
                contentView.updateSecondForbiddenArcaneSchoolView();
            }
        }
    };     
    
    private ActionListener clericDomainListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Cleric Domain Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentClericDomainItems((String)dropDownMenu.getSelectedItem());
                contentView.updateClericDomainView();
            }
        }
    };      
    
    private ActionListener secondClericDomainListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Second Cleric Domain Selection") == 0)
            {
                JComboBox dropDownMenu = (JComboBox)event.getSource();
                uiModel.setCurrentSecondClericDomainItems((String)dropDownMenu.getSelectedItem());
                contentView.updateSecondClericDomainView();
            }
        }
    };  
    
    /**
     * Updates the displayed information on the equipment screen.
     */
    private ActionListener equipmentListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Purchase Item") == 0) {
                ((EquipmentPanel)contentView.getContentPanel("Equipment Screen")).addItem();
            }
            else if (event.getActionCommand().compareTo("Sell Item") == 0) {
                ((EquipmentPanel)contentView.getContentPanel("Equipment Screen")).removeItem();
            }
            contentView.updateEquipmentView();
        }
    };
    
    /**
     * Updates the displayed information on the misc screen.
     */
    private ActionListener miscListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Randomize - Age") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).randomAge();
            }
            else if (event.getActionCommand().compareTo("Randomize - Height") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).randomHeight();
            }
            else if (event.getActionCommand().compareTo("Randomize - Weight") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).randomWeight();
            }
            else if (event.getActionCommand().compareTo("Gender Selection") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).updateGender();
            }
            else if (event.getActionCommand().compareTo("Alignment Selection") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).updateAlignment();
            }
            else if (event.getActionCommand().compareTo("More Info - Background") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).displayPopup("Background", 
                        (String)uiModel.getMiscInfo().get("Background"));
            }
            else if (event.getActionCommand().compareTo("More Info - Age") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).displayPopup("Age", 
                        (String)uiModel.getMiscInfo().get("Age"));
            }
            else if (event.getActionCommand().compareTo("More Info - Height") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).displayPopup("Height", 
                        (String)uiModel.getMiscInfo().get("Height"));
            }
            else if (event.getActionCommand().compareTo("More Info - Weight") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).displayPopup("Weight", 
                        (String)uiModel.getMiscInfo().get("Weight"));
            }
            else if (event.getActionCommand().compareTo("More Info - Alignment") == 0) {
                ((MiscPanel)contentView.getContentPanel("Misc Screen")).displayPopup("Alignment", 
                        (String)uiModel.getMiscInfo().get("Alignment"));
            }
            contentView.updateMiscView();
        }
       };
            
    private ActionListener spellbookListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().matches("Add Spell")) {
                ((SpellbookPanel)contentView.getContentPanel("Spellbook Screen")).addSpell();
            }
            else if (event.getActionCommand().matches("Remove Spell")) {
                ((SpellbookPanel)contentView.getContentPanel("Spellbook Screen")).removeSpell();
            }
            contentView.updateSpellbookView();
        }
    };
}
