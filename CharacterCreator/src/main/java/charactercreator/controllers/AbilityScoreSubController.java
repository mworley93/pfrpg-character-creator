/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.controllers;

import charactercreator.models.AbilityScore;
import charactercreator.models.AbilityScoreGenerator;
import charactercreator.views.AbilityScoreSubPanel;
import charactercreator.views.RollPanel;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JButton;

/**
 *
 * @author Megan Worley, Brandon Sharp
 */
public class AbilityScoreSubController {
    
    AbilityScoreGenerator scoreGenModel;
    HashMap<String, AbilityScoreSubPanel> subPanelViews;
    
    /**
     * Main constructor.
     * @param model The ability score generator.
     * @param views The set of subpanels.
     */
    public AbilityScoreSubController(AbilityScoreGenerator model, HashMap<String, AbilityScoreSubPanel> views) {
        scoreGenModel = model;
        subPanelViews = views;
    }
    
    public void initViewAndModel() {
        ((AbilityScoreSubPanel)subPanelViews.get("Standard")).initializePanel(scoreGenModel);
        ((AbilityScoreSubPanel)subPanelViews.get("Purchase")).initializePanel(scoreGenModel);
        addActionListeners();
    }
            
    
    /**
     * Attaches the action listeners to the ability score sub panels.
     */
    public void addActionListeners() {
        ((AbilityScoreSubPanel)subPanelViews.get("Standard")).attachActionListener(rollListener);
        ((AbilityScoreSubPanel)subPanelViews.get("Purchase")).attachActionListener(purchaseListener);
    }
    
    /**
     * Manages the actions on the three dice rolling screens.
     */
    private ActionListener rollListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Roll - Standard") == 0) {
                scoreGenModel.fillRolledArray();
                ((RollPanel)subPanelViews.get("Standard")).removeActionListener(rollListener);
                ((AbilityScoreSubPanel)subPanelViews.get("Standard")).updateView();
                ((AbilityScoreSubPanel)subPanelViews.get("Standard")).attachActionListener(rollListener);
            }
            else if (event.getActionCommand().contains("Assign")) {
                JComboBox comboBox = (JComboBox)event.getSource();
                ((RollPanel)subPanelViews.get("Standard")).removeActionListener(rollListener);
                ((RollPanel)subPanelViews.get("Standard")).updateComboBoxes((String)comboBox.getSelectedItem(), event.getActionCommand());
                ((AbilityScoreSubPanel)subPanelViews.get("Standard")).attachActionListener(rollListener);
            }
        }
    };
    
    /**
     * Manages the actions on the purchase screen.
     */
    private ActionListener purchaseListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            switch(event.getActionCommand()) {
                case "STR+":
                    scoreGenModel.changePoints(AbilityScore.STR, AbilityScoreGenerator.Adjustment.UP);
                    break;
                case "STR-":
                    scoreGenModel.changePoints(AbilityScore.STR, AbilityScoreGenerator.Adjustment.DOWN);
                    break;    
                case "DEX+":
                    scoreGenModel.changePoints(AbilityScore.DEX, AbilityScoreGenerator.Adjustment.UP);
                    break;
                case "DEX-":
                    scoreGenModel.changePoints(AbilityScore.DEX, AbilityScoreGenerator.Adjustment.DOWN);
                    break;
                case "CON+":
                    scoreGenModel.changePoints(AbilityScore.CON, AbilityScoreGenerator.Adjustment.UP);
                    break;
                case "CON-":
                    scoreGenModel.changePoints(AbilityScore.CON, AbilityScoreGenerator.Adjustment.DOWN);
                    break;
                case "INT+":
                    scoreGenModel.changePoints(AbilityScore.INT, AbilityScoreGenerator.Adjustment.UP);
                    break;
                case "INT-":
                    scoreGenModel.changePoints(AbilityScore.INT, AbilityScoreGenerator.Adjustment.DOWN);
                    break;
                case "WIS+":
                    scoreGenModel.changePoints(AbilityScore.WIS, AbilityScoreGenerator.Adjustment.UP);
                    break;
                case "WIS-":
                    scoreGenModel.changePoints(AbilityScore.WIS, AbilityScoreGenerator.Adjustment.DOWN);
                    break;
                case "CHA+":
                    scoreGenModel.changePoints(AbilityScore.CHA, AbilityScoreGenerator.Adjustment.UP);
                    break;
                case "CHA-":
                    scoreGenModel.changePoints(AbilityScore.CHA, AbilityScoreGenerator.Adjustment.DOWN);
                    break;
                case "Reset":
                    scoreGenModel.resetPointsAndPurchaseScores();
            }
            ((AbilityScoreSubPanel)subPanelViews.get("Purchase")).updateView();
        }
    };
}
