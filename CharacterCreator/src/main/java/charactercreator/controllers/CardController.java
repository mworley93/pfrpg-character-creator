package charactercreator.controllers;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import charactercreator.components.PathfinderDecisionPopup;
import charactercreator.components.PathfinderDocumentPopup;
import charactercreator.components.PathfinderMessagePopup;
import charactercreator.components.PathfinderPopup;
import charactercreator.models.CharacterClass;
import charactercreator.models.CharacterRace;
import charactercreator.models.ModelWrapper;
import charactercreator.views.*;
import charactercreator.helpers.XMLReader;
import java.awt.CardLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * This class is used to control the switching of the cards.  It dynamically decides which cards should be displayed based on the user's actions.  
 * In addition, it also handles the initialization of the character and stores the variables from the view into the character.
 * @author Megan Worley, Brandon Sharp
 */
public class CardController {
    
    private CardView view;
    private ModelWrapper modelWrapper; // Acts as a wrapper for the multiple data classes needed by this controller and other controllers in the program.
    private CardLayout cards;
    
    /**
     * Main constructor.
     */
    public CardController(CardView cardView, ModelWrapper modelWrapper) {
        view = cardView;
        this.modelWrapper = modelWrapper;
        cards = view.getCardLayout();
        addActionListeners();
    }
    
    /**
     * Attaches the action listeners to the buttons on the cards.
     */
    private void addActionListeners() {
        ((TitlePanel)view.getCard("Title Screen")).attachActionListener(startScreenListener);
        ((WelcomePanel)view.getCard("Welcome Screen")).attachActionListener(welcomeScreenListener);
        ((TutorialSequence)view.getCard("Tutorial Sequence")).attachActionListener(tutorialScreenListener);
        ((CreationPanel)view.getCard("Class Selection Screen")).attachActionListener(classScreenListener);
        ((CreationPanel)view.getCard("Race Selection Screen")).attachActionListener(raceScreenListener);
        ((CreationPanel)view.getCard("Ability Score Screen")).attachActionListener(abilityScoreScreenListener);
        ((CreationPanel)view.getCard("Race Ability Score Screen")).attachActionListener(raceAbilityScoreScreenListener);
        ((CreationPanel)view.getCard("Bloodline Selection Screen")).attachActionListener(bloodlineScreenListener);
        ((CreationPanel)view.getCard("Favored Enemy Selection Screen")).attachActionListener(favoredEnemyScreenListener);
        ((CreationPanel)view.getCard("Druid Branch Selection Screen")).attachActionListener(druidBranchScreenListener);     
        ((CreationPanel)view.getCard("Animal Companion Selection Screen")).attachActionListener(animalCompanionScreenListener);
        ((CreationPanel)view.getCard("Druid Domain Selection Screen")).attachActionListener(druidDomainScreenListener);   
        ((CreationPanel)view.getCard("Arcane Bond Selection Screen")).attachActionListener(arcaneBondScreenListener);
        ((CreationPanel)view.getCard("Wizard Familiar Selection Screen")).attachActionListener(wizardFamiliarScreenListener);      
        ((CreationPanel)view.getCard("Arcane School Selection Screen")).attachActionListener(arcaneSchoolScreenListener);
        ((CreationPanel)view.getCard("Forbidden Arcane School Selection Screen")).attachActionListener(forbiddenArcaneSchoolScreenListener);  
        ((CreationPanel)view.getCard("Second Forbidden Arcane School Selection Screen")).attachActionListener(secondForbiddenArcaneSchoolScreenListener);    
        ((CreationPanel)view.getCard("Cleric Domain Selection Screen")).attachActionListener(clericDomainScreenListener);  
        ((CreationPanel)view.getCard("Second Cleric Domain Selection Screen")).attachActionListener(secondClericDomainScreenListener);   
        ((CreationPanel)view.getCard("Skills Selection Screen")).attachActionListener(skillsScreenListener);
        ((CreationPanel)view.getCard("Feats Selection Screen")).attachActionListener(featsScreenListener);  
        ((CreationPanel)view.getCard("Combat Feats Selection Screen")).attachActionListener(combatFeatsScreenListener);
        ((CreationPanel)view.getCard("Monk Feat Selection Screen")).attachActionListener(monkFeatScreenListener);
        ((CreationPanel)view.getCard("Equipment Screen")).attachActionListener(equipmentListener);
        ((CreationPanel)view.getCard("Misc Screen")).attachActionListener(miscListener);
        ((CreationPanel)view.getCard("Spellbook Screen")).attachActionListener(spellbookScreenListener);
        ((ConfirmationPanel)view.getCard("Confirmation Screen")).attachActionListener(confirmationScreenListener);
    }
    
    /**
     * Adds the implementation for the begin and exit buttons on the title screen.  Instantiates a character when the user begins the creation
     * process.
     */
    private ActionListener startScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Begin") == 0) {
                modelWrapper.initializeModel("BaseCharacter");
                modelWrapper.initializeModel("FeatGenerator");
                cards.show(view.getCard("Card Panel"), "Welcome Screen");
            }
            else if (event.getActionCommand().compareTo("About") == 0) {
                PathfinderMessagePopup aboutDialog = new PathfinderMessagePopup("About");
                aboutDialog.setString("This program is designed to help you create a 1st level character using the Pathfinder rules system.  " 
                        + "Have fun, and thank you for using our program!\n\n" 
                        + "This project was made possible due to Paizo's Open Game License, which states that the game mechanics of the"
                        + " Pathfinder Roleplaying Game are Open Game Content.  All artwork shown during use is copyright to Paizo Publishing LLC.\n\n"
                        + "Version: 1.0.0.0\n"
                        + "Developed by Megan Worley & Brandon Sharp");
                // Specifies that the JDialog should appear directly over the main app.
                aboutDialog.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                System.exit(0);
            }
        }
    };
    
    /**
     * Adds the implementation for the start and skip tutorial buttons on the Welcome Screen.  Will either start the tutorial sequence or skip to 
     * character creation.
     */
    private ActionListener welcomeScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Start Tutorial") == 0) {
                cards.show(view.getCard("Card Panel"), "Tutorial Sequence");
                if (!((TutorialSequence)view.getCard("Tutorial Sequence")).isInitialized()) {
                    ((TutorialSequence)view.getCard("Tutorial Sequence")).initialize(modelWrapper.getUIModel());
                }
                ((TutorialSequence)view.getCard("Tutorial Sequence")).start();
            }
            else {
                modelWrapper.initializeModel("BaseCharacter");
                modelWrapper.initializeModel("FeatGenerator");
                cards.show(view.getCard("Card Panel"), "Class Selection Screen");
            }
        }
    };
    
    private ActionListener confirmationScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Go Back") == 0) {
                cards.show(view.getCard("Card Panel"), "Misc Screen");
            }
            else if (event.getActionCommand().compareTo("Save as PDF") == 0) {
                String saveDest = ((ConfirmationPanel)view.getCard("Confirmation Screen")).getSaveFilePath();
                if (saveDest != null&& !saveDest.matches("CANCEL")) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderDocumentPopup documentPopup = new PathfinderDocumentPopup(frame, 
                            modelWrapper.getBaseCharacter(), saveDest);
            }
        }
        }
    };    
    
    private ActionListener tutorialScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Exit Tutorial") == 0) {
                modelWrapper.initializeModel("BaseCharacter");
                modelWrapper.initializeModel("FeatGenerator");
                cards.show(view.getCard("Card Panel"), "Class Selection Screen");
            } 
        }
    };
    
    /**
     * Adds the implementation for the navigation buttons on the character creation screens.  Stores the character's class based on the user's 
     * decision.
     */
    private ActionListener classScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.deallocateModel("BaseCharacter");
                cards.show(view.getCard("Card Panel"), "Title Screen");
            }
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                String selectedClass = ((ClassPanel)view.getContentView().getContentPanel("Class Selection Screen")).getSelectedClass();
                CharacterClass characterClass = new CharacterClass();
                XMLReader fileReader = new XMLReader();
                // Use the XML filereader to fill in the appropriate data and give the base character this new class.
                fileReader.fillCharacterClass("/data/classData.xml", selectedClass, characterClass);
                fileReader.fillCharacterClassSkills("/data/classSkillsData.xml", characterClass);
                modelWrapper.getBaseCharacter().setCharacterClass(characterClass);
                
                // Reset the feat list, fill the feat list up, and add the appropriate feats to the BaseCharacter!
                modelWrapper.getBaseCharacter().resetFeatsList();
                modelWrapper.getFeatGenerator().resetFeatList();
                modelWrapper.getFeatGenerator().fillFeatList("/data/featData.xml");
                modelWrapper.getFeatGenerator().addClassFeatsToBaseCharacter(modelWrapper.getBaseCharacter());
                
                modelWrapper.getBaseCharacter().removeDuplicateFeats();
                // Sorcerers need to choose a bloodline.
                if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Sorcerer")) {
                    view.addCardToLayout(view.getCard("Bloodline Selection Screen"), "Bloodline Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Bloodline Selection Screen");
                }
                
                // Rangers need a favored enemy.
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Ranger")) {
                    view.addCardToLayout(view.getCard("Favored Enemy Selection Screen"), "Favored Enemy Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Favored Enemy Selection Screen");
                }
                
                // Druids need a Nature Bond.
                
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Druid")) {
                    view.addCardToLayout(view.getCard("Druid Branch Selection Screen"), "Druid Branch Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Druid Branch Selection Screen");
                }
                
                // Wizards need an Arcane Bond
                
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Wizard")) {
                    view.addCardToLayout(view.getCard("Arcane Bond Selection Screen"), "Arcane Bond Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Arcane Bond Selection Screen");
                }        
                
                // Clerics need a Domain!
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Cleric")) {
                    view.addCardToLayout(view.getCard("Cleric Domain Selection Screen"), "Cleric Domain Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Cleric Domain Selection Screen");
                }
                
                else {
                    view.addCardToLayout(view.getCard("Race Selection Screen"), "Race Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Race Selection Screen");
                }

               
                
            }
        }
    };
    
    /**
     * Adds the implementation for the navigation buttons on the character creation screens.  Stores the character's class based on the user's 
     * decision.
     */
    private ActionListener raceScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            
            // This event handles if the previous button is selected.
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setCharacterRace(null);
                if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Sorcerer")) {
                    cards.show(view.getCard("Card Panel"), "Bloodline Selection Screen");
                }

                
                // Moving backwards logic
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Druid")) {
                    if(modelWrapper.getBaseCharacter().getDruidFeature().matches("Animal Companion")){
                        cards.show(view.getCard("Card Panel"), "Animal Companion Selection Screen");
                    }
                    if(modelWrapper.getBaseCharacter().getDruidFeature().matches("Domain")){
                        cards.show(view.getCard("Card Panel"), "Druid Domain Selection Screen");
                    }
                }
                
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Ranger")) {
                    cards.show(view.getCard("Card Panel"), "Favored Enemy Selection Screen");
                }
                
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Wizard")) {
                    if(modelWrapper.getBaseCharacter().getArcaneSchool().matches("Universalist")) {
                        cards.show(view.getCard("Card Panel"), "Arcane School Selection Screen");
                    }
                    else {
                        cards.show(view.getCard("Card Panel"), "Second Forbidden Arcane School Selection Screen");
                    }
                }
                
                else if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Cleric")) {
                    cards.show(view.getCard("Card Panel"), "Second Cleric Domain Selection Screen");
                }
                
                else {
                    cards.show(view.getCard("Card Panel"), "Class Selection Screen");
                }
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                String selectedRace = ((RacePanel)view.getContentView().getContentPanel("Race Selection Screen")).getSelectedRace();
                CharacterRace characterRace = new CharacterRace();
                XMLReader fileReader = new XMLReader();
                fileReader.fillRaceClass("/data/raceDescriptions.xml", selectedRace, characterRace);
                modelWrapper.getBaseCharacter().setCharacterRace(characterRace);
                modelWrapper.getBaseCharacter().resetAndSetRacialSkillBonuses();
                modelWrapper.getBaseCharacter().getCharacterRace().calculateBonusPoints();
                modelWrapper.getBaseCharacter().getCharacterRace().calculateNumFeatures();
                modelWrapper.getBaseCharacter().getCharacterRace().cleanFeatures();
                modelWrapper.getBaseCharacter().getCharacterRace().cleanLanguages();
                
                view.addCardToLayout(view.getCard("Ability Score Screen"), "Ability Score Screen");
                cards.show(view.getCard("Card Panel"), "Ability Score Screen");
            }
        }
    };
    
    /**
     * Adds the implementation for the navigation buttons on the race ability score generation screen.
     */
    private ActionListener abilityScoreScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().resetBaseAbilityScoreMap();
                cards.show(view.getCard("Card Panel"), "Race Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                if(((AbilityScorePanel)view.getContentView().getContentPanel("Ability Score Screen")).getCurrentMethod().matches("Purchase")) {
                    if(modelWrapper.getAbilityScoreGenerator().getPoints() != 0) {
                        JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                        PathfinderPopup warningPopup = new PathfinderPopup("Please spend all of your points before continuing, thank you!", 400, 150, frame);
                        warningPopup.setLocationRelativeTo(view.getCard("Card Panel"));                        
                        return;
                    }
                }
                if(((AbilityScorePanel)view.getContentView().getContentPanel("Ability Score Screen")).getCurrentMethod().matches("Standard")) {
                    if(modelWrapper.getAbilityScoreGenerator().checkRollForEmpty()) {
                        JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                        PathfinderPopup warningPopup = new PathfinderPopup("Please roll and select a value for each ability score before continuing, thank you!", 400, 150, frame);
                        warningPopup.setLocationRelativeTo(view.getCard("Card Panel"));
                        return;
                    }
                }
                       
                if (modelWrapper.getUIModel().getCurrentInformation("abilityScoreMethod").matches("Standard")) {
                    modelWrapper.getBaseCharacter().setBaseAbilityScores(modelWrapper.getAbilityScoreGenerator().getRollScores());
                }
                else {
                    modelWrapper.getBaseCharacter().setBaseAbilityScores(modelWrapper.getAbilityScoreGenerator().getPurchaseScores());
                }              
                ((RaceAbilityScorePanel)view.getContentView().getContentPanel("Race Ability Score Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                ((RaceAbilityScorePanel)view.getContentView().getContentPanel("Race Ability Score Screen")).setBaseStats();
                ((RaceAbilityScorePanel)view.getContentView().getContentPanel("Race Ability Score Screen")).updateView();
                view.addCardToLayout(view.getCard("Race Ability Score Screen"), "Race Ability Score Screen");
                cards.show(view.getCard("Card Panel"), "Race Ability Score Screen");
            }
        }
    };
    
    private ActionListener raceAbilityScoreScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().resetRaceAbilityScoreMap();
                ((RaceAbilityScorePanel)view.getContentView().getContentPanel("Race Ability Score Screen")).resetPanel();
                cards.show(view.getCard("Card Panel"), "Ability Score Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                
                ((SkillsPanel)view.getContentView().getContentPanel("Skills Selection Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                ((SkillsPanel)view.getContentView().getContentPanel("Skills Selection Screen")).updateView();   
                
                if(modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Wizard")) {
                    modelWrapper.initializeModel("SpellGenerator");
                    modelWrapper.getSpellGenerator().fillSpellList("/data/spellData.xml");
                    ((SpellbookPanel)view.getContentView().getContentPanel("Spellbook Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                    ((SpellbookPanel)view.getContentView().getContentPanel("Spellbook Screen")).setSpellGenerator(modelWrapper.getSpellGenerator());
                    modelWrapper.getSpellGenerator().resetRemainingSpells();
                    view.addCardToLayout(view.getCard("Spellbook Screen"), "Spellbook Screen");
                    cards.show(view.getCard("Card Panel"), "Spellbook Screen");
                    ((SpellbookPanel)view.getContentView().getContentPanel("Spellbook Screen")).updateView();
                }
                else {
                    view.addCardToLayout(view.getCard("Skills Selection Screen"), "Skills Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Skills Selection Screen");
                }
            }            
        }    
    };    

    private ActionListener bloodlineScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setBloodline("");
                cards.show(view.getCard("Card Panel"), "Class Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setBloodline((String)((BloodlinePanel)view.getContentView().getContentPanel("Bloodline Selection Screen")).getSelectedBloodline());
                view.addCardToLayout(view.getCard("Race Selection Screen"), "Race Selection Screen");
                cards.show(view.getCard("Card Panel"), "Race Selection Screen");
            }
        }
    };    
    
    private ActionListener favoredEnemyScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setFavoredEnemy("");
                cards.show(view.getCard("Card Panel"), "Class Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setFavoredEnemy((String)((FavoredEnemyPanel)view.getContentView().getContentPanel("Favored Enemy Selection Screen")).getSelectedFavoredEnemy());
                view.addCardToLayout(view.getCard("Race Selection Screen"), "Race Selection Screen");
                cards.show(view.getCard("Card Panel"), "Race Selection Screen");
            }
        }
    };
    
    private ActionListener druidBranchScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setDruidFeature("");
                cards.show(view.getCard("Card Panel"), "Class Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setDruidFeature((String)((DruidBranchPanel)view.getContentView().getContentPanel("Druid Branch Selection Screen")).getSelectedDruidBranch());
                // pick next card accordingly
                if(modelWrapper.getBaseCharacter().getDruidFeature().matches("Animal Companion")) {
                    view.addCardToLayout(view.getCard("Animal Companion Selection Screen"), "Animal Companion Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Animal Companion Selection Screen");
                }
                else if(modelWrapper.getBaseCharacter().getDruidFeature().matches("Domain")) {
                    view.addCardToLayout(view.getCard("Druid Domain Selection Screen"), "Druid Domain Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Druid Domain Selection Screen");
                }
                
            }
        }
    };
    
    private ActionListener animalCompanionScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setAnimalCompanion("");
                cards.show(view.getCard("Card Panel"), "Druid Branch Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setAnimalCompanion((String)((AnimalCompanionPanel)view.getContentView().getContentPanel("Animal Companion Selection Screen")).getSelectedAnimalCompanion());
                view.addCardToLayout(view.getCard("Race Selection Screen"), "Race Selection Screen");
                cards.show(view.getCard("Card Panel"), "Race Selection Screen");
                
            }
        }
    };    
    
    private ActionListener druidDomainScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setFirstDomain("");
                cards.show(view.getCard("Card Panel"), "Druid Branch Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setFirstDomain((String)((DruidDomainPanel)view.getContentView().getContentPanel("Druid Domain Selection Screen")).getSelectedDruidDomain());
                view.addCardToLayout(view.getCard("Race Selection Screen"), "Race Selection Screen");
                cards.show(view.getCard("Card Panel"), "Race Selection Screen");
                
            }
        }
    };    
          
    private ActionListener arcaneBondScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setArcaneBond("");
                cards.show(view.getCard("Card Panel"), "Class Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setArcaneBond((String)((ArcaneBondPanel)view.getContentView().getContentPanel("Arcane Bond Selection Screen")).getSelectedArcaneBond());
                if(modelWrapper.getBaseCharacter().getArcaneBond().matches("Familiar")) {
                    view.addCardToLayout(view.getCard("Wizard Familiar Selection Screen"), "Wizard Familiar Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Wizard Familiar Selection Screen");
                }
                else {
                    view.addCardToLayout(view.getCard("Arcane School Selection Screen"), "Arcane School Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Arcane School Selection Screen");
                }
            }
        }
    };    
    
    private ActionListener wizardFamiliarScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setFamiliar("");
                cards.show(view.getCard("Card Panel"), "Arcane Bond Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setFamiliar((String)((WizardFamiliarPanel)view.getContentView().getContentPanel("Wizard Familiar Selection Screen")).getSelectedWizardFamiliar());
                view.addCardToLayout(view.getCard("Arcane School Selection Screen"), "Arcane School Selection Screen");
                cards.show(view.getCard("Card Panel"), "Arcane School Selection Screen");
            }
        }
    };         
           
    private ActionListener arcaneSchoolScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setArcaneSchool("");
                if(modelWrapper.getBaseCharacter().getArcaneBond().matches("Familiar")) {
                    cards.show(view.getCard("Card Panel"), "Wizard Familiar Selection Screen");
                }
                else {
                    cards.show(view.getCard("Card Panel"), "Arcane Bond Selection Screen");
                }
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setArcaneSchool((String)((ArcaneSchoolPanel)view.getContentView().getContentPanel("Arcane School Selection Screen")).getSelectedArcaneSchool());
                if(modelWrapper.getBaseCharacter().getArcaneSchool().matches("Universalist")) {
                    modelWrapper.initializeModel("SpellGenerator");
                    modelWrapper.getSpellGenerator().fillSpellList("/data/spellData.xml");
                    modelWrapper.getSpellGenerator().fillSeparatedSpellLists();
                    ((SpellbookPanel)view.getContentView().getContentPanel("Spellbook Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                    ((SpellbookPanel)view.getContentView().getContentPanel("Spellbook Screen")).setSpellGenerator(modelWrapper.getSpellGenerator());                    
                    view.addCardToLayout(view.getCard("Spellbook Screen"), "Spellbook Screen");
                    cards.show(view.getCard("Card Panel"), "Spellbook Screen");
                }
                else {
                    view.addCardToLayout(view.getCard("Forbidden Arcane School Selection Screen"), "Forbidden Arcane School Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Forbidden Arcane School Selection Screen");
                }
            }
        }
    };   
    
    private ActionListener forbiddenArcaneSchoolScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setForbiddenArcaneSchoolOne("");
                cards.show(view.getCard("Card Panel"), "Arcane School Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {               
                modelWrapper.getBaseCharacter().setForbiddenArcaneSchoolOne((String)((ForbiddenSchoolPanel)view.getContentView().getContentPanel("Forbidden Arcane School Selection Screen")).getSelectedArcaneSchool());
                if(modelWrapper.getBaseCharacter().getForbiddenArcaneSchoolOne().matches(modelWrapper.getBaseCharacter().getArcaneSchool())) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup featWarning = new PathfinderPopup("Please select a different school than your chosen arcane school to be your forbidden school. Thank you!", 400, 150, frame);
                    featWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }                      
                view.addCardToLayout(view.getCard("Second Forbidden Arcane School Selection Screen"), "Second Forbidden Arcane School Selection Screen");
                cards.show(view.getCard("Card Panel"), "Second Forbidden Arcane School Selection Screen");
            }
        }
    };  

    private ActionListener secondForbiddenArcaneSchoolScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setForbiddenArcaneSchoolTwo("");
                cards.show(view.getCard("Card Panel"), "Forbidden Arcane School Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setForbiddenArcaneSchoolTwo((String)((SecondForbiddenSchoolPanel)view.getContentView().getContentPanel("Second Forbidden Arcane School Selection Screen")).getSelectedArcaneSchool());
                if(modelWrapper.getBaseCharacter().getForbiddenArcaneSchoolTwo().matches(modelWrapper.getBaseCharacter().getForbiddenArcaneSchoolOne())) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup featWarning = new PathfinderPopup("Please don't choose the same forbidden school twice. Thank you!", 400, 150, frame);
                    featWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                else if(modelWrapper.getBaseCharacter().getForbiddenArcaneSchoolTwo().matches(modelWrapper.getBaseCharacter().getArcaneSchool())) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup featWarning = new PathfinderPopup("Please select a different school than your chosen arcane school to be your forbidden school. Thank you!", 400, 150, frame);
                    featWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }                
                view.addCardToLayout(view.getCard("Race Selection Screen"), "Race Selection Screen");
                cards.show(view.getCard("Card Panel"), "Race Selection Screen");
            }
        }
    };     

    
    private ActionListener clericDomainScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setFirstDomain("");
                cards.show(view.getCard("Card Panel"), "Class Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setFirstDomain((String)((ClericDomainPanel)view.getContentView().getContentPanel("Cleric Domain Selection Screen")).getSelectedClericDomain());
                view.addCardToLayout(view.getCard("Second Cleric Domain Selection Screen"), "Second Cleric Domain Selection Screen");
                cards.show(view.getCard("Card Panel"), "Second Cleric Domain Selection Screen");
            }
        }
    };     
    
    private ActionListener secondClericDomainScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().setSecondDomain("");
                cards.show(view.getCard("Card Panel"), "Cleric Domain Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setSecondDomain((String)((SecondClericDomainPanel)view.getContentView().getContentPanel("Second Cleric Domain Selection Screen")).getSelectedClericDomain());
                if(modelWrapper.getBaseCharacter().getFirstDomain().matches(modelWrapper.getBaseCharacter().getSecondDomain())) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup domainWarning = new PathfinderPopup("Please don't select the same domain twice. Thank you!", 400, 150, frame);
                    domainWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                view.addCardToLayout(view.getCard("Race Selection Screen"), "Race Selection Screen");
                cards.show(view.getCard("Card Panel"), "Race Selection Screen");
            }
        }
    };
    
    /**
    * Adds the implementation for the navigation buttons on the skills selection screen.
    */
    private ActionListener skillsScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
        // Resets the character's skill maps and the skill generators data when moving backwards from this page.
                modelWrapper.getBaseCharacter().initializeSkillMaps();
                modelWrapper.getSkillsGenerator().resetPointsAndSkills();
        // Updates the table and view on the screen to be appropriately reset.
                ((SkillsPanel)view.getContentView().getContentPanel("Skills Selection Screen")).skillTableReset();
                ((SkillsPanel)view.getContentView().getContentPanel("Skills Selection Screen")).updateView();
                cards.show(view.getCard("Card Panel"), "Race Ability Score Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
        // Warn user if they have not spent all their points using popup
                if(modelWrapper.getSkillsGenerator().getPoints() != 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup pointsWarning = new PathfinderPopup("Please spend all your skill points before continuing. Thank you!", 400, 150, frame);
                    pointsWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
        // Update basecharacter's final skill map.
                modelWrapper.getBaseCharacter().setRankSkillMap(modelWrapper.getSkillsGenerator().getRankSkillMap());
                modelWrapper.getBaseCharacter().setMiscSkillMap(modelWrapper.getSkillsGenerator().getMiscSkillMap());
                modelWrapper.getBaseCharacter().setFinalSkillMap(modelWrapper.getSkillsGenerator().getTotalSkillMap());
        // This code prepares the feat generator and FeatsPanel for use.
                modelWrapper.getFeatGenerator().checkPrerequisites(modelWrapper.getBaseCharacter());
                modelWrapper.getFeatGenerator().setNumAllowedFeats(modelWrapper.getBaseCharacter().getCharacterClass().getNumberOfFeats() + 
                       modelWrapper.getBaseCharacter().getCharacterRace().getBonusFeatPoints());
                ((FeatsPanel)view.getContentView().getContentPanel("Feats Selection Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                ((FeatsPanel)view.getContentView().getContentPanel("Feats Selection Screen")).setFeatGenerator(modelWrapper.getFeatGenerator());
                modelWrapper.getFeatGenerator().addClassFeatsToBaseCharacter(modelWrapper.getBaseCharacter());
                  
                
                view.addCardToLayout(view.getCard("Feats Selection Screen"), "Feats Selection Screen");
                cards.show(view.getCard("Card Panel"), "Feats Selection Screen");
                
            }
        }
    };   
    
    
    private ActionListener spellbookScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                cards.show(view.getCard("Card Panel"), "Race Ability Score Screen");
            }
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                if(modelWrapper.getSpellGenerator().getLevelOneSpellsRemaining() > 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup pointsWarning = new PathfinderPopup("Don't you want all your spells? Pick a few more, you'll need them!", 400, 150, frame);
                    pointsWarning.setLocationRelativeTo(view.getCard("Card Panel"));                  
                }
                else {
                modelWrapper.getBaseCharacter().setSpellList(modelWrapper.getSpellGenerator().getSelectedLevelOneSpellList());
                view.addCardToLayout(view.getCard("Skills Selection Screen"), "Skills Selection Screen");
                cards.show(view.getCard("Card Panel"), "Skills Selection Screen");
                }
            }
        }
    };         
    /**
     * Adds the implementation for the begin and exit buttons on the feats screen. 
     * process.
     */
    private ActionListener featsScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().removeFeatsFromFeatList(modelWrapper.getFeatGenerator().getSelectedFeats());
                modelWrapper.getFeatGenerator().resetTemporaryLists();
                cards.show(view.getCard("Card Panel"), "Skills Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                if(modelWrapper.getFeatGenerator().getNumFeatsRemaining() != 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup featWarning = new PathfinderPopup("Please select a feat. Thank you!", 400, 150, frame);
                    featWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                if (modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Fighter")) {
                    modelWrapper.getFeatGenerator().fillCombatFeats();
                    modelWrapper.getFeatGenerator().checkPrerequisites(modelWrapper.getBaseCharacter());
                    modelWrapper.getFeatGenerator().setNumAllowedCombatFeats(1);
                    ((CombatFeatsPanel)view.getContentView().getContentPanel("Combat Feats Selection Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                    ((CombatFeatsPanel)view.getContentView().getContentPanel("Combat Feats Selection Screen")).setFeatGenerator(modelWrapper.getFeatGenerator());
                    view.addCardToLayout(view.getCard("Combat Feats Selection Screen"), "Combat Feats Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Combat Feats Selection Screen");
                }
                else if (modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Monk")) {
                    modelWrapper.getFeatGenerator().fillMonkFeats();
                    modelWrapper.getFeatGenerator().checkPrerequisites(modelWrapper.getBaseCharacter());
                    modelWrapper.getFeatGenerator().setNumAllowedMonkFeats(1);
                    ((MonkFeatPanel)view.getContentView().getContentPanel("Monk Feat Selection Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                    ((MonkFeatPanel)view.getContentView().getContentPanel("Monk Feat Selection Screen")).setFeatGenerator(modelWrapper.getFeatGenerator());
                    view.addCardToLayout(view.getCard("Monk Feat Selection Screen"), "Monk Feat Selection Screen");
                    cards.show(view.getCard("Card Panel"), "Monk Feat Selection Screen");
                }
                else {
                    modelWrapper.initializeModel("EquipmentGenerator");
                    modelWrapper.getEquipmentGenerator().fillEquipmentList("/data/equipmentData.xml");
                    modelWrapper.getEquipmentGenerator().filterEquipmentToMatchProficiencies(modelWrapper.getBaseCharacter());
                    modelWrapper.getEquipmentGenerator().setGold(modelWrapper.getBaseCharacter().getCharacterClass().getStartingGold());
                    ((EquipmentPanel)view.getContentView().getContentPanel("Equipment Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                    ((EquipmentPanel)view.getContentView().getContentPanel("Equipment Screen")).setEquipmentGenerator(modelWrapper.getEquipmentGenerator());
                    view.addCardToLayout(view.getCard("Equipment Screen"), "Equipment Screen");
                    cards.show(view.getCard("Card Panel"), "Equipment Screen");

                }
            }
        }
    };  
    
    /**
     * Adds the implementation for the begin and exit buttons on the feats screen. 
     * process.
     */
    private ActionListener combatFeatsScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().removeFeatsFromFeatList(modelWrapper.getFeatGenerator().getSelectedCombatFeats());
                modelWrapper.getFeatGenerator().resetCombatLists();
                cards.show(view.getCard("Card Panel"), "Feats Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                if(modelWrapper.getFeatGenerator().getNumCombatFeatsRemaining() != 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup featWarning = new PathfinderPopup("Please select a combat feat. Thank you!", 400, 150, frame);
                    featWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                modelWrapper.initializeModel("EquipmentGenerator");
                modelWrapper.getEquipmentGenerator().fillEquipmentList("/data/equipmentData.xml");
                modelWrapper.getEquipmentGenerator().filterEquipmentToMatchProficiencies(modelWrapper.getBaseCharacter());
                modelWrapper.getEquipmentGenerator().setGold(modelWrapper.getBaseCharacter().getCharacterClass().getStartingGold());
                ((EquipmentPanel)view.getContentView().getContentPanel("Equipment Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                ((EquipmentPanel)view.getContentView().getContentPanel("Equipment Screen")).setEquipmentGenerator(modelWrapper.getEquipmentGenerator());
                view.addCardToLayout(view.getCard("Equipment Screen"), "Equipment Screen");
                cards.show(view.getCard("Card Panel"), "Equipment Screen");
            }
        }
    };    
    
    private ActionListener monkFeatScreenListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.getBaseCharacter().removeFeatsFromFeatList(modelWrapper.getFeatGenerator().getSelectedMonkFeats());
                modelWrapper.getFeatGenerator().resetMonkLists();
                cards.show(view.getCard("Card Panel"), "Feats Selection Screen");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                if(modelWrapper.getFeatGenerator().getNumMonkFeatsRemaining() != 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup featWarning = new PathfinderPopup("Please select a bonus monk feat. Thank you!", 400, 150, frame);
                    featWarning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                modelWrapper.initializeModel("EquipmentGenerator");
                modelWrapper.getEquipmentGenerator().fillEquipmentList("/data/equipmentData.xml");
                modelWrapper.getEquipmentGenerator().filterEquipmentToMatchProficiencies(modelWrapper.getBaseCharacter());
                modelWrapper.getEquipmentGenerator().setGold(modelWrapper.getBaseCharacter().getCharacterClass().getStartingGold());
                ((EquipmentPanel)view.getContentView().getContentPanel("Equipment Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                ((EquipmentPanel)view.getContentView().getContentPanel("Equipment Screen")).setEquipmentGenerator(modelWrapper.getEquipmentGenerator());
                view.addCardToLayout(view.getCard("Equipment Screen"), "Equipment Screen");
                cards.show(view.getCard("Card Panel"), "Equipment Screen");
            }
        }
    };     
    
    /**
     * Adds the implementation for the begin and previous buttons on the equipment screen.
     */
    private ActionListener equipmentListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                modelWrapper.deallocateModel("EquipmentGenerator");
                if (modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Fighter")) {
                    cards.show(view.getCard("Card Panel"), "Combat Feats Selection Screen");
                }
                else if (modelWrapper.getBaseCharacter().getCharacterClass().getClassName().matches("Monk")) {
                    cards.show(view.getCard("Card Panel"), "Monk Feat Selection Screen");
                }
                else {
                    cards.show(view.getCard("Card Panel"), "Feats Selection Screen");
                    view.removeCardFromLayout(view.getCard("Equipment Screen"));
                }
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                modelWrapper.getBaseCharacter().setArmorList(modelWrapper.getEquipmentGenerator().getSelectedArmor());
                modelWrapper.getBaseCharacter().setWeaponsList(modelWrapper.getEquipmentGenerator().getSelectedWeapons());
                modelWrapper.initializeModel("AttributeGenerator");
                view.addCardToLayout(view.getCard("Misc Screen"), "Misc Screen");
                ((MiscPanel)view.getContentView().getContentPanel("Misc Screen")).setAttributeGenerator(modelWrapper.getAttributeGenerator());
                ((MiscPanel)view.getContentView().getContentPanel("Misc Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                ((MiscPanel)view.getContentView().getContentPanel("Misc Screen")).updateView();
                cards.show(view.getCard("Card Panel"), "Misc Screen");

            }
        }
    };
                
    /**
     * Adds the implementation for the begin and previous buttons on the misc/background screen.
     */
    private ActionListener miscListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Previous") == 0) {
                cards.show(view.getCard("Card Panel"), "Equipment Screen");
                view.removeCardFromLayout(view.getCard("Misc Screen"));
                modelWrapper.deallocateModel("AttributeGenerator");
            }                
            // This event handles the decisions so far popup.
            else if (event.getActionCommand().matches("Decision")) {
                    PathfinderDecisionPopup decisionPopup = new PathfinderDecisionPopup(modelWrapper.getBaseCharacter());
                    decisionPopup.setLocationRelativeTo(view.getCard("Card Panel"));
            }
            else {
                if (modelWrapper.getAttributeGenerator().getCharacterName().isEmpty()) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup warning = new PathfinderPopup("Please choose a name for your character. Thank you!", 400, 150, frame);
                    warning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;                    
                }
                else if (modelWrapper.getAttributeGenerator().getAge() == 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup warning = new PathfinderPopup("Please choose an age for your character. Thank you!", 400, 150, frame);
                    warning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                else if (modelWrapper.getAttributeGenerator().getHeight() == 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup warning = new PathfinderPopup("Please choose a height for your character. Thank you!", 400, 150, frame);
                    warning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                else if (modelWrapper.getAttributeGenerator().getWeight() == 0) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(view.getCard("Card Panel"));
                    PathfinderPopup warning = new PathfinderPopup("Please choose a weight for your character. Thank you!", 400, 150, frame);
                    warning.setLocationRelativeTo(view.getCard("Card Panel"));
                    return;
                }
                modelWrapper.getBaseCharacter().setName(modelWrapper.getAttributeGenerator().getCharacterName());
                modelWrapper.getBaseCharacter().setPlayerName(modelWrapper.getAttributeGenerator().getPlayerName());
                modelWrapper.getBaseCharacter().setAlignment(modelWrapper.getAttributeGenerator().getAlignment().getString());
                modelWrapper.getBaseCharacter().setBiography(modelWrapper.getAttributeGenerator().getBackground());
                modelWrapper.getBaseCharacter().setAge(modelWrapper.getAttributeGenerator().getAge());
                modelWrapper.getBaseCharacter().setHeight(modelWrapper.getAttributeGenerator().getHeight());
                modelWrapper.getBaseCharacter().setWeight(modelWrapper.getAttributeGenerator().getWeight());
                modelWrapper.getBaseCharacter().setGender(modelWrapper.getAttributeGenerator().getGender().getString());
                
                view.addCardToLayout(view.getCard("Confirmation Screen"), "Confirmation Screen");
                ((ConfirmationPanel)view.getCard("Confirmation Screen")).setBaseCharacter(modelWrapper.getBaseCharacter());
                ((ConfirmationPanel)view.getCard("Confirmation Screen")).updateView();
                cards.show(view.getCard("Card Panel"), "Confirmation Screen");
            }
            
        }
    };
}
            

    
            
