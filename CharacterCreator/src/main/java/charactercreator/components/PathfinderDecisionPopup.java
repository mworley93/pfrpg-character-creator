/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import charactercreator.models.AbilityScore;
import charactercreator.models.BaseCharacter;
import charactercreator.models.Feat;
import charactercreator.models.Spell;
import charactercreator.models.equipment.Armor;
import charactercreator.models.equipment.Weapon;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author Brandon
 */

public class PathfinderDecisionPopup extends PathfinderMessagePopup implements ActionListener{
    
    private BaseCharacter character;
    private String text;  
    // These are all the Strings to be filled.
    String className;
    String raceName;
    ArrayList<String> classFeatures = new ArrayList<>();

    String strengthScore;
    String dexterityScore;
    String constitutionScore;
    String intelligenceScore;
    String wisdomScore;
    String charismaScore;    
    // These are some preset spacings to make the formatting more convenient.
    private static String indentation = "    ";    
    
    // May possibly need 2-3 different constructors to account for different situations.
    public PathfinderDecisionPopup(BaseCharacter character) {
        super("Your Decisions Thus Far"); // This calls the base class's constructor.  There are lots of different JDialog constructors where you can specify exiting behavior, titles, etc.
        this.character = character;
        createText();
        this.textPane.setText(text);
        this.textPane.setCaretPosition(0);
    }   
    
    /**
     * This function takes the BaseCharacter supplied, and parses its data members for the appropriate text to display.
     * If a given member is null, then it should instead display "Not chosen yet." This text is used in the JTextArea for
     * displaying in the popup.
     */
    private void createText() {            
        
        // Filling Class Name.
        if(character.getCharacterClass() == null) className = "Not chosen yet.";
        else className = character.getCharacterClass().getClassName();
        
        // Filling Race Name.
        if(character.getCharacterRace() == null) raceName = "Not chosen yet.";
        else raceName = character.getCharacterRace().getRaceName();     
        
        // Creating total text string, one item at a time.
        text = "Class: " + className + "\n";
        text = text + indentation + "Class Features:" + "\n";
        fillClassFeatures(classFeatures);        
        if (character.getCharacterClass() == null) {
            text = text + indentation + indentation + "Not chosen yet." + "\n";
        }
        for (String s : classFeatures) text = text + indentation + indentation + s + "\n";
        
        text = text + "\n";
        
        text = text + "Race: " + raceName + "\n" + "\n";
        
        // This function adds all the ability scores
        text = appendAbilityScores(text);
        
        // Feats
        text = text + "Feats Selected: " + "\n";
        text = appendFeats(text);
        
        // This will append all the equipment they have purchased.
        text = text + "Equipment Purchased" + "\n";
        ArrayList<Weapon> weaponsList = character.getWeaponsList();
        ArrayList<Armor> armorList = character.getArmorList();        
        for (Weapon w : weaponsList) text = text + indentation + w.getName() + "\n";
        for (Armor a : armorList) text = text + indentation + a.getName() + "\n";
        
        // If they didn't pick buy any items yet, this does so.
        if(weaponsList.isEmpty() && armorList.isEmpty()) text = text + indentation + "No equipment purchased." + "\n" + "\n";
        
        // If they are a wizard, attach the spells.
        if((character.getCharacterClass() != null) && (character.getCharacterClass().getClassName().matches("Wizard"))) {
            text = text + "Spells:" + "\n";
            text = appendSpells(text);
        }
        
        
    }
    
    // This function creates an arraylist of strings depending on which class the user has selected.
    private ArrayList<String> fillClassFeatures(ArrayList<String> classFeatures) {
        
        if(character.getCharacterClass() == null) {
            return null;
        }
        
        if(character.getCharacterClass().getClassName().matches("Barbarian")) classFeatures.add("Barbarians have no class features to select.");
        
        if(character.getCharacterClass().getClassName().matches("Bard")) classFeatures.add("Bards have no class features to select.");
        
        if(character.getCharacterClass().getClassName().matches("Cleric")) {
            if (character.getFirstDomain() == null) classFeatures.add("First Domain not chosen yet.");
            else classFeatures.add("First Domain: " + character.getFirstDomain());
            if (character.getSecondDomain() == null) classFeatures.add("Second Domain not chosen yet.");
            else classFeatures.add("Second Domain: " + character.getSecondDomain());
        }
        
        if(character.getCharacterClass().getClassName().matches("Druid")) {
            if (character.getDruidFeature() == null) classFeatures.add("Druid Class Feature not chosen yet.");
            else classFeatures.add("Druid Class Feature: " + character.getDruidFeature());
            if (character.getDruidFeature().matches("Animal Companion")) classFeatures.add("Animal Companion: " + character.getAnimalCompanion());
            if (character.getDruidFeature().matches("Domain")) classFeatures.add("Domain: " + character.getFirstDomain());
        }        

        if(character.getCharacterClass().getClassName().matches("Fighter")) classFeatures.add("Fighters have no class features to select.");
                
        if(character.getCharacterClass().getClassName().matches("Monk")) classFeatures.add("Monks have no class features to select.");
                
        if(character.getCharacterClass().getClassName().matches("Paladin")) classFeatures.add("Paladins have no class features to select.");
            
        if(character.getCharacterClass().getClassName().matches("Ranger")) {
            if (character.getFavoredEnemy() == null) classFeatures.add("Ranger Class Feature not chosen yet.");
            else classFeatures.add("Favored Enemy: " + character.getFavoredEnemy());
        }                
        
        if(character.getCharacterClass().getClassName().matches("Rogue")) classFeatures.add("Rogues have no class features to select.");
             
        if(character.getCharacterClass().getClassName().matches("Sorcerer")) {
            if (character.getBloodline()== null) classFeatures.add("Sorcerer Class Feature not chosen yet.");
            else classFeatures.add("Bloodline: " + character.getBloodline());
        }    
        
        if(character.getCharacterClass().getClassName().matches("Wizard")) {
            if (character.getArcaneBond() == null) classFeatures.add("Wizard Class Feature not chosen yet.");
            else classFeatures.add("Arcane Bond: " + character.getArcaneBond());
            if (character.getArcaneSchool() == null) classFeatures.add("Arcane School not chosen yet.");
            else classFeatures.add("Arcane School: " + character.getArcaneSchool());
            if (character.getForbiddenArcaneSchoolOne() == null) classFeatures.add("First Forbidden School not chosen yet.");
            else classFeatures.add("First Forbidden School: " + character.getForbiddenArcaneSchoolOne());
            if (character.getForbiddenArcaneSchoolTwo() == null) classFeatures.add("Second Forbidden School not chosen yet.");
            else classFeatures.add("Second Forbidden School: " + character.getForbiddenArcaneSchoolTwo());
        }
        
        return classFeatures;
    }
    
    private String appendAbilityScores(String text) {
        // Filling Ability Scores.
        strengthScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.STR));
        if (strengthScore.matches("0")) strengthScore = "Not chosen yet.";
        dexterityScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.DEX));
        if (dexterityScore.matches("0")) dexterityScore = "Not chosen yet.";
        constitutionScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.CON));
        if (constitutionScore.matches("0")) constitutionScore = "Not chosen yet.";
        intelligenceScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.INT));
        if (intelligenceScore.matches("0")) intelligenceScore = "Not chosen yet.";
        wisdomScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.WIS));
        if (wisdomScore.matches("0")) wisdomScore = "Not chosen yet.";
        charismaScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.CHA));
        if (charismaScore.matches("0")) charismaScore = "Not chosen yet.";
        
        text = text + "Ability Scores " + "\n";
        text = text + indentation + "STR: " + strengthScore + "\n";
        text = text + indentation + "DEX: " + dexterityScore + "\n";
        text = text + indentation + "CON: " + constitutionScore + "\n";
        text = text + indentation + "INT: " + intelligenceScore + "\n";
        text = text + indentation + "WIS: " + wisdomScore + "\n";
        text = text + indentation + "CHA: " + charismaScore + "\n";
        text = text + "\n";   
        
        return text;
    }
    
    private String appendSpells(String text) {
        if (character.getSpellList().isEmpty()) text = text + indentation + "Spells not selected yet.";
        for (Spell s : character.getSpellList()) text = text + indentation + s.getSpellName() + "\n";
        text = text + "\n" + "\n";
        return text;
    }
    
    private String appendFeats(String text) {
        if (character.getFeatList().isEmpty()) text = text + indentation + "Not chosen yet.";
        for (Feat f : character.getFeatList()) text = text + indentation + f.getFeatName() + "\n";
        text = text + "\n" + "\n";
        return text;
    }
}
