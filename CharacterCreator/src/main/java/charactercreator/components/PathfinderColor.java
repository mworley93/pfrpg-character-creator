/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Color;
/**
 * This enumeration contains a set of constants that define new color values commonly used in this application.
 * @author Megan Worley
 */
public enum PathfinderColor {
    DARK_BROWN (Color.getHSBColor(33, .67f, .17f)),
    BROWN (new Color(92/255.0f, 64/255.0f, 51/255.0f)),
    RICH_CRIMSON (Color.getHSBColor(40, .9f, .99f)),
    CRIMSON (Color.getHSBColor(33, .75f, .99f)),
    DARK_RED (Color.getHSBColor(33, .75f, .75f)),
    BEIGE (Color.getHSBColor(.17f, .31f, .97f)),
    DARK_TAN (new Color(151,105,79)),
    GOLD (Color.getHSBColor(.14f, .39f, .89f)),
    BROWN_HIGHLIGHT (Color.getHSBColor(33, .67f, .17f)),
    SAND (Color.getHSBColor(.14f, .1f, .97f)),
    DARK_SAND (new Color(212,181,126,110)),
    SHADOW (new Color(0, 0, 0, 50));
    
    private final Color color;
    
    PathfinderColor(Color color) {
        this.color = color;
    }
    
    public Color value() {
        return color;
    }
}
