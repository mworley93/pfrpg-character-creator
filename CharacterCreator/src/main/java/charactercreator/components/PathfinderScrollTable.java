/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.JTableHeader;
import java.awt.Color;
import java.awt.FontMetrics;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * This table is a custom JTable that is meant to reside on a JScrollPane.  NOTE: Using this table as a standalone component will result in a component that will not render
 * properly.  It will be transparent and the rows and header may be clipped.  
 * @author Megan Worley
 */
public class PathfinderScrollTable extends JTable {
    
    private final static int ROW_HEIGHT = 25;
    private final static int DEFAULT_COLUMN_MARGIN = 4;
    
    /**
     * Default constructor.
     * @param data The row/column data saved in a 2D array.
     * @param columnNames The header names (titles for each column).
     * @param size The preferred size of the table.
     */
    public PathfinderScrollTable(Object[][] data, String[] columnNames, Dimension size) {
        super(data, columnNames); 
        disableSelectionSettings();
        customizeHeader();
        customizeCellAppearance();
    }
    
    public PathfinderScrollTable(TableModel model, Dimension size) {
        super(model);
        customizeHeader();
        customizeCellAppearance();
    }
    
    /**
     * Disable the user's ability to change the text on each cell.
     * @param row The row of the specified cell.
     * @param column The column of the specified cell.
     * @return false
     */
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
    
    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component comp = super.prepareRenderer(renderer, row, column);
        int modelRow = convertRowIndexToModel(row);
        int modelColumn = convertColumnIndexToModel(column);
        if (!isRowSelected(modelRow)) {
            comp.setBackground(new Color(0, 0, 0, 0));
        }
        else {
            comp.setBackground(PathfinderColor.GOLD.value());
        }
        return comp;
    }
    
    /**
     * Disables the selection settings so the user cannot select or drag and drop columns/rows/cells.
     */
    private void disableSelectionSettings() {
        this.setRowSelectionAllowed(false);
        this.setColumnSelectionAllowed(false);
        this.setCellSelectionEnabled(false);
        this.setDragEnabled(false); 
    }
    
    /**
     * Customizes the appearance of the header.  
     */
    private void customizeHeader() {
        getTableHeader().setBackground(PathfinderColor.DARK_BROWN.value());
        getTableHeader().setForeground(PathfinderColor.GOLD.value());
        getTableHeader().setFont(new Font("Times New Roman", Font.BOLD, 16));
        getTableHeader().setReorderingAllowed(false);
    }
    
    /**
     * Customizes the appearance of the individual cells.  Adjusts the height of each row, sets the opacity, aligns the font in the center,
     * and sets the grid color.
     */
    private void customizeCellAppearance() {
        this.setFillsViewportHeight(true);
        this.setRowHeight(ROW_HEIGHT);
        this.setOpaque(false);
        this.setGridColor(PathfinderColor.DARK_BROWN.value());

        // Adjust text alignment by iterating through all of the cells and setting a custom renderer on them.
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        centerRenderer.setOpaque(false);
        for (int i = 0; i < this.getColumnCount(); i++) {
            this.getColumnModel().getColumn(i).setCellRenderer( centerRenderer );
        }
    }
    
    /**
     * Sizes the columns to fit using the default column margin.  See overridden function
     * for more details.
     */
    public void sizeColumnsToFitText() {
        sizeColumnsToFit(DEFAULT_COLUMN_MARGIN);
    }
    
    /**
     * Sizes the columns to fit the data string lengths, adding the provided column margin to the sides for extra space.
     * @param columnMargin The desired margin size on either size of the text in the columns.
     * Assistance from: http://stackoverflow.com/questions/5820238/how-to-resize-jtable-column-to-string-length
     */
    public void sizeColumnsToFitText(int columnMargin) {
        TableColumnModel colModel = (TableColumnModel)this.getColumnModel();
        
        for (int c = 0; c < this.getColumnCount(); c++) {
            TableColumn col = colModel.getColumn(c);
            int width = 0;

            // Get width of column header
            TableCellRenderer renderer = col.getHeaderRenderer();
            if (renderer == null) {
                renderer = this.getTableHeader().getDefaultRenderer();
            }
            java.awt.Component comp = renderer.getTableCellRendererComponent(
                this, col.getHeaderValue(), false, false, 0, 0);
            width = comp.getPreferredSize().width;

            // Get maximum width of column data
            for (int r = 0; r < this.getRowCount(); r++) {
                renderer = this.getCellRenderer(r, c);
                comp = renderer.getTableCellRendererComponent(
                    this, this.getValueAt(r, c), false, false, r, c);
                width = Math.max(width, comp.getPreferredSize().width);
            }

            // Add margin
            width += 2*columnMargin;

            // Set the width
            col.setPreferredWidth(width);
        }
    }
    
    
}