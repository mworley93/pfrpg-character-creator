/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Color;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * This class creates a new JButton that is comprised of a custom image.  It adds beveling, highlighting, and low-lighting for MouseEvents 
 * around its border.
 * @author Megan Worley
 */
public class ImageButton extends JButton implements MouseListener {
    
    BevelBorder pressedBorder;
    BevelBorder raisedBorder;
    EmptyBorder emptyBorder;
    Boolean paintDark;
    Boolean paintLight;
    Boolean enabled;
    
    /**
     * Main constructor.
     * @param text The text to show on the button.  Null if no text is desired.
     * @param icon The custom button image.
     */
    public ImageButton(String text, Icon icon) {
        super(text);
        if (text != null) 
            setText(text);
        setIcon(icon);
        initialize();
    }
    
    /**
     * Constructor.
     * @param text The text to display on the button, null if no text is desired.
     * @param path The file path containing the image resource.
     */
    public ImageButton(String text, String path) {
        super(text);
        if (text != null) 
            setText(text);
        ImageIcon icon = new ImageIcon(getClass().getResource(path));
        setIcon(icon);
        initialize();
    }
    
    /**
     * Initializes the custom settings for this button.  
     */
    private void initialize() {
        enabled = true;
        pressedBorder = (BevelBorder)BorderFactory.createLoweredBevelBorder();
        raisedBorder = (BevelBorder)BorderFactory.createRaisedBevelBorder();
        // The EmptyBorder values keep the button from jumping around when the bevel is added and removed.
        emptyBorder = (EmptyBorder)BorderFactory.createEmptyBorder(2, 2, 2, 2); 
        
        // Set the image to be the only thing displayed.
        setContentAreaFilled(false);
        setFocusPainted(false);
        setBorder(emptyBorder);
        
        // Specify that highlights and lowlights should not be applied until the button is touched/pressed.
        paintDark = false;
        paintLight = false;
        
        this.addMouseListener(this);
    }
    
    /**
     * Sets the button so the mouse events do not trigger.
     */
    public void disableMouseEvents() {
        enabled = false;
    }
    
    /**
     * Sets the button so the mouse events do not trigger.
     */
    public void enableMouseEvents() {
        enabled = true;
    }
    
    @Override
    public void mousePressed(MouseEvent event) {
        if (enabled) {
            this.setBorder(pressedBorder);
            paintDark = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        if (enabled) {
            this.setBorder(raisedBorder);
            paintDark = false;
        }
    }

    @Override
    public void mouseEntered(MouseEvent event) {
        if (enabled) {
            this.setBorder(raisedBorder);
            paintLight = true;
        }
    }

    @Override
    public void mouseExited(MouseEvent event) {
        if (enabled) {
            this.setBorder(emptyBorder);
            paintLight = false;
        }
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        if (enabled) {
            Component source = (Component)event.getSource();
            source.getParent().dispatchEvent(event);
        }
    }
    
    /**
     * Adds effects to the custom button if a lowlight or highlight is needed.
     * @param g The graphics component used for rendering.
     */
    @Override
    protected void paintComponent(Graphics g) {
        if (paintDark) {
            g.setColor(new Color(75, 45, 45, 150)); // Darken the area when pressed.
            Insets insets = getInsets();
            g.fillRect(insets.left, insets.top, 
                    getWidth() - insets.left - insets.right, 
                    getHeight() - insets.top - insets.bottom);
        }
        else if (paintLight) {
            g.setColor(new Color(245, 255, 255, 25)); // Very subtle highlight effect.
            Insets insets = getInsets();
            g.fillRect(insets.left, insets.top, 
                    getWidth() - insets.left - insets.right, 
                    getHeight() - insets.top - insets.bottom);
        }
        super.paintComponent(g); 
    }
}
