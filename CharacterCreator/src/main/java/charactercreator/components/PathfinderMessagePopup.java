/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import charactercreator.helpers.GUIHelper;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import charactercreator.views.BackgroundPanel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Megan
 */

public class PathfinderMessagePopup extends JDialog implements ActionListener {
    protected BackgroundPanel background;
    protected PathfinderButton okButton;
    protected TranslucentTextPane textPane;
    protected PathfinderLabel titleLabel;
    protected TranslucentScrollPane textScrollPane;
    protected GUIHelper guiMethods;
    protected Dimension size;
    protected String title;
    // Screen panels.
    protected JPanel titlePanel;
    protected JPanel middlePanel;
    protected JPanel buttonPanel;    
    
    // May possibly need 2-3 different constructors to account for different situations.
    public PathfinderMessagePopup(String title) {
        super(); // This calls the base class's constructor.  There are lots of different JDialog constructors where you can specify exiting behavior, titles, etc.
        guiMethods = new GUIHelper();
        size = new Dimension(600,600);
        this.title = title;
        initializeComponents();
        initializeLayout();
        this.setSize(600, 600);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
    }   
    
    public PathfinderMessagePopup(String title, int x, int y) {
        super(); // This calls the base class's constructor.  There are lots of different JDialog constructors where you can specify exiting behavior, titles, etc.
        guiMethods = new GUIHelper();
        size = new Dimension(x,y);
        this.title = title;
        initializeComponents();
        initializeLayout();
        this.setSize(x, y);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
    }
    
    private void initializeComponents() {
        // * TranslucentTextPane,/TranslucentScrollPane for displaying messages.
        // * Confirm button, to exit the dialog.
        titleLabel = new PathfinderLabel(title, 35);
        titleLabel.drawShadow(true);
        textPane = new TranslucentTextPane("", new Dimension(size.width-100, size.height-100), StyleConstants.ALIGN_LEFT);
        textPane.borderEnabled(false);
        textPane.setAlpha(0);
        textScrollPane = new TranslucentScrollPane(textPane, new Dimension(size.width-100,size.height-300));
        background = new BackgroundPanel("/images/ui_elements/backgroundTexture.jpg");
        
        initializeButtons();
        initializePanels();
        setComponentOrientation();
    }
    
        /**
     * Initializes the buttons found on this screen.  Sets their text as well as their maximum and minimum sizes so they can stretch and scale 
     * appropriately in the BoxLayout.
     */
    private void initializeButtons() {
        okButton = new PathfinderButton("Got it!");
        okButton.setMinimumSize(new Dimension(50, 25));
        okButton.setPreferredSize(new Dimension(50, 25));
        okButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));      
        
        okButton.addActionListener(this);        
    }
    
    /**
     * Initializes the subpanels on this screen and makes them completely transparent so the background can show through.
     */
    private void initializePanels() {
        titlePanel = new JPanel();
        titlePanel.setOpaque(false);
        middlePanel = new JPanel();
        middlePanel.setOpaque(false);
        buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);        
    }
    
    /**
     * Sets the components' orientation, which is super important for BoxLayout.  In any given subpanel, the X-component orientation
     * must be the same.  Some components on the screen are left aligned and some are center aligned, so they are broken up into 
     * groups.
     */
    private void setComponentOrientation() {
        // Title panel.
        titleLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        titleLabel.setAlignmentY(Component.LEFT_ALIGNMENT);
        
        // Middle panel.
        textScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        textScrollPane.setAlignmentY(Component.CENTER_ALIGNMENT);
        
        // Sets subpanel alignments.
        titlePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        middlePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titlePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        middlePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
    }         
    
    private void initializeLayout() {
        // A few subpanels needed to be used to get the component alignments to be correct.  All use BoxLayout.  Vertical and horizontal 
        // glue is used to dynamically space the components.
        
        // Title section.
        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));
        titlePanel.add(Box.createVerticalGlue());
        titlePanel.add(titleLabel);
        titlePanel.add(Box.createVerticalGlue());
        
        // Text section.
        middlePanel.add(Box.createVerticalGlue());
        middlePanel.add(textScrollPane);
        
        // Button section.
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(okButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        // Adds each section to this entire screen.
        background.setLayout(new BoxLayout(background, BoxLayout.Y_AXIS));
        background.add(titlePanel);
        background.add(middlePanel);
        background.add(Box.createVerticalGlue());
        background.add(buttonPanel);
        background.add(Box.createVerticalGlue());       
        
        this.add(background);
    }
    
    /**
     * Sets the text displayed on this popup.
     * @param str 
     */
    public void setString(String str) {
        this.textPane.setText(str);
        textPane.setCaretPosition(0);
    }
    
    /**
     * Adds a new component to the end of the message box (for example, an icon, table, etc..).
     * @param comp 
     */
    public void insertComponent(Component comp) {
        this.textPane.insertComponent(comp);
    }
    
    public void actionPerformed(ActionEvent e) {
        dispose();
    }   
}