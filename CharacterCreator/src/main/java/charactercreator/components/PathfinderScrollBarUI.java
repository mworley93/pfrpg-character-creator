/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 * This class defines a custom UI for the scroll bars in this app.  
 * @author Megan
 */
public class PathfinderScrollBarUI extends BasicScrollBarUI {
    
      // Create our own scrollbar UI!
  public static ComponentUI createUI( JComponent c ) {
    return new PathfinderScrollBarUI();
  }
    public PathfinderScrollBarUI() {
    }
    
    // Removes the decrease arrow button.
     @Override
    protected JButton createDecreaseButton(int orientation) {
        return createZeroButton();
    }

    // Removes the increase arrow button.
    @Override
    protected JButton createIncreaseButton(int orientation) {
        return createZeroButton();
    }
    
    // Paints the track that the scroll bar moves on.
    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        Graphics2D g2 = (Graphics2D)g;
        g2.setPaint(PathfinderColor.DARK_SAND.value());
        g2.fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
    }

    // Paints the actual scroll bar.
    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        Graphics2D g2 = (Graphics2D)g;
        g2.setPaint(PathfinderColor.BROWN.value());
        g2.fillRoundRect(thumbBounds.x + 2, thumbBounds.y + 2, thumbBounds.width - 4, thumbBounds.height - 2, 4, 4);
    }
    
    /**
     * Creates a button of zero width and height.
     * @return 
     */
    private JButton createZeroButton() {
            JButton jbutton = new JButton();
            jbutton.setPreferredSize(new Dimension(0, 0));
            jbutton.setMinimumSize(new Dimension(0, 0));
            jbutton.setMaximumSize(new Dimension(0, 0));
            return jbutton;
        }
}
