/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import charactercreator.views.BackgroundPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingWorker;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Megan Worley
 */
public class PathfinderPopup extends JDialog implements ActionListener {
    private BackgroundPanel background; // The background panel, which holds the background image and all of the components.
    private TranslucentTextPane message; // The message box that holds the text.
    private PathfinderButton okButton; // The button that closes the dialog.
    
    // Misc components for building the dialog.
    private String text;
    private JPanel titleBar;
    private JPanel bottomBar;
    private JPanel buttonArea;
    private JPanel extraPadding;
    private Dimension size;
    
    // Constants.
    private static final int BAR_HEIGHT = 30; // How many pixels high the menu bar should appear.
    
    /**
     * Default constructor.
     * @param titleNum The current tutorial section number.
     * @param text The message to display.
     * @param size The size of the box, dependent on the amount of text.
     */
    public PathfinderPopup(String text, int x, int y, JFrame parent) { 
        this.text = text;
        this.size = new Dimension(x, y);
        parent.addWindowListener(new MinimizeAdapter());
        parent.addComponentListener(new AttachedAdapter(parent, this));
        createComponents();
        addComponents();
        this.setUndecorated(true);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
    }
    
    /**
     * Creates the components that will be displayed on this JDialog.  Similar to the PathfinderPopup, they should consist of 
     * a background image, message box, and button.
     */
    private void createComponents() {
        background = new BackgroundPanel("/images/ui_elements/backgroundTexture.jpg");
        
        this.setMinimumSize(size);
        this.setPreferredSize(size);
        
        titleBar = new JPanel();
        titleBar.setBackground(PathfinderColor.DARK_BROWN.value());
        titleBar.setMaximumSize(new Dimension(Short.MAX_VALUE, BAR_HEIGHT));
        
        bottomBar = new JPanel();
        bottomBar.setOpaque(false);
        bottomBar.setMaximumSize(new Dimension(Short.MAX_VALUE, BAR_HEIGHT + 5));
        buttonArea = new JPanel();
        buttonArea.setOpaque(false);
        buttonArea.setMaximumSize(new Dimension(Short.MAX_VALUE, BAR_HEIGHT));
        extraPadding = new JPanel();
        extraPadding.setOpaque(false);
        extraPadding.setMaximumSize(new Dimension(Short.MAX_VALUE, 5));
        
        message = new TranslucentTextPane(text, new Dimension((int)size.getWidth(), (int)size.getHeight() - BAR_HEIGHT), StyleConstants.ALIGN_LEFT);
        message.setAlignmentX(Component.CENTER_ALIGNMENT);
        message.setLayout(new BorderLayout());
        
        okButton = new PathfinderButton("OK");
        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
    }
     
   /**
     * Adds the components to this JDialog using GroupLayout.  All components are added to the BackgroundPanel, which is then set on the JDialog.
     */
    private void addComponents() {
        // Adds the title bar and message area to the background.
        background.setLayout(new BoxLayout(background, BoxLayout.Y_AXIS));  // Specifies that the components are ordered from top to buttom.
        background.add(titleBar);
        background.add(message);
        
        // Formats the bottom area on the message box with the continue button.
        buttonArea.setLayout(new BoxLayout(buttonArea, BoxLayout.X_AXIS));
        buttonArea.add(Box.createHorizontalGlue());
        buttonArea.add(okButton);     
        buttonArea.add(Box.createRigidArea(new Dimension(10, 0)));
        bottomBar.setLayout(new BoxLayout(bottomBar, BoxLayout.Y_AXIS));
        bottomBar.add(buttonArea);
        bottomBar.add(extraPadding);
        message.add(bottomBar, BorderLayout.SOUTH);
        
        // Adds the background panel (and all of the components) to the JDialog.
        this.add(background);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        dispose();
    }
    
        /**
     * This private class starts another thread that tells the tutorial dialog to wait until the main window has maximized to reappear.
     */
    private class MinimizeAdapter extends WindowAdapter {
            @Override
            public void windowIconified(WindowEvent e){
                  PathfinderPopup.this.setVisible(false);
            }
            
            @Override
            public void windowDeiconified(WindowEvent e){
                MaximizeWait process = new MaximizeWait();
                process.execute();
            }
    };
    
    /**
     * This background thread waits for a set number of seconds.  It sets the dialog to reappear after the main window has maximized.  Without this waiting,
     * the dialog will pop up prematurely when the main window is minimized and maximized.
     */
    private class MaximizeWait extends SwingWorker<Void, String> {
        public MaximizeWait() {
        }

        @Override
        protected Void doInBackground() throws Exception {
            try {
                Thread.sleep(250);
            }
            catch (Exception ex) {

            }
            return (null);
        }

        @Override
        protected void done() {
            super.done();
            PathfinderPopup.this.setVisible(true);
        }
    }
    
      /**
     * This private class is used to keep the JDialog attached to the parent window it hovers over.  It should 
     * move with the parent window when it is moved.
     * Assistance from: http://www.devx.com/tips/Tip/30528
     */
    private class AttachedAdapter extends ComponentAdapter{
        private Window winA, winB;
        
        public AttachedAdapter(JFrame winA, JDialog winB){
            this.winA = winA;
            this.winB = winB;
        }
        
        @Override
        public void componentMoved(ComponentEvent e) {
            Window win = (Window) e.getComponent();
            if(win==winA){
                winB.removeComponentListener(this);
                winB.setLocationRelativeTo(winA);
                winB.addComponentListener(this);
            }            
        }
    };
}

