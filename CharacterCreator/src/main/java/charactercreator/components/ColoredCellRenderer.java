/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * This class is a ListCellRenderer used to customize the colors on a Pathfinder component.  
 * Assistance from: http://stackoverflow.com/questions/9171258/colored-jcombobox-with-colored-items-and-focus
 */
public class ColoredCellRenderer implements ListCellRenderer {
    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
    private final Color selectionBackground = PathfinderColor.DARK_RED.value();
    private final Color selectionForeground = PathfinderColor.GOLD.value();

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        // Sets the colors of a selection.
        list.setSelectionForeground(selectionForeground);
        list.setSelectionBackground(selectionBackground);
        
        // Adjusts the sizing of a cell with an empty string.
        if (value instanceof String && ((String)value).isEmpty()) {
            value = " ";
        }
        
        JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(
            list, value, index, isSelected, cellHasFocus);
        
        
        return renderer;
    }
}
