/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Color;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GradientPaint;
import java.awt.Point;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 * This class is the standard button for this application.  It is a modified JButton with custom colors, highlighting, low-lighting, etc, 
 * that match the Pathfinder theme.
 * @author Megan
 */
public class PathfinderButton extends JButton implements MouseListener {
    
    private CompoundBorder pressedBorder;
    private CompoundBorder raisedBorder;
    private EmptyBorder emptySpace;
    private LineBorder line;
    private Boolean paintPressed;
    private Boolean paintHover;
    private boolean enabled;
    
    /**
     * Main constructor.
     * @param text The text to display on this button.
     */
    public PathfinderButton(String text) {
        super(text);
        initialize(); 
    }
    
    /**
     * Initializes this button with custom colors and a border.
     */
    private void initialize() {
        // Set the text color and font.
        this.setForeground(PathfinderColor.GOLD.value());
        this.setFont(new Font("Times New Roman", Font.BOLD, 14));
        
        // Set borders.  The JButton border needs to have three parts: the bevel, the visible line, the margin between it and the text.
        emptySpace = (EmptyBorder)BorderFactory.createEmptyBorder(0, 12, 0, 12);
        line = (LineBorder)BorderFactory.createLineBorder(Color.getHSBColor(33f, .67f, .17f), 2); 
        BevelBorder pressedBevel = (BevelBorder)BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.getHSBColor(.17f, .1f, .65f), PathfinderColor.BROWN_HIGHLIGHT.value());
        BevelBorder raisedBevel = (BevelBorder)BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.getHSBColor(.17f, .1f, .65f), PathfinderColor.BROWN_HIGHLIGHT.value());
        CompoundBorder raisedVisibleBorder = (CompoundBorder)BorderFactory.createCompoundBorder(raisedBevel, line);
        CompoundBorder pressedVisibleBorder = (CompoundBorder)BorderFactory.createCompoundBorder(pressedBevel, line);
        raisedBorder = (CompoundBorder)BorderFactory.createCompoundBorder(raisedVisibleBorder, emptySpace);
        pressedBorder = (CompoundBorder)BorderFactory.createCompoundBorder(pressedVisibleBorder, emptySpace);
        this.setBorder(raisedBorder);   
        
        // Remove the default focus and button highlighting.
        this.setContentAreaFilled(false);
        this.setFocusPainted(false);    
        
        // Paint the button normally when it is not interacted with.
        paintPressed = false;
        paintHover = false;
        enabled = true;
        
        this.addMouseListener(this);
    }
    
    /**
     * Sets the button so the mouse events do not trigger.
     */
    public void disableMouseEvents() {
        enabled = false;
    }
    
    /**
     * Sets the button so the mouse events do not trigger.
     */
    public void enableMouseEvents() {
        enabled = true;
    }
    
    @Override
    public void mousePressed(MouseEvent event) {
        if (enabled) {
            paintPressed = true;
            this.setBorder(pressedBorder);
        }
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        if (enabled) {
            paintPressed = false;
            this.setBorder(raisedBorder);
        }
    }

    @Override
    public void mouseEntered(MouseEvent event) {
        if (enabled) {
            paintHover = true;
            this.setBorder(raisedBorder);
        }
    }

    @Override
    public void mouseExited(MouseEvent event) {
        if (enabled) {
            paintHover = false;
            this.setBorder(raisedBorder);
        }
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        Component source = (Component)event.getSource();
        source.getParent().dispatchEvent(event);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D)g.create();
        
        if (paintPressed) { // Reverse the gradiant when the button is pressed.
            g2.setPaint(new GradientPaint(new Point(0, 0),PathfinderColor.DARK_BROWN.value(), new Point(0, getHeight()), PathfinderColor.CRIMSON.value()));
        }
        else if (paintHover) { // Paint a richer crimson color when the button is hovered over.
            g2.setPaint(new GradientPaint(new Point(0, 0),PathfinderColor.RICH_CRIMSON.value(), new Point(0, getHeight()), PathfinderColor.DARK_BROWN.value()));  
        }
        else {
            g2.setPaint(new GradientPaint(new Point(0, 0),PathfinderColor.CRIMSON.value(), new Point(0, getHeight()), PathfinderColor.DARK_BROWN.value()));
        }

        g2.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    }
}
