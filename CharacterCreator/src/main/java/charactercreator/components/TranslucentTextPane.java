/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;
import javax.swing.BorderFactory;
import javax.swing.border.LineBorder;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;


/**
 * This custom JTextPane acts as a translucent text area with the option for a border.
 * @author Megan Worley, Brandon Sharp
 */
public class TranslucentTextPane extends JTextPane {
    private int alpha;
    private boolean hasBorder;
    
    /**
     * Default constructor.
     */
    public TranslucentTextPane() {
        initializeGraphics();
    }
    
    /**
     * Constructor.
     * @param topic  The string containing the text to be displayed.
     * @param size  The preferred size of this component.
     * @param alignment The text alignment, e.g. StyleConstants.ALIGN_CENTER.
     */
    public TranslucentTextPane(String topic, Dimension size, int alignment) {
        StyleContext context = new StyleContext();
        StyledDocument document = new DefaultStyledDocument(context);
        Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setAlignment(style, alignment);
        this.setDocument(document);
        this.setText(topic);
        this.setFont(new Font("Times New Roman", Font.BOLD, 15));
        this.setMaximumSize(size);
        this.setMinimumSize(size);
        this.setPreferredSize(size);
        initializeGraphics();
    }
    
    /**
     * Constructor.
     * @param document The StyledDocument containing the custom styled text.
     * @param size  The preferred size of this component.
     */
    public TranslucentTextPane(StyledDocument document, Dimension size) {
        super(document);
        this.setMaximumSize(size);
        this.setMinimumSize(size);
        this.setPreferredSize(size);
        initializeGraphics();
    }
    
    /**
     * Initializes the visual elements for this custom component.
     */
    private void initializeGraphics() {
        alpha = 125;
        setEditable(false);
        setOpaque(false);
        createBorder();
    }
    
    /**
     * Creates the border of this custom JTextPane.  Recommended settings: LineBorder, Dark Brown, 3 px thickness.
     */
    private void createBorder() {
        LineBorder border = (LineBorder)BorderFactory.createLineBorder(Color.getHSBColor(32, .65f, .19f), 3);
        EmptyBorder empty = (EmptyBorder)BorderFactory.createEmptyBorder(0, 5, 0, 5); 
        CompoundBorder compoundBorder = (CompoundBorder)BorderFactory.createCompoundBorder(border, empty);
        this.setBorder(compoundBorder);
        hasBorder = true;
    }
    
    public void borderEnabled(boolean border) {
        if (!border) {
            this.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
            hasBorder = false;
        }
        else {
            createBorder();
        }
    }
    
    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    /**
     * Paints the component with a custom RGB and alpha value.  
     * Source: http://stackoverflow.com/questions/4982960/java-swing-translucent-components
     * @param g The graphics to render.
     */
    @Override
    public void paintComponent(Graphics g) {
        g.setColor(new Color(245, 255, 255, alpha));
        Insets insets = getInsets();
        if (hasBorder) {
            insets.set(insets.top, insets.left - 5, insets.bottom, insets.right - 5);
        }
        g.fillRect(insets.left, insets.top, 
                getWidth() - insets.left - insets.right, 
                getHeight() - insets.top - insets.bottom);
        super.paintComponent(g);
    }
}

