/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.border.LineBorder;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 * Creates a custom translucent JScrollPane with a border.
 * @author Megan Worley
 */
public class TranslucentScrollPane extends JScrollPane {
    
    /**
     * Default constructor.
     */
    public TranslucentScrollPane() {
        setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        initializeGraphics();
    }
    
    /**
     * Constructor.
     * @param size The preferred size of this component.
     */
    public TranslucentScrollPane(JComponent component, Dimension size) {
        super(component);
        setMaximumSize(size);
        setMinimumSize(size);
        setPreferredSize(size);
        setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        initializeGraphics();
    }
    
    /**
     * Initializes the graphical elements for this custom JScrollPane.  
     */
    private void initializeGraphics() {
        setOpaque(false);
        getViewport().setOpaque(false);
        createBorder();
    }
    
    /**
     * Creates the border of this ScrollPane.  Recommended settings: LineBorder, Dark Brown, 3 px thickness.
     */
    private void createBorder() {
        LineBorder border = (LineBorder)BorderFactory.createLineBorder(Color.getHSBColor(32, .65f, .19f), 3);
        this.setBorder(border);
    }

    /**
     * Paints the component with a custom RGB and alpha value.  
     * Source: http://stackoverflow.com/questions/4982960/java-swing-translucent-components
     */
    @Override
    public void paintComponent(Graphics g) {
        g.setColor(new Color(245, 255, 255, 125));
        Insets insets = getInsets();
        g.fillRect(insets.left, insets.top, 
                getWidth() - insets.left - insets.right, 
                getHeight() - insets.top - insets.bottom);
        super.paintComponent(g);
    }
    
}
