/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.util.Iterator;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 * This is a custom JComboBox with a Pathfinder color scheme that is in-theme with this application.
 * @author Megan Worley
 */
public class PathfinderComboBox extends JComboBox {
    
    /**
     * Default constructor.
     * @param options The String array containing the options.
     * @param size The max size of the component.
     */
    public PathfinderComboBox(String[] options, Dimension size) {
        for (int i = 0; i < options.length; i++) {
            this.addItem(options[i]);
        }
        initializeLook(size);
    }
    
    /**
     * Constructor.
     * @param options An ArrayList containing the options.
     * @param size The max size of the component.
     */
    public PathfinderComboBox(ArrayList options, Dimension size) {
        while (options.iterator().hasNext()) {
            this.addItem((String)options.iterator().next());
        }
        initializeLook(size);
    }
    
    /**
     * Initializes the custom size and colors of this PathfinderComboBox.
     * @param size The max size desired.
     */
    private void initializeLook(Dimension size) {
        this.setMaximumSize(size);
        this.setBackground(PathfinderColor.SAND.value());
        this.setForeground(PathfinderColor.DARK_BROWN.value());
        ColoredCellRenderer renderer = new ColoredCellRenderer();
        this.setRenderer(renderer);
        this.setFont(new Font("Times New Roman", Font.BOLD, 14));
    }

    
    /**
     * Replaces the ComboBox's available choices with a new list of choices.
     * @param newList The desired list of items.
     * @param currentSelection The item that should be currently selected.
     */
    public void updateSelectionList(ArrayList<String> newList, String currentSelection) {
        this.removeAllItems();
        Iterator<String> iter = newList.iterator();
        while (iter.hasNext()) {
            this.addItem(iter.next());
        }
        this.setSelectedItem(currentSelection);
    }
    
}
