/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import charactercreator.helpers.PDFCreator;
import charactercreator.models.BaseCharacter;
import charactercreator.views.BackgroundPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.LineBorder;

/**
 * This custom dialog is used to show the user their progress while saving their character sheet.
 * @author Megan Worley
 */
public class PathfinderDocumentPopup extends JDialog implements ActionListener {
    private BackgroundPanel background;
    
    private JPanel progressPanel;
    private JPanel progressLabelPanel;
    private JPanel progressBarPanel;
    
    private JPanel completionPanel;
    private JPanel completionLabelPanel;
    private JPanel completionButtonPanel;
    
    private JPanel errorPanel;
    private JPanel errorLabelPanel;
    private JPanel errorButtonPanel;
    
    private String message1;
    private String message2;
    private String message3;
    private String message4;
    private static final String errorMessage1 = "Sorry!  An error seems to have occurred.";
    private static final String errorMessage2 = "Please try saving your character sheet again.";
    
    private PathfinderLabel waitLabel;
    private PathfinderLabel minuteLabel;
    private JProgressBar progressBar;
    private PathfinderButton cancelButton;
    
    private PathfinderLabel completionLabel;
    private PathfinderLabel openLabel;
    private PathfinderButton okButton;
    private PathfinderButton openButton;
    
    private PathfinderLabel errorLabel;
    private PathfinderLabel errorSubLabel;
    private PathfinderButton errorButton;
    
    private PDFTask task;
    private BaseCharacter character;
    private String saveDest;
    
    /**
     * @param message The messages to display, or null if the default message is desired.
     */
    public PathfinderDocumentPopup(JFrame parent, BaseCharacter character, String saveDest) {
        super(parent);
        initializeComponents();
        this.setSize(new Dimension(500, 200));
        this.setResizable(false);
        this.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.character = character;
        this.saveDest = saveDest;
        task = new PDFTask(character, saveDest);
        task.execute();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                runEDT();
            }
        });
    }
    
    private void initializeComponents() {
        createProgressBar();
        setMessages();
        initializeLabels();
        initializeButtons();
        initializePanels();
        setComponentOrientation();
        initializeLayout();
    }
    
    private void createProgressBar() {
        progressBar = new JProgressBar();
        progressBar.setUI(new PathfinderProgressBarUI());
        progressBar.setBackground(PathfinderColor.BEIGE.value());
        progressBar.setBorder((LineBorder)BorderFactory.createLineBorder(Color.getHSBColor(32, .65f, .19f), 3));
        progressBar.setStringPainted(true);
        progressBar.setPreferredSize(new Dimension(300, 25));
        progressBar.setIndeterminate(true);
    }
    
    private void setMessages() {
        message1 = "Please wait while your character sheet is created.";
        message2 = "This could take several minutes.";
        message3 = "Complete! Thank you for using the Character Creator.";
        message4 = "Click 'Open PDF' to view your character sheet now.";
    }
    
    private void initializeLabels() {
        waitLabel = new PathfinderLabel(message1, 20);
        minuteLabel = new PathfinderLabel(message2, 18);
        completionLabel = new PathfinderLabel(message3, 20);
        openLabel = new PathfinderLabel(message4, 18);
        errorLabel = new PathfinderLabel(errorMessage1, 20);
        errorSubLabel = new PathfinderLabel(errorMessage2, 18);
    }
    
        /**
     * Initializes the buttons found on this screen.  Sets their text as well as their maximum and minimum sizes so they can stretch and scale 
     * appropriately in the BoxLayout.
     */
    private void initializeButtons() {
        cancelButton = new PathfinderButton("Cancel");
        cancelButton.setMinimumSize(new Dimension(75, 25));
        cancelButton.setPreferredSize(new Dimension(75, 25));
        cancelButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));      
        cancelButton.addActionListener(this);  
        cancelButton.setActionCommand("Cancel");
        
        okButton = new PathfinderButton("Close");
        okButton.setMinimumSize(new Dimension(100, 25));
        okButton.setPreferredSize(new Dimension(100, 25));
        okButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));      
        okButton.addActionListener(this); 
        okButton.setActionCommand("Close");
        
        openButton = new PathfinderButton("Open PDF");
        openButton.setMinimumSize(new Dimension(100, 25));
        openButton.setPreferredSize(new Dimension(100, 25));
        openButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));      
        openButton.addActionListener(this);  
        openButton.setActionCommand("Open PDF");
        
        errorButton = new PathfinderButton("Close");
        errorButton.setMinimumSize(new Dimension(75, 25));
        errorButton.setPreferredSize(new Dimension(75, 25));
        errorButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));      
        errorButton.addActionListener(this);  
        errorButton.setActionCommand("Close");
    }
    
    /**
     * Initializes the subpanels on this screen and makes them completely transparent so the background can show through.
     */
    private void initializePanels() {
        background = new BackgroundPanel("/images/ui_elements/backgroundTexture.jpg");
        progressPanel = new JPanel();
        progressPanel.setOpaque(false);
        progressLabelPanel = new JPanel();
        progressLabelPanel.setOpaque(false);
        progressBarPanel = new JPanel();
        progressBarPanel.setOpaque(false);
        completionPanel = new JPanel();
        completionPanel.setOpaque(false);
        completionLabelPanel = new JPanel(); 
        completionLabelPanel.setOpaque(false);
        completionButtonPanel = new JPanel();
        completionButtonPanel.setOpaque(false);
        errorPanel = new JPanel();
        errorPanel.setOpaque(false);
        errorLabelPanel = new JPanel();
        errorLabelPanel.setOpaque(false);
        errorButtonPanel = new JPanel();
        errorButtonPanel.setOpaque(false);
    }
    
    /**
     * Sets the components' orientation, which is super important for BoxLayout.  In any given subpanel, the X-component orientation
     * must be the same.  Some components on the screen are left aligned and some are center aligned, so they are broken up into 
     * groups.
     */
    private void setComponentOrientation() {
        // Labels.
        waitLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        waitLabel.setAlignmentY(Component.LEFT_ALIGNMENT);
        minuteLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        minuteLabel.setAlignmentY(Component.LEFT_ALIGNMENT);
        completionLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        completionLabel.setAlignmentY(Component.LEFT_ALIGNMENT);
        openLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        openLabel.setAlignmentY(Component.LEFT_ALIGNMENT);
        errorLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        errorLabel.setAlignmentY(Component.LEFT_ALIGNMENT);
        errorSubLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        errorSubLabel.setAlignmentY(Component.LEFT_ALIGNMENT); 
        
        // Progress bar. 
        progressBar.setAlignmentX(Component.CENTER_ALIGNMENT);
        progressBar.setAlignmentY(Component.CENTER_ALIGNMENT);
        
        // Sets subpanel alignments.
        progressLabelPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        progressLabelPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        progressBarPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        progressBarPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        completionLabelPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        completionLabelPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        completionButtonPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        completionButtonPanel.setAlignmentY(Component.LEFT_ALIGNMENT);
        errorLabelPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        errorLabelPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        errorButtonPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        errorButtonPanel.setAlignmentY(Component.LEFT_ALIGNMENT);
    }         
    
    private void initializeLayout() {
        // A few subpanels needed to be used to get the component alignments to be correct.  All use BoxLayout.  Vertical and horizontal 
        // glue is used to dynamically space the components.
        initializeProgressPanelLayout();
        initializeCompletionPanelLayout();
        initializeErrorPanelLayout();
        
        background.setLayout(new BoxLayout(background, BoxLayout.Y_AXIS));
        background.add(progressPanel);
        this.add(background);
    }
    
    private void initializeProgressPanelLayout() {
        progressLabelPanel.setLayout(new BoxLayout(progressLabelPanel, BoxLayout.Y_AXIS));
        progressLabelPanel.add(Box.createVerticalGlue());
        progressLabelPanel.add(waitLabel);
        progressLabelPanel.add(minuteLabel);
        progressLabelPanel.add(Box.createVerticalGlue());
        
        // Text section.
        progressBarPanel.add(progressBar);
        progressBarPanel.add(Box.createHorizontalGlue());
        progressBarPanel.add(cancelButton);
        
        // Adds each section to this entire screen.
        progressPanel.setLayout(new BoxLayout(progressPanel, BoxLayout.Y_AXIS));
        progressPanel.add(progressLabelPanel);
        progressPanel.add(progressBarPanel);
    }
    
    private void initializeCompletionPanelLayout() {
        completionLabelPanel.setLayout(new BoxLayout(completionLabelPanel, BoxLayout.Y_AXIS));
        completionLabelPanel.add(Box.createVerticalGlue());
        completionLabelPanel.add(completionLabel);
        completionLabelPanel.add(openLabel);
        completionLabel.add(Box.createVerticalGlue());
        
        completionButtonPanel.add(Box.createHorizontalGlue());
        completionButtonPanel.add(okButton);
        completionButtonPanel.add(Box.createHorizontalGlue());
        completionButtonPanel.add(openButton);
        
        // Adds each section to this entire screen.
        completionPanel.setLayout(new BoxLayout(completionPanel, BoxLayout.Y_AXIS));
        completionPanel.add(completionLabelPanel);
        completionPanel.add(Box.createVerticalGlue());
        completionPanel.add(completionButtonPanel);
    }
    
    private void initializeErrorPanelLayout() {
        errorLabelPanel.setLayout(new BoxLayout(errorLabelPanel, BoxLayout.Y_AXIS));
        errorLabelPanel.add(Box.createVerticalGlue());
        errorLabelPanel.add(errorLabel);
        errorLabelPanel.add(errorSubLabel);
        errorLabelPanel.add(Box.createVerticalGlue());
        
        errorButtonPanel.add(Box.createHorizontalGlue());
        errorButtonPanel.add(errorButton);
        
        // Adds each section to this entire screen.
        errorPanel.setLayout(new BoxLayout(errorPanel, BoxLayout.Y_AXIS));
        errorPanel.add(errorLabelPanel);
        errorPanel.add(errorButtonPanel);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().matches("Cancel")) {
            task.cancel(true);
            closeAndDispose();
        }
        else if (e.getActionCommand().matches("Close")) {
            closeAndDispose();
        }
        else {
            System.out.println("Saving");
            try {
                File document = new File(saveDest);
                Desktop desktop = Desktop.getDesktop();
                desktop.open(document);
            }
            catch (Exception ex) {
                System.out.println("saving ex");
                closeAndDispose();
            }
        }
    }  
    
    private void closeAndDispose() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                System.out.println("Close and dispose hit");
                PathfinderDocumentPopup.this.setVisible(false);
                PathfinderDocumentPopup.this.dispose();
            }
        });
    }
    
    /**
     * This method runs the task that fills the PDF and updates the progress bar.
     */
    private void runEDT() {
        System.out.println("runEDT hit");
        this.setVisible(true);
    }
    
    /**
     * This worker thread is used to fill and save the PDF in the background while a 
     * progress bar is updated incrementally.  Upon completion, it switches the dialog 
     * screen to notify the user that the task is complete.
     */
    class PDFTask extends SwingWorker<Void, Object> implements PropertyChangeListener {
        private boolean done;
        private boolean error;
        private BaseCharacter character;
        private String saveDest;
        
        PDFTask(BaseCharacter character, String saveDest) {
            done = false;
            error = false;
            this.character = character;
            this.saveDest = saveDest;
            this.addPropertyChangeListener(this);
        }
        
        @Override
        public Void doInBackground() {   
            PDFCreator pdfCreator = new PDFCreator();
            try {
                    pdfCreator.loadCharacterSheet();
                    pdfCreator.fillCharacterSheet(character);
                    File outFile = new File(saveDest);
                    pdfCreator.savePDF(outFile);
            }
            catch (Exception e) {
                error = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void done() {
            try {
                done = true;
                if (!error) {
                    // Switches the view to the completion message on the dialog.
                    background.removeAll();
                    background.add(completionPanel);
                    PathfinderDocumentPopup.this.revalidate();
                }
                else {
                    background.removeAll();
                    background.add(errorPanel);
                    PathfinderDocumentPopup.this.revalidate();
                }
            } 
            catch (Exception ignore) {
                ignore.printStackTrace();
            }
        }    
        
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            // Shows an indeterminate progress bar unless progress is being made, in 
            // which case the percentage is shown.
            if (!done) {
                int progress = this.getProgress();
                if (progress == 0) {
                    progressBar.setIndeterminate(true);
                } 
                else {
                    progressBar.setIndeterminate(false); 
                    progressBar.setString(null);
                    progressBar.setValue(progress);
                }
            }
        }
    }
    
}
