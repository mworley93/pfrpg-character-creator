/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Font;
import java.awt.font.GlyphVector;
import java.awt.Shape;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import java.awt.BasicStroke;
import javax.swing.JLabel;

/**
 * Creates a JLabel with a custom, in-theme font.  
 * @author Megan Worley, Brandon Sharp
 */
public class PathfinderLabel extends JLabel {
    
    private GlyphVector glyphVector;
    private Shape shape;
    private Dimension bounds;
    private int outerStroke;
    private int innerStroke;
    private Boolean shadow;
    private static final int PADDING_Y = 1; // Adds 1 pixel of padding to the y-drawing space to avoid clipping when rendering.
    private static final int PADDING_X = 2; // Adds 2 pixels of padding to the x-drawing space to avoid clipping when rendering drop shadows.
    
    /**
     * Default constructor.
     * @param string The text to display.  NOTE: Recommended not to exceed 20 unless you're making a title!  The labels become too large to manage.
     */
    public PathfinderLabel(String string) {
        super(string);
        this.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        constructTextShape();
    }
    
    /**
     * Constructor.
     * @param string The text to display.
     * @param fontSize The size of the font. NOTE: Recommended not to exceed 20 unless you're making a title!  The labels become too large to manage.
     */
    public PathfinderLabel(String string, int fontSize) {
        super(string);
        this.setFont(new Font("Times New Roman", Font.PLAIN, fontSize));
        constructTextShape();
    }
    
    /**
     * Constructor.
     * @param string The text to display.
     * @param font The type of font to use. NOTE: Recommended not to exceed a font size of 20 unless you're making a title!  The labels become too 
     *             large to manage.
     */
    public PathfinderLabel(String string, Font font) {
        super(string);
        this.setFont(font);
        constructTextShape();
    }
    
    /**
     * Gets the shape of the text assigned to this JLabel and calculates the size of this JLabel based on it.
     */
    private void constructTextShape() {
        glyphVector = getFont().createGlyphVector(getFontMetrics(getFont()).getFontRenderContext(), getText());
        Rectangle2D glyphRect = glyphVector.getVisualBounds();
        shape = glyphVector.getOutline();
        
        // Scale the stroke sizes based on the font size.
        outerStroke = (int)Math.ceil(getFont().getSize() * 0.25);
        innerStroke = (int)(getFont().getSize() * 0.05 - 1);
        
        // The size is calculated based on the width shape representing this text and the
        // extra area that the outer stroke occupies.  The height is too variable to rely on for whatever reason.
        bounds = new Dimension((int)glyphRect.getWidth() + outerStroke * 2 + PADDING_X, (int)outerStroke * 4 + PADDING_Y);
        Rectangle boundingRect = new Rectangle(bounds);
        this.setBounds(boundingRect);
        this.setPreferredSize(bounds);
        this.setMinimumSize(bounds);
        this.setMaximumSize(bounds);
        shadow = false;
    }   
    
    public void drawShadow(Boolean isShadowVisible) {
        shadow = isShadowVisible;
        repaint();
    }
    
    /**
     * Paints the JLabel with a custom text that has an outline and varying colors.
     * @param g The graphics component used for rendering.
     */
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D)g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);  // Visually "smooths" curves by blending pixels.
        
        // Draw the drop shadow with the same size and softness as the regular text.
        if (shadow) {
            // Magic numbers, I know.  Drop shadow distance should be about half of the font size, I think.  I hope.
            g2.translate(outerStroke + getFont().getSize() / 5.5, outerStroke * 3 + PADDING_Y + getFont().getSize() / 5.5);
            g2.setPaint(PathfinderColor.SHADOW.value());
            g2.setStroke(new BasicStroke(outerStroke / 2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
            g2.fill(shape);
            g2.draw(shape);
            // Refresh the graphics component.
            g2.dispose();
            g2 = (Graphics2D)g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        
        g2.translate(outerStroke, outerStroke * 3 + PADDING_Y); // Adjust the drawing space to account for the stroke and font size.  
        
        // Had to pull a trick to get the font to look the way I wanted.  First, a very large outer stroke is drawn, which accounts for the
        // dark brown outline on the font.  Then the paint is changed to the light beige color and the shape is filled with it at its 
        // normal size.  The inner stroke adds padding to the lighter color inside to account for scaling font sizes.  
        g2.setPaint(PathfinderColor.DARK_BROWN.value());
        g2.setStroke(new BasicStroke(outerStroke, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2.draw(shape);     
        g2.setPaint(PathfinderColor.BEIGE.value());
        g2.fill(shape);
        g2.setStroke(new BasicStroke(innerStroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
        g2.draw(shape);
    } 
}
