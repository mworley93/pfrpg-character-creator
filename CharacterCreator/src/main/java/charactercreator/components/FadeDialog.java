/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import charactercreator.views.BackgroundPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Graphics;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.text.StyleConstants;
import javax.swing.SwingWorker;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 *
 * @author Megan Worley
 */
public class FadeDialog extends JDialog  {
    private float alpha; // The alpha value (opacity) of the window.
    private float increment = 0.05f; // The amount to change the opacity by on each cycle.  Can be positive or negative.
    private BackgroundPanel background; // The background panel, which holds the background image and all of the components.
    private TranslucentTextPane message; // The message box that holds the text.
    private PathfinderButton continueButton; // The button that advances to the next tutorial phase.
    
    // Misc components for building the FadeDialog.
    private JLabel titleString;
    private JPanel titleBar;
    private JPanel bottomBar;
    private JPanel buttonArea;
    private JPanel extraPadding;
    private Dimension size;
    
    // Constants.
    private static final int BAR_HEIGHT = 30; // How many pixels high the menu bar should appear.
    
    /**
     * Default constructor.
     * @param titleNum The current tutorial section number.
     * @param text The message to display.
     * @param size The size of the box, dependent on the amount of text.
     */
    public FadeDialog(Dimension size, JFrame parent) { 
        super(parent, false);
        this.size = size;
        createComponents();
        addComponents();
        this.setUndecorated(true);
        this.setOpacity(0);
        this.setVisible(true);
    }
    
    /**
     * Creates the components that will be displayed on this JDialog.  Similar to the PathfinderPopup, they should consist of 
     * a background image, message box, and button.
     */
    private void createComponents() {
        background = new BackgroundPanel("/images/ui_elements/backgroundTexture.jpg");
        
        this.setMinimumSize(size);
        this.setPreferredSize(size);
        
        titleString = new JLabel();
        titleString.setForeground(PathfinderColor.GOLD.value());
        
        titleBar = new JPanel();
        titleBar.setBackground(PathfinderColor.DARK_BROWN.value());
        titleBar.setMaximumSize(new Dimension(Short.MAX_VALUE, BAR_HEIGHT));
        
        bottomBar = new JPanel();
        bottomBar.setOpaque(false);
        bottomBar.setMaximumSize(new Dimension(Short.MAX_VALUE, BAR_HEIGHT + 5));
        buttonArea = new JPanel();
        buttonArea.setOpaque(false);
        buttonArea.setMaximumSize(new Dimension(Short.MAX_VALUE, BAR_HEIGHT));
        extraPadding = new JPanel();
        extraPadding.setOpaque(false);
        extraPadding.setMaximumSize(new Dimension(Short.MAX_VALUE, 5));
        
        message = new TranslucentTextPane("", new Dimension((int)size.getWidth(), (int)size.getHeight() - BAR_HEIGHT), StyleConstants.ALIGN_LEFT);
        message.setAlignmentX(Component.CENTER_ALIGNMENT);
        message.setLayout(new BorderLayout());
        
        continueButton = new PathfinderButton("Continue");
        continueButton.setActionCommand("Continue");
        continueButton.addActionListener(buttonClick);
    }
    
    /**
     * Adds the components to this JDialog using GroupLayout.  All components are added to the BackgroundPanel, which is then set on the JDialog.
     */
    private void addComponents() {
        // Adds the title bar and message area to the background.
        titleBar.add(titleString);
        background.setLayout(new BoxLayout(background, BoxLayout.Y_AXIS));  // Specifies that the components are ordered from top to buttom.
        background.add(titleBar);
        background.add(message);
        
        // Formats the bottom area on the message box with the continue button.
        buttonArea.setLayout(new BoxLayout(buttonArea, BoxLayout.X_AXIS));
        buttonArea.add(Box.createHorizontalGlue());
        buttonArea.add(continueButton);     
        buttonArea.add(Box.createRigidArea(new Dimension(10, 0)));
        bottomBar.setLayout(new BoxLayout(bottomBar, BoxLayout.Y_AXIS));
        bottomBar.add(buttonArea);
        bottomBar.add(extraPadding);
        message.add(bottomBar, BorderLayout.SOUTH);
        
        // Adds the background panel (and all of the components) to the JDialog.
        this.add(background);
    }
    
    /**
     * Sets the title and message of this dialog.
     * @param num The current stage number.
     * @param title The title of the dialog.
     */
    public void setDetails(String title, String text) {
        message.setText(text);
        titleString.setText(title);
    }
    
    /**
     * This methods starts the fade-in effect for the window.  It stops when the window is at 100% opacity.
     */
    public void fadeIn() {
        alpha = 0;
        continueButton.disableMouseEvents();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ClockTask clock = new ClockTask();
                clock.execute();
            }
        });
    }
    
    /**
     * This method starts the fade-out effect for the window.  It stops when the window is at 0% opacity.
     */
    public void fadeOut() {
        alpha = 1.0f;
        continueButton.disableMouseEvents();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ClockTask clock = new ClockTask();
                clock.execute();
            }
        });
    }
    
    /**
     * Updates the opacity of the JDialog.
     */
    private void updateWindowOpacity() {
        this.setOpacity(alpha);
    }
    
    /**
     * This private ActionListener is added to the button on the JDialog.  When clicked, it fades out and closes the dialog.
     */
    private ActionListener buttonClick = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().compareTo("Continue") == 0) {
                fadeOut();
            }
        }
    };
    
    /**
     * This private ActionListener is added to a timer.  On each "tick", it adjusts the JDialog's opacity.
     * Assistance from http://stackoverflow.com/questions/13203415/how-to-add-fade-fade-out-effects-to-a-jlabel.
     */
    private ActionListener taskPerformer = new ActionListener() {
        @Override
            public void actionPerformed(ActionEvent event) {
                alpha += increment;
                if (alpha < 0) {
                    alpha = 0;
                    increment = -increment;
                    Timer timer = (Timer)event.getSource();
                    timer.stop();
                } 
                else if (alpha > 1.0f) {
                    alpha = 1.0f;
                    increment = -increment;
                    Timer timer = (Timer)event.getSource();
                    timer.stop();
                }
                updateWindowOpacity();
            }
    };
    
    /**
     * This background thread is used to fade out the JDialog.  It fades it out, then re-enables the MouseEvents and disposes of the window.  
     * This provides a convenient way to "wait" on the dialog to fade, and to notify the parent panel when it has completely closed.
     */
    private class ClockTask extends SwingWorker<Void, String> {
        public ClockTask() {
        }

        @Override
        protected Void doInBackground() throws Exception {
            Timer timer = new Timer(40, taskPerformer);
            timer.start();
            while (timer.isRunning()) {
            }
            return(null);
        }

        @Override
        protected void done() {
            super.done();
            continueButton.enableMouseEvents();
            if (alpha == 0) {
                setVisible(false);
                dispose();
            }
        }
    }
    
    @Override
    public void paint(Graphics g) {
        
        //g.setClip(new Rectangle(0, 0, 200, 400));
        //g.drawRect(0, 0, getWidth(), getHeight());
        //background.paintComponent(g);
        super.paint(g);
    }
    
}
