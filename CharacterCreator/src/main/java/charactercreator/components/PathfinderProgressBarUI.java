/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicProgressBarUI;

/**
 * This class defines a custom UI for a JProgressBar.
 * @author Megan
 */
public class PathfinderProgressBarUI extends BasicProgressBarUI {
    
    @Override
    protected Color getSelectionBackground() {
        return PathfinderColor.DARK_BROWN.value();
    }
    
    @Override
    protected Color getSelectionForeground() {
        return PathfinderColor.GOLD.value();
    }
 
    @Override
    protected void paintDeterminate(Graphics g, JComponent c) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        Rectangle bounds = progressBar.getVisibleRect();
        int amountFull = this.getAmountFull(progressBar.getInsets(), bounds.width, bounds.height);
        g2d.setPaint(new GradientPaint(new Point(0, 0),PathfinderColor.CRIMSON.value(), new Point(0, bounds.height), 
                PathfinderColor.DARK_BROWN.value()));
        g2d.fillRect(bounds.x, bounds.y, amountFull, bounds.height);
        
        // Deal with possible text painting
        if(progressBar.isStringPainted()) {
            paintString(g, bounds.x, bounds.y, 
                    bounds.width, bounds.height, 
                    amountFull, progressBar.getInsets());
        }
        
    }
    
    @Override
    protected void paintIndeterminate(Graphics g, JComponent c) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        Rectangle bounds = new Rectangle();
        bounds = getBox(bounds);
        g2d.setPaint(new GradientPaint(new Point(0, bounds.height / 4),PathfinderColor.CRIMSON.value(), new Point(0, bounds.height), 
                PathfinderColor.DARK_BROWN.value()));
        g2d.fillOval(bounds.x, bounds.y, bounds.width, bounds.height);
    }
}
