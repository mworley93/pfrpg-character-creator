/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.components;

import java.awt.Insets;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.JTabbedPane;

/**
 * This class is a custom JTabbedPane that is mean to have a TranslucentScrollPane or TranslucentTextPane within it.  NOTE: Rendering may not look right if a regular JTextArea
 * is place inside.  Proceed with caution.
 * @author Megan Worley
 */
public class PathfinderTabbedPane extends JTabbedPane {
    
    public PathfinderTabbedPane() {
        initialize();
    }
    
    /**
     * Initializes the appearance for this custom tabbed pane.
     */
    private void initialize() {
        // This creates a new UI for the tabbed pane so that some default appearance values can be set that are otherwise
        // inaccessible.
        setUI(new BasicTabbedPaneUI() {
                @Override
                protected void installDefaults() {
                    super.installDefaults();
                    highlight = PathfinderColor.BROWN_HIGHLIGHT.value();
                    lightHighlight = PathfinderColor.BROWN_HIGHLIGHT.value();
                    shadow = PathfinderColor.GOLD.value();
                    darkShadow = PathfinderColor.DARK_BROWN.value();
                    focus = PathfinderColor.DARK_BROWN.value();
                    contentBorderInsets = new Insets(0,0,0,0);
                }
             });
        setCustomColors();
    }
    
    /**
     * Defines the new color scheme for the tabbed pane.
     */
    private void setCustomColors() {
        this.setBackground(PathfinderColor.GOLD.value());
        this.setForeground(PathfinderColor.DARK_BROWN.value());
    }
}
