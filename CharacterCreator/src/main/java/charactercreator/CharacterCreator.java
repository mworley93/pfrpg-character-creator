/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator;

import charactercreator.models.ModelWrapper;
import charactercreator.views.AbilityScorePanel;
import charactercreator.controllers.ContentPanelController;
import charactercreator.controllers.CardController;
import charactercreator.controllers.AbilityScoreSubController;
import charactercreator.views.CardView;
import charactercreator.views.ContentPanelView;
import charactercreator.components.PathfinderColor;
import charactercreator.components.PathfinderScrollBarUI;
import charactercreator.helpers.PDFCreator;
import java.awt.EventQueue;
import java.awt.Dimension;
import java.io.File;
import javax.swing.UIManager;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;

/**
 * This class is responsible for constructing the main components of the program and executing it.
 * @author Megan Worley, Brandon Sharp
 */
public class CharacterCreator {
    
    private JFrame guiFrame;
    private ModelWrapper modelWrapper;

    public static void main(String[] args) {
     
         //Use the event dispatch thread for Swing components
         EventQueue.invokeLater(new Runnable()
         {
             
            @Override
             public void run()
             {
                 CharacterCreator creator = new CharacterCreator(); 
             }
         });
              
    }
  
    /** 
     * Instantiates the GUI and creates the main CardLayout interface that will hold all of the information.
    */
    public CharacterCreator()
    { 
        createAndShowGUI();
        initializeEssentialModels();
        initializeViewAndControllers();
    }
    
    /**
     * Creates the frame that holds the GUI.
     */
    private void createAndShowGUI()
    {
        // The window that will hold the GUI.
        guiFrame = new JFrame();  
        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Set the title, resolution, and position of the GUI.
        guiFrame.setTitle("PFRPG Character Creator");
        guiFrame.setMinimumSize(new Dimension(800, 600));
        guiFrame.setSize(1024, 768);
        guiFrame.setLocationRelativeTo(null);
        guiFrame.setIconImage(new ImageIcon(getClass().getResource("/images/ui_elements/Icon.png")).getImage());
        guiFrame.setVisible(true);
        adjustUIManagerProperties();
        
    }
    
    /**
     * Initialize the models needed on program startup (mostly UI-related ones).
     */
    private void initializeEssentialModels() {
        modelWrapper = new ModelWrapper();
        modelWrapper.initializeModel("AbilityScoreGenerator");
        modelWrapper.initializeModel("UIModel");
        modelWrapper.initializeModel("SkillsGenerator");
    }
    
    /**
     * Initializes the views and controllers for the program.
     */
    private void initializeViewAndControllers() {
        ContentPanelView contentView = new ContentPanelView(modelWrapper);
        ContentPanelController contentController = new ContentPanelController(modelWrapper.getUIModel(), contentView);
        contentController.initViewAndModel();
        AbilityScoreSubController scoreSubController = 
                new AbilityScoreSubController(modelWrapper.getAbilityScoreGenerator(), ((AbilityScorePanel)contentView.getContentPanel("Ability Score Screen")).getSubPanels());
        scoreSubController.initViewAndModel();
        CardView cardView = new CardView(contentView);
        CardController cardController = new CardController(cardView, modelWrapper);
        // This makes it so that scroll bars pop up when the window is resized below the 
        // preferred resolution (1024 x 768).  The preferred size is a little smaller here 
        // though since the scroll bars have a width and height that needs to be accounted for.
        JScrollPane scroller = new JScrollPane(cardView.getCard("Card Panel"));
        scroller.setPreferredSize(new Dimension(1000, 700));
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        guiFrame.getContentPane().add(scroller);
    }
    
    /**
     * Adjusts a few default UI settings so that some component attributes can be changed.
     */
    private void adjustUIManagerProperties() {
        UIManager.put("TabbedPane.selected", PathfinderColor.SAND.value());
        UIManager.put("TabbedPane.contentOpaque", false);
        UIManager.put("ScrollBarUI", PathfinderScrollBarUI.class.getName());
    }
}
