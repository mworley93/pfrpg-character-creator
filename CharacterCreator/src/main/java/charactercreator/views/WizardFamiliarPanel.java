/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon
 */

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class WizardFamiliarPanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox wizardFamiliarSelectionMenu;
    private JTextArea wizardFamiliarOverview;
    private JTextArea wizardFamiliarDescription;
    private JLabel wizardFamiliarImage;
    private JLabel wizardFamiliarSelectionLabel;
    private JLabel wizardFamiliarDescriptionLabel;
    private JScrollPane wizardFamiliarOverviewScroll;
    private JScrollPane wizardFamiliarDescriptionScroll;
    private JLabel wizardFamiliarOverviewLabel;
    
    public WizardFamiliarPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        
        wizardFamiliarOverview = guiMethods.createPlainTextArea(uiModel.getWizardFamiliarOverview(), false, null);
        wizardFamiliarOverviewScroll = new TranslucentScrollPane(wizardFamiliarOverview, new Dimension(600, 240));
        wizardFamiliarDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("wizardFamiliar"), false, null);
        wizardFamiliarDescriptionScroll = new TranslucentScrollPane(wizardFamiliarDescription, new Dimension (600, 200));
        wizardFamiliarImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("wizardFamiliar"));
        wizardFamiliarImage.setMinimumSize(new Dimension(357, 500));
        String[] items = {"Bat", "Cat", "Hawk", "Lizard", "Monkey", "Owl", "Rat", "Raven", "Toad", "Viper", "Weasel"};
        wizardFamiliarSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        wizardFamiliarSelectionLabel = new PathfinderLabel("Choose your Familiar: ");
        wizardFamiliarDescriptionLabel = new PathfinderLabel("Wizard Familiars");
        wizardFamiliarOverviewLabel = new PathfinderLabel("Familiar Overview");
        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(wizardFamiliarSelectionLabel).
                addComponent(wizardFamiliarSelectionMenu);

        mainHorizontalGroup.addComponent(wizardFamiliarImage).addGroup(layout.createParallelGroup().addComponent(wizardFamiliarOverviewLabel).addComponent(wizardFamiliarOverviewScroll).
                addGroup(secondaryHorizontalGroup).addComponent(wizardFamiliarDescriptionLabel).addComponent(wizardFamiliarDescriptionScroll));

        layout.setHorizontalGroup(mainHorizontalGroup);         

        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(Alignment.CENTER);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(Alignment.CENTER).
                  addComponent(wizardFamiliarSelectionLabel).addComponent(wizardFamiliarSelectionMenu);

        mainVerticalGroup.addComponent(wizardFamiliarImage).addGroup(layout.createSequentialGroup().addComponent(wizardFamiliarOverviewLabel).addComponent(wizardFamiliarOverviewScroll).addGroup(secondaryVerticalGroup).
                  addComponent(wizardFamiliarDescriptionLabel).addComponent(wizardFamiliarDescriptionScroll));

        layout.setVerticalGroup(mainVerticalGroup);         
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        wizardFamiliarSelectionMenu.setActionCommand("Wizard Familiar Selection");
        wizardFamiliarSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        wizardFamiliarDescription.setText(uiModel.getCurrentInformation("wizardFamiliar"));
        wizardFamiliarDescription.setCaretPosition(0);
        wizardFamiliarImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("wizardFamiliar"))));
    }
    
    public String getSelectedWizardFamiliar() {
        return (String) wizardFamiliarSelectionMenu.getSelectedItem();
    }
}
