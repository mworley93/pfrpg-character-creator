/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon and if we're being straight here, Megan made the first one that I super modelled this off of, so she gets double credit.
 */

import charactercreator.models.UIModel;
import charactercreator.models.FeatGenerator;
import charactercreator.models.Feat;
import charactercreator.models.BaseCharacter;
import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.components.TranslucentTextPane;
import charactercreator.components.ColoredCellRenderer;
import charactercreator.components.PathfinderPopup;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.text.StyleConstants;

/**
 * This class is just like the Feats Selection Screen, but only Fighters will be looking at this one, as they get to choose an extra combat feat.
 * There are two JLists, the list to choose from and the feat you've chosen. At the bottom is a description of the currently selected feat.
 * This page specifically only shows combat feats.
 * 
 */

public class CombatFeatsPanel extends ContentPanel {
    
    private UIModel uiModel;
    private FeatGenerator featGen;
    private BaseCharacter baseCharacter;
    private PathfinderLabel overviewLabel;
    private TranslucentScrollPane overviewPane;
    private JLabel combatFeatsImage;
    private PathfinderLabel numRemainingLabel;
    private TranslucentTextPane numRemainingBox;
    private PathfinderLabel availableLabel;
    private JList availableList;
    private TranslucentScrollPane availableFeats;
    private PathfinderLabel selectedLabel;
    private JList selectedList;
    private TranslucentScrollPane selectedFeats;
    private JPanel buttonPanel;
    private PathfinderButton addButton;
    private PathfinderButton removeButton;
    private PathfinderLabel descriptionLabel;
    private JTextArea descriptionText;
    private TranslucentScrollPane descriptionPane;
    private final static int TABLE_GAP = 20;
    
    
   /**
     * Default constructor.
     * @param uiModel The UIModel that contains the overview description.
     */
    public CombatFeatsPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }    
    
    /**
     * Initializes the components for this FeatsPanel.
     */
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        overviewLabel = new PathfinderLabel("Overview");
        JTextArea overviewText = guiMethods.createPlainTextArea(uiModel.getCombatFeatsOverview(), false, null);
        overviewPane = new TranslucentScrollPane(overviewText, new Dimension(670, 150));
        combatFeatsImage = guiMethods.constructImageLabel(uiModel.getCombatFeatsImage());
        combatFeatsImage.setMinimumSize(new Dimension(280, 425));
        
        numRemainingLabel = new PathfinderLabel("Number of combat feats remaining: ");
        numRemainingBox = new TranslucentTextPane("0", new Dimension(100, 25), StyleConstants.ALIGN_CENTER);
        
        availableLabel = new PathfinderLabel("Available Combat Feats: ");
        Object[] availableData = { };
        availableList = guiMethods.createTransparentList(availableData, ListSelectionModel.SINGLE_SELECTION, JList.VERTICAL);
        availableList.setBackground(new Color(0, 0, 0, 0));
        availableList.setCellRenderer(new ColoredCellRenderer());
        availableFeats = new TranslucentScrollPane(availableList, new Dimension(240, 170));
        availableList.getSelectionModel().addListSelectionListener(new CombatFeatsSelectionHandler());
        
        selectedLabel = new PathfinderLabel("Selected Combat Feats: ");
        Object[] selectedData = {};
        selectedList = guiMethods.createTransparentList(selectedData, ListSelectionModel.SINGLE_SELECTION, JList.VERTICAL);
        selectedList.setBackground(new Color(0, 0, 0, 0));
        selectedList.setCellRenderer(new ColoredCellRenderer());
        selectedFeats = new TranslucentScrollPane(selectedList, new Dimension(240, 170));
        selectedList.getSelectionModel().addListSelectionListener(new CombatFeatsSelectionHandler());
        
        initializeButtons();
        
        descriptionLabel = new PathfinderLabel("Combat Feat Description");
        descriptionText = guiMethods.createPlainTextArea("placeholder", false, null);
        descriptionPane = new TranslucentScrollPane(descriptionText, new Dimension(960, 100));
    }
    
    /**
     * This method initializes the components and layout for the add and remove buttons in between the two JLists.
     */
    private void initializeButtons() {
        // The upper limit on both buttons is set to the highest possible value.  By 
        // doing this, the buttons will completely fill in the container that is the BoxLayout JPanel that 
        // they are placed inside.
        
        // Add Feat button.
        addButton = new PathfinderButton("Add feat >> ");
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setMinimumSize(new Dimension(50, 25));
        addButton.setPreferredSize(new Dimension(50, 25));
        addButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        
        // Remove Feat button.
        removeButton = new PathfinderButton("<< Remove feat");       
        removeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        removeButton.setMinimumSize(new Dimension(50, 25));
        removeButton.setPreferredSize(new Dimension(50, 25));
        removeButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        
        // Add the two buttons to a BoxLayout.  This aligns them to be the same size and allows us to easily 
        // specify a gap in between them.
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        buttonPanel.setMaximumSize(new Dimension(150, 75));
        buttonPanel.setOpaque(false);
        buttonPanel.add(addButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonPanel.add(removeButton);
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
        GroupLayout.ParallelGroup overviewGroupH = layout.createParallelGroup().addComponent(overviewLabel).addComponent(overviewPane);
        GroupLayout.SequentialGroup numRemainingGroupH = layout.createSequentialGroup().addComponent(numRemainingLabel).addComponent(numRemainingBox);
        GroupLayout.ParallelGroup availableGroupH = layout.createParallelGroup().addComponent(availableLabel).addComponent(availableFeats);
        GroupLayout.ParallelGroup buttonGroupH = layout.createParallelGroup().addComponent(buttonPanel);
        GroupLayout.ParallelGroup selectedGroupH = layout.createParallelGroup().addComponent(selectedLabel).addComponent(selectedFeats);
        GroupLayout.SequentialGroup decisionGroupH = layout.createSequentialGroup().addGroup(availableGroupH).addGap(TABLE_GAP).addGroup(buttonGroupH).addGap(TABLE_GAP).
                addGroup(selectedGroupH);
        GroupLayout.ParallelGroup descriptionGroupH = layout.createParallelGroup().addComponent(descriptionLabel).addComponent(descriptionPane);
        GroupLayout.ParallelGroup selectionGroupH = layout.createParallelGroup().addGroup(overviewGroupH).addGroup(numRemainingGroupH).addGroup(decisionGroupH);
        GroupLayout.SequentialGroup monkGroupH = layout.createSequentialGroup().addComponent(combatFeatsImage).addGroup(selectionGroupH);
        mainHorizontalGroup.addGroup(monkGroupH).addGroup(descriptionGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup overviewGroupV = layout.createSequentialGroup().addComponent(overviewLabel).addComponent(overviewPane);
        GroupLayout.ParallelGroup numRemainingGroupV = layout.createParallelGroup().addComponent(numRemainingLabel).addComponent(numRemainingBox);
        GroupLayout.SequentialGroup availableGroupV = layout.createSequentialGroup().addComponent(availableLabel).addComponent(availableFeats);
        GroupLayout.SequentialGroup buttonGroupV = layout.createSequentialGroup().addComponent(buttonPanel);
        GroupLayout.SequentialGroup selectedGroupV = layout.createSequentialGroup().addComponent(selectedLabel).addComponent(selectedFeats);
        GroupLayout.ParallelGroup decisionGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(availableGroupV).addGroup(buttonGroupV).addGroup(selectedGroupV);
        GroupLayout.SequentialGroup descriptionGroupV = layout.createSequentialGroup().addComponent(descriptionLabel).addComponent(descriptionPane);
        GroupLayout.SequentialGroup selectionGroupV = layout.createSequentialGroup().addGroup(overviewGroupV).addGroup(numRemainingGroupV).addGroup(decisionGroupV);
        GroupLayout.ParallelGroup monkGroupV = layout.createParallelGroup().addComponent(combatFeatsImage).addGroup(selectionGroupV);
        mainVerticalGroup.addGroup(monkGroupV).addGroup(descriptionGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }    
    
    @Override
    public void attachActionListener(ActionListener listener) {
        addButton.setActionCommand("Add Feat");
        addButton.addActionListener(listener);
        removeButton.setActionCommand("Remove Feat");
        removeButton.addActionListener(listener);
    }    
    
    @Override
    public void updateView() {
        // Updates the selected list and number of feats remaining to choose to reflect the actual selected feats.
        ArrayList<Feat> feats = featGen.getSelectedCombatFeats();
        selectedList.setListData(featGen.convertNamesToReadable(feats));
        numRemainingBox.setText(Integer.toString(featGen.getNumCombatFeatsRemaining()));
    }    
    
    
    /**
     * Adds a new feat to the selected list if possible.
     */
    public void addFeat() {
        if (featGen.getNumCombatFeatsRemaining() > 0) {
            Feat newFeat = featGen.getFeat((String)availableList.getSelectedValue());
            if (newFeat != null) {
                // Checks for the case where the user has already selected the feat.
                if (featGen.getSelectedFeats().contains(newFeat)) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(this);
                    PathfinderPopup alreadyHaveFeatWarning = new PathfinderPopup("You already have the selected feat.", 400, 150, frame);
                    alreadyHaveFeatWarning.setLocationRelativeTo(this.getParent());
                }
                // Checks to see if they qualify for the feat.
                else if (!featGen.getFilteredFeats().contains(newFeat)) {
                    JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(this);
                    PathfinderPopup featPrerequisiteWarning = new PathfinderPopup("You do not have the prerequisites necessary to select this feat.", 400, 150, frame);
                    featPrerequisiteWarning.setLocationRelativeTo(this.getParent());
                }
                // Adds the feat normally.
                else {
                    // Adds the new feat to the selected list, updates the base character, and updates the filtered feat array (which checks for prerequisites).
                    featGen.addSelectedCombatFeat(newFeat);
                    baseCharacter.addToFeatList(newFeat);
                    featGen.checkPrerequisites(baseCharacter);
                }
            }
        }
    }
    
    /**
     * Removes a given feat from the selected list if possible.
     */
    public void removeFeat() {
        Feat newFeat = featGen.getFeat((String)selectedList.getSelectedValue());
        Feat prereqFeat;
        // If this feat has a prerequisite feat, get it.
        try {
            prereqFeat = featGen.getFeat(newFeat.getPrerequisite());
        }
        catch (Exception e) {
            prereqFeat = null;
        }
        if (newFeat != null) {
            // Does not allow the user to remove feats due to a character's class.
            if (featGen.getCharacterFeats().contains(newFeat)) {
                // TODO: Popup message saying, "You cannot remove feats inherent to your character's class or race."
            }
            // Checks for the case in which a user might try to remove a prerequisite to a higher feat they've selected.
            else if (prereqFeat != null && featGen.getSelectedFeats().contains(newFeat)) {
                // TODO: Popup message saying, "This feat cannot be removed because it is a prerequisite of another feat you have chosen."
            }
            // Removes the feat and updates the character/filtered feat list as normal.
            else {
                featGen.removeSelectedCombatFeat(newFeat);
                baseCharacter.removeFromFeatList(newFeat);
                featGen.checkPrerequisites(baseCharacter);
            }
        }
    }
    
    /**
     * This method sets the BaseCharacter for this class to use.
     * @param baseCharacter The BaseCharacter to set.
     */
    public void setBaseCharacter(BaseCharacter baseCharacter) {
        this.baseCharacter = baseCharacter;
    }    
    
    /**
     * This method sets the FeatGenerator for this class and updates the display to reflect its information.
     * @param featGen The FeatGenerator to set.
     */
    public void setFeatGenerator(FeatGenerator featGen) {
        this.featGen = featGen;
        
        // Set the total available feats to choose from.
        ArrayList<Feat> feats = featGen.getCombatFeatList();
        availableList.setListData(featGen.convertNamesToReadable(feats));
        
        // Set the feats currently selected/inherent to the character.
        ArrayList<Feat> selected = featGen.getSelectedCombatFeats();
        selectedList.setListData(featGen.convertNamesToReadable(selected));
        
        // Set the shown description.
        if (availableList.getSelectedIndex() >= 0) {
            descriptionText.setText(featGen.getFeat((String)availableList.getSelectedValue()).getDescription());
        }
        else {
            descriptionText.setText("");
        }

        // Sets the number of feats available.
        numRemainingBox.setText(Integer.toString(featGen.getNumCombatFeatsRemaining()));  
    }    
    
    /**
     * This private class defines the behavior for the JLists containing the available feats to choose from 
     * and the user-selected feats.  It primarily updates the displayed feat description as a new feat is 
     * selected in the lists.  It handles new mouse selections, as well as changes due to keyboard arrow keys.
     */
    private class CombatFeatsSelectionHandler implements ListSelectionListener {    
        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            
            // Update whichever list had a new selection.  Deselect the selection in the other list if a different list was selected.
            if (!lsm.isSelectionEmpty() && (availableList.getSelectionModel() == lsm)) {
                String newText = featGen.getFeat((String)availableList.getSelectedValue()).getDescription() + "\n\nPrerequisite(s): " +
                        featGen.getFeat((String)availableList.getSelectedValue()).getPrerequisite();;
                descriptionText.setText(newText);
                descriptionText.setCaretPosition(0);
                selectedList.removeSelectionInterval(selectedList.getSelectedIndex(), selectedList.getSelectedIndex());
            } 
            else if (!lsm.isSelectionEmpty() && (selectedList.getSelectionModel() == lsm)) {
                String newText = featGen.getFeat((String)selectedList.getSelectedValue()).getDescription() + "\n\nPrerequisite(s): " +
                        featGen.getFeat((String)selectedList.getSelectedValue()).getPrerequisite();
                descriptionText.setText(newText);
                descriptionText.setCaretPosition(0);
                availableList.removeSelectionInterval(availableList.getSelectedIndex(), availableList.getSelectedIndex());
            }
             
        }
    }    
}
