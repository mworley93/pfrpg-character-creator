/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 *
 * @author Brandon
 */
public class DruidDomainPanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox druidDomainSelectionMenu;
    private JTextArea druidDomainOverview;
    private JTextArea druidDomainFeature;
    private JLabel druidDomainImage;
    private JLabel druidDomainOverviewLabel;
    private JLabel druidDomainSelectionLabel;
    private JLabel druidDomainFeatureLabel;
    private JScrollPane druidDomainOverviewScroll;
    private JScrollPane druidDomainScrollFeature;    
    
    public DruidDomainPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        
        druidDomainOverview = guiMethods.createPlainTextArea(uiModel.getDruidDomainOverview(), false, null);
        druidDomainOverviewScroll = new TranslucentScrollPane(druidDomainOverview, new Dimension(970, 100));
        druidDomainFeature = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("druidDomain"), false, null);
        druidDomainScrollFeature = new TranslucentScrollPane(druidDomainFeature, new Dimension (555, 305));
        druidDomainImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("druidDomain"));
        guiMethods.addBeveledImageBorder(druidDomainImage);
        druidDomainImage.setMinimumSize(new Dimension(390, 346));
        String[] items = {"Air", "Animal", "Earth", "Fire", "Plant", "Water", "Weather"};
        druidDomainSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        druidDomainSelectionLabel = new PathfinderLabel("Choose your Domain: ");
        druidDomainFeatureLabel = new PathfinderLabel("Domain Features");
        druidDomainOverviewLabel = new PathfinderLabel("Druid Domains");   
        
    }
    
    @Override
    protected void initializeLayout() {
        
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align the components along the horizontal axis
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup selectionGroupH = layout.createSequentialGroup().addComponent(druidDomainSelectionLabel).
                addComponent(druidDomainSelectionMenu);
        GroupLayout.ParallelGroup infoGroupH = layout.createParallelGroup().addGroup(selectionGroupH)
                .addComponent(druidDomainFeatureLabel).addComponent(druidDomainScrollFeature);
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addGroup(infoGroupH).addComponent(druidDomainImage);
        mainHorizontalGroup.addComponent(druidDomainOverviewLabel).addComponent(druidDomainOverviewScroll).addGroup(secondaryHorizontalGroup);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup selectionGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(druidDomainSelectionLabel).addComponent(druidDomainSelectionMenu);
        GroupLayout.SequentialGroup infoGroupV = layout.createSequentialGroup().addGroup(selectionGroupV).
                addComponent(druidDomainFeatureLabel).addComponent(druidDomainScrollFeature);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(infoGroupV).addComponent(druidDomainImage);
        mainVerticalGroup.addComponent(druidDomainOverviewLabel).addComponent(druidDomainOverviewScroll).addGroup(secondaryVerticalGroup);
        layout.setVerticalGroup(mainVerticalGroup);   
        
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        druidDomainSelectionMenu.setActionCommand("Druid Domain Selection");
        druidDomainSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        druidDomainFeature.setText(uiModel.getCurrentFeature("druidDomain"));
        druidDomainFeature.setCaretPosition(0);
        druidDomainImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("druidDomain"))));
    }
    
    public String getSelectedDruidDomain() {       
        return (String) druidDomainSelectionMenu.getSelectedItem();       
    }
}
