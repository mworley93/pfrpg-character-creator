/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderColor;
import charactercreator.components.PathfinderProgressBarUI;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.LineBorder;

/**
 * This class is responsible for the construction of the program's title screen.
 * @author Megan Worley, Brandon Sharp
 */
public class TitlePanel extends JPanel {

    private JButton startButton;
    private JButton exitButton;
    private JButton aboutButton;
    private JLabel mainLogo;
    private JPanel menu;
    
    /**
     * Main constructor.
     */
    public TitlePanel()
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setOpaque(false); 
        createComponents();        
        addComponents();
    }
    
    /**
     * Creates the main logo and the buttons and the start screen and sets their alignment.
     */
    private void createComponents()
    {
        ImageIcon logoImage = new ImageIcon(getClass().getResource("/images/ui_elements/PFRPGLogo.png"));
        mainLogo = new JLabel(logoImage);
        startButton = new PathfinderButton("Begin");
        exitButton = new PathfinderButton("Exit");
        aboutButton = new PathfinderButton("About");
        
        startButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainLogo.setAlignmentX(Component.CENTER_ALIGNMENT);
        exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        aboutButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        startButton.setMinimumSize(new Dimension(50, 25));
        startButton.setPreferredSize(new Dimension(50, 25));
        startButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));
        exitButton.setMinimumSize(new Dimension(50, 25));
        exitButton.setPreferredSize(new Dimension(50, 25));
        exitButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));
        
        aboutButton.setMinimumSize(new Dimension(50, 25));
        aboutButton.setPreferredSize(new Dimension(50, 25));
        aboutButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));
        
        // Used for aligning the components.
        menu = new JPanel();
        menu.setLayout(new BoxLayout(menu, BoxLayout.Y_AXIS));
        menu.setMaximumSize(new Dimension(200, 120));
        menu.setOpaque(false);
    }
    
    /**
     * Adds the created components to their appropriate positions on the screen.
     */
    private void addComponents()
    {
        this.add(Box.createRigidArea(new Dimension(0,50)));
        this.add(mainLogo);
        this.add(Box.createRigidArea(new Dimension(0,50)));
        menu.add(startButton);
        menu.add(Box.createRigidArea(new Dimension(0,5)));
        menu.add(exitButton);
        menu.add(Box.createRigidArea(new Dimension(0,5)));
        menu.add(aboutButton);
        menu.add(Box.createRigidArea(new Dimension(0,5)));
        this.add(menu);
    }
 
    public void attachActionListener(ActionListener listener) {
        startButton.setActionCommand("Begin");
        startButton.addActionListener(listener);
        aboutButton.setActionCommand("About");
        aboutButton.addActionListener(listener);
        exitButton.setActionCommand("Exit");
        exitButton.addActionListener(listener);
    }
}
