/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.models.BaseCharacter;
import charactercreator.models.AbilityScoreGenerator.Adjustment;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.components.TranslucentTextPane;
import charactercreator.components.PathfinderScrollTable;
import charactercreator.models.AbilityScore;
import charactercreator.models.Skill;
import charactercreator.models.UIModel;
import charactercreator.models.SkillsGenerator;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.table.TableColumn;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.text.StyleConstants;
import javax.swing.table.AbstractTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Point;
import javax.swing.table.TableModel;

/**
 * This class is an inherited ContentPanel that implements the "Select Your Skills" screen. The main feature of it is a custom table that drives the events of the 
 * entire page.  Because it has a checkbox and needs to have the ability to update its rows, it has a custom model associated with it and a MouseAdapter that changes 
 * the information on clicks.
 * @author Megan
 */
public class SkillsPanel extends ContentPanel {
    
    // Model classes.
    private TableModel tableModel;
    private UIModel uiModel;
    private SkillsGenerator skillsGenerator;
    private BaseCharacter baseCharacter;
    
    // GUI components.
    private PathfinderLabel overviewLabel;
    private PathfinderLabel howToLabel;
    private PathfinderLabel classSkillsLabel;
    private PathfinderButton skillsInfoButton;
    private PathfinderLabel worksheetLabel;
    private PathfinderLabel remainingPointsLabel;
    private TranslucentScrollPane overviewPane;
    private TranslucentScrollPane howToPane;
    private JTextArea classSkillsText;
    private TranslucentScrollPane classSkillsPane;
    private TranslucentTextPane remainingPointsBox;
    private PathfinderScrollTable skillsTable;
    private TranslucentScrollPane tableScroll;
    
    // Constants that define column data.
    private static final int TOTAL_COLUMN = 0;
    private static final int SKILL_COLUMN = 1;
    private static final int ABILITY_COLUMN = 2;
    private static final int CHECKBOX_COLUMN = 3;
    private static final int RANKS_COLUMN = 4;
    private static final int MOD_COLUMN = 5;
    private static final int CLASS_COLUMN = 6;
    private static final int MISC_COLUMN = 7;
    private static final int NUM_SKILLS = 35;
    
    /**
     * Default constructor.
     * @param uiModel The UI Model instance for this application.
     * @param skillsGenerator The SkillsGenerator instance for this application.
     */
    public SkillsPanel(UIModel uiModel, SkillsGenerator skillsGenerator) {
        this.uiModel = uiModel;
        this.skillsGenerator = skillsGenerator;
        initializePanel();
    }
    
    /**
     * Initializes this SkillsPanel.
     */
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        overviewLabel = new PathfinderLabel("Overview");
        JTextArea overviewText = guiMethods.createPlainTextArea(uiModel.getSkillsOverview(), false, null);
        overviewPane = new TranslucentScrollPane(overviewText, new Dimension(425, 150));
        howToLabel = new PathfinderLabel("How to Use the Skills Worksheet");
        JTextArea howToText = guiMethods.createPlainTextArea(uiModel.getSkillsHowTo(), false, null);
        howToPane = new TranslucentScrollPane(howToText, new Dimension(425, 160));
        classSkillsLabel = new PathfinderLabel("Your Class Skills and Available Points");
        classSkillsText = guiMethods.createPlainTextArea("Unknown", false, null);
        classSkillsPane = new TranslucentScrollPane(classSkillsText, new Dimension(425,160));
        skillsInfoButton = new PathfinderButton("?");
        worksheetLabel = new PathfinderLabel("Skills Worksheet");
        remainingPointsLabel = new PathfinderLabel("Skill Points Remaining");
        remainingPointsBox = new TranslucentTextPane("10", new Dimension(50,25), StyleConstants.ALIGN_CENTER);
        customizeTable();
    }
    
    /**
     * Initializes the skills table.  Fills it with the headers and default values, sets the MouseListener, and sets the 
     * rendering properties.
     */
    public void customizeTable() {
        String[] colNames = {"Total", "Skill", "Ability", " ", "Ranks", "Mod", "Class", "Misc." };
        Object[][] data = { {0, "Acrobatics", "DEX", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Appraise", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Bluff", "CHA", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Climb", "STR", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Craft", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Diplomacy", "CHA", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Disable Device", "DEX", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Disguise", "CHA", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Escape Artist", "DEX", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Fly", "DEX", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Handle Animal", "CHA", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Heal", "WIS", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Intimidate", "CHA", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Arcana)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Dungeoneering)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Engineering)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Geography)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (History)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Local)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Nature)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Nobility)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Planes)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Knowledge (Religion)", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Linguistics", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Perception", "WIS", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Perform", "CHA", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Profession", "WIS", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Ride", "DEX", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Sense Motive", "WIS", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Sleight of Hand", "DEX", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Spellcraft", "INT", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Stealth", "DEX", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Survival", "WIS", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Swim", "STR", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}, 
                            {0, "Use Magic Device", "CHA", false, "+" + 0, "+" + 0, "+" + 0, "+" + 0}
                            
        };
        tableModel = new SkillsTableModel(colNames, data);  
        skillsTable = new PathfinderScrollTable(tableModel, new Dimension(540,500));  
        tableScroll = new TranslucentScrollPane(skillsTable, new Dimension(540, 500));  
        
        // Allows the use of a check box (which is type Boolean).
        TableColumn tableColumn = skillsTable.getColumnModel().getColumn(CHECKBOX_COLUMN);  
        tableColumn.setCellEditor(skillsTable.getDefaultEditor(Boolean.class));  
        tableColumn.setCellRenderer(skillsTable.getDefaultRenderer(Boolean.class));
        
        skillsTable.addMouseListener(new SkillsTableMouseAdapter());
        
        // Specify that the table's columns should change based on the size of the data.
        skillsTable.sizeColumnsToFitText(5);
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup textBoxGroupH = layout.createParallelGroup().addComponent(overviewLabel).addComponent(overviewPane).addComponent(howToLabel).
                addComponent(howToPane).addComponent(classSkillsLabel).addComponent(classSkillsPane);
        GroupLayout.SequentialGroup skillsLabelGroupH = layout.createSequentialGroup().addComponent(worksheetLabel).addComponent(skillsInfoButton);
        GroupLayout.SequentialGroup pointsRemainingGroupH = layout.createSequentialGroup().addComponent(remainingPointsLabel).addComponent(remainingPointsBox);
        GroupLayout.ParallelGroup skillsGroupH = layout.createParallelGroup().addGroup(skillsLabelGroupH).addGroup(pointsRemainingGroupH).
                addComponent(tableScroll);
        mainHorizontalGroup.addGroup(textBoxGroupH).addGroup(skillsGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup textBoxGroupV = layout.createSequentialGroup().addComponent(overviewLabel).addComponent(overviewPane).addComponent(howToLabel).
                addComponent(howToPane).addComponent(classSkillsLabel).addComponent(classSkillsPane);
        GroupLayout.ParallelGroup skillsLabelGroupV = layout.createParallelGroup().addComponent(worksheetLabel).addComponent(skillsInfoButton);
        GroupLayout.ParallelGroup pointsRemainingGroupV = layout.createParallelGroup().addComponent(remainingPointsLabel).addComponent(remainingPointsBox);
        GroupLayout.SequentialGroup skillsGroupV = layout.createSequentialGroup().addGroup(skillsLabelGroupV).addGroup(pointsRemainingGroupV).
                addComponent(tableScroll);
        mainVerticalGroup.addGroup(textBoxGroupV).addGroup(skillsGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        skillsInfoButton.setActionCommand("More Info - Skills");
        skillsInfoButton.addActionListener(listener);
    }
    
    /**
     * This function will only be called once per visit to the skills screen.  It takes the values set in the BaseCharacter 
     * so far and updates the ability score modifier, misc. score, and total score columns appropriately. Every other value should 
     * initially be set to zero.
     */
    @Override
    public void updateView() {    
        // First make sure the ability score modifiers, misc. skills, points, and totals are up-to-date.
        skillsGenerator.calculateModifierMap(baseCharacter);
        skillsGenerator.calculateMiscSkillMap(baseCharacter);
        skillsGenerator.calculateTotalSkillMap();
        skillsGenerator.calculatePoints(baseCharacter);
        
        String classSkills = new String();
        
        Object[] rowData = new Object[skillsTable.getColumnCount()]; // This will hold the row data on a given row.
        for (int row = 0; row < skillsTable.getRowCount(); row++) {
            // Iterate through all of the columns in a row and retrieve the data.
            for (int col = 0 ; col < rowData.length ; col++) {
                rowData[col] = (Object)skillsTable.getValueAt(row, col); 
            }
            
            // Determine which skill is being set, and then set the ability score modifier and misc. points.  Keep track of the class skills.
            Skill currentSkill = Skill.fromString((String)rowData[SKILL_COLUMN]);
            rowData[TOTAL_COLUMN] = skillsGenerator.getTotalSkill(currentSkill);
            int modValue = skillsGenerator.getAbilityModSkill(currentSkill);
            String modString = "";
            if (modValue >= 0) {
                modString += "+";
            }
            modString += modValue;
            rowData[MOD_COLUMN] = (String)modString;
            rowData[MISC_COLUMN] = "+" + skillsGenerator.getMiscSkill(currentSkill);
            
            if (baseCharacter.getCharacterClass().getClassSkill(currentSkill)) {
                if (!classSkills.isEmpty()) {
                    classSkills += ", ";
                }
                classSkills += currentSkill.getString();
            }
            
            // Update the table with the new data.
            ((SkillsTableModel)tableModel).updateRow(row, rowData);
        }
        
         // Set the informative box in the lower-left corner. Corrects your INT mod's points to never be negative.
        int intModPoints = baseCharacter.getAbilityScoreModifier(AbilityScore.INT);
        if (intModPoints < 0) {
            intModPoints = 0;
        }        
        classSkillsText.setText("Your class choice of " + baseCharacter.getCharacterClass().getClassName() + " grants you " + baseCharacter.getCharacterClass().getSkillPoints() + 
                " + your INT modifier (" + intModPoints + ") skill points per level.\n\nYour race choice of " + 
                baseCharacter.getCharacterRace().getRaceName() +  " grants you +" + baseCharacter.getCharacterRace().getBonusSkillPoints() + " skill point(s).\n\nIn addition, " + 
                baseCharacter.getCharacterClass().getClassName() + "'s have the following"  + " class skills, which grant a +3 bonus if selected: " + classSkills + ".");
        classSkillsText.setCaretPosition(0);
        // Set the number of points remaining.
        remainingPointsBox.setText(Integer.toString(skillsGenerator.getPoints()));
    }
    
    
    /**
     * Sets the BaseCharacter so that the skills information can be calculated and displayed appropriately.
     * @param baseCharacter 
     */
    public void setBaseCharacter(BaseCharacter baseCharacter) {
        this.baseCharacter = baseCharacter;
    }
        
    /**
     * resetTable
     * This function is called when the user leaves backwards from the skills selection screen.
     * It resets the checkbox on each row, resetting the whole panel.
     */
    
    public void skillTableReset() {
    // There are 35 rows, so this just sets each checkbox to false for each row (i is the row, 3 is the column.)   
        for(int i = 0; i<34; i++) {
            //String skillName = (String)skillsTable.getValueAt(i, 1);
            //Skill currentSkill = Skill.fromString((String)skillName);            
            //skillsTable.setValueAt(skillsGenerator.getTotalSkill(currentSkill), i, 0);
            skillsTable.setValueAt("+0", i, 6);
            skillsTable.setValueAt("+0", i, 4);
            skillsTable.setValueAt(false, i, 3);
            this.skillsGenerator.calculateClassSkillMap(baseCharacter);
        }
    }
    
    /**
     * This private MouseAdapter class is needed by, and only by, the skills table in order for it to update.  It detects whenever the 
     * mouse is clicked on the table and determines whether or not the user has clicked on the checkbox.  If so, it then calls the 
     * SkillsTableModel to populate that row in the skills table with the new skill point values.
     * Assistance from: http://stackoverflow.com/questions/15555183/jtable-update-selected-row
     */
    private class SkillsTableMouseAdapter extends MouseAdapter
    {
        @Override
        public void mousePressed(MouseEvent event) {    
            // Find the cell that was clicked.
            int x = event.getX();
            int y = event.getY();
            int row = skillsTable.rowAtPoint(new Point(x,y));
            int col = skillsTable.columnAtPoint(new Point(x,y));
            
            // If the cell clicked was the column containing the checkbox, get all of the data from that row.
            if (col == CHECKBOX_COLUMN)
            {
                Object[] rowData = new Object[skillsTable.getColumnCount() - 1];
                for (int i = 0 ; i < rowData.length ; i++) {
                    rowData[i] = (Object)skillsTable.getValueAt(row, i);
                }
            
                Skill currentSkill = Skill.fromString((String)rowData[SKILL_COLUMN]);
                
                if (rowData[CHECKBOX_COLUMN] instanceof Boolean) {
                    // If the checkbox was not currently checked and there are still points left, switch it to checked and update the skills/points.
                    if (rowData[CHECKBOX_COLUMN].equals(false) && skillsGenerator.getPoints() > 0) {
                        rowData[CHECKBOX_COLUMN] = !((Boolean)skillsTable.getValueAt(row, CHECKBOX_COLUMN));
                        // Increase the number of skill points by 1.
                        skillsGenerator.changePoints(currentSkill, Adjustment.UP);
                        skillsGenerator.calculateClassSkillMap(baseCharacter);
                        rowData[RANKS_COLUMN] = "+1";
                        rowData[CLASS_COLUMN] = "+" + skillsGenerator.getClassSkill(currentSkill);  
                    }
                    // If the checkbox was already checked, just remove the bonuses and add the point back.
                    else if (rowData[CHECKBOX_COLUMN].equals(true)) {
                        rowData[CHECKBOX_COLUMN] = !((Boolean)skillsTable.getValueAt(row, CHECKBOX_COLUMN));
                        // Decrease the number of skill points by 1.
                        skillsGenerator.changePoints(currentSkill, Adjustment.DOWN);
                        rowData[RANKS_COLUMN] = "+0";
                        rowData[CLASS_COLUMN] = "+0";
                    }
                    
                }
                
                // Update the current row.
                skillsGenerator.calculateAllSkillMaps(baseCharacter);
                rowData[TOTAL_COLUMN] = skillsGenerator.getTotalSkill(currentSkill);
                remainingPointsBox.setText(Integer.toString(skillsGenerator.getPoints()));
                ((SkillsTableModel)tableModel).updateRow(row, rowData);
            }
        }
    }
    
    /**
     * This private class is a custom TableModel that holds onto the data in the skills table 
     * and provides methods to update the data.  This is necessary in order to change the displayed skill points whenever 
     * the user clicks a checkbox on the table.
     * Assistance from: http://stackoverflow.com/questions/15555183/jtable-update-selected-row
     */
    private class SkillsTableModel extends AbstractTableModel 
    {
        public SkillsTableModel(String[] columns, Object[][] inData) {
            columnNames = columns;
            data = inData;
        }
        String[] columnNames;
        Object[][] data;
        TableColumn[] columns;
        
        @Override
        public void setValueAt(Object value, int row, int col) {
            data[row][col] = (Object)value;
            fireTableCellUpdated(row,col);
        }
        
        @Override
        public Object getValueAt(int row, int col) {
            return data[row][col];
        }
        
        @Override
        public int getColumnCount() {
            return columnNames.length;
        }
        
        @Override 
        public int getRowCount() {
            return data.length;
        }
        
        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }
        
        @Override
        public boolean isCellEditable(int row ,int col)
        {
            return true;
        }
        
        /**
         * This method replaces the values in a table's row with the values in a given input row.
         * @param index The index of the row to be updated.
         * @param values The new values for that row.
         */
        public void updateRow(int index, Object[] values)
        {
            for (int i = 0 ; i < values.length ; i++)
            {
                setValueAt(values[i], index, i);
            }
        }
    };
}
