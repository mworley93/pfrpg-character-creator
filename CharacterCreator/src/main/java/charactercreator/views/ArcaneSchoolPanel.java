/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon
 */


import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class ArcaneSchoolPanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox arcaneSchoolSelectionMenu;
    private JTextArea arcaneSchoolOverview;
    private JTextArea arcaneSchoolDescription;
    private JLabel arcaneSchoolImage;
    private JLabel arcaneSchoolSelectionLabel;
    private JLabel arcaneSchoolDescriptionLabel;
    private JScrollPane arcaneSchoolOverviewScroll;
    private JScrollPane arcaneSchoolDescriptionScroll;
    private JLabel arcaneSchoolOverviewLabel;
    private JLabel arcaneSchoolFeatureLabel;
    private JScrollPane arcaneSchoolFeatureScroll;
    private JTextArea arcaneSchoolFeature;
    
    public ArcaneSchoolPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        
        arcaneSchoolOverview = guiMethods.createPlainTextArea(uiModel.getArcaneSchoolOverview(), false, null);
        arcaneSchoolOverviewScroll = new TranslucentScrollPane(arcaneSchoolOverview, new Dimension(960, 120));
        arcaneSchoolDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("arcaneSchool"), false, null);
        arcaneSchoolDescriptionScroll = new TranslucentScrollPane(arcaneSchoolDescription, new Dimension (560, 100));
        arcaneSchoolFeature = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("arcaneSchool"), false, null);
        arcaneSchoolFeatureScroll = new TranslucentScrollPane(arcaneSchoolFeature, new Dimension(560, 200));
        arcaneSchoolImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("arcaneSchool"));
        arcaneSchoolImage.setMinimumSize(new Dimension(390, 330));
        arcaneSchoolImage.setMaximumSize(new Dimension(390, 330));
        String[] items = {"Abjuration", "Conjuration", "Divination", "Enchantment", "Evocation", "Illusion", "Necromancy", "Transmutation", "Universalist"};
        arcaneSchoolSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        arcaneSchoolSelectionLabel = new PathfinderLabel("Choose your Arcane School: ");
        arcaneSchoolFeatureLabel = new PathfinderLabel("School Features");
        arcaneSchoolDescriptionLabel = new PathfinderLabel("Arcane Schools");
        arcaneSchoolOverviewLabel = new PathfinderLabel("Arcane School Overview");
        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align the components along the horizontal axis
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup selectionGroupH = layout.createSequentialGroup().addComponent(arcaneSchoolSelectionLabel).
                addComponent(arcaneSchoolSelectionMenu);
        GroupLayout.ParallelGroup infoGroupH = layout.createParallelGroup().addGroup(selectionGroupH)
                .addComponent(arcaneSchoolDescriptionLabel).addComponent(arcaneSchoolDescriptionScroll).addComponent(arcaneSchoolFeatureLabel).addComponent(arcaneSchoolFeatureScroll);
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(arcaneSchoolImage).addGroup(infoGroupH);
        mainHorizontalGroup.addComponent(arcaneSchoolOverviewLabel).addComponent(arcaneSchoolOverviewScroll).addGroup(secondaryHorizontalGroup);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup selectionGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(arcaneSchoolSelectionLabel).addComponent(arcaneSchoolSelectionMenu);
        GroupLayout.SequentialGroup infoGroupV = layout.createSequentialGroup().addGroup(selectionGroupV).
                addComponent(arcaneSchoolDescriptionLabel).addComponent(arcaneSchoolDescriptionScroll).addComponent(arcaneSchoolFeatureLabel).addComponent(arcaneSchoolFeatureScroll);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(arcaneSchoolImage).addGroup(infoGroupV);
        mainVerticalGroup.addComponent(arcaneSchoolOverviewLabel).addComponent(arcaneSchoolOverviewScroll).addGroup(secondaryVerticalGroup);
        layout.setVerticalGroup(mainVerticalGroup);     
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        arcaneSchoolSelectionMenu.setActionCommand("Arcane School Selection");
        arcaneSchoolSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        arcaneSchoolDescription.setText(uiModel.getCurrentInformation("arcaneSchool"));
        arcaneSchoolDescription.setCaretPosition(0);
        arcaneSchoolFeature.setText(uiModel.getCurrentFeature("arcaneSchool"));
        arcaneSchoolFeature.setCaretPosition(0);
        arcaneSchoolImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("arcaneSchool"))));
    }
    
    public String getSelectedArcaneSchool() {
        return (String) arcaneSchoolSelectionMenu.getSelectedItem();
    }
    
}
