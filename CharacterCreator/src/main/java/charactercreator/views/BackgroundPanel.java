/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.CardLayout;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * This class is a custom JPanel that is used to draw an unchanging background image to a GUI utilizing a Card Layout.
 * @author Megan Worley, Brandon Sharp
 */
 public class BackgroundPanel extends JPanel 
 {
    private Image background;

    /**
     * Main constructor.
     * @param imagePath The file containing the background image.
     * @param cards The CardLayout that this panel will have.
     */
    public BackgroundPanel(String imagePath, CardLayout cards) 
    {
        background = new ImageIcon(getClass().getResource(imagePath)).getImage();
        this.setLayout(cards);
        this.setPreferredSize(new Dimension(1000, 700));
        this.setVisible(true);
    }
    
    /**
     * Non-CardLayout constructor.
     * @param imagePath The file containing the background image.
     */
    public BackgroundPanel(String imagePath) 
    {
      background = new ImageIcon(getClass().getResource(imagePath)).getImage();
      this.setVisible(true);
    }
    
    /**
     * Paints the custom background image to the screen.
     * @param graphics Used to draw the image.
     */
    @Override
    public void paintComponent(Graphics graphics) {
        graphics.drawImage(background, 0, 0, getWidth(), getHeight(), this);
    }
}
