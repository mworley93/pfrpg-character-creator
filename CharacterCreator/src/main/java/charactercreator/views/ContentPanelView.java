/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.models.ModelWrapper;
import java.util.Map;
import java.util.HashMap;
import java.awt.event.ActionListener;
import javax.swing.JPanel;


/**
 * This class is responsible for creating the user interface for the center panel on the creation screens.  It constructs all of them using the provided 
 * helper functions from GUIHelper.java and by checking the data stored in UIModel.  It then stores the created panels into a HashMap for 
 * retrieval by the card manager.  It updates the display whenever an action triggers a change in the model that holds the data.  
 * @author Megan Worley, Brandon Sharp
 */
public class ContentPanelView {
    
    // Panels and necessary models.
    private ModelWrapper modelWrapper;
    private Map<String, JPanel> contentPanelMap = new HashMap();
    
    public ContentPanelView(ModelWrapper modelWrapper) {
        this.modelWrapper = modelWrapper;
    }
    
    /**
     * Used by the controller to initialize the content panels.  Necessary because the controller must have flexibility over when to initialize the model
     * and view.  Initializing the view too soon causes null exceptions from the model.
     */
    public void initView() {
        constructClassScreen();
        constructRaceScreen();
        constructAbilityScoreScreen();
        constructRaceAbilityScoreScreen();
        constructBloodlineScreen();
        constructSkillsScreen();
        constructFeatsScreen();
        constructCombatFeatsScreen();
        constructMonkFeatScreen();
        constructFavoredEnemyScreen();
        constructDruidBranchScreen();
        constructAnimalCompanionScreen();
        constructDruidDomainScreen();
        constructArcaneBondScreen();
        constructWizardFamiliarScreen();
        constructArcaneSchoolScreen();
        constructForbiddenArcaneSchoolScreen();
        constructSecondForbiddenArcaneSchoolScreen();
        constructClericDomainScreen();
        constructSecondClericDomainScreen();
        constructEquipmentScreen();
        constructMiscScreen();
        constructSpellbookScreen();
    }
         
// <editor-fold defaultstate="collapsed" desc=" Content Panel Construction Methods ">
    
    private void constructClassScreen() {
        JPanel classScreen = new ClassPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Class Selection Screen", classScreen);
    }

    private void constructRaceScreen() {
        JPanel raceScreen = new RacePanel(modelWrapper.getUIModel());
        contentPanelMap.put("Race Selection Screen", raceScreen);
    }
    
    private void constructAbilityScoreScreen() {
        AbilityScorePanel abilityScoreScreen = new AbilityScorePanel(modelWrapper.getUIModel());
        contentPanelMap.put("Ability Score Screen", abilityScoreScreen);
    }
    
    private void constructRaceAbilityScoreScreen() {
        JPanel raceAbilityScoreScreen = new RaceAbilityScorePanel(modelWrapper.getUIModel());
        contentPanelMap.put("Race Ability Score Screen", raceAbilityScoreScreen);
    }

    private void constructBloodlineScreen() {
        JPanel bloodlineScreen = new BloodlinePanel(modelWrapper.getUIModel());
        contentPanelMap.put("Bloodline Selection Screen", bloodlineScreen);
    }
    
    private void constructCombatFeatsScreen() {
        JPanel combatFeatsScreen = new CombatFeatsPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Combat Feats Selection Screen", combatFeatsScreen);
    }
    
    private void constructMonkFeatScreen() {
        JPanel monkFeatScreen = new MonkFeatPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Monk Feat Selection Screen", monkFeatScreen);
    }
    
    public void constructFavoredEnemyScreen() {
        JPanel favoredEnemyScreen = new FavoredEnemyPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Favored Enemy Selection Screen", favoredEnemyScreen);
    }
    
    public void constructDruidBranchScreen() {
        JPanel druidBranchScreen = new DruidBranchPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Druid Branch Selection Screen", druidBranchScreen);
    }
       
    public void constructAnimalCompanionScreen() {
        JPanel animalCompanionScreen = new AnimalCompanionPanel(modelWrapper.getUIModel());      
        contentPanelMap.put("Animal Companion Selection Screen", animalCompanionScreen);
    }
    
    public void constructDruidDomainScreen() {
        JPanel druidDomainScreen = new DruidDomainPanel(modelWrapper.getUIModel());        
        contentPanelMap.put("Druid Domain Selection Screen", druidDomainScreen);
    }
    
    public void constructArcaneBondScreen() {
        JPanel arcaneBondScreen = new ArcaneBondPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Arcane Bond Selection Screen", arcaneBondScreen);
    }
    
    public void constructWizardFamiliarScreen() {
        JPanel wizardFamiliarScreen = new WizardFamiliarPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Wizard Familiar Selection Screen", wizardFamiliarScreen);
    }
    
    public void constructArcaneSchoolScreen() {
        JPanel arcaneSchoolScreen = new ArcaneSchoolPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Arcane School Selection Screen", arcaneSchoolScreen);
    }
    
    public void constructForbiddenArcaneSchoolScreen() {
        JPanel forbiddenArcaneSchoolScreen = new ForbiddenSchoolPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Forbidden Arcane School Selection Screen", forbiddenArcaneSchoolScreen);
    }
    
    public void constructSecondForbiddenArcaneSchoolScreen() {
        JPanel secondForbiddenArcaneSchoolScreen = new SecondForbiddenSchoolPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Second Forbidden Arcane School Selection Screen", secondForbiddenArcaneSchoolScreen);
    }    
    
    public void constructClericDomainScreen() {
        JPanel clericDomainScreen = new ClericDomainPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Cleric Domain Selection Screen", clericDomainScreen);
    }
    
    public void constructSecondClericDomainScreen() {
        JPanel secondClericDomainScreen = new SecondClericDomainPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Second Cleric Domain Selection Screen", secondClericDomainScreen);
    }      
    
    private void constructSkillsScreen() {
        SkillsPanel skillsScreen = new SkillsPanel(modelWrapper.getUIModel(), modelWrapper.getSkillsGenerator());
        contentPanelMap.put("Skills Selection Screen", skillsScreen);
    }
    
    private void constructFeatsScreen() {
        JPanel featsScreen = new FeatsPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Feats Selection Screen", featsScreen);
    }
    
    private void constructEquipmentScreen() {
        EquipmentPanel equipmentScreen = new EquipmentPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Equipment Screen", equipmentScreen);
    }
    
    private void constructMiscScreen() {
        MiscPanel miscScreen = new MiscPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Misc Screen", miscScreen);
    }
    
    private void constructSpellbookScreen() {
        SpellbookPanel spellbookScreen = new SpellbookPanel(modelWrapper.getUIModel());
        contentPanelMap.put("Spellbook Screen", spellbookScreen);
    }
    
// </editor-fold>
   
// <editor-fold defaultstate="collapsed" desc=" Action Listener Assignment Methods ">
    /**
     * Adds the action commands and listener to the class screen.
     *
     * @param classListener The action listener to be added.
     */
    public void addClassListener(ActionListener classListener) {
        ((ClassPanel)getContentPanel("Class Selection Screen")).attachActionListener(classListener);
    }

    /**
     * Adds the action commands and listener to the race screen.
     *
     * @param raceListener The action listener to be added.
     */
    public void addRaceListener(ActionListener raceListener) {
        ((RacePanel)getContentPanel("Race Selection Screen")).attachActionListener(raceListener);
    }
    
    /**
     * Adds the action commands and listener to the ability score screen.
     * @param abilityListener The action listener to be added.
     */
    public void addAbilityScoreListener(ActionListener abilityListener) {
        ((AbilityScorePanel)getContentPanel("Ability Score Screen")).attachActionListener(abilityListener);
    }
    
    public void addRaceAbilityScoreListener(ActionListener raceAbilityListener) {
        ((RaceAbilityScorePanel)getContentPanel("Race Ability Score Screen")).attachActionListener(raceAbilityListener);
    }
    
    /**
     * Adds the action command and listener to the bloodline screen.
     * @param bloodlineListener The action listener to be added.
     */
    public void addBloodlineListener(ActionListener bloodlineListener) {
       ((BloodlinePanel)getContentPanel("Bloodline Selection Screen")).attachActionListener(bloodlineListener);
    }
    
    /**
     * Adds the action command and listener to the feats screen.
     * @param featsListener The action listener to be added.
     */
    public void addFeatsListener(ActionListener featsListener) {
       ((FeatsPanel)getContentPanel("Feats Selection Screen")).attachActionListener(featsListener);
    }
    
    /**
     * Adds the action command and listener to the feats screen.
     * @param combatFeatsListener The action listener to be added.
     */
    public void addCombatFeatsListener(ActionListener combatFeatsListener) {
       ((CombatFeatsPanel)getContentPanel("Combat Feats Selection Screen")).attachActionListener(combatFeatsListener);
    }    
    
    public void addMonkFeatListener(ActionListener monkFeatListener) {
        ((MonkFeatPanel)getContentPanel("Monk Feat Selection Screen")).attachActionListener(monkFeatListener);
    }
    
    /**
     * Adds the action command and listener to the skills screen.
     * @param skillsListener The action listener to be added.
     */
    public void addSkillsListener(ActionListener skillsListener) {
       ((SkillsPanel)getContentPanel("Skills Selection Screen")).attachActionListener(skillsListener);
    }
    public void addFavoredEnemyListener(ActionListener favoredEnemyListener) {
        ((FavoredEnemyPanel)getContentPanel("Favored Enemy Selection Screen")).attachActionListener(favoredEnemyListener);
    }
    
    public void addDruidBranchListener(ActionListener druidBranchListener) {
        ((DruidBranchPanel)getContentPanel("Druid Branch Selection Screen")).attachActionListener(druidBranchListener);
    }
    
    public void addAnimalCompanionListener(ActionListener animalCompanionListener) {
        ((AnimalCompanionPanel)getContentPanel("Animal Companion Selection Screen")).attachActionListener(animalCompanionListener);
    }
    
    public void addDruidDomainListener(ActionListener druidDomainListener) {
        ((DruidDomainPanel)getContentPanel("Druid Domain Selection Screen")).attachActionListener(druidDomainListener);
    }
    
    public void addArcaneBondListener(ActionListener arcaneBondListener) {
        ((ArcaneBondPanel)getContentPanel("Arcane Bond Selection Screen")).attachActionListener(arcaneBondListener);
    }
    
    public void addWizardFamiliarListener(ActionListener wizardFamiliarListener) {
        ((WizardFamiliarPanel)getContentPanel("Wizard Familiar Selection Screen")).attachActionListener(wizardFamiliarListener);
    }

    public void addArcaneSchoolListener(ActionListener arcaneSchoolListener) {
        ((ArcaneSchoolPanel)getContentPanel("Arcane School Selection Screen")).attachActionListener(arcaneSchoolListener);
    }

    public void addForbiddenArcaneSchoolListener(ActionListener forbiddenArcaneSchoolListener) {
        ((ForbiddenSchoolPanel)getContentPanel("Forbidden Arcane School Selection Screen")).attachActionListener(forbiddenArcaneSchoolListener);
    }    
    
    public void addSecondForbiddenArcaneSchoolListener(ActionListener secondForbiddenArcaneSchoolListener) {
        ((SecondForbiddenSchoolPanel)getContentPanel("Second Forbidden Arcane School Selection Screen")).attachActionListener(secondForbiddenArcaneSchoolListener);
    }       
    
    public void addClericDomainListener(ActionListener clericDomainListener) {
        ((ClericDomainPanel)getContentPanel("Cleric Domain Selection Screen")).attachActionListener(clericDomainListener);
    }
    
    public void addSecondClericDomainListener(ActionListener secondClericDomainListener) {
        ((SecondClericDomainPanel)getContentPanel("Second Cleric Domain Selection Screen")).attachActionListener(secondClericDomainListener);
    }
    
    public void addEquipmentListener(ActionListener equipListener) {
        ((ContentPanel)getContentPanel("Equipment Screen")).attachActionListener(equipListener);
    }
    
    public void addMiscListener(ActionListener miscListener) {
        ((ContentPanel)getContentPanel("Misc Screen")).attachActionListener(miscListener);
    }
    
    public void addSpellbookListener(ActionListener spellbookListener) {
        ((ContentPanel)getContentPanel("Spellbook Screen")).attachActionListener(spellbookListener);
    }
// </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc=" Update Methods ">
    /**
     * Updates the GUI if any of the class panel information has changed.
     */
    public void updateClassView() {
        ((ClassPanel)getContentPanel("Class Selection Screen")).updateView();
    }

    /**
     * Updates the GUI if any of the class panel information has changed.
     */
    public void updateRaceView() {
        ((RacePanel)getContentPanel("Race Selection Screen")).updateView();
    }
    
    /**
     * Updates the GUI if any of the ability score panel information has changed.
     */
    public void updateAbilityScoresView() {
        ((AbilityScorePanel)getContentPanel("Ability Score Screen")).updateView();
    }
    
    public void updateRaceAbilityScoresView() {
        ((RaceAbilityScorePanel)getContentPanel("Race Ability Score Screen")).updateView();
    }
    
    public void updateBloodlineView() {
        ((BloodlinePanel)getContentPanel("Bloodline Selection Screen")).updateView();
    }
    
    /**
     * Updates the GUI if any of the feats panel information has changed.
     */
    public void updateFeatsView() {
        ((FeatsPanel)getContentPanel("Feats Selection Screen")).updateView();
    }
    
    public void updateCombatFeatsView() {
        ((CombatFeatsPanel)getContentPanel("Combat Feats Selection Screen")).updateView();
    }
    
    public void updateMonkFeatView() {
        ((MonkFeatPanel)getContentPanel("Monk Feat Selection Screen")).updateView();
    }
    
    public void updateSkillsView() {
        ((SkillsPanel)getContentPanel("Skills Selection Screen")).updateView();
    }
    
    public void updateFavoredEnemyView() {
        ((FavoredEnemyPanel)getContentPanel("Favored Enemy Selection Screen")).updateView();
    }
    
    public void updateDruidBranchView() {
        ((DruidBranchPanel)getContentPanel("Druid Branch Selection Screen")).updateView();
    }
    
    public void updateAnimalCompanionView() {
        ((AnimalCompanionPanel)getContentPanel("Animal Companion Selection Screen")).updateView();
    }
    
    public void updateDruidDomainView() {
        ((DruidDomainPanel)getContentPanel("Druid Domain Selection Screen")).updateView();
    }
    
    public void updateArcaneBondView() {
        ((ArcaneBondPanel)getContentPanel("Arcane Bond Selection Screen")).updateView();
    }
    
    public void updateWizardFamiliarView() {
        ((WizardFamiliarPanel)getContentPanel("Wizard Familiar Selection Screen")).updateView();
    }
    
    public void updateArcaneSchoolView() {
        ((ArcaneSchoolPanel)getContentPanel("Arcane School Selection Screen")).updateView();
    }
    
    public void updateForbiddenArcaneSchoolView() {
        ((ForbiddenSchoolPanel)getContentPanel("Forbidden Arcane School Selection Screen")).updateView();
    }
    
    public void updateSecondForbiddenArcaneSchoolView() {
        ((SecondForbiddenSchoolPanel)getContentPanel("Second Forbidden Arcane School Selection Screen")).updateView();
    }
    
    public void updateClericDomainView() {
        ((ClericDomainPanel)getContentPanel("Cleric Domain Selection Screen")).updateView();
    }
    
    public void updateSecondClericDomainView() {
        ((SecondClericDomainPanel)getContentPanel("Second Cleric Domain Selection Screen")).updateView();
    }    
    
    public void updateEquipmentView() {
        ((ContentPanel)getContentPanel("Equipment Screen")).updateView();
    }
    

    public void updateMiscView() {
        ((ContentPanel)getContentPanel("Misc Screen")).updateView();
    }
    
    public void updateSpellbookView() {
        ((ContentPanel)getContentPanel("Spellbook Screen")).updateView();
    }
    
// </editor-fold>
    
    /**
     * This function is retrieved a stored JPanel from the HashMap by using a
     * key associated with it.
     *
     * @param panelKey
     * @return
     */
    public JPanel getContentPanel(String panelKey) {
        return (JPanel) contentPanelMap.get(panelKey);
    }
    
}
