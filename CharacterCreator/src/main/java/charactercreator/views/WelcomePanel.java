/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.helpers.GUIHelper;

import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Component;

/**
 * This custom JPanel represents the Welcome Screen that the user will see after they hit "Begin" on the title screen.  It's primary purpose is to introduce the 
 * application and to ask if they would like to view a tutorial section. This class inherits from BackgroundPanel so that the custom drawing of the paper background image can 
 * be preserved.
 * @author Megan Worley
 */
public class WelcomePanel extends BackgroundPanel {
    GUIHelper guiMethods;
    
    // Screen panels.
    JPanel titlePanel;
    JPanel middlePanel;
    JPanel questionPanel;
    JPanel buttonPanel;
    
    // Title components.
    PathfinderLabel title;
    JLabel banner;
    JLabel border;
    
    // Informative components.
    TranslucentScrollPane introPane;
    PathfinderLabel recommendationLabel;
    PathfinderLabel questionLabel;
    
    // Button components.
    PathfinderButton confirmButton;
    PathfinderButton passButton;
    
    /**
     * Default constructor.
     * @param image Themed background image (should be /images/ui_elements/backgroundTexture.png for consistency).
     */
    public WelcomePanel(String image) {
        super(image);
        guiMethods = new GUIHelper();
        initializeComponents();
        initializeLayout();
    }
    
    /**
     * Initializes the component values found on this screen, such as the title text, introductory text, image banner, etc.
     */
    private void initializeComponents() {
        title = new PathfinderLabel("Welcome to the Pathfinder© RPG Character Creator!", 35);
        title.drawShadow(true);
        banner = guiMethods.constructImageLabel("/images/ui_elements/welcomebanner.jpg");
        border = guiMethods.constructImageLabel("/images/ui_elements/border.png");
        guiMethods.addBeveledImageBorder(banner);
        String introStr = "Welcome to the (unofficial) Pathfinder RPG Character Creator!  " + 
                          "If you’re unfamiliar with Pathfinder, here’s a little background information to get you up to speed:\n" +
                          "\n" +
                          "The Pathfinder Roleplaying Game (PFRPG) is an extension of the Revised 3rd Edition of Dungeons & Dragons published by " +
                          "Wizards of the Coast under the Open Game License.  	Pathfinder was created by the Paizo Publishing Company as an " +
                          "alternative to the then unreleased D&D 4th Edition.  In addition to being backwards compatible with D&D 3.5, it also features " +
                          "its own setting and adventures.\n" +
                          "\n" +
                          "This Character Creator walks players through the character creation process, explaining Pathfinder’s features while allowing " +
                          "players to build a new character for their campaign.  At the end, these decisions will be compiled into a character sheet that " +
                          "can be saved, copied, or printed at your convenience.  While primarily aimed at players who are still somewhat unfamiliar " +
                          "with Pathfinder, veteran players may also find that the Character Creator is a quick way to generate a new character sheet.\n" +
                          "\n" +
                          "The PFRPG Character Creator currently only supports classes and races featured in the Core Rulebook, though others may be added " + 
                          "in future updates.";
        JTextArea introText = guiMethods.createPlainTextArea(introStr, false, null);
        introPane = new TranslucentScrollPane(introText, new Dimension(955, 200));
        recommendationLabel = new PathfinderLabel("If this is the first time using the Character Creator, we recommend viewing the tutorial section.", 25);
        questionLabel = new PathfinderLabel("Would you like to take the tour?", 25);
    
        initializeButtons();
    
        initializePanels();
        
        setComponentOrientation();
    }
    
    /**
     * Initializes the buttons found on this screen.  Sets their text as well as their maximum and minimum sizes so they can stretch and scale 
     * appropriately in the BoxLayout.
     */
    private void initializeButtons() {
        confirmButton = new PathfinderButton("Sure, let's do it!");
        confirmButton.setMinimumSize(new Dimension(260, 40));
        confirmButton.setPreferredSize(new Dimension(260, 40));
        confirmButton.setMaximumSize(new Dimension(260, 40));
        passButton = new PathfinderButton("Nah, I'm an experienced character creator.");
        passButton.setMinimumSize(new Dimension(260, 40));
        passButton.setPreferredSize(new Dimension(300, 40));
        passButton.setMaximumSize(new Dimension(300, 40));
    }
    
    /**
     * Initializes the subpanels on this screen and makes them completely transparent so the background can show through.
     */
    private void initializePanels() {
        titlePanel = new JPanel();
        titlePanel.setOpaque(false);
        middlePanel = new JPanel();
        middlePanel.setOpaque(false);
        questionPanel = new JPanel();
        questionPanel.setOpaque(false);
        buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);
    }
    
    /**
     * Sets the components' orientation, which is super important for BoxLayout.  In any given subpanel, the X-component orientation
     * must be the same.  Some components on the screen are left aligned and some are center aligned, so they are broken up into 
     * groups.
     */
    private void setComponentOrientation() {
        // Title panel.
        title.setAlignmentX(Component.LEFT_ALIGNMENT);
        title.setAlignmentY(Component.LEFT_ALIGNMENT);
        
        // Middle panel.
        banner.setAlignmentX(Component.CENTER_ALIGNMENT);
        border.setAlignmentX(Component.CENTER_ALIGNMENT);
        introPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        banner.setAlignmentY(Component.CENTER_ALIGNMENT);
        border.setAlignmentY(Component.CENTER_ALIGNMENT);
        introPane.setAlignmentY(Component.CENTER_ALIGNMENT);
        
        // Question panel.
        recommendationLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        questionLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        recommendationLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        questionLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        
        // Sets subpanel alignments.
        titlePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        middlePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        questionPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titlePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        middlePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        questionPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
    }
    
    /**
     * Adds all of the components to the screen.  BoxLayout is used on everything, since the arrangement isn't too complex.  
     */
    private void initializeLayout() {  
        // A few subpanels needed to be used to get the component alignments to be correct.  All use BoxLayout.  Vertical and horizontal 
        // glue is used to dynamically space the components.
        
        // Title section.
        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));
        titlePanel.add(Box.createVerticalGlue());
        titlePanel.add(title);
        titlePanel.add(Box.createVerticalGlue());
        
        // Image/text section.
        middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.Y_AXIS));
        middlePanel.add(banner);
        middlePanel.add(Box.createVerticalGlue());
        middlePanel.add(border);
        middlePanel.add(Box.createVerticalGlue());
        middlePanel.add(introPane);
        
        // Section that presents the tutorial information to the user.
        questionPanel.setLayout(new BoxLayout(questionPanel, BoxLayout.Y_AXIS));
        questionPanel.add(recommendationLabel);
        questionPanel.add(questionLabel);
        
        // Button section.
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(confirmButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(passButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        // Adds each section to this entire screen.
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(titlePanel);
        this.add(middlePanel);
        this.add(Box.createVerticalGlue());
        this.add(questionPanel);
        this.add(Box.createVerticalGlue());
        this.add(buttonPanel);
        this.add(Box.createVerticalGlue());
    }
    
    /**
     * Adds the action listener for the buttons on the welcome screen.  Determines 
     * whether or not a tutorial section will start.
     * @param listener The ActionListener that checks if the user wants the tutorial.
     */
    public void attachActionListener(ActionListener listener) {
        confirmButton.setActionCommand("Start Tutorial");
        confirmButton.addActionListener(listener);
        passButton.setActionCommand("Skip Tutorial");
        passButton.addActionListener(listener);
    }
}
