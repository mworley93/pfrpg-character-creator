/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;
import java.util.Map;
import java.util.HashMap;
import javax.swing.JPanel;
import java.awt.CardLayout;

/**
 * This class instantiates the main card layout that will hold all of the different screens in the program.  It creates all of the possible cards and
 * adds the actions to the buttons that trigger card-changing events.  It has methods for dynamically adding screens to the card layout based on the 
 * user's decisions.
 * @author Megan Worley, Brandon Sharp
 */
public class CardView {
    
    private CardLayout cards;
    private Map<String, JPanel> cardMap;
    private BackgroundPanel cardPanel;
    private TitlePanel titleScreen;
    private WelcomePanel welcomeScreen;
    private TutorialSequence tutorialScreen;
    private CreationPanel classSelectionScreen;
    private CreationPanel raceSelectionScreen;
    private CreationPanel abilityScoreScreen;
    private CreationPanel raceAbilityScoreScreen;
    private CreationPanel bloodlineSelectionScreen;
    private CreationPanel featsSelectionScreen;
    private CreationPanel combatFeatsSelectionScreen;
    private CreationPanel monkFeatSelectionScreen;
    private CreationPanel skillsSelectionScreen;
    private CreationPanel favoredEnemySelectionScreen;
    private CreationPanel druidBranchSelectionScreen;
    private CreationPanel animalCompanionSelectionScreen;
    private CreationPanel druidDomainSelectionScreen;
    private CreationPanel arcaneBondSelectionScreen;
    private CreationPanel wizardFamiliarSelectionScreen;
    private CreationPanel arcaneSchoolSelectionScreen;
    private CreationPanel forbiddenArcaneSchoolSelectionScreen;
    private CreationPanel secondForbiddenArcaneSchoolSelectionScreen;
    private CreationPanel clericDomainSelectionScreen;
    private CreationPanel secondClericDomainSelectionScreen;
    private CreationPanel equipmentScreen;
    private CreationPanel miscScreen;
    private CreationPanel spellbookScreen;
    private ContentPanelView contentView;
    private ConfirmationPanel confirmationScreen;
    
    /**
     * Main constructor.
     */
    public CardView(ContentPanelView contentView) {           
        cards = new CardLayout();
        cardMap = new HashMap();
        this.contentView = contentView;
        createCards();
        addCards();
        addFirstCards();
    }
    
    /**
     * Creates all of the cards in the program.
     */
    private void createCards()
    {     
        cardPanel = new BackgroundPanel("/images/ui_elements/backgroundTexture.jpg", cards);
        titleScreen = new TitlePanel();
        welcomeScreen = new WelcomePanel("/images/ui_elements/backgroundTexture.jpg");
        tutorialScreen = new TutorialSequence();
        confirmationScreen = new ConfirmationPanel("/images/ui_elements/backgroundTexture.jpg");
        classSelectionScreen = new CreationPanel("Class Selection Screen", "Select Your Class:", 
                contentView.getContentPanel("Class Selection Screen"));
        raceSelectionScreen = new CreationPanel("Race Selection Screen", "Select Your Race:",
                contentView.getContentPanel("Race Selection Screen"));
        abilityScoreScreen = new CreationPanel("Ability Score Screen", "Generate Ability Scores:", 
                contentView.getContentPanel("Ability Score Screen"));
        raceAbilityScoreScreen = new CreationPanel("Race Ability Score Screen", "Choose Your Racial Ability Score Bonuses: ",
                contentView.getContentPanel("Race Ability Score Screen"));
        bloodlineSelectionScreen = new CreationPanel("Bloodline Selection Screen", "Select Your Bloodline:",
                contentView.getContentPanel("Bloodline Selection Screen"));
        skillsSelectionScreen = new CreationPanel("Skills Selection Screen", "Select Your Skills:",
                contentView.getContentPanel("Skills Selection Screen"));
        featsSelectionScreen = new CreationPanel("Feats Selection Screen", "Select Your Feats:",
                contentView.getContentPanel("Feats Selection Screen"));
        combatFeatsSelectionScreen = new CreationPanel("Combat Feats Selection Screen", "Select Your Combat Feats:",
                contentView.getContentPanel("Combat Feats Selection Screen"));
        monkFeatSelectionScreen = new CreationPanel("Monk Feat Selection Screen", "Select Your Monk Feat:",
                contentView.getContentPanel("Monk Feat Selection Screen"));
        favoredEnemySelectionScreen = new CreationPanel("Favored Enemy Selection Screen", "Select Your Favored Enemy:",
                contentView.getContentPanel("Favored Enemy Selection Screen"));
        druidBranchSelectionScreen = new CreationPanel("Druid Branch Selection Screen", "Select Your Nature Bond:",
                contentView.getContentPanel("Druid Branch Selection Screen"));
        animalCompanionSelectionScreen = new CreationPanel("Animal Companion Selection Screen", "Select Your Animal Companion:",
                contentView.getContentPanel("Animal Companion Selection Screen"));
        druidDomainSelectionScreen = new CreationPanel("Druid Domain Selection Screen", "Select Your Domain:",
                contentView.getContentPanel("Druid Domain Selection Screen"));
        arcaneBondSelectionScreen = new CreationPanel("Arcane Bond Selection Screen", "Select Your Arcane Bond:",
                contentView.getContentPanel("Arcane Bond Selection Screen"));
        wizardFamiliarSelectionScreen = new CreationPanel("Wizard Familiar Selection Screen", "Select Your Wizard Familiar:",
                contentView.getContentPanel("Wizard Familiar Selection Screen"));      
        arcaneSchoolSelectionScreen = new CreationPanel("Arcane School Selection Screen", "Select Your Arcane School: ",
                contentView.getContentPanel("Arcane School Selection Screen"));
        forbiddenArcaneSchoolSelectionScreen = new CreationPanel("Forbidden Arcane School Selection Screen", "Select Your First Forbidden Arcane School: ",
                contentView.getContentPanel("Forbidden Arcane School Selection Screen"));        
        secondForbiddenArcaneSchoolSelectionScreen = new CreationPanel("Second Forbidden Arcane School Selection Screen", "Select Your Second Forbidden Arcane School: ",
                contentView.getContentPanel("Second Forbidden Arcane School Selection Screen"));  
        clericDomainSelectionScreen = new CreationPanel("Cleric Domain Selection Screen", "Select Your First Cleric Domain: ",
                contentView.getContentPanel("Cleric Domain Selection Screen"));
        secondClericDomainSelectionScreen = new CreationPanel("Second Cleric Domain Selection Screen", "Select Your Second Cleric Domain: ",
                contentView.getContentPanel("Second Cleric Domain Selection Screen"));
        equipmentScreen = new CreationPanel("Equipment Screen", "Purchase Equipment: ",
                contentView.getContentPanel("Equipment Screen"));
        miscScreen = new CreationPanel("Misc Screen", "Customize Background: ",
                contentView.getContentPanel("Misc Screen"));
        spellbookScreen = new CreationPanel("Spellbook Screen", "Fill in your Spellbook:",
                contentView.getContentPanel("Spellbook Screen"));
    }
    
    /**
     * Adds all of the cards to a container so that they can be easily accessed by the CardController.
     */
    private void addCards() {
        cardMap.put("Card Panel", cardPanel);
        cardMap.put("Title Screen", titleScreen);
        cardMap.put("Welcome Screen", welcomeScreen);
        cardMap.put("Tutorial Sequence", tutorialScreen);
        cardMap.put("Class Selection Screen", classSelectionScreen);
        cardMap.put("Race Selection Screen", raceSelectionScreen);
        cardMap.put("Ability Score Screen", abilityScoreScreen);
        cardMap.put("Race Ability Score Screen", raceAbilityScoreScreen);
        cardMap.put("Bloodline Selection Screen", bloodlineSelectionScreen);
        cardMap.put("Skills Selection Screen", skillsSelectionScreen);
        cardMap.put("Feats Selection Screen", featsSelectionScreen);
        cardMap.put("Combat Feats Selection Screen", combatFeatsSelectionScreen);
        cardMap.put("Monk Feat Selection Screen", monkFeatSelectionScreen);
        cardMap.put("Favored Enemy Selection Screen", favoredEnemySelectionScreen);
        cardMap.put("Druid Branch Selection Screen", druidBranchSelectionScreen);
        cardMap.put("Animal Companion Selection Screen", animalCompanionSelectionScreen);
        cardMap.put("Druid Domain Selection Screen", druidDomainSelectionScreen);
        cardMap.put("Arcane Bond Selection Screen", arcaneBondSelectionScreen);
        cardMap.put("Wizard Familiar Selection Screen", wizardFamiliarSelectionScreen);
        cardMap.put("Arcane School Selection Screen", arcaneSchoolSelectionScreen);
        cardMap.put("Forbidden Arcane School Selection Screen", forbiddenArcaneSchoolSelectionScreen);
        cardMap.put("Second Forbidden Arcane School Selection Screen", secondForbiddenArcaneSchoolSelectionScreen);
        cardMap.put("Cleric Domain Selection Screen", clericDomainSelectionScreen);
        cardMap.put("Second Cleric Domain Selection Screen", secondClericDomainSelectionScreen);
        cardMap.put("Equipment Screen", equipmentScreen);
        cardMap.put("Misc Screen", miscScreen);
        cardMap.put("Spellbook Screen", spellbookScreen);
        cardMap.put("Confirmation Screen", confirmationScreen);
    }
    
    /**
     * Adds the first few vital cards to the layout to be displayed when the program starts.
     */
    private void addFirstCards()
    {      
        // Add the cards and set the title screen to display upon launch.
        cardPanel.add(titleScreen, "Title Screen");
        cardPanel.add(welcomeScreen, "Welcome Screen");
        cardPanel.add(tutorialScreen, "Tutorial Sequence");
        cardPanel.add(classSelectionScreen, "Class Selection Screen");
        cards.show(cardPanel, "Title Screen");
    }
    
    /**
     * Get/set functions
     */
    public CardLayout getCardLayout() {
        return cards;
    }
  
    public JPanel getCard(String key) {
        return cardMap.get(key);
    }
    
    public ContentPanelView getContentView() {
        return contentView;
    }
    /**
     * This function adds a card to the CardLayout to be displayed.
     * @param newCard The card to be added.
     * @param name The name of the new card.
     */
    public void addCardToLayout(JPanel newCard, String name) {
        cardPanel.add(newCard, name);
    }
    
    /**
     * This function removes a card from the CardLayout.  Use this if the 
     * user goes back screens so that unnecessary cards won't be present if 
     * different choices are made.
     * @param card The card to remove.
     */
    public void removeCardFromLayout(JPanel card) {
        cardPanel.remove(card);
    }
}
