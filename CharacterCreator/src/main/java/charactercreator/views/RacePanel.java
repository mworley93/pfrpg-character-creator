/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;

/**
 *
 * @author Megan
 */
public class RacePanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox raceSelectionMenu;
    private JTextArea raceDescription;
    private JTextArea raceFeaturesArea;
    private JLabel racePreview;
    JLabel selectionLabel;
    JLabel descriptionLabel;
    JLabel featuresLabel;
    JScrollPane scrollDescription;
    JScrollPane scrollFeatures;
    private PathfinderButton raceInfoButton;
    private PathfinderButton qualitiesInfoButton;
    
    public RacePanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        // Create the screen components.
        raceDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("race"), false, null);
        raceFeaturesArea = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("race"), false, null);
        scrollDescription = new TranslucentScrollPane(raceDescription, new Dimension(650, 200));
        scrollFeatures = new TranslucentScrollPane(raceFeaturesArea, new Dimension(650, 270));
        racePreview = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("race"));
        racePreview.setMinimumSize(new Dimension(310, 500));
        String[] items = {"Dwarf", "Elf", "Gnome", "Half-elf", "Half-orc", "Halfling", "Human"};
        raceSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        selectionLabel = new PathfinderLabel("Choose your race: ");
        descriptionLabel = new PathfinderLabel("Description");
        featuresLabel = new PathfinderLabel("Racial Qualities");
        raceInfoButton = new PathfinderButton("?");
        qualitiesInfoButton = new PathfinderButton("?");
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(raceInfoButton).
                addComponent(selectionLabel).addComponent(raceSelectionMenu);
        GroupLayout.SequentialGroup featuresGroupH = layout.createSequentialGroup().addComponent(featuresLabel).addComponent(qualitiesInfoButton);
        mainHorizontalGroup.addComponent(racePreview).addGroup(layout.createParallelGroup().addGroup(secondaryHorizontalGroup).
                addComponent(descriptionLabel).addComponent(scrollDescription).addGroup(featuresGroupH).addComponent(scrollFeatures));
        secondaryHorizontalGroup.addComponent(selectionLabel).addComponent(raceSelectionMenu);
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(raceInfoButton).addComponent(selectionLabel).addComponent(raceSelectionMenu);
                GroupLayout.ParallelGroup featuresGroupV = layout.createParallelGroup().addComponent(featuresLabel).addComponent(qualitiesInfoButton);
        mainVerticalGroup.addComponent(racePreview).addGroup(layout.createSequentialGroup().addGroup(secondaryVerticalGroup).
                addComponent(descriptionLabel).addComponent(scrollDescription).addGroup(featuresGroupV).addComponent(scrollFeatures));
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        raceSelectionMenu.setActionCommand("Race Selection");
        raceSelectionMenu.addActionListener(listener);
        raceInfoButton.setActionCommand("More Info - Races");
        raceInfoButton.addActionListener(listener);
        qualitiesInfoButton.setActionCommand("More Info - Racial Qualities");
        qualitiesInfoButton.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        raceDescription.setText(uiModel.getCurrentInformation("race"));
        raceDescription.setCaretPosition(0);
        raceFeaturesArea.setText(uiModel.getCurrentFeature("race"));
        raceFeaturesArea.setCaretPosition(0);
        racePreview.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("race"))));
    }
    
    public String getSelectedRace() {
        return (String)raceSelectionMenu.getSelectedItem();
    }
}
