/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon
 */

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ClericDomainPanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox clericDomainSelectionMenu;
    private JTextArea clericDomainOverview;
    private JLabel clericDomainImage;
    private JLabel clericDomainSelectionLabel;
    private JScrollPane clericDomainOverviewScroll;
    private JLabel clericDomainOverviewLabel;
    private JLabel clericDomainFeatureLabel;
    private JScrollPane clericDomainFeatureScroll;
    private JTextArea clericDomainFeature;        
    
    public ClericDomainPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    public void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        clericDomainOverview = guiMethods.createPlainTextArea(uiModel.getClericDomainOverview(), false, null);
        clericDomainOverviewScroll = new TranslucentScrollPane(clericDomainOverview, new Dimension(535, 120));
        clericDomainFeature = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("clericDomain"), false, null);
        clericDomainFeatureScroll = new TranslucentScrollPane(clericDomainFeature, new Dimension(535, 280));
        clericDomainImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("clericDomain"));
        clericDomainImage.setMinimumSize(new Dimension(425, 350));
        String[] items = {"Air", "Animal", "Artifice", "Chaos", "Charm", "Community", "Darkness", "Death", "Destruction", "Earth", "Fire", "Glory", "Good", "Healing", "Knowledge", "Law", "Liberation", "Luck", "Madness", "Magic", "Nobility", "Plant", "Protection", "Repose", "Rune", "Strength", "Sun", "Travel", "Trickery", "War", "Water", "Weather"};
        clericDomainSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        clericDomainSelectionLabel = new PathfinderLabel("Choose your Cleric's Domain: ");
        clericDomainFeatureLabel = new PathfinderLabel("Domain Features");
        clericDomainOverviewLabel = new PathfinderLabel("Domain Overview");        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup()
                .addComponent(clericDomainSelectionLabel).addComponent(clericDomainSelectionMenu);
        GroupLayout.SequentialGroup featuresLabelGroupH = layout.createSequentialGroup().addComponent(clericDomainFeatureLabel);
        mainHorizontalGroup.addGroup(layout.createParallelGroup().addGroup(secondaryHorizontalGroup).
                addComponent(clericDomainOverviewLabel).addComponent(clericDomainOverviewScroll).addGroup(featuresLabelGroupH).addComponent(clericDomainFeatureScroll))
                .addComponent(clericDomainImage);
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(clericDomainSelectionLabel).addComponent(clericDomainSelectionMenu);
        GroupLayout.ParallelGroup featuresLabelGroupV = layout.createParallelGroup().addComponent(clericDomainFeatureLabel);
        mainVerticalGroup.addGroup(layout.createSequentialGroup().addGroup(secondaryVerticalGroup).
                addComponent(clericDomainOverviewLabel).addComponent(clericDomainOverviewScroll).addGroup(featuresLabelGroupV).addComponent(clericDomainFeatureScroll))
                .addComponent(clericDomainImage);
        layout.setVerticalGroup(mainVerticalGroup);      
        

    }    
    
    @Override
    public void attachActionListener(ActionListener listener) {
        clericDomainSelectionMenu.setActionCommand("Cleric Domain Selection");
        clericDomainSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        clericDomainFeature.setText(uiModel.getCurrentFeature("clericDomain"));
        clericDomainFeature.setCaretPosition(0);
        clericDomainImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("clericDomain"))));
    }
    
    public String getSelectedClericDomain() {
        return (String) clericDomainSelectionMenu.getSelectedItem();
    }    
}
