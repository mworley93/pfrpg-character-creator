/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon
 */

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class SecondClericDomainPanel extends ContentPanel {

    private UIModel uiModel;
    private JComboBox secondClericDomainSelectionMenu;
    private JTextArea secondClericDomainOverview;
    private JLabel secondClericDomainImage;
    private JLabel secondClericDomainSelectionLabel;
    private JScrollPane secondClericDomainOverviewScroll;
    private JLabel secondClericDomainOverviewLabel;
    private JLabel secondClericDomainFeatureLabel;
    private JScrollPane secondClericDomainFeatureScroll;
    private JTextArea secondClericDomainFeature;        
    
    public SecondClericDomainPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    public void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        secondClericDomainOverview = guiMethods.createPlainTextArea(uiModel.getClericDomainOverview(), false, null);
        secondClericDomainOverviewScroll = new TranslucentScrollPane(secondClericDomainOverview, new Dimension(535, 120));
        secondClericDomainFeature = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("secondClericDomain"), false, null);
        secondClericDomainFeatureScroll = new TranslucentScrollPane(secondClericDomainFeature, new Dimension(535, 280));
        secondClericDomainImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("secondClericDomain"));
        secondClericDomainImage.setMinimumSize(new Dimension(350, 500));
        String[] items = {"Air", "Animal", "Artifice", "Chaos", "Charm", "Community", "Darkness", "Death", "Destruction", "Earth", "Fire", "Glory", "Good", "Healing", "Knowledge", "Law", "Liberation", "Luck", "Madness", "Magic", "Nobility", "Plant", "Protection", "Repose", "Rune", "Strength", "Sun", "Travel", "Trickery", "War", "Water", "Weather"};
        secondClericDomainSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        secondClericDomainSelectionLabel = new PathfinderLabel("Choose your Cleric's Second Domain: ");
        secondClericDomainFeatureLabel = new PathfinderLabel("Domain Features");
        secondClericDomainOverviewLabel = new PathfinderLabel("Domain Overview");        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup()
                .addComponent(secondClericDomainSelectionLabel).addComponent(secondClericDomainSelectionMenu);
        GroupLayout.SequentialGroup featuresLabelGroupH = layout.createSequentialGroup().addComponent(secondClericDomainFeatureLabel);
        mainHorizontalGroup.addComponent(secondClericDomainImage).addGroup(layout.createParallelGroup().addGroup(secondaryHorizontalGroup).
                addComponent(secondClericDomainOverviewLabel).addComponent(secondClericDomainOverviewScroll).addGroup(featuresLabelGroupH).addComponent(secondClericDomainFeatureScroll));
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(secondClericDomainSelectionLabel).addComponent(secondClericDomainSelectionMenu);
        GroupLayout.ParallelGroup featuresLabelGroupV = layout.createParallelGroup().addComponent(secondClericDomainFeatureLabel);
        mainVerticalGroup.addComponent(secondClericDomainImage).addGroup(layout.createSequentialGroup().addGroup(secondaryVerticalGroup).
                addComponent(secondClericDomainOverviewLabel).addComponent(secondClericDomainOverviewScroll).addGroup(featuresLabelGroupV).addComponent(secondClericDomainFeatureScroll));
        layout.setVerticalGroup(mainVerticalGroup);        
    }    
    
    @Override
    public void attachActionListener(ActionListener listener) {
        secondClericDomainSelectionMenu.setActionCommand("Second Cleric Domain Selection");
        secondClericDomainSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        secondClericDomainFeature.setText(uiModel.getCurrentFeature("secondClericDomain"));
        secondClericDomainFeature.setCaretPosition(0);
        secondClericDomainImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("secondClericDomain"))));
    }
    
    public String getSelectedClericDomain() {
        return (String) secondClericDomainSelectionMenu.getSelectedItem();
    }    
    
}
