/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;

/**
 *
 * @author Megan
 */
public class BloodlinePanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox bloodlineSelectionMenu;
    private JTextArea bloodlineDescription;
    private JTextArea bloodlineFeaturesArea;
    private JTextArea bloodlineIntroduction;
    private JScrollPane introductionScroll;
    JLabel selectionLabel;
    JLabel descriptionLabel;
    JLabel featuresLabel;
    JLabel overviewLabel;
    JScrollPane scrollDescription;
    JScrollPane scrollFeatures;
    private JLabel bloodlineImage;
    
    public BloodlinePanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        //Create the screen components
        bloodlineIntroduction = guiMethods.createPlainTextArea(uiModel.getBloodlineOverview(), false, null);
        introductionScroll = new TranslucentScrollPane(bloodlineIntroduction, new Dimension(970, 140));
        bloodlineDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("bloodline"), false, null);
        bloodlineFeaturesArea = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("bloodline"), false, null);
        scrollDescription = new TranslucentScrollPane(bloodlineDescription, new Dimension(570, 100));
        scrollFeatures = new TranslucentScrollPane(bloodlineFeaturesArea, new Dimension(570, 180));
        bloodlineImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("bloodline"));
        bloodlineImage.setMinimumSize(new Dimension(390, 242));
        String[] items = {"Aberrant", "Abyssal", "Arcane", "Celestial", "Destined", "Draconic", "Elemental", "Fey", "Infernal", "Undead"};
        bloodlineSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        selectionLabel = new PathfinderLabel("Choose your bloodline: ");
        descriptionLabel = new PathfinderLabel("Description");
        featuresLabel = new PathfinderLabel("Bloodline Features");  
        overviewLabel = new PathfinderLabel("Bloodline Overview");
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align the components along the horizontal axis
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup selectionGroupH = layout.createSequentialGroup().addComponent(selectionLabel).
                addComponent(bloodlineSelectionMenu);
        GroupLayout.ParallelGroup infoGroupH = layout.createParallelGroup().addGroup(selectionGroupH)
                .addComponent(descriptionLabel).addComponent(scrollDescription).addComponent(featuresLabel).addComponent(scrollFeatures);
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(bloodlineImage).addGroup(infoGroupH);
        mainHorizontalGroup.addComponent(overviewLabel).addComponent(introductionScroll).addGroup(secondaryHorizontalGroup);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup selectionGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(selectionLabel).addComponent(bloodlineSelectionMenu);
        GroupLayout.SequentialGroup infoGroupV = layout.createSequentialGroup().addGroup(selectionGroupV).
                addComponent(descriptionLabel).addComponent(scrollDescription).addComponent(featuresLabel).addComponent(scrollFeatures);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(bloodlineImage).addGroup(infoGroupV);
        mainVerticalGroup.addComponent(overviewLabel).addComponent(introductionScroll).addGroup(secondaryVerticalGroup);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        bloodlineSelectionMenu.setActionCommand("Bloodline Selection");
        bloodlineSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        bloodlineDescription.setText(uiModel.getCurrentInformation("bloodline"));
        bloodlineDescription.setCaretPosition(0);
        bloodlineFeaturesArea.setText(uiModel.getCurrentFeature("bloodline"));
        bloodlineFeaturesArea.setCaretPosition(0);
        bloodlineImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("bloodline"))));
    }
    
    public String getSelectedBloodline() {
        return (String) bloodlineSelectionMenu.getSelectedItem();
    }
}