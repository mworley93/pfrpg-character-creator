/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon
 */

import charactercreator.components.ColoredCellRenderer;
import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.PathfinderPopup;
import charactercreator.components.PathfinderTabbedPane;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.components.TranslucentTextPane;
import charactercreator.models.AbilityScore;
import charactercreator.models.BaseCharacter;
import charactercreator.models.Spell;
import charactercreator.models.SpellGenerator;
import charactercreator.models.UIModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.StyleConstants;

public class SpellbookPanel extends ContentPanel {
    // Data instances
    private UIModel uiModel;
    private SpellGenerator spellGen;
    private BaseCharacter baseCharacter;
    
    // Overview & how-to components
    private PathfinderLabel overviewLabel;
    private TranslucentScrollPane overviewPane;
    private PathfinderLabel howToLabel;
    private TranslucentScrollPane howToPane;
    
    // Spells Remaining Component
     private TranslucentTextPane spellsRemaining;
    
    // Store components
    private PathfinderLabel storeLabel;
    private PathfinderTabbedPane storeCategories;
    private JList levelZeroStoreList;
    private JList levelOneStoreList;
    
    // Inventory components
    private PathfinderLabel inventoryLabel;
    private PathfinderTabbedPane inventoryCategories;
    private JList levelZeroInventoryList;
    private JList levelOneInventoryList;
    
    // Transaction components
    private JPanel buttonPanel;
    private PathfinderButton addButton;
    private PathfinderButton removeButton;
    
    // Item information
    private PathfinderLabel statsLabel;
    private JTextArea statsText;
    private TranslucentScrollPane statsPane;
    
    private JLabel spellbookImage;
    
    // Constants for specifying layout.
    private final static int TABLE_GAP = 20;
    
    // Constants that define tab indices.
    private final static int TAB_ZERO = 0;
    private final static int TAB_ONE = 1;    
    /**
     * Default constructor.
     * @param uiModel The UIModel that contains the overview description.
     */
    public SpellbookPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    /**
     * Initializes the components for this FeatsPanel.
     */
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        overviewLabel = new PathfinderLabel("Overview");
        JTextArea overviewText = guiMethods.createPlainTextArea(uiModel.getSpellbookOverview(), false, null);
        overviewPane = new TranslucentScrollPane(overviewText, new Dimension(480, 125));
        
        howToLabel = new PathfinderLabel("How To Use the Spellbook Page");
        JTextArea howToText = guiMethods.createPlainTextArea(uiModel.getSpellbookHowTo(), false, null);
        howToPane = new TranslucentScrollPane(howToText, new Dimension(480, 125));
        
        storeLabel = new PathfinderLabel("Spells to Choose From: ");
        Object[] initData = {};
        levelZeroStoreList = initializeSpellList(initData);
        levelOneStoreList = initializeSpellList(initData);
        createStore();
        
        inventoryLabel = new PathfinderLabel("Your Inventory: ");
        levelZeroInventoryList = initializeSpellList(initData);
        levelOneInventoryList = initializeSpellList(initData);
        createInventory();
        
        initializeButtons();
        
        statsLabel = new PathfinderLabel("Spell Information");
        statsText = guiMethods.createPlainTextArea("", false, null);
        statsPane = new TranslucentScrollPane(statsText, new Dimension(660, 150));

        spellsRemaining = new TranslucentTextPane("0", new Dimension(60, 25), StyleConstants.ALIGN_CENTER);        
        spellbookImage = guiMethods.constructImageLabel(uiModel.getSpellbookImage());
        spellbookImage.setMaximumSize(new Dimension(290, 400));
        spellbookImage.setMinimumSize(new Dimension(290, 400));
    }
    
    /**
     * This method initializes a new JList that will contain equipment information.
     * @param data An array full of items.
     * @return The new JList.
     */
    private JList initializeSpellList(Object[] data) {
        JList list = guiMethods.createTransparentList(data, ListSelectionModel.SINGLE_SELECTION, JList.VERTICAL);
        list.setBackground(new Color(0, 0, 0, 0));
        list.setCellRenderer(new ColoredCellRenderer());
        list.getSelectionModel().addListSelectionListener(new SpellbookPanel.SpellSelectionHandler());
        return list;
    }    
    
    /**
     * Creates the store widget that will display all of the spells that the user can select.
     */
    private void createStore() {
        storeCategories = new PathfinderTabbedPane();
        storeCategories.setMaximumSize(new Dimension(250,150));
        TranslucentScrollPane storeLevelOnePane = new TranslucentScrollPane(levelOneStoreList, new Dimension(250, 150));
        storeCategories.addTab("Level 1", storeLevelOnePane);
    }    
    
    /**
     * Creates the inventory widget that will display all of the items that the user has purchased.
     */
    private void createInventory() {
        inventoryCategories = new PathfinderTabbedPane();
        inventoryCategories.setMaximumSize(new Dimension(250,150));
        TranslucentScrollPane inventoryLevelOnePane = new TranslucentScrollPane(levelOneInventoryList, new Dimension(250, 150));
        inventoryCategories.addTab("Level 1", inventoryLevelOnePane);
    }    
    
    /**
     * This method initializes the components and layout for the add and remove buttons in between the store and inventory.
     */
    private void initializeButtons() {
        // The upper limit on both buttons is set to the highest possible value.  By 
        // doing this, the buttons will completely fill in the container that is the BoxLayout JPanel that 
        // they are placed inside.
        
        // Purchase button.
        addButton = new PathfinderButton("Add >> ");
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setMinimumSize(new Dimension(50, 25));
        addButton.setPreferredSize(new Dimension(50, 25));
        addButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        
        // Sell button.
        removeButton = new PathfinderButton("<< Remove");       
        removeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        removeButton.setMinimumSize(new Dimension(50, 25));
        removeButton.setPreferredSize(new Dimension(50, 25));
        removeButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        
        // Add the two buttons to a BoxLayout.  This aligns them to be the same size and allows us to easily 
        // specify a gap in between them.
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        buttonPanel.setMaximumSize(new Dimension(120, 75));
        buttonPanel.setOpaque(false);
        buttonPanel.add(addButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonPanel.add(removeButton);
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
        GroupLayout.ParallelGroup overviewGroupH = layout.createParallelGroup().addComponent(overviewLabel).addComponent(overviewPane);
        GroupLayout.ParallelGroup howToGroupH = layout.createParallelGroup().addComponent(howToLabel).addComponent(howToPane);
        GroupLayout.SequentialGroup introGroupH = layout.createSequentialGroup().addGroup(overviewGroupH).addGroup(howToGroupH);
        GroupLayout.ParallelGroup storeGroupH = layout.createParallelGroup().addComponent(storeLabel).addComponent(storeCategories);
        GroupLayout.ParallelGroup inventoryGroupH = layout.createParallelGroup().addComponent(inventoryLabel).addComponent(inventoryCategories);
        GroupLayout.SequentialGroup spellsRemainingGroupH = layout.createSequentialGroup().addComponent(spellsRemaining);
        GroupLayout.ParallelGroup buttonGroupH = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(spellsRemainingGroupH).addComponent(buttonPanel);
        GroupLayout.SequentialGroup equipmentGroupH = layout.createSequentialGroup().addGroup(storeGroupH).addGap(TABLE_GAP).
                addGroup(buttonGroupH).addGap(TABLE_GAP).addGroup(inventoryGroupH);
        GroupLayout.ParallelGroup itemGroupH = layout.createParallelGroup().addComponent(statsLabel).addComponent(statsPane);
        GroupLayout.ParallelGroup selectionGroupH = layout.createParallelGroup().addGroup(equipmentGroupH)
                .addGroup(itemGroupH);
        GroupLayout.SequentialGroup spellbookGroupH = layout.createSequentialGroup().addComponent(spellbookImage)
                .addGroup(selectionGroupH);
        mainHorizontalGroup.addGroup(introGroupH).addGroup(spellbookGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);
           
        // Align components along the vertical axis.
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup overviewGroupV = layout.createSequentialGroup().addComponent(overviewLabel).addComponent(overviewPane);
        GroupLayout.SequentialGroup howToGroupV = layout.createSequentialGroup().addComponent(howToLabel).addComponent(howToPane);
        GroupLayout.ParallelGroup introGroupV = layout.createParallelGroup().addGroup(overviewGroupV).addGroup(howToGroupV);
        GroupLayout.SequentialGroup storeGroupV = layout.createSequentialGroup().addComponent(storeLabel).addComponent(storeCategories);
        GroupLayout.SequentialGroup inventoryGroupV = layout.createSequentialGroup().addComponent(inventoryLabel).addComponent(inventoryCategories);
        GroupLayout.ParallelGroup spellsRemainingGroupV = layout.createParallelGroup().addComponent(spellsRemaining);
        GroupLayout.SequentialGroup buttonGroupV = layout.createSequentialGroup().addGroup(spellsRemainingGroupV).addComponent(buttonPanel);
        GroupLayout.ParallelGroup equipmentGroupV= layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(storeGroupV).addGap(TABLE_GAP).
                addGroup(buttonGroupV).addGap(TABLE_GAP).addGroup(inventoryGroupV);
        GroupLayout.SequentialGroup itemGroupV = layout.createSequentialGroup().addComponent(statsLabel).addComponent(statsPane);
        GroupLayout.SequentialGroup selectionGroupV = layout.createSequentialGroup().addGroup(equipmentGroupV)
                .addGroup(itemGroupV);
        GroupLayout.ParallelGroup spellbookGroupV = layout.createParallelGroup().addComponent(spellbookImage)
                .addGroup(selectionGroupV);
        mainVerticalGroup.addGroup(introGroupV).addGroup(spellbookGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        addButton.setActionCommand("Add Spell");
        addButton.addActionListener(listener);
        removeButton.setActionCommand("Remove Spell");
        removeButton.addActionListener(listener);
    }
    
    
    @Override
    public void updateView() {
        // Updates the level 1 spells in the user's spellbook.
        ArrayList<Spell> levelOneSpells = spellGen.getSelectedLevelOneSpellList();
        levelOneInventoryList.setListData(spellGen.convertNamesToReadable(levelOneSpells));

        // Updates the number of level 1 spells remaining to select
        spellsRemaining.setText(Integer.toString(spellGen.getLevelOneSpellsRemaining()));        
    }
    
    
    /**
     * Adds a new spell to the user's inventory if possible.
     */
    public void addSpell() {
        Spell newSpell = spellGen.getSpell(determineStoreSelection());
        if (newSpell != null) {
            if ((newSpell.getSpellLevel() == 1) && (spellGen.getLevelOneSpellsRemaining() < 1)) {
                // Informs the user that they are out of level 1 spells to choose.
                JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(this);
                PathfinderPopup warning = new PathfinderPopup("Sorry, but you can't choose any more level 1 spells!", 400, 150, frame);
                warning.setLocationRelativeTo(this.getParent());
            }
            else {
                spellGen.addSpell(newSpell);
            }
        }
    }
    
    /**
     * Removes a given spell from the inventory if possible.
     */
    public void removeSpell() {
       Spell newSpell = spellGen.getSpell(determineInventorySelection());
        if (newSpell != null) {
            spellGen.removeSpell(newSpell);
        }
    }
    
    /**
     * This method sets the BaseCharacter for this class to use.
     * @param baseCharacter The BaseCharacter to set.
     */
    public void setBaseCharacter(BaseCharacter baseCharacter) {
        this.baseCharacter = baseCharacter;
    }
    
    /**
     * This method sets the SpellGenerator for this class and updates the display to reflect its information.
     * @param spellGen The SpellGenerator to set.
     */
    public void setSpellGenerator(SpellGenerator spellGen) {
        this.spellGen = spellGen;
        spellGen.fillSeparatedSpellLists();
        
        // Set the available level one spells to choose from.
        ArrayList<Spell> levelOneSpellList = spellGen.getLevelOneSpellList();
        levelOneStoreList.setListData(spellGen.convertNamesToReadable(levelOneSpellList));
        
        // Updates the displayed weapons in the user's inventory.
        ArrayList<Spell> levelOneSpellInv = spellGen.getSelectedLevelOneSpellList();
        levelOneInventoryList.setListData(spellGen.convertNamesToReadable(levelOneSpellInv));
        
        // Set the limit of level 1 spells to select.
        spellGen.setSpellLimit(3 + baseCharacter.getAbilityScoreModifier(AbilityScore.INT));
        spellsRemaining.setText(Integer.toString(spellGen.getLevelOneSpellsRemaining()));
    }
    
    /**
     * This method determines which item is selected in the store.  It first finds which tab is selected, and then 
     * checks if any of the items are selected in that tab. 
     * @return The item name, if one is selected, or an empty string.
     */
    private String determineStoreSelection() {
        int tab = storeCategories.getSelectedIndex();
        String selection = "";
        selection = (String)levelOneStoreList.getSelectedValue();
        return selection;
    }
    
    /**
     * This method determines which spell is selected in the inventory.  It first finds which tab is selected, and then 
     * checks if any of the spells are selected in that tab. 
     * @return The spell name, if one is selected, or an empty string.
     */
    private String determineInventorySelection() {
        String selection = "";
        selection = (String)levelOneInventoryList.getSelectedValue();

        return selection;
    }
    
    /**
     * This private class defines the behavior for the JLists containing the available spells to choose from 
     * and the user-selected spells.  It primarily updates the displayed spell description as a new spell is 
     * selected in the lists.  It handles new mouse selections, as well as changes due to keyboard arrow keys.
     */
    private class SpellSelectionHandler implements ListSelectionListener {    
        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            
            // Update whichever list had a new selection.  Deselect the selection in the other lists if a different list was selected.
            if (!lsm.isSelectionEmpty()) {
                if (levelOneStoreList.getSelectionModel() == lsm) {
                    String newText = "Spell Name: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getSpellName();
                    newText = newText + "\nSpell School: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getSchool();
                    newText = newText + "\nShort Description: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getDescription();
                    newText = newText + "\nLong Description: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getLongDescription();
                    statsText.setText(newText);
                    statsText.setCaretPosition(0);
                    levelZeroInventoryList.removeSelectionInterval(levelZeroInventoryList.getSelectedIndex(), levelZeroInventoryList.getSelectedIndex());
                    levelOneInventoryList.removeSelectionInterval(levelOneInventoryList.getSelectedIndex(), levelOneInventoryList.getSelectedIndex());
                    levelZeroStoreList.removeSelectionInterval(levelZeroStoreList.getSelectedIndex(), levelZeroStoreList.getSelectedIndex());
                }
                else if (levelOneInventoryList.getSelectionModel() == lsm) {
                    String newText = "Spell Name: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getSpellName();
                    newText = newText + "\nSpell School: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getSchool();
                    newText = newText + "\nShort Description: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getDescription();
                    newText = newText + "\nLong Description: " + spellGen.getSpell((String)levelOneStoreList.getSelectedValue()).getLongDescription();
                    statsText.setText(newText);
                    statsText.setCaretPosition(0);
                    levelZeroInventoryList.removeSelectionInterval(levelZeroInventoryList.getSelectedIndex(), levelZeroInventoryList.getSelectedIndex());
                    levelZeroStoreList.removeSelectionInterval(levelZeroStoreList.getSelectedIndex(), levelZeroStoreList.getSelectedIndex());
                    levelZeroStoreList.removeSelectionInterval(levelOneStoreList.getSelectedIndex(), levelOneStoreList.getSelectedIndex());
                }
            }
        }    
    }
}
