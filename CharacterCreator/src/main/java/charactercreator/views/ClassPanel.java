/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Megan
 */
public class ClassPanel extends ContentPanel {
    private UIModel uiModel;
    private JComboBox classSelectionMenu;
    private JTextArea classDescription;
    private JTextArea classFeaturesArea;
    private JLabel classPreview;
    private JScrollPane scrollDescription;
    private JScrollPane scrollFeatures;
    private JLabel selectionLabel;
    private JLabel descriptionLabel;
    private JLabel featuresLabel;
    private PathfinderButton classInfoButton;
    private PathfinderButton featuresInfoButton;
    
    public ClassPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override 
    protected void initializeComponents() {
        // Create the screen components.
        classDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("class"), false, null);
        classFeaturesArea = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("class"), false, null);
        scrollDescription = new TranslucentScrollPane(classDescription, new Dimension(560, 300));
        scrollFeatures = new TranslucentScrollPane(classFeaturesArea, new Dimension(560, 160));
        classPreview = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("class"));
        classPreview.setMinimumSize(new Dimension(390, 500));
        classPreview.setMaximumSize(new Dimension(390, 500));
        String[] items = {"Barbarian", "Bard", "Cleric", "Druid", "Fighter", "Monk", "Paladin", "Ranger", "Rogue", "Sorcerer", "Wizard"};
        classSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        selectionLabel = new PathfinderLabel("Choose your class: ");
        descriptionLabel = new PathfinderLabel("Description");
        featuresLabel = new PathfinderLabel("Class Features");
        classInfoButton = new PathfinderButton("?");
        featuresInfoButton = new PathfinderButton("?");
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(classInfoButton)
                .addComponent(selectionLabel).addComponent(classSelectionMenu);
        GroupLayout.SequentialGroup featuresLabelGroupH = layout.createSequentialGroup().addComponent(featuresLabel).
                addComponent(featuresInfoButton);
        mainHorizontalGroup.addComponent(classPreview).addGroup(layout.createParallelGroup().addGroup(secondaryHorizontalGroup).
                addComponent(descriptionLabel).addComponent(scrollDescription).addGroup(featuresLabelGroupH).addComponent(scrollFeatures));
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(classInfoButton).addComponent(selectionLabel).addComponent(classSelectionMenu);
        GroupLayout.ParallelGroup featuresLabelGroupV = layout.createParallelGroup().addComponent(featuresLabel).
                addComponent(featuresInfoButton);
        mainVerticalGroup.addComponent(classPreview).addGroup(layout.createSequentialGroup().addGroup(secondaryVerticalGroup).
                addComponent(descriptionLabel).addComponent(scrollDescription).addGroup(featuresLabelGroupV).addComponent(scrollFeatures));
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        classSelectionMenu.setActionCommand("Class Selection");
        classSelectionMenu.addActionListener(listener);
        classInfoButton.setActionCommand("More Info - Class");
        classInfoButton.addActionListener(listener);
        featuresInfoButton.setActionCommand("More Info - Class Features");
        featuresInfoButton.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        classDescription.setText(uiModel.getCurrentInformation("class"));
        classDescription.setCaretPosition(0);
        classFeaturesArea.setText(uiModel.getCurrentFeature("class"));
        classFeaturesArea.setCaretPosition(0);
        classPreview.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("class"))));
    }
    
    public String getSelectedClass() {
        return (String)classSelectionMenu.getSelectedItem();
    }
    
}
