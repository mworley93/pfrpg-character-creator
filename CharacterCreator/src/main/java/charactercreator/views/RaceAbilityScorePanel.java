/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.AbilityScore;
import charactercreator.models.BaseCharacter;
import charactercreator.models.RaceAbilityScoreScreenModel;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.EnumMap;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

/**
 *
 * @author Brandon
 */
public class RaceAbilityScorePanel extends ContentPanel {
    
    private BaseCharacter character;
    private String currentRaceInstructions;
    
    private EnumMap<AbilityScore, Integer> baseStatsMap = new EnumMap<>(AbilityScore.class);
    
    
    
    private UIModel uiModel;
    private JComboBox abilityScoreSelectionMenu;
    private JLabel overviewLabel;
    private JLabel selectionLabel;
    private JLabel tableLabel;
    private JLabel instructionsLabel;
    private JTextArea overview;
    private JTextArea instructions;
    private JScrollPane overviewScroll;
    private JScrollPane instructionsScroll;
    private TranslucentScrollPane scoreTable;
    private JLabel racialImage;
    
    private RaceAbilityScoreScreenModel model = new RaceAbilityScoreScreenModel();
    
    public RaceAbilityScorePanel(UIModel uiModel) {
        this.uiModel = uiModel;
        currentRaceInstructions = "Human";        
        initializePanel();
        initializeAbilityScoreMap();
    }
    
    private void initializeAbilityScoreMap() {
        baseStatsMap.put(AbilityScore.CHA, 0);
        baseStatsMap.put(AbilityScore.CON, 0);
        baseStatsMap.put(AbilityScore.DEX, 0);
        baseStatsMap.put(AbilityScore.INT, 0);
        baseStatsMap.put(AbilityScore.STR, 0);
        baseStatsMap.put(AbilityScore.WIS, 0);
    }
        
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        overview = guiMethods.createPlainTextArea(uiModel.getRaceAbilityScoreOverview(), false, null);
        overviewScroll = new TranslucentScrollPane(overview, new Dimension(490, 190));
        instructions = guiMethods.createPlainTextArea(getRaceInstructions(), false, null);
        instructionsScroll = new TranslucentScrollPane(instructions, new Dimension(470, 190));
        String[] items = {"STR", "DEX", "CON", "INT", "WIS", "CHA"};
        abilityScoreSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        overviewLabel = new PathfinderLabel("What are Racial Bonuses?");
        selectionLabel = new PathfinderLabel("Select a score to add the bonus to: ");
        tableLabel = new PathfinderLabel("Ability Score Worksheet");
        instructionsLabel = new PathfinderLabel("What does your race give you?");
        String[] tableHeaders = {"Ability Score", "Total", "Modifier", "Base", "Racial Bonus"};
        scoreTable = guiMethods.createScrollingTable(model.getTable(), tableHeaders, new Dimension(490, 180));
        racialImage = guiMethods.constructImageLabel(uiModel.getRacialAbilitiesImage("Human"));
        racialImage.setMinimumSize(new Dimension(470, 340));
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(selectionLabel).
                addComponent(abilityScoreSelectionMenu);
        
        mainHorizontalGroup.addGroup(layout.createParallelGroup().addComponent(overviewLabel).addComponent(overviewScroll).
                addGroup(secondaryHorizontalGroup).addComponent(tableLabel).addComponent(scoreTable));
        mainHorizontalGroup.addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup().addComponent(instructionsLabel).addComponent(instructionsScroll)
                .addComponent(racialImage)));
        
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup();
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup().
                addComponent(selectionLabel).addComponent(abilityScoreSelectionMenu);
        
        mainVerticalGroup.addGroup(layout.createSequentialGroup().addComponent(overviewLabel).addComponent(overviewScroll).
            addGroup(secondaryVerticalGroup).addComponent(tableLabel).addComponent(scoreTable));
        mainVerticalGroup.addGroup(layout.createParallelGroup().addGroup(layout.createSequentialGroup().addComponent(instructionsLabel).addComponent(instructionsScroll)
                .addComponent(racialImage)));
        

        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        abilityScoreSelectionMenu.setActionCommand("Race Ability Score Selection");
        abilityScoreSelectionMenu.addActionListener(listener);
    }
    
    @Override public void updateView() {
        this.currentRaceInstructions = character.getCharacterRace().getRaceName();
        
        character.resetBaseAbilityScoreMap();
        character.resetRaceAbilityScoreMap();
        character.resetFinalAbilityScoreMap();
        character.resetAbilityScoreModifiers();
        
        character.setBaseAbilityScores(baseStatsMap);
        
        this.instructions.setText(getRaceInstructions());
        
        
        if (getsToChoose()) {
            character.setRaceAbilityScore(AbilityScore.valueOf(abilityScoreSelectionMenu.getSelectedItem().toString()), 2);
        }
        
        else {
            addRaceAbilityScoreBonus();
        }
        
        character.calculateFinalAbilityScores();
        character.calculateAbilityModifiers();

        this.model.updateTableForCharacter(character);           
        this.removeAll();
     
        racialImage = guiMethods.constructImageLabel(uiModel.getRacialAbilitiesImage(character.getCharacterRace().getRaceName()));
        racialImage.setMinimumSize(new Dimension(470, 340));
        initializeLayout();   
        if (!getsToChoose()) {
            abilityScoreSelectionMenu.hide();
            selectionLabel.hide();          
        }
        else {
            abilityScoreSelectionMenu.show();
            selectionLabel.show();
        }
        
    }
    
    public void setBaseCharacter(BaseCharacter character) {
        this.character = character;
    }
    
    public void setBaseStats() {
        baseStatsMap.putAll(character.getBaseAbilityScores());
    }
    
    public void resetPanel() {
        character.setBaseAbilityScores(baseStatsMap);
    }
    
    private String getRaceInstructions() {
        
        if(this.currentRaceInstructions.matches("Human")) {
            return "Humans get a +2 bonus to one ability score of their choice to represent their varied nature. Please select an ability score with the dropdown menu to the left!";
        }
        
        if(this.currentRaceInstructions.matches("Dwarf")) {
            return "Dwarves are both tough and wise, but they are also a bit gruff. They have +2 to Constitution and Wisdom, but a -2 to Charisma.";
        }
        
        if(this.currentRaceInstructions.matches("Elf")) {
            return "Elves are nimble, both in body and mind, but their physical form is frail. They have a +2 to Dexterity and Intelligence, but a -2 to Constitution.";
        }
        
        if(this.currentRaceInstructions.matches("Gnome")) {
            return "Gnomes are physically weak, but surprisingly hardy. In addition, their attitudes make them naturally agreeable. They receive a +2 to Constitution and Charisma, but a -2 to Strength.";
        }
        
        if(this.currentRaceInstructions.matches("Half-elf")) {
            return "Half-elves get a +2 bonus to one ability score of their choice to represent their varied nature. Please select an ability score with the dropdown menu to the left!";
        }
        
        if(this.currentRaceInstructions.matches("Half-orc")) {
            return "Half-orc characters get a +2 bonus to one ability score of their choice at creation to represent their varied nature. Please select an ability score with the dropdown menu to the left!";
        }        
        
        if(this.currentRaceInstructions.matches("Halfling")) {
            return "Halflings are nimble and strong-willed, but their small stature makes them weaker than other races. They get a +2 to Dexterity and Charisma, but a -2 to Strength.";
        }      
        
        return null;
    }
    
    private Boolean getsToChoose() {
        if(this.character.getCharacterRace().getRaceName().matches("Human") || this.character.getCharacterRace().getRaceName().matches("Half-elf") || this.character.getCharacterRace().getRaceName().matches("Half-orc")) {
            return true;
        }
        return false;
    }
    
    private void addRaceAbilityScoreBonus() {
        if(this.character.getCharacterRace().getRaceName().matches("Dwarf")) {
            character.setRaceAbilityScore(AbilityScore.CON, 2);
            character.setRaceAbilityScore(AbilityScore.WIS, 2);
            character.setRaceAbilityScore(AbilityScore.CHA, -2);
        }
        
        if(this.character.getCharacterRace().getRaceName().matches("Elf")) {
            character.setRaceAbilityScore(AbilityScore.DEX, 2);
            character.setRaceAbilityScore(AbilityScore.INT, 2);
            character.setRaceAbilityScore(AbilityScore.CON, -2);
        }
        
        if(this.character.getCharacterRace().getRaceName().matches("Gnome")) {
            character.setRaceAbilityScore(AbilityScore.CON, 2);
            character.setRaceAbilityScore(AbilityScore.CHA, 2);
            character.setRaceAbilityScore(AbilityScore.STR, -2);
        }
        
        if(this.character.getCharacterRace().getRaceName().matches("Halfling")) {
            character.setRaceAbilityScore(AbilityScore.DEX, 2);
            character.setRaceAbilityScore(AbilityScore.CHA, 2);
            character.setRaceAbilityScore(AbilityScore.STR, -2);
        }        
    }
}
