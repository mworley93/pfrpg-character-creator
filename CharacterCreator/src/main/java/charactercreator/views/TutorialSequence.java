/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.models.UIModel;
import charactercreator.components.FadeDialog;
import charactercreator.components.ImageButton;
import charactercreator.components.TranslucentTextPane;
import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderLabel;
import charactercreator.helpers.GUIHelper;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.Color;
import java.awt.RadialGradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Point;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Megan
 */
public class TutorialSequence extends JPanel {
    GUIHelper guiMethods;
    UIModel uiModel;
    JPanel blankPanel;
    CreationPanel tutorialPanel;
    boolean initialized;
    
    // Components that will become visible on during tutorial stages.
    PathfinderLabel overviewLabel;
    TranslucentTextPane overviewPane;
    PathfinderButton moreInfoButton;
    JLabel howToLabel;
    TranslucentTextPane howToPane;
    
    // Tutorial components and variables.
    FadeDialog currentDialog;
    JLabel arrowLowerLeft;
    JLabel arrowLowerLeftUnder;
    JLabel arrowUpperLeftOver;
    JLabel arrowUpperLeftUnder;
    JLabel arrowUpperRight;
    JLabel currentArrow;
    Rectangle focus;
    Rectangle arrowBound;
    Point dialogLocation;
    int currentSection = 1;
    boolean focusNeeded = false;
    boolean arrowNeeded = false;
    ActionListener exitListener;
    
    // Tutorial constants.
    private static final int NUM_STAGES = 7;
    private static final int JDIALOG_WIDTH = 400;
    private static final int JDIALOG_HEIGHT = 150;
    private static final int ARROW_WIDTH = 200;
    private static final int ARROW_HEIGHT = 200;
    
    /**
     * Default constructor.
     */
    public TutorialSequence() {
        initialized = false;
    }
    
    public void initialize(UIModel uiModel) {
        this.uiModel = uiModel;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setOpaque(false); 
        guiMethods = new GUIHelper();
        blankPanel = new JPanel();
        blankPanel.setOpaque(false);
        addPanelComponents();
        tutorialPanel = new CreationPanel("Tutorial Panel", "Getting Started:", blankPanel);
        disableButtons();
        this.add(tutorialPanel);
        initialized = true;
    }
    
    /**
     * 
     */
    private void addPanelComponents() {
        overviewLabel = new PathfinderLabel("Overview");
        overviewPane = new TranslucentTextPane("", new Dimension(400, 200), StyleConstants.ALIGN_CENTER);
        moreInfoButton = new PathfinderButton("?");
        howToLabel = new PathfinderLabel("How-To Use...");
        howToPane = new TranslucentTextPane("", new Dimension(400, 200), StyleConstants.ALIGN_CENTER);

        arrowLowerLeft = guiMethods.constructImageLabel(uiModel.getTutorialImage("Lower Left"));
        arrowLowerLeftUnder = guiMethods.constructImageLabel(uiModel.getTutorialImage("Lower Left Under"));
        arrowUpperLeftUnder = guiMethods.constructImageLabel(uiModel.getTutorialImage("Upper Left Under"));
        arrowUpperLeftOver = guiMethods.constructImageLabel(uiModel.getTutorialImage("Upper Left Over"));
        arrowUpperRight = guiMethods.constructImageLabel(uiModel.getTutorialImage("Upper Right"));
        
        GroupLayout layout = guiMethods.constructGroupLayout(blankPanel);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup labelGroupH = layout.createSequentialGroup().addComponent(overviewLabel).
                addComponent(moreInfoButton);
        GroupLayout.ParallelGroup secondaryHGroup = layout.createParallelGroup().addGroup(labelGroupH).addComponent(overviewPane).addComponent(howToLabel).
                addComponent(howToPane);
        mainHorizontalGroup.addGroup(secondaryHGroup).addGap(500);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis.
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup labelGroupV = layout.createParallelGroup().addComponent(overviewLabel).
                addComponent(moreInfoButton);
        mainVerticalGroup.addGroup(labelGroupV).addComponent(overviewPane).addComponent(howToLabel).
                addComponent(howToPane);
        layout.setVerticalGroup(mainVerticalGroup);
    }   
    
    /**
     * Disables the buttons on the embedded creation panel, since they're just used for display here.
     */
    private void disableButtons() {
        ImageButton prevButton = tutorialPanel.getPreviousButton();
        prevButton.disableMouseEvents();
        ImageButton nextButton = tutorialPanel.getNextButton();
        nextButton.disableMouseEvents();
        tutorialPanel.getDecisionsButton().disableMouseEvents();
    }
    
    /**
     * Starts the tutorial sequence.  Should be called when the user indicates that they would like to see the tutorial.
     */
    public void start() {
        currentSection = 1;
        focus = new Rectangle(0, 0, 0, 0);
        arrowBound = new Rectangle(0, 0, ARROW_WIDTH, ARROW_HEIGHT);
        dialogLocation = new Point(0, 0);
        focusNeeded = false;
        arrowNeeded = false;
        // Kind of a hack to fix the painting on the more info button during this sequence.
        launchSection(currentSection);
    }
    
    /**
     * Launches a new section of the tutorial.
     * @param num The number of the stage that needs to be executed.
     */
    private void launchSection(int num) {
          JFrame parent = (JFrame)SwingUtilities.getRoot(this);
          currentDialog = new FadeDialog(new Dimension(JDIALOG_WIDTH, JDIALOG_HEIGHT), parent);
          currentDialog.addWindowListener(new FadeAdapter());
          MinimizeAdapter minimizeAdapter = new MinimizeAdapter();
          parent.addWindowListener(minimizeAdapter);
          AttachedAdapter attachedAdapter = new AttachedAdapter(parent, currentDialog);
          parent.addComponentListener(attachedAdapter);
          currentDialog.addComponentListener(attachedAdapter);
        
        if (num > 0 && num <= NUM_STAGES) {
            switch(num) {
                case 1:
                    launchSectionOne();
                    break;
                case 2:
                    launchSectionTwo();
                    break;
                case 3:
                    launchSectionThree();
                    break;
                case 4:
                    launchSectionFour();
                    break;
                case 5:
                    launchSectionFive();
                    break;
                case 6:
                    launchSectionSix();
                    break;
                case 7:
                    launchSectionSeven();
                    break;
            }

            currentDialog.fadeIn();
        }
        else if (num > NUM_STAGES) {
            parent.removeComponentListener(attachedAdapter);
            parent.removeWindowListener(minimizeAdapter);
            ActionEvent exitEvent = new ActionEvent(parent, ActionEvent.ACTION_PERFORMED, "Exit Tutorial");
            exitListener.actionPerformed(exitEvent);
        }
    }
    
    public void attachActionListener(ActionListener listener) {
        exitListener = listener;
    }
    
    /**
     * Returns the initialized status of the tutorial sequence.
     * @return 
     */
    public boolean isInitialized() {
        return initialized;
    }
    
    /**
     * Launches the first section of the tutorial, which is just a simple message in the center. 
     * @param text The text to display.
     */
    private void launchSectionOne() {
        currentDialog.setDetails(1 + "/" + NUM_STAGES, uiModel.getTutorialMessage("Message 1"));
        dialogLocation.setLocation(getWidth() / 2 - JDIALOG_WIDTH / 2, getHeight()/ 2 - JDIALOG_HEIGHT / 2);
        Point compPoint = dialogLocation.getLocation();
        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
        currentDialog.setLocation(compPoint);
        repaint();
    }
    
    /**
     * Launches the second section of the tutorial, which is an explanation of the title bar.  
     */
    private void launchSectionTwo() {
        focusNeeded = true;
        arrowNeeded = true;
        currentDialog.setDetails(2 + "/" + NUM_STAGES, uiModel.getTutorialMessage("Message 2"));
        Component titleBar = tutorialPanel.getComponentAt(new Point(0, 0));
        int focusX = 0 - titleBar.getWidth() / 2;
        int focusY = 0 - titleBar.getHeight() / 2;
        focus.setRect(focusX, focusY, titleBar.getWidth() * 2, titleBar.getHeight() * 2);  
        arrowBound.setLocation(titleBar.getWidth() / 6, 7 * titleBar.getHeight() / 8);
        dialogLocation.setLocation(arrowBound.x + ARROW_WIDTH, arrowBound.y + ARROW_HEIGHT - 50);
        Point compPoint = dialogLocation.getLocation();
        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
        currentDialog.setLocation(compPoint);
        currentArrow = arrowUpperLeftUnder;
        repaint();
    }
    
    /**
     * Launches the third section of the tutorial, which is an explanation of the navigation bar.  
     */
    private void launchSectionThree() {
        currentDialog.setDetails(3 + "/" + NUM_STAGES, uiModel.getTutorialMessage("Message 3"));
        Component navBar = tutorialPanel.getComponentAt(0, tutorialPanel.getHeight() - 1);
        int focusX = 0 - navBar.getWidth() / 2;
        int focusY = navBar.getY() - navBar.getHeight() / 2;
        focus.setRect(focusX, focusY, navBar.getWidth() * 2, navBar.getHeight() * 2);
        arrowBound.setLocation(navBar.getWidth() / 8, navBar.getY() - ARROW_HEIGHT);
        dialogLocation.setLocation(arrowBound.x + ARROW_WIDTH, arrowBound.y);
        Point compPoint = dialogLocation.getLocation();
        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
        currentDialog.setLocation(compPoint);
        currentArrow = arrowLowerLeft;
        repaint();
        tutorialPanel.getDecisionsButton().setEnabled(false);
        tutorialPanel.getDecisionsButton().setEnabled(true);
    }
    
    /**
     * Launches the fourth section of the tutorial, which is an explanation of the decision pop-up.
     */
    private void launchSectionFour() {
        currentDialog.setDetails(4 + "/" + NUM_STAGES, uiModel.getTutorialMessage("Message 4"));
        // TODO: Change focus to be based on the actual decision pop up button.
        int focusX = 7 * this.getWidth() / 8;
        int focusY = -this.getHeight() / 16;
        focus.setRect(focusX, focusY, this.getWidth() / 6, this.getWidth() / 6);
        arrowBound.setLocation(focusX - ARROW_WIDTH / 2, focusY + this.getWidth() / 8);
        dialogLocation.setLocation(arrowBound.x - ARROW_WIDTH * 2, arrowBound.y + ARROW_HEIGHT - 50);
        Point compPoint = dialogLocation.getLocation();
        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
        currentDialog.setLocation(compPoint);
        currentArrow = arrowUpperRight;
        repaint();
    }
    
    /**
     * Launches the fifth section of the tutorial, which is an explanation of the overview boxes.  Calculates where the box is, which should be at the most right x coordinate 
     * of it and halfway through its width.
     */
    private void launchSectionFive() {
        currentDialog.setDetails(5 + "/" + NUM_STAGES, uiModel.getTutorialMessage("Message 5"));
        int focusX = overviewPane.getX() - overviewPane.getWidth() / 2;
        int focusY = overviewPane.getY() - overviewPane.getHeight() / 2;
        int arrowX = overviewPane.getX() + overviewPane.getWidth();
        int arrowY = overviewPane.getY() + overviewPane.getHeight() / 2;
        Point relLocation = SwingUtilities.convertPoint(overviewPane.getParent(), focusX, focusY, 
                tutorialPanel);
        Point arrowLocation = SwingUtilities.convertPoint(overviewPane.getParent(), arrowX, arrowY, 
                tutorialPanel);
        focus.setRect(relLocation.x, relLocation.y, overviewPane.getWidth() * 2, overviewPane.getHeight() * 2);
        arrowBound.setLocation(arrowLocation.x, arrowLocation.y);
        dialogLocation.setLocation(arrowLocation.x, arrowLocation.y + ARROW_HEIGHT);
        Point compPoint = dialogLocation.getLocation();
        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
        currentDialog.setLocation(compPoint);
        currentArrow = arrowUpperLeftOver;
        moreInfoButton.setEnabled(false);
        moreInfoButton.setEnabled(true);
        repaint();
    }
    
    /**
     * Launches the sixth section of the tutorial, which is an explanation of the More Info buttons.  Calculates its location based on the lower left corner of the button.
     */
    private void launchSectionSix() {
        currentDialog.setDetails(6 + "/" + NUM_STAGES, uiModel.getTutorialMessage("Message 6"));
        int focusX = moreInfoButton.getX() - moreInfoButton.getWidth() / 2;
        int focusY = moreInfoButton.getY() - moreInfoButton.getHeight() / 2;
        int arrowX = moreInfoButton.getX() + moreInfoButton.getWidth();
        int arrowY = moreInfoButton.getY() - moreInfoButton.getHeight();
        Point relLocation = SwingUtilities.convertPoint(moreInfoButton.getParent(), focusX, focusY, tutorialPanel);
        Point arrowLocation = SwingUtilities.convertPoint(moreInfoButton.getParent(), arrowX, arrowY, 
                tutorialPanel);
        focus.setRect(relLocation.x, relLocation.y, moreInfoButton.getWidth() * 2, moreInfoButton.getHeight() * 2);
        arrowBound.setLocation(arrowLocation.x, arrowLocation.y);
        dialogLocation.setLocation(arrowBound.x + ARROW_WIDTH - 50, arrowBound.y + ARROW_HEIGHT);
        Point compPoint = dialogLocation.getLocation();
        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
        currentDialog.setLocation(compPoint);
        currentArrow = arrowUpperLeftOver;
        repaint();
    }
    
    /**
     * Launches the seventh section of the tutorial, which is an explanation of the How-To boxes. 
     */
    private void launchSectionSeven() {
        currentDialog.setDetails(7 + "/" + NUM_STAGES, uiModel.getTutorialMessage("Message 7"));
        int focusX = howToPane.getX() - howToPane.getWidth() / 2;
        int focusY = howToPane.getY() - howToPane.getHeight() / 2;
        int arrowX = howToPane.getX() + howToPane.getWidth();
        int arrowY = howToPane.getY();
        Point relLocation = SwingUtilities.convertPoint(howToPane.getParent(), focusX, focusY, tutorialPanel);
        Point arrowLocation = SwingUtilities.convertPoint(howToPane.getParent(), arrowX, arrowY, 
                tutorialPanel);
        focus.setRect(relLocation.x, relLocation.y, howToPane.getWidth() * 2, howToPane.getHeight() * 2);
        arrowBound.setLocation(arrowLocation.x, arrowLocation.y);
        dialogLocation.setLocation(arrowBound.x, arrowBound.y - ARROW_HEIGHT + 50);
        Point compPoint = dialogLocation.getLocation();
        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
        currentDialog.setLocation(compPoint);
        currentArrow = arrowLowerLeftUnder;
        repaint();
    }
    
    /**
     * This private class is responsible for keeping track of if the current stage of the tutorial has closed.  When it does, it moves to the next section.
     */
    private class FadeAdapter extends WindowAdapter {
        @Override
        public void windowClosed(WindowEvent e) {
            currentSection++;
            launchSection(currentSection);
        }
    }
    
     /**
     * This private class is used to keep the JDialog attached to the parent window it hovers over.  It should 
     * move with the parent window when it is moved.
     * Assistance from: http://www.devx.com/tips/Tip/30528
     */
    private class AttachedAdapter extends ComponentAdapter{
        private Window winA, winB;
        
        public AttachedAdapter(JFrame winA, JDialog winB){
            this.winA = winA;
            this.winB = winB;
        }
        
        @Override
        public void componentMoved(ComponentEvent e) {
            Window win = (Window) e.getComponent();
            if(win==winA){
                winB.removeComponentListener(this);
                if (winA.getX() + winA.getWidth() > java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth() || 
                        winA.getY() + winA.getHeight() > java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight() ||
                        winA.getX() < 0 || winA.getY() < 0) {
                    if (focusNeeded) {
                        Point compPoint = dialogLocation.getLocation();
                        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
                        winB.setLocation(compPoint.x, compPoint.y);
                        repaint();
                    }
                    else {
                        winB.setLocationRelativeTo(winA);
                    }
                }
                else {
                    if (focusNeeded) {
                        Point compPoint = dialogLocation.getLocation();
                        SwingUtilities.convertPointToScreen(compPoint, tutorialPanel.getParent());
                        winB.setLocation(compPoint.x, compPoint.y);
                        repaint();
                    }
                    else {
                        winB.setLocationRelativeTo(winA);
                    }
                }
                winB.addComponentListener(this);
            } 
        }
    };
    
    /**
     * This private class starts another thread that tells the tutorial dialog to wait until the main window has maximized to reappear.
     */
    private class MinimizeAdapter extends WindowAdapter {
            @Override
            public void windowIconified(WindowEvent e){
                  currentDialog.setVisible(false);
            }
            
            @Override
            public void windowDeiconified(WindowEvent e){
                TutorialSequence.MaximizeWait process = new TutorialSequence.MaximizeWait();
                process.execute();
            }
    };
    
    /**
     * This background thread waits for a set number of seconds.  It sets the dialog to reappear after the main window has maximized.  Without this waiting,
     * the dialog will pop up prematurely when the main window is minimized and maximized.
     */
    private class MaximizeWait extends SwingWorker<Void, String> {
        public MaximizeWait() {
        }

        @Override
        protected Void doInBackground() throws Exception {
            try {
                Thread.sleep(250);
            }
            catch (Exception ex) {

            }
            return (null);
        }

        @Override
        protected void done() {
            super.done();
            currentDialog.setVisible(true);
        }
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g.create();
        
        // Simply draw the entire screen out of focus if no focus is needed.
        if (!focusNeeded) {
            g2.setPaint(new Color(255,255,255,115));
            g2.fillRect(0, 0, getWidth(), getHeight());
            g2.dispose();
            return;
        }

        // Determine the radius of the gradient that will be the blended focused spot.  Choose the shorter side of the area.  This should be done 
        // because it will result in a circle that can be "stretched" into an oval on the side that is too short, and scaling up avoids strange 
        // errors more easily than scaling down.  
        int radius = focus.height;
        int scaleRatioX = 1;
        int scaleRatioY = 1;
        if (focus.height >  focus.width) {
            radius = focus.width;
            scaleRatioY = focus.height / radius;
        }
        else if (focus.height < focus.width) {
            radius = focus.height;
            scaleRatioX = focus.width / radius;
        }
        
        // The concatentation code looks backwards, but that's because it's a stack-implementation, not a queue.  Steps that should be done:
        // 1. Move the gradient to the origin.  It starts off at the center of the focused area, so we can move it backwards that many units.
        // 2. Scale the circular gradient to fit the focus area.
        // 3. Move the gradient back to its original location in the focus area.
        // 4. Set the transform containing all of these operations to the graphics object.
        AffineTransform moveToOrigin = AffineTransform.getTranslateInstance(-focus.getCenterX(), -focus.getCenterY());
        AffineTransform scale = AffineTransform.getScaleInstance(scaleRatioX, scaleRatioY);
        AffineTransform moveToLocation = AffineTransform.getTranslateInstance(focus.getCenterX(), focus.getCenterY());
        AffineTransform at = new AffineTransform();  // This is the transform that will hold the operations of the previous three.
        at.concatenate(moveToLocation);
        at.concatenate(scale);
        at.concatenate(moveToOrigin);
        g2.setTransform(at);
        
        // Set up the gradient's appearance, which blends the tranlucent overlay with the clear spot that is in focus.
        Color[] colors = { new Color(255, 255, 255, 0), new Color (255, 255, 255, 115) };
        float[] frac = { 0.35f, .55f};
        g2.setPaint(new RadialGradientPaint(focus.x + focus.width / 2, focus.y + focus.height / 2, radius, frac, colors));
        
        // Draw the gradient to the screen.
        g2.fillRect(0, 0, getWidth(), getHeight());    
        if (arrowNeeded) {
            Icon icon = currentArrow.getIcon();
            BufferedImage image = new BufferedImage(icon.getIconWidth(),
                            icon.getIconHeight(),BufferedImage.TYPE_INT_RGB);
                g2 = image.createGraphics();
                // paint the Icon to the BufferedImage.
                icon.paintIcon(null, g, arrowBound.x,arrowBound.y);
        }
        g2.dispose();
    }
   
}
    
  