/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderLabel;
import charactercreator.models.AbilityScoreGenerator;
import javax.swing.JLabel;

/**
 * This abstract class is responsible provides an outline for a JPanel containing the sub-user interfaces for the specific techniques available for 
 * generating ability scores.  Each panel holds a different set of buttons and labels associated with that technique.  
 * @author Megan Worley, Brandon Sharp
 */
public abstract class AbilityScoreSubPanel extends ContentPanel {
    
    protected AbilityScoreGenerator scoreGen;
    protected JLabel assignLabel;
    protected JLabel strLabel;
    protected JLabel dexLabel;
    protected JLabel conLabel;
    protected JLabel intLabel;
    protected JLabel wisLabel;
    protected JLabel chaLabel;
    
    /**
     * Main constructor.
     * @param model The AbilityScoreGenerator that holds the needed data.
     */
    public AbilityScoreSubPanel() {
        super();
    }
    
    /**
     * Called by the controller after the ability score generator has been instantiated to create the components and set them to the layout.
     */
    public abstract void initializePanel(AbilityScoreGenerator scoreGen);
    
    /**
     * Initializes the ability score labels.
     */
    protected void createLabels() {
        strLabel = new PathfinderLabel("STR");
        dexLabel = new PathfinderLabel("DEX");
        conLabel = new PathfinderLabel("CON");
        intLabel = new PathfinderLabel("INT");
        wisLabel = new PathfinderLabel("WIS");
        chaLabel = new PathfinderLabel("CHA");
    }
}


