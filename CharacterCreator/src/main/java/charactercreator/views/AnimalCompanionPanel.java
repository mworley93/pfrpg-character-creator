/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;

/**
 *
 * @author Brandon
 */
public class AnimalCompanionPanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox animalCompanionSelectionMenu;
    private JTextArea animalCompanionOverview;
    private JTextArea animalCompanionDescription;
    private JLabel animalCompanionImage;
    private JLabel animalCompanionOverviewLabel;
    private JLabel animalCompanionSelectionLabel;
    private JLabel animalCompanionDescriptionLabel;
    private JScrollPane animalCompanionOverviewScroll;
    private JScrollPane animalCompanionScrollDescription;    
    
    public AnimalCompanionPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        animalCompanionOverview = guiMethods.createPlainTextArea(uiModel.getAnimalCompanionOverview(), false, null);
        animalCompanionOverviewScroll = new TranslucentScrollPane(animalCompanionOverview, new Dimension(550, 120));
        animalCompanionDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("animalCompanion"), false, null);
        animalCompanionScrollDescription = new TranslucentScrollPane(animalCompanionDescription, new Dimension (550, 220));
        animalCompanionImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("animalCompanion"));
        animalCompanionImage.setMinimumSize(new Dimension(400, 475));
        String[] items = {"Ape", "Badger", "Bear", "Bird", "Boar", "Camel", "Big Cat", "Small Cat", "Crocodile", "Dinosaur", "Dog", "Horse", "Pony", "Shark", "Constrictor", "Viper", "Wolf"};
        animalCompanionSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        animalCompanionSelectionLabel = new PathfinderLabel("Choose your Animal Companion: ");
        animalCompanionDescriptionLabel = new PathfinderLabel("Animal Description");
        animalCompanionOverviewLabel = new PathfinderLabel("Animal Companions");           
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(animalCompanionSelectionLabel).
                addComponent(animalCompanionSelectionMenu);
        
        mainHorizontalGroup.addComponent(animalCompanionImage).addGroup(layout.createParallelGroup().addComponent(animalCompanionOverviewLabel).addComponent(animalCompanionOverviewScroll).
                addGroup(secondaryHorizontalGroup).addComponent(animalCompanionDescriptionLabel).addComponent(animalCompanionScrollDescription));
        
        layout.setHorizontalGroup(mainHorizontalGroup);         

       GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(Alignment.CENTER);
       GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(Alignment.CENTER).
                addComponent(animalCompanionSelectionLabel).addComponent(animalCompanionSelectionMenu);
       
        mainVerticalGroup.addComponent(animalCompanionImage).addGroup(layout.createSequentialGroup().addComponent(animalCompanionOverviewLabel).addComponent(animalCompanionOverviewScroll).addGroup(secondaryVerticalGroup).
                addComponent(animalCompanionDescriptionLabel).addComponent(animalCompanionScrollDescription));
        
        layout.setVerticalGroup(mainVerticalGroup);             
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        animalCompanionSelectionMenu.setActionCommand("Animal Companion Selection");
        animalCompanionSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        animalCompanionDescription.setText(uiModel.getCurrentInformation("animalCompanion"));
        animalCompanionDescription.setCaretPosition(0);
        animalCompanionImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("animalCompanion"))));
    }
    
    public String getSelectedAnimalCompanion() {
        return (String) animalCompanionSelectionMenu.getSelectedItem();
    }
}
