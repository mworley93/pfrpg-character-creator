/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Brandon
 */
public class FavoredEnemyPanel extends ContentPanel {
 
    private UIModel uiModel;
    
    private JComboBox favoredEnemySelectionMenu;
    private JTextArea favoredEnemyOverview;
    private JTextArea favoredEnemyDescription;
    private JLabel favoredEnemyImage;
    private JLabel favoredEnemyLabel;
    private JLabel favoredEnemySelectionLabel;
    private JLabel favoredEnemyDescriptionLabel;
    private JScrollPane favoredEnemyOverviewScroll;
    private JScrollPane favoredEnemyScrollDescription;
    
    public FavoredEnemyPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        favoredEnemyOverview = guiMethods.createPlainTextArea(uiModel.getFavoredEnemyOverview(), false, null);
        favoredEnemyOverviewScroll = new TranslucentScrollPane(favoredEnemyOverview, new Dimension(570, 200));
        favoredEnemyDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("favoredEnemy"), false, null);
        favoredEnemyScrollDescription = new TranslucentScrollPane(favoredEnemyDescription, new Dimension (570, 140));
        favoredEnemyImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("favoredEnemy"));
        favoredEnemyImage.setMinimumSize(new Dimension(390, 540));
        String[] items = {"Aberration", "Animal", "Construct", "Dragon", "Fey", "Aquatic Humanoid", "Dwarf", "Elf", "Giant", "Goblinoid", "Gnoll", "Gnome", "Halfling", "Human", "Orc", "Reptillian Humanoid", "Magical Beast", "Ooze", "Outsider", "Plant", "Undead", "Vermin"};
        favoredEnemySelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        favoredEnemySelectionLabel = new PathfinderLabel("Choose your Favored Enemy: ");
        favoredEnemyDescriptionLabel = new PathfinderLabel("Enemy Description");
        favoredEnemyLabel = new PathfinderLabel("Favored Enemy");        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(favoredEnemySelectionLabel).
                addComponent(favoredEnemySelectionMenu);
        
        mainHorizontalGroup.addComponent(favoredEnemyImage).addGroup(layout.createParallelGroup().addComponent(favoredEnemyLabel).addComponent(favoredEnemyOverviewScroll).
                addGroup(secondaryHorizontalGroup).addComponent(favoredEnemyDescriptionLabel).addComponent(favoredEnemyScrollDescription));
        
        layout.setHorizontalGroup(mainHorizontalGroup);
        
       GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(Alignment.CENTER);
       GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(Alignment.CENTER).
                addComponent(favoredEnemySelectionLabel).addComponent(favoredEnemySelectionMenu);
       
        mainVerticalGroup.addComponent(favoredEnemyImage).addGroup(layout.createSequentialGroup().addComponent(favoredEnemyLabel).addComponent(favoredEnemyOverviewScroll).addGroup(secondaryVerticalGroup).
                addComponent(favoredEnemyDescriptionLabel).addComponent(favoredEnemyScrollDescription));
        
        layout.setVerticalGroup(mainVerticalGroup);          
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        favoredEnemySelectionMenu.setActionCommand("Favored Enemy Selection");
        favoredEnemySelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        favoredEnemyDescription.setText(uiModel.getCurrentInformation("favoredEnemy"));
        favoredEnemyDescription.setCaretPosition(0);
        favoredEnemyImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("favoredEnemy"))));
    }
    
    public String getSelectedFavoredEnemy() {
        return (String) favoredEnemySelectionMenu.getSelectedItem();
    }
}
