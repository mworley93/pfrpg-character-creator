/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.ImageButton;
import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentTextPane;
import charactercreator.models.AbilityScore;
import charactercreator.models.AbilityScoreGenerator;
import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.text.StyleConstants;

/**
 * Constructs the panel containing the user interface for the Pathfinder "Standard" method of generating ability scores. See ContentPanel.java
 * for documentation.
 * @author Megan Worley, Brandon Sharp
 */
public class RollPanel extends AbilityScoreSubPanel {
    
    private String rollResults;
    private PathfinderLabel rollLabel;
    private ImageButton rollButton;
    private PathfinderLabel resultsLabel;
    private TranslucentTextPane resultsText;
    private PathfinderComboBox strBox;
    private PathfinderComboBox dexBox;
    private PathfinderComboBox conBox;
    private PathfinderComboBox intBox;
    private PathfinderComboBox wisBox;
    private PathfinderComboBox chaBox;
    private ArrayList<String> availableScores;
    private ArrayList<String> strList;
    private ArrayList<String> dexList;
    private ArrayList<String> conList;
    private ArrayList<String> intList;
    private ArrayList<String> wisList;
    private ArrayList<String> chaList;
    
    public RollPanel() {
        super();
    }
    
    /**
     * Initializes this JPanel.
     */
    @Override
    public void initializePanel(AbilityScoreGenerator scoreGen) {
        this.scoreGen = scoreGen;
        this.scoreGen.resetRollScores();
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        this.setOpaque(false);
        super.createLabels();
        
        // Rolling labels and buttons.
        rollLabel = new PathfinderLabel("Click to Roll!");
        rollButton = new ImageButton(null, "/images/ui_elements/d20.png");
        
        // Results label and text.
        resultsLabel = new PathfinderLabel("Results");
        rollResults = "  Your generated numbers are: ";

        resultsText = new TranslucentTextPane(rollResults, new Dimension(400, 25), StyleConstants.ALIGN_LEFT);
        
        // Ability score labels and selection menus.
        Dimension selectionMenuSize = new Dimension(100, 25);
        assignLabel = new PathfinderLabel("Assign using the drop down menus: ");
        
        // Score array initialization.
        availableScores = new ArrayList(7);
        String[] tempScores = { "", "", "", "", "", "", ""};
        
        // Combo boxes.
        strBox = new PathfinderComboBox(tempScores, selectionMenuSize);
        dexBox = new PathfinderComboBox(tempScores, selectionMenuSize);
        conBox = new PathfinderComboBox(tempScores, selectionMenuSize);
        intBox = new PathfinderComboBox(tempScores, selectionMenuSize);
        wisBox = new PathfinderComboBox(tempScores, selectionMenuSize);
        chaBox = new PathfinderComboBox(tempScores, selectionMenuSize);
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup rollGroupH = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(rollLabel).addComponent(rollButton);
        GroupLayout.ParallelGroup scoreGroup1H = layout.createParallelGroup().addComponent(strLabel).addComponent(dexLabel).addComponent(conLabel);
        GroupLayout.ParallelGroup selectionGroup1H = layout.createParallelGroup().addComponent(strBox).addComponent(dexBox).addComponent(conBox);
        GroupLayout.ParallelGroup scoreGroup2H = layout.createParallelGroup().addComponent(intLabel).addComponent(wisLabel).addComponent(chaLabel);
        GroupLayout.ParallelGroup selectionGroup2H = layout.createParallelGroup().addComponent(intBox).addComponent(wisBox).addComponent(chaBox);
        GroupLayout.SequentialGroup abilityGroupH = layout.createSequentialGroup().addGroup(scoreGroup1H).addGroup(selectionGroup1H).
                addGroup(scoreGroup2H).addGroup(selectionGroup2H);
        GroupLayout.ParallelGroup resultsGroupH = layout.createParallelGroup().addComponent(resultsLabel).addComponent(resultsText).
                addComponent(assignLabel).addGroup(abilityGroupH);
        mainHorizontalGroup.addGap(30).addGroup(rollGroupH).addGap(30).addGroup(resultsGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
        GroupLayout.SequentialGroup rollGroupV = layout.createSequentialGroup().addComponent(rollLabel).addComponent(rollButton);
        GroupLayout.ParallelGroup strIntGroupV = layout.createParallelGroup().addComponent(strLabel).addComponent(strBox).addComponent(intLabel).
                addComponent(intBox);
        GroupLayout.ParallelGroup dexWisGroupV = layout.createParallelGroup().addComponent(dexLabel).addComponent(dexBox).addComponent(wisLabel).
                addComponent(wisBox);
        GroupLayout.ParallelGroup conChaGroupV = layout.createParallelGroup().addComponent(conLabel).addComponent(conBox).addComponent(chaLabel).
                addComponent(chaBox);
        GroupLayout.SequentialGroup abilityGroupV = layout.createSequentialGroup().addGroup(strIntGroupV).addGroup(dexWisGroupV).
                addGroup(conChaGroupV);
        GroupLayout.SequentialGroup resultsGroupV = layout.createSequentialGroup().addComponent(resultsLabel).addComponent(resultsText).
                addGap(20).addComponent(assignLabel).addGroup(abilityGroupV);
        mainVerticalGroup.addGroup(rollGroupV).addGroup(resultsGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        rollButton.setActionCommand("Roll - Standard");
        strBox.setActionCommand("Assign - STR");
        dexBox.setActionCommand("Assign - DEX");
        conBox.setActionCommand("Assign - CON");
        intBox.setActionCommand("Assign - INT");
        wisBox.setActionCommand("Assign - WIS");
        chaBox.setActionCommand("Assign - CHA");
        
        rollButton.addActionListener(listener);
        strBox.addActionListener(listener);
        dexBox.addActionListener(listener);
        conBox.addActionListener(listener);
        intBox.addActionListener(listener);
        wisBox.addActionListener(listener);
        chaBox.addActionListener(listener);
    }
    
    /**
     * Removes the ActionListener from the combo boxes to prevent the firing of the event when the selection lists are 
     * programmatically altered.
     * @param listener The ActionListener to remove.
     */
    public void removeActionListener(ActionListener listener) {
        rollButton.removeActionListener(listener);
        strBox.removeActionListener(listener);
        dexBox.removeActionListener(listener);
        conBox.removeActionListener(listener);
        intBox.removeActionListener(listener);
        wisBox.removeActionListener(listener);
        chaBox.removeActionListener(listener);
    }
    
    @Override
    public void updateView() {
        int[] rolledArray = scoreGen.getGeneratedNumbers();
        resultsText.setText("  Your generated numbers are: " + rolledArray[0]  + ", " + rolledArray[1] + ", " +
                rolledArray[2] + ", " + rolledArray[3] + ", " + rolledArray[4] + ", " + rolledArray[5]);
        availableScores.clear();
        availableScores.add("");
        for (int i = 0; i < 6; i++) {
            availableScores.add(Integer.toString(rolledArray[i]));
        }
        clearComboLists();
        scoreGen.resetRollScores();
        updateComboBoxes(null, null);
    }
    
    /**
     * Updates the combo box selection items when the user assigns an ability score or when the dice are rolled.
     * @param selectedItem
     * @param actionCommand 
     */
    public void updateComboBoxes(String selectedItem, String actionCommand) {
        if (selectedItem != null) {
            // If the user chose an empty space, they are "undoing" their last choice.  Their last choice should then
            // be added to the list of available scores to choose from.
            if (selectedItem.isEmpty()) {
                addAvailableScore(actionCommand);
            }
            // If the user chose a number, then they are assigning a score.  If that score was previously assigned, then the numbers need to be
            // "swapped out" with the chosen score.  If the score has not been previously assigned, then the number can simply be removed from the list of 
            // available scores.
            else {
                addAvailableScore(actionCommand); // For cases in which a number is already assigned and needs to be swapped out.
                availableScores.remove(selectedItem);
            }
        }
        resetComboLists();
        
        // This updates the combo list pertaining to the box that was clicked with the new selection.
        if (selectedItem != null && actionCommand != null) {
            switch(actionCommand) {
                case "Assign - STR":
                    if (!selectedItem.isEmpty())
                        strList.add(selectedItem);
                    scoreGen.setRollScore(AbilityScore.STR, selectedItem);
                    break;
                case "Assign - DEX":
                    if (!selectedItem.isEmpty())
                        dexList.add(selectedItem);
                    scoreGen.setRollScore(AbilityScore.DEX, selectedItem);
                    break;
                case "Assign - CON":
                    if (!selectedItem.isEmpty())
                        conList.add(selectedItem);
                    scoreGen.setRollScore(AbilityScore.CON, selectedItem);
                    break;
                case "Assign - INT":
                    if (!selectedItem.isEmpty())
                        intList.add(selectedItem);
                    scoreGen.setRollScore(AbilityScore.INT, selectedItem);
                    break;
                case "Assign - WIS":
                    if (!selectedItem.isEmpty())
                        wisList.add(selectedItem);
                    scoreGen.setRollScore(AbilityScore.WIS, selectedItem);
                    break;
                case "Assign - CHA":
                    if (!selectedItem.isEmpty())
                        chaList.add(selectedItem);
                    scoreGen.setRollScore(AbilityScore.CHA, selectedItem);
                    break;
            }
        }     
        updateComboLists();
    }
    
    /**
     * Adds a previously chosen score to the list of available scores to choose from, providing it is not an empty selection.
     * @param actionCommand The action command pertaining to the clicked combo box.
     */
    private void addAvailableScore(String actionCommand) {
        switch (actionCommand) {
            case "Assign - STR":
                if (!scoreGen.getRollScore(AbilityScore.STR).isEmpty())
                    availableScores.add(scoreGen.getRollScore(AbilityScore.STR));
                break;
            case "Assign - DEX":
                if (!scoreGen.getRollScore(AbilityScore.DEX).isEmpty())
                    availableScores.add(scoreGen.getRollScore(AbilityScore.DEX));
                break;
            case "Assign - CON":
                if (!scoreGen.getRollScore(AbilityScore.CON).isEmpty())
                    availableScores.add(scoreGen.getRollScore(AbilityScore.CON));
                break;
            case "Assign - INT":
                if (!scoreGen.getRollScore(AbilityScore.INT).isEmpty())
                    availableScores.add(scoreGen.getRollScore(AbilityScore.INT));
                break;
            case "Assign - WIS":
                if (!scoreGen.getRollScore(AbilityScore.WIS).isEmpty())
                    availableScores.add(scoreGen.getRollScore(AbilityScore.WIS));
                break;
            case "Assign - CHA":
                if (!scoreGen.getRollScore(AbilityScore.CHA).isEmpty())
                    availableScores.add(scoreGen.getRollScore(AbilityScore.CHA));
                break;
        }
    }
    
    /**
     * Resets the combo lists with the available scores to choose from and their last selected score.
     */
    private void resetComboLists() {
        // Sets all of the lists to contain the list of available scores.  (clone is used to do a deep copy)
        strList = (ArrayList)availableScores.clone();
        dexList = (ArrayList)availableScores.clone();
        conList = (ArrayList)availableScores.clone();
        intList = (ArrayList)availableScores.clone();
        wisList = (ArrayList)availableScores.clone();
        chaList = (ArrayList)availableScores.clone();
        
        // The last selected score for each box is then appended to the list, unless it was empty or already equal to the 
        // currently selected score.
        if (!scoreGen.getRollScore(AbilityScore.STR).isEmpty() && !((String)strBox.getSelectedItem()).isEmpty()) {
            if (scoreGen.getRollScore(AbilityScore.STR).compareTo((String)strBox.getSelectedItem()) == 0)
                strList.add(scoreGen.getRollScore(AbilityScore.STR));
        }
        if (!scoreGen.getRollScore(AbilityScore.DEX).isEmpty() && !((String)dexBox.getSelectedItem()).isEmpty()) {
            if (scoreGen.getRollScore(AbilityScore.DEX).compareTo((String)dexBox.getSelectedItem()) == 0)
                dexList.add(scoreGen.getRollScore(AbilityScore.DEX));
        }
        if (!scoreGen.getRollScore(AbilityScore.CON).isEmpty() && !((String)conBox.getSelectedItem()).isEmpty()) {
            if (scoreGen.getRollScore(AbilityScore.CON).compareTo((String)conBox.getSelectedItem()) == 0)
                conList.add(scoreGen.getRollScore(AbilityScore.CON));
        }
        if (!scoreGen.getRollScore(AbilityScore.INT).isEmpty() && !((String)intBox.getSelectedItem()).isEmpty()) {
            if (scoreGen.getRollScore(AbilityScore.INT).compareTo((String)intBox.getSelectedItem()) == 0)
                intList.add(scoreGen.getRollScore(AbilityScore.INT));
        }
        if (!scoreGen.getRollScore(AbilityScore.WIS).isEmpty() && !((String)wisBox.getSelectedItem()).isEmpty()) {
            if (scoreGen.getRollScore(AbilityScore.WIS).compareTo((String)wisBox.getSelectedItem()) == 0)
                wisList.add(scoreGen.getRollScore(AbilityScore.WIS));
        }
        if (!scoreGen.getRollScore(AbilityScore.CHA).isEmpty() && !((String)chaBox.getSelectedItem()).isEmpty()) {
            if (scoreGen.getRollScore(AbilityScore.CHA).compareTo((String)chaBox.getSelectedItem()) == 0)
                chaList.add(scoreGen.getRollScore(AbilityScore.CHA)); 
        }
    }
    
    /**
     * Updates the displayed items on the combo boxes.
     */
    private void updateComboLists() {
        strBox.updateSelectionList(strList, scoreGen.getRollScore(AbilityScore.STR));
        dexBox.updateSelectionList(dexList, scoreGen.getRollScore(AbilityScore.DEX));
        conBox.updateSelectionList(conList, scoreGen.getRollScore(AbilityScore.CON));
        intBox.updateSelectionList(intList, scoreGen.getRollScore(AbilityScore.INT));
        wisBox.updateSelectionList(wisList, scoreGen.getRollScore(AbilityScore.WIS));
        chaBox.updateSelectionList(chaList, scoreGen.getRollScore(AbilityScore.CHA));  
    }
    
    /**
     * Resets the combo lists to empty.
     */
    private void clearComboLists() {
        if (strList != null)
            strList.clear();
        if (dexList != null)
            dexList.clear();
        if (conList != null)
            conList.clear();
        if (intList != null)
            intList.clear();
        if (wisList != null)
            wisList.clear();
        if (chaList != null)
            chaList.clear();
    }
    
}
