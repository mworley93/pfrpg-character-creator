/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderMessagePopup;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.PathfinderPopup;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.components.TranslucentTextPane;
import charactercreator.models.AttributeGenerator;
import charactercreator.models.BaseCharacter;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.StyleConstants;

/**
 * This panel is the page that asks the user to input information about a character's background, 
 * appearance, and personality.
 * @author Megan Worley
 */
public class MiscPanel extends ContentPanel {
    private UIModel uiModel;
    private AttributeGenerator attributeGenerator;
    private BaseCharacter character;
    
    private PathfinderLabel overviewLabel;
    private TranslucentScrollPane overviewPane;
    
    private PathfinderLabel vitalsLabel;
    private PathfinderLabel characteristicsLabel;
    
    private PathfinderLabel ageLabel; 
    private PathfinderButton ageInfoButton;
    private TranslucentTextPane agePane;
    private PathfinderButton randomAgeButton;
    
    private PathfinderLabel heightLabel;
    private PathfinderButton heightInfoButton;
    private TranslucentTextPane heightFeetPane;
    private TranslucentTextPane heightInchesPane;
    private PathfinderButton randomHeightButton;
    
    private PathfinderLabel weightLabel; 
    private PathfinderButton weightInfoButton;
    private TranslucentTextPane weightPane;
    private PathfinderButton randomWeightButton;
    
    private PathfinderLabel genderLabel;
    private PathfinderComboBox genderBox;
    
    private PathfinderLabel alignmentLabel;
    private PathfinderButton alignmentInfoButton;
    private PathfinderComboBox alignmentBox;
    
    private PathfinderLabel nameLabel;
    private TranslucentTextPane namePane;
    
    private PathfinderLabel playerNameLabel;
    private TranslucentTextPane playerNamePane;
    
    private PathfinderLabel backgroundLabel;
    private PathfinderButton backgroundInfoButton;
    private JTextArea backgroundText;
    private TranslucentScrollPane backgroundPane;
    
    private boolean actionHit;
    
    /**
     * Default constructor.
     * @param uiModel The UIModel that contains the overview description.
     */
    public MiscPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
        actionHit = false;
    }
    
    /**
     * Initializes the components for this MiscPanel.
     */
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        initializeLabels();
        initializeComboBoxes();
        initializeButtons();
        initializeTextPanes();
    }
    
    private void initializeLabels() {
        overviewLabel = new PathfinderLabel("Overview: ", 24);
        vitalsLabel = new PathfinderLabel("Vitals: ", 24);
        genderLabel = new PathfinderLabel("Gender: ");
        ageLabel = new PathfinderLabel("Age: ");
        heightLabel = new PathfinderLabel("Height (ft.,in.): ");
        weightLabel = new PathfinderLabel("Weight (lbs): ");
        characteristicsLabel = new PathfinderLabel("Characteristics: ", 24);
        alignmentLabel = new PathfinderLabel("Alignment: ");
        nameLabel = new PathfinderLabel("Character Name: ");
        playerNameLabel = new PathfinderLabel("Player Name (optional): ");
        backgroundLabel = new PathfinderLabel("Background (optional): ", 24);
    }
    
    private void initializeComboBoxes() {
        String[] genderItems = {"Male", "Female"};
        genderBox = new PathfinderComboBox(genderItems, new Dimension(100,25));
        String[] alignmentItems = {"Lawful Good", "Neutral Good", "Chaotic Good",
                                   "Lawful Neutral", "Neutral", "Chaotic Neutral",
                                   "Lawful Evil", "Neutral Evil", "Chaotic Evil",
        };
        alignmentBox = new PathfinderComboBox(alignmentItems, new Dimension(100, 25));
    }
    
    /**
     * Initializes the More Info and the Randomize buttons on this page.
     */
    private void initializeButtons() {
        ageInfoButton = new PathfinderButton("?");
        heightInfoButton = new PathfinderButton("?");
        weightInfoButton = new PathfinderButton("?");
        alignmentInfoButton = new PathfinderButton("?");
        backgroundInfoButton = new PathfinderButton("?");
        
        randomAgeButton = new PathfinderButton("Randomize");
        randomHeightButton = new PathfinderButton("Randomize");
        randomWeightButton = new PathfinderButton("Randomize");
    }
    
    private void initializeTextPanes() {
        JTextArea overviewText = guiMethods.createPlainTextArea(uiModel.getMiscOverview(), false, null);
        overviewPane = new TranslucentScrollPane(overviewText, new Dimension(450, 200));
        agePane = new TranslucentTextPane("", new Dimension(50, 25), StyleConstants.ALIGN_CENTER);
        agePane.setEditable(true);
        agePane.getDocument().addDocumentListener(new CharacterDocListener());
        heightFeetPane = new TranslucentTextPane("", new Dimension(50, 25), StyleConstants.ALIGN_CENTER);
        heightFeetPane.setEditable(true);
        heightFeetPane.getDocument().addDocumentListener(new CharacterDocListener());
        heightInchesPane = new TranslucentTextPane("", new Dimension(50, 25), StyleConstants.ALIGN_CENTER);
        heightInchesPane.setEditable(true);
        heightInchesPane.getDocument().addDocumentListener(new CharacterDocListener());
        weightPane = new TranslucentTextPane("", new Dimension(50, 25), StyleConstants.ALIGN_CENTER);
        weightPane.setEditable(true);
        weightPane.getDocument().addDocumentListener(new CharacterDocListener());
        backgroundText = guiMethods.createPlainTextArea("", false, null);
        backgroundPane = new TranslucentScrollPane(backgroundText, new Dimension(450, 200));
        backgroundText.setEditable(true);
        backgroundText.getDocument().addDocumentListener(new CharacterDocListener());
        namePane = new TranslucentTextPane("", new Dimension(200, 25), StyleConstants.ALIGN_CENTER);
        namePane.setEditable(true);
        namePane.getDocument().addDocumentListener(new CharacterDocListener());
        playerNamePane = new TranslucentTextPane("", new Dimension(200, 25), StyleConstants.ALIGN_CENTER);
        playerNamePane.setEditable(true);
        playerNamePane.getDocument().addDocumentListener(new CharacterDocListener());
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup overviewGroupH = layout.createParallelGroup().addComponent(overviewLabel)
                .addComponent(overviewPane);
        GroupLayout.SequentialGroup alignmentGroupH = layout.createSequentialGroup().
                addComponent(alignmentInfoButton).addComponent(alignmentLabel).addComponent(alignmentBox);
        GroupLayout.SequentialGroup charNameGroupH = layout.createSequentialGroup().
                addComponent(nameLabel).addComponent(namePane);
        GroupLayout.SequentialGroup playerNameGroupH = layout.createSequentialGroup().
                addComponent(playerNameLabel).addComponent(playerNamePane);
        GroupLayout.SequentialGroup backgroundTitleGroupH = layout.createSequentialGroup().
                addComponent(backgroundLabel).addComponent(backgroundInfoButton);
        GroupLayout.ParallelGroup backgroundGroupH = layout.createParallelGroup().
                addGroup(backgroundTitleGroupH).addComponent(backgroundPane);
        GroupLayout.SequentialGroup genderGroupH = layout.createSequentialGroup().addComponent(genderLabel).
                addComponent(genderBox);
        GroupLayout.SequentialGroup ageGroupH = layout.createSequentialGroup().addComponent(ageInfoButton).
                addComponent(ageLabel).addComponent(agePane).addComponent(randomAgeButton);
        GroupLayout.SequentialGroup heightGroupH = layout.createSequentialGroup().addComponent(heightInfoButton).
                addComponent(heightLabel).addComponent(heightFeetPane).addComponent(heightInchesPane).
                addComponent(randomHeightButton);
        GroupLayout.SequentialGroup weightGroupH = layout.createSequentialGroup().addComponent(weightInfoButton)
                .addComponent(weightLabel).addComponent(weightPane).addComponent(randomWeightButton);
        GroupLayout.ParallelGroup vitalsGroupH = layout.createParallelGroup().addComponent(vitalsLabel)
                .addGroup(genderGroupH).addGroup(ageGroupH).addGroup(heightGroupH).addGroup(weightGroupH);
        GroupLayout.ParallelGroup characterGroupH = layout.createParallelGroup().addComponent(characteristicsLabel)
                .addGroup(charNameGroupH).addGroup(playerNameGroupH).addGroup(alignmentGroupH);
        GroupLayout.ParallelGroup overviewBackgroundGroupH = layout.createParallelGroup().addGroup(overviewGroupH)
                .addGap(50).addGroup(backgroundGroupH);
        GroupLayout.ParallelGroup vitalsCharacteristicsGroupH = layout.createParallelGroup().addGroup(characterGroupH)
                .addGroup(vitalsGroupH);
        mainHorizontalGroup.addGroup(overviewBackgroundGroupH).addGap(50).addGroup(vitalsCharacteristicsGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup overviewGroupV = layout.createSequentialGroup().addComponent(overviewLabel)
                .addComponent(overviewPane);
        GroupLayout.ParallelGroup alignmentGroupV = layout.createParallelGroup().
                addComponent(alignmentInfoButton).addComponent(alignmentLabel).addComponent(alignmentBox);
        GroupLayout.ParallelGroup charNameGroupV = layout.createParallelGroup().
                addComponent(nameLabel).addComponent(namePane);
        GroupLayout.ParallelGroup playerNameGroupV = layout.createParallelGroup().
                addComponent(playerNameLabel).addComponent(playerNamePane);
        GroupLayout.ParallelGroup backgroundTitleGroupV = layout.createParallelGroup().
                addComponent(backgroundLabel).addComponent(backgroundInfoButton);
        GroupLayout.SequentialGroup backgroundGroupV = layout.createSequentialGroup().
                addGroup(backgroundTitleGroupV).addComponent(backgroundPane);
        GroupLayout.ParallelGroup genderGroupV = layout.createParallelGroup().addComponent(genderLabel).
                addComponent(genderBox);
        GroupLayout.ParallelGroup ageGroupV = layout.createParallelGroup().addComponent(ageInfoButton).
                addComponent(ageLabel).addComponent(agePane).addComponent(randomAgeButton);
        GroupLayout.ParallelGroup heightGroupV = layout.createParallelGroup().addComponent(heightInfoButton).
                addComponent(heightLabel).addComponent(heightFeetPane).addComponent(heightInchesPane).
                addComponent(randomHeightButton);
        GroupLayout.ParallelGroup weightGroupV = layout.createParallelGroup().addComponent(weightInfoButton)
                .addComponent(weightLabel).addComponent(weightPane).addComponent(randomWeightButton);
        GroupLayout.SequentialGroup vitalsGroupV = layout.createSequentialGroup().addComponent(vitalsLabel)
                .addGap(25).addGroup(genderGroupV).addGap(10).addGroup(ageGroupV).addGap(10)
                .addGroup(heightGroupV).addGap(10).addGroup(weightGroupV);
        GroupLayout.SequentialGroup characterGroupV = layout.createSequentialGroup().addComponent(characteristicsLabel)
                .addGap(25).addGroup(charNameGroupV).addGap(10).addGroup(playerNameGroupV).addGap(10)
                .addGroup(alignmentGroupV);
        GroupLayout.SequentialGroup overviewBackgroundGroupV = layout.createSequentialGroup().addGroup(overviewGroupV)
                .addGap(25).addGroup(backgroundGroupV);
        GroupLayout.SequentialGroup vitalsCharacteristicsGroupV = layout.createSequentialGroup().addGroup(characterGroupV)
                .addGap(150).addGroup(vitalsGroupV);
        mainVerticalGroup.addGroup(overviewBackgroundGroupV).addGroup(vitalsCharacteristicsGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        randomAgeButton.addActionListener(listener);
        randomAgeButton.setActionCommand("Randomize - Age");
        randomHeightButton.addActionListener(listener);
        randomHeightButton.setActionCommand("Randomize - Height");
        randomWeightButton.addActionListener(listener);
        randomWeightButton.setActionCommand("Randomize - Weight");
        genderBox.addActionListener(listener);
        genderBox.setActionCommand("Gender Selection");
        alignmentBox.addActionListener(listener);
        alignmentBox.setActionCommand("Alignment Selection");
        backgroundInfoButton.addActionListener(listener);
        backgroundInfoButton.setActionCommand("More Info - Background");
        ageInfoButton.addActionListener(listener);
        ageInfoButton.setActionCommand("More Info - Age");
        heightInfoButton.addActionListener(listener);
        heightInfoButton.setActionCommand("More Info - Height");
        weightInfoButton.addActionListener(listener);
        weightInfoButton.setActionCommand("More Info - Weight");
        alignmentInfoButton.addActionListener(listener);
        alignmentInfoButton.setActionCommand("More Info - Alignment");
    }
    
    @Override
    public void updateView() {
        // This actionHit variable prevents the DocumentListener from updating when the text field 
        // values are changed programmatically here.  The DocumentListener should only fire when 
        // the user enters text.
        actionHit = true;
        genderBox.setSelectedItem(attributeGenerator.getGender().getString());
        alignmentBox.setSelectedItem(attributeGenerator.getAlignment().getString());
        agePane.setText(String.valueOf(attributeGenerator.getAge()));
        heightFeetPane.setText(String.valueOf(attributeGenerator.getHeight() / 12));
        heightInchesPane.setText(String.valueOf(attributeGenerator.getHeight() % 12));
        weightPane.setText(String.valueOf(attributeGenerator.getWeight())); 
        backgroundText.setText(attributeGenerator.getBackground());
        namePane.setText(attributeGenerator.getCharacterName());
        playerNamePane.setText(attributeGenerator.getPlayerName());
        actionHit = false;
    }
    
    public void setAttributeGenerator(AttributeGenerator atGen) {
        attributeGenerator = atGen;
    }
    
    public void setBaseCharacter(BaseCharacter character) {
        this.character = character;
    }
    
    /**
     * Randomizes the character's age.
     */
    public void randomAge() {
        String race = character.getCharacterRace().getRaceName();
        attributeGenerator.randomizeAge(race);
    }
    
    /**
     * Randomizes the character's height.
     */
    public void randomHeight() {
        String race = character.getCharacterRace().getRaceName();
        attributeGenerator.randomizeHeight(attributeGenerator.getGender(), race);
    }
    
    /**
     * Randomizes the character's weight.
     */
    public void randomWeight() {
        String race = character.getCharacterRace().getRaceName();
        attributeGenerator.randomizeWeight(attributeGenerator.getGender(), race);
    }
    
    /**
     * Updates the character's gender.
     */
    public void updateGender() {
        attributeGenerator.setGender((String)(genderBox.getSelectedItem()));
    }
    
    /**
     * Updates the character's alignment.
     */
    public void updateAlignment() {
        attributeGenerator.setAlignment((String)(alignmentBox.getSelectedItem()));
    }
    
    /**
     * Updates the information in the AttributeGenerator whenever text is entered into a 
     * field.
     */
    private class CharacterDocListener implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent e) {
            if (!actionHit) {
                insertModel(e);
            }
        }
        @Override
        public void removeUpdate(DocumentEvent e) {
            if (!actionHit) {
                removeModel(e);
            }
        }
        @Override
        public void changedUpdate(DocumentEvent e) {
        }
        
        /**
         * Tests to see if a given string can be converted to an integer.
         * @param text
         * @return 
         */
        private boolean testInteger(String text) {
            try {
               Integer.parseInt(text);
               return true;
            } 
            catch (NumberFormatException e) {
               return false;
            }
        }
        
        /**
         * Called when new text is typed into a field.  Updates the AttributeGenerator with 
         * the newly added info.
         * @param e 
         */
        private void insertModel(DocumentEvent e) {
            Document doc = e.getDocument();
            try {
                String text = doc.getText(0, doc.getLength());
                if (doc == namePane.getDocument()) {
                        attributeGenerator.setCharacterName(text);
                }
                else if (doc == playerNamePane.getDocument()) {
                    attributeGenerator.setPlayerName(text);
                }
                else if (doc == backgroundText.getDocument()) {
                    attributeGenerator.setBackground(text);
                }
                else {
                    if (testInteger(text)) {
                        if (doc == heightFeetPane.getDocument()) {
                            attributeGenerator.setHeight(Integer.parseInt(text), 
                                attributeGenerator.getHeight() % 12);
                        }
                        else if (doc == heightInchesPane.getDocument()) {
                            attributeGenerator.setHeight(attributeGenerator.getHeight() / 12, 
                                Integer.parseInt(text));
                        }
                        else if (doc == agePane.getDocument()) {
                            attributeGenerator.setAge(Integer.parseInt(text));    
                        }
                        else if (doc == weightPane.getDocument()) {
                            attributeGenerator.setWeight(Integer.parseInt(text));
                        }
                    } 
                    else {
                        this.removeUpdate(e);
                        JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(MiscPanel.this);
                        PathfinderPopup warning = new PathfinderPopup("Please only enter numbers for age, height, and weight.  Thank you!", 
                                400, 150, frame);
                        warning.setLocationRelativeTo(MiscPanel.this.getParent());
                    }
                }
            }
            catch(Exception ex) {

            }
            
            
        }
        
        /**
         * Called when text is removed from a field.  Updates the AttributeGenerator with 
         * the newly displayed information.  Also used to deal with invalid input.
         * @param e 
         */
        private void removeModel(DocumentEvent e) {
            Document doc = e.getDocument();
            try {
                String text = doc.getText(0, doc.getLength());
                if (doc == namePane.getDocument()) {
                        attributeGenerator.setCharacterName(text);
                }
                else if (doc == playerNamePane.getDocument()) {
                    attributeGenerator.setPlayerName(text);
                }
                else if (doc == backgroundText.getDocument()) {
                    attributeGenerator.setBackground(text);
                }
                else {
                    if (testInteger(text)) {
                        if (doc == heightFeetPane.getDocument()) {
                            attributeGenerator.setHeight(Integer.parseInt(text), 
                                attributeGenerator.getHeight() % 12);
                        }
                        else if (doc == heightInchesPane.getDocument()) {
                            attributeGenerator.setHeight(attributeGenerator.getHeight() / 12, 
                                Integer.parseInt(text));
                        }
                        else if (doc == agePane.getDocument()) {
                            attributeGenerator.setAge(Integer.parseInt(text));    
                        }
                        else if (doc == weightPane.getDocument()) {
                            attributeGenerator.setWeight(Integer.parseInt(text));
                        }
                    } 
                    else {
                        if (doc == heightFeetPane.getDocument()) {
                            attributeGenerator.setHeight(0, 
                                attributeGenerator.getHeight() % 12);
                        }
                        else if (doc == heightInchesPane.getDocument()) {
                            attributeGenerator.setHeight(attributeGenerator.getHeight() / 12, 
                                0);
                        }
                        else if (doc == agePane.getDocument()) {
                            attributeGenerator.setAge(0);    
                        }
                        else if (doc == weightPane.getDocument()) {
                            attributeGenerator.setWeight(0);
                        }
                    }
                }
                
            }
            catch(Exception ex) {

            }
        }
    }
}
