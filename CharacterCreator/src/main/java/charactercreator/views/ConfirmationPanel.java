/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.helpers.GUIHelper;
import charactercreator.models.AbilityScore;
import charactercreator.models.BaseCharacter;
import charactercreator.models.Feat;
import charactercreator.models.Skill;
import charactercreator.models.Spell;
import charactercreator.models.equipment.Armor;
import charactercreator.models.equipment.Weapon;

import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Component;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import javax.swing.JFileChooser;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.plaf.metal.MetalScrollBarUI;

/**
 * This custom JPanel represents the Confirmation Screen that the user will see after they have finished 
 * making decisions for their character.  It will let them save the information as a PDF character 
 * sheet.  This class inherits from BackgroundPanel so that the custom drawing of the paper background image can 
 * be preserved.
 * @author Brandon Sharp
 */
public class ConfirmationPanel extends BackgroundPanel {
    private GUIHelper guiMethods;
    
    private BaseCharacter character;
    
    // Screen panels.
    private JPanel titlePanel;
    private JPanel middlePanel;
    private JPanel buttonPanel;
    private JPanel linePanel;
    
    // Title components.
    private PathfinderLabel title;
    
    // Informative components.
    private TranslucentScrollPane introPane;
    private JLabel image;
    private PathfinderLabel line1;
    private PathfinderLabel line2;
    private PathfinderLabel line3;
    
    // Button components.
    private PathfinderButton confirmButton;
    private PathfinderButton passButton;
    
    private JTextArea introText;
    
    // These are all the Strings to be filled.
    private String className;
    private String raceName;
    private ArrayList<String> classFeatures = new ArrayList<>();

    private String strengthScore;
    private String dexterityScore;
    private String constitutionScore;
    private String intelligenceScore;
    private String wisdomScore;
    private String charismaScore;   
    
    private EnumMap<Skill, Integer> tempSkillMap;
    
    private static String indentation = "    ";   
    
    /**
     * Default constructor.
     * @param image Themed background image (should be /images/ui_elements/backgroundTexture.png for consistency).
     */
    public ConfirmationPanel(String image) {
        super(image);
        guiMethods = new GUIHelper();
        initializeComponents();
        initializeLayout();
    }
    
    /**
     * Initializes the component values found on this screen, such as the title text, introductory text, image banner, etc.
     */
    private void initializeComponents() {
        title = new PathfinderLabel("You're almost done!", 35);
        title.drawShadow(true);
        introText = guiMethods.createPlainTextArea("", false, null);
        introPane = new TranslucentScrollPane(introText, new Dimension(460, 410));
        image = guiMethods.constructImageLabel("/images/ui_elements/confirmation.png");
        guiMethods.addBeveledImageBorder(image);
        image.setMinimumSize(new Dimension(460, 410));
        line1 = new PathfinderLabel("Please make sure that your character is perfect!  Once you've confirmed everything below is correct, please select", 20);
        line2 = new PathfinderLabel("'Save as PDF' to save your character.  If something needs to be changed, please click the 'Go Back' button and go", 20);
        line3 = new PathfinderLabel("back to the appropriate page for your change.", 20);
        
        initializeButtons();
    
        initializePanels();
        
        setComponentOrientation();
    }
    
    /**
     * Initializes the buttons found on this screen.  Sets their text as well as their maximum and minimum sizes so they can stretch and scale 
     * appropriately in the BoxLayout.
     */
    private void initializeButtons() {
        confirmButton = new PathfinderButton("Go Back");
        confirmButton.setMinimumSize(new Dimension(50, 25));
        confirmButton.setPreferredSize(new Dimension(50, 25));
        confirmButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));
        passButton = new PathfinderButton("Save as PDF");
        passButton.setMinimumSize(new Dimension(50, 25));
        passButton.setPreferredSize(new Dimension(50, 25));
        passButton.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  Short.MAX_VALUE));
    }
    
    /**
     * Initializes the subpanels on this screen and makes them completely transparent so the background can show through.
     */
    private void initializePanels() {
        titlePanel = new JPanel();
        titlePanel.setOpaque(false);
        linePanel = new JPanel();
        linePanel.setOpaque(false);
        middlePanel = new JPanel();
        middlePanel.setOpaque(false);
        buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);
    }
    
    /**
     * Sets the components' orientation, which is super important for BoxLayout.  In any given subpanel, the X-component orientation
     * must be the same.  Some components on the screen are left aligned and some are center aligned, so they are broken up into 
     * groups.
     */
    private void setComponentOrientation() {
        // Title panel.
        title.setAlignmentX(Component.LEFT_ALIGNMENT);
        title.setAlignmentY(Component.LEFT_ALIGNMENT);
        
        // Lines Panel
        line1.setAlignmentX(Component.LEFT_ALIGNMENT);
        line1.setAlignmentY(Component.CENTER_ALIGNMENT);
        line2.setAlignmentX(Component.LEFT_ALIGNMENT);
        line2.setAlignmentY(Component.CENTER_ALIGNMENT);
        line3.setAlignmentX(Component.LEFT_ALIGNMENT);
        line3.setAlignmentY(Component.CENTER_ALIGNMENT);
        
        // Middle panel.
        introPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        introPane.setAlignmentY(Component.CENTER_ALIGNMENT);
        
        // Sets subpanel alignments.
        titlePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        middlePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        linePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        linePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titlePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        middlePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
    }
    
    /**
     * Adds all of the components to the screen.  BoxLayout is used on everything, since the arrangement isn't too complex.  
     */
    private void initializeLayout() {  
        // A few subpanels needed to be used to get the component alignments to be correct.  All use BoxLayout.  Vertical and horizontal 
        // glue is used to dynamically space the components.
        
        // Title section.
        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));
        titlePanel.add(Box.createVerticalGlue());
        titlePanel.add(title);
        titlePanel.add(Box.createVerticalGlue());
        
        // Line Panel.
        linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.Y_AXIS));
        linePanel.add(line1);
        linePanel.add(line2);
        linePanel.add(line3);
        
        // Image/text section.
        middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.X_AXIS));
        middlePanel.add(Box.createHorizontalGlue());
        middlePanel.add(introPane);
        middlePanel.add(Box.createRigidArea(new Dimension(20, 0)));
        middlePanel.add(image);
        middlePanel.add(Box.createHorizontalGlue());
        
        
        // Button section.
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(confirmButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(passButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        // Adds each section to this entire screen.
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(titlePanel);
        this.add(linePanel);
        this.add(Box.createVerticalGlue());
        this.add(middlePanel);
        this.add(Box.createVerticalGlue());
        this.add(buttonPanel);
        this.add(Box.createVerticalGlue());
    }
    
    /**
     * Adds the action listener for the buttons on the welcome screen.  Determines 
     * whether or not a tutorial section will start.
     * @param listener The ActionListener that checks if the user wants the tutorial.
     */
    public void attachActionListener(ActionListener listener) {
        confirmButton.setActionCommand("Go Back");
        confirmButton.addActionListener(listener);
        passButton.setActionCommand("Save as PDF");
        passButton.addActionListener(listener);
    }
    
    public void setBaseCharacter(BaseCharacter character) {
        this.character = character;
    }
    
    public void updateView() {
        introText.setText(generateCharacterText(character));
        introText.setCaretPosition(0);
    }
    
    /**
     * This method launches a JFileChooser and allows the user to select 
     * a path to save the PDF.  
     * @return The save file destination, or CANCEL.
     */
    public String getSaveFilePath() {
        JFileChooser chooser = null;
        LookAndFeel previousLF = UIManager.getLookAndFeel();
        try {
            // Change the UIManager for the moment that the JFileChooser is created, and then
            // switch it back after being instansiated.  This allows the JFileChooser look to 
            // reflect the OS appearance, while the other components in the program retain their 
            // custom appearance.
            UIManager.put("ScrollBarUI", MetalScrollBarUI.class.getName());
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            chooser = new JFileChooser();
            UIManager.put("ScrollBarUI", ScrollBarUI.class.getName());
            UIManager.setLookAndFeel(previousLF);
        } 
        catch (IllegalAccessException | UnsupportedLookAndFeelException | InstantiationException | ClassNotFoundException e) {
            chooser = new JFileChooser();
        }
        int retrival = chooser.showSaveDialog(null);
        if (retrival == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile() + ".pdf";
        }
        else {
            return "CANCEL";
        }
    }
    
    private String generateCharacterText(BaseCharacter character) {      
        // Filling Misc Info.
        String text = "Character Information: " + "\n" + "\n";
        text = appendMiscInfo(text);
        
        // Filling Class Name.
        if(character.getCharacterClass() == null) className = "Not chosen yet.";
        else className = character.getCharacterClass().getClassName();
        
        // Filling Race Name.
        if(character.getCharacterRace() == null) raceName = "Not chosen yet.";
        else raceName = character.getCharacterRace().getRaceName();     
        
        // Creating total text string, one item at a time.
        text = text + "Class: " + className + "\n";
        text = text + indentation + "Class Features:" + "\n";
        fillClassFeatures(classFeatures);        
        if (character.getCharacterClass() == null) {
            text = text + indentation + indentation + "Not chosen yet." + "\n";
        }
        for (String s : classFeatures) text = text + indentation + indentation + s + "\n";
        
        text = text + "\n";
        
        text = text + "Race: " + raceName + "\n" + "\n";
        
        // This function adds all the ability scores
        text = appendAbilityScores(text);
        
        // Feats
        text = text + "Feats Selected: " + "\n";
        text = appendFeats(text);
        
        // Skills
        
        text = text + "Skills: " + "\n";
        text = appendSkills(text);
        
        // This will append all the equipment they have purchased.
        text = text + "Equipment Purchased" + "\n";
        ArrayList<Weapon> weaponsList = character.getWeaponsList();
        ArrayList<Armor> armorList = character.getArmorList();        
        for (Weapon w : weaponsList) text = text + indentation + w.getName() + "\n";
        for (Armor a : armorList) text = text + indentation + a.getName() + "\n";
        
        // If they didn't pick buy any items yet, this does so.
        if(weaponsList.isEmpty() && armorList.isEmpty()) text = text + indentation + "No equipment purchased." + "\n" + "\n";
        
        // If they are a wizard, attach the spells.
        if((character.getCharacterClass() != null) && (character.getCharacterClass().getClassName().matches("Wizard"))) {
            text = text + "Spells:" + "\n";
            text = appendSpells(text);
        }
        return text;
    }
    
    // This function creates an arraylist of strings depending on which class the user has selected.
    private ArrayList<String> fillClassFeatures(ArrayList<String> classFeatures) {
        
        if(character.getCharacterClass() == null) {
            return null;
        }
        
        if(character.getCharacterClass().getClassName().matches("Barbarian")) classFeatures.add("Barbarians have no class features to select.");
        
        if(character.getCharacterClass().getClassName().matches("Bard")) classFeatures.add("Bards have no class features to select.");
        
        if(character.getCharacterClass().getClassName().matches("Cleric")) {
            if (character.getFirstDomain() == null) classFeatures.add("First Domain not chosen yet.");
            else classFeatures.add("First Domain: " + character.getFirstDomain());
            if (character.getSecondDomain() == null) classFeatures.add("Second Domain not chosen yet.");
            else classFeatures.add("Second Domain: " + character.getSecondDomain());
        }
        
        if(character.getCharacterClass().getClassName().matches("Druid")) {
            if (character.getDruidFeature() == null) classFeatures.add("Druid Class Feature not chosen yet.");
            else classFeatures.add("Druid Class Feature: " + character.getDruidFeature());
            if (character.getDruidFeature().matches("Animal Companion")) classFeatures.add("Animal Companion: " + character.getAnimalCompanion());
            if (character.getDruidFeature().matches("Domain")) classFeatures.add("Domain: " + character.getFirstDomain());
        }        

        if(character.getCharacterClass().getClassName().matches("Fighter")) classFeatures.add("Fighters have no class features to select.");
                
        if(character.getCharacterClass().getClassName().matches("Monk")) classFeatures.add("Monks have no class features to select.");
                
        if(character.getCharacterClass().getClassName().matches("Paladin")) classFeatures.add("Paladins have no class features to select.");
            
        if(character.getCharacterClass().getClassName().matches("Ranger")) {
            if (character.getFavoredEnemy() == null) classFeatures.add("Ranger Class Feature not chosen yet.");
            else classFeatures.add("Favored Enemy: " + character.getFavoredEnemy());
        }                
        
        if(character.getCharacterClass().getClassName().matches("Rogue")) classFeatures.add("Rogues have no class features to select.");
             
        if(character.getCharacterClass().getClassName().matches("Sorcerer")) {
            if (character.getBloodline()== null) classFeatures.add("Sorcerer Class Feature not chosen yet.");
            else classFeatures.add("Bloodline: " + character.getBloodline());
        }    
        
        if(character.getCharacterClass().getClassName().matches("Wizard")) {
            if (character.getArcaneBond() == null) classFeatures.add("Wizard Class Feature not chosen yet.");
            else classFeatures.add("Arcane Bond: " + character.getArcaneBond());
            if (character.getArcaneSchool() == null) classFeatures.add("Arcane School not chosen yet.");
            else classFeatures.add("Arcane School: " + character.getArcaneSchool());
            if (character.getForbiddenArcaneSchoolOne() == null) classFeatures.add("First Forbidden School not chosen yet.");
            else classFeatures.add("First Forbidden School: " + character.getForbiddenArcaneSchoolOne());
            if (character.getForbiddenArcaneSchoolTwo() == null) classFeatures.add("Second Forbidden School not chosen yet.");
            else classFeatures.add("Second Forbidden School: " + character.getForbiddenArcaneSchoolTwo());
        }
        
        return classFeatures;
    }
    
    private String appendAbilityScores(String text) {
        // Filling Ability Scores.
        strengthScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.STR));
        if (strengthScore.matches("0")) strengthScore = "Not chosen yet.";
        dexterityScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.DEX));
        if (dexterityScore.matches("0")) dexterityScore = "Not chosen yet.";
        constitutionScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.CON));
        if (constitutionScore.matches("0")) constitutionScore = "Not chosen yet.";
        intelligenceScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.INT));
        if (intelligenceScore.matches("0")) intelligenceScore = "Not chosen yet.";
        wisdomScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.WIS));
        if (wisdomScore.matches("0")) wisdomScore = "Not chosen yet.";
        charismaScore = String.valueOf(character.getFinalAbilityScores().get(AbilityScore.CHA));
        if (charismaScore.matches("0")) charismaScore = "Not chosen yet.";
        
        text = text + "Ability Scores " + "\n";
        text = text + indentation + "STR: " + strengthScore + "\n";
        text = text + indentation + "DEX: " + dexterityScore + "\n";
        text = text + indentation + "CON: " + constitutionScore + "\n";
        text = text + indentation + "INT: " + intelligenceScore + "\n";
        text = text + indentation + "WIS: " + wisdomScore + "\n";
        text = text + indentation + "CHA: " + charismaScore + "\n";
        text = text + "\n";   
        
        return text;
    }
    
    private String appendSpells(String text) {
        if (character.getSpellList().isEmpty()) text = text + indentation + "Spells not selected yet.";
        for (Spell s : character.getSpellList()) text = text + indentation + s.getSpellName() + "\n";
        text = text + "\n" + "\n";
        return text;
    }
    
    private String appendFeats(String text) {
        if (character.getFeatList().isEmpty()) text = text + indentation + "Not chosen yet.";
        for (Feat f : character.getFeatList()) text = text + indentation + f.getFeatName() + "\n";
        text = text + "\n" + "\n";
        return text;
    }    
    
    private String appendMiscInfo(String text) {
        text = text + indentation  + "Name: " + character.getName() + "\n";
        text = text + indentation  + "Player Name: " + character.getPlayerName() + "\n";
        text = text + indentation  + "Alignment: " + character.getAlignment() + "\n";
        text = text + indentation  + "Biography: " + character.getBiography() + "\n";
        text = text + indentation  + "Age: " + character.getAge() + " years" + "\n";
        text = text + indentation + "Height: " + character.getHeight() + " inches" + "\n";
        text = text + indentation + "Weight: " + character.getWeight() + " pounds" + "\n";
        text = text + indentation + "Gender: " + character.getGender() + "\n";
        text = text + "\n\n";
        return text;
    }
    
    private String appendSkills(String text) {
        tempSkillMap = character.getFinalSkillMap();
        Iterator<Skill> enumKeySet = tempSkillMap.keySet().iterator();
            while(enumKeySet.hasNext()) {
                Skill currentSkill = enumKeySet.next();
                text = text + indentation + currentSkill.name() + ": " + tempSkillMap.get(currentSkill) + "\n";
            }
        text = text + "\n\n";
        return text;
    }
}
