/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon
 */

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class ForbiddenSchoolPanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox forbiddenSchoolSelectionMenu;
    private JTextArea forbiddenSchoolOverview;
    private JTextArea forbiddenSchoolDescription;
    private JLabel forbiddenSchoolImage;
    private JLabel forbiddenSchoolSelectionLabel;
    private JLabel forbiddenSchoolDescriptionLabel;
    private JScrollPane forbiddenSchoolOverviewScroll;
    private JScrollPane forbiddenSchoolDescriptionScroll;
    private JLabel forbiddenSchoolOverviewLabel;
    private JLabel forbiddenSchoolFeatureLabel;
    private JScrollPane forbiddenSchoolFeatureScroll;
    private JTextArea forbiddenSchoolFeature;    
    
    public ForbiddenSchoolPanel(UIModel uiModel) {
        
        this.uiModel = uiModel;
        initializePanel();
    }
    
    public void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        
        forbiddenSchoolOverview = guiMethods.createPlainTextArea("A wizard that chooses to specialize in one school of magic must select two other schools as his opposition schools, representing knowledge sacrificed in one area of arcane lore to gain mastery in another. A wizard who prepares spells from his opposition schools must use two spell slots of that level to prepare the spell. For example, a wizard with evocation as an opposition school must expend two of his available 3rd-level spell slots to prepare a fireball.", false, null);
        forbiddenSchoolOverviewScroll = new TranslucentScrollPane(forbiddenSchoolOverview, new Dimension(970, 120));
        forbiddenSchoolDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("forbiddenArcaneSchool"), false, null);
        forbiddenSchoolDescriptionScroll = new TranslucentScrollPane(forbiddenSchoolDescription, new Dimension (680, 100));
        forbiddenSchoolFeature = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("forbiddenArcaneSchool"), false, null);
        forbiddenSchoolFeatureScroll = new TranslucentScrollPane(forbiddenSchoolFeature, new Dimension(680, 220));
        forbiddenSchoolImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("forbiddenArcaneSchool"));
        forbiddenSchoolImage.setMinimumSize(new Dimension(272, 287));
        String[] items = {"Abjuration", "Conjuration", "Divination", "Enchantment", "Evocation", "Illusion", "Necromancy", "Transmutation", "Universalist"};
        forbiddenSchoolSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        forbiddenSchoolSelectionLabel = new PathfinderLabel("Choose your Forbidden Arcane School: ");
        forbiddenSchoolFeatureLabel = new PathfinderLabel("School Features");
        forbiddenSchoolDescriptionLabel = new PathfinderLabel("Arcane Schools");
        forbiddenSchoolOverviewLabel = new PathfinderLabel("Forbidden Arcane School Overview");
        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align the components along the horizontal axis
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup selectionGroupH = layout.createSequentialGroup().addComponent(forbiddenSchoolSelectionLabel).
                addComponent(forbiddenSchoolSelectionMenu);
        GroupLayout.ParallelGroup infoGroupH = layout.createParallelGroup().addGroup(selectionGroupH)
                .addComponent(forbiddenSchoolDescriptionLabel).addComponent(forbiddenSchoolDescriptionScroll).addComponent(forbiddenSchoolFeatureLabel).addComponent(forbiddenSchoolFeatureScroll);
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addGroup(infoGroupH).addComponent(forbiddenSchoolImage);
        mainHorizontalGroup.addComponent(forbiddenSchoolOverviewLabel).addComponent(forbiddenSchoolOverviewScroll).addGroup(secondaryHorizontalGroup);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup selectionGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(forbiddenSchoolSelectionLabel).addComponent(forbiddenSchoolSelectionMenu);
        GroupLayout.SequentialGroup infoGroupV = layout.createSequentialGroup().addGroup(selectionGroupV).
                addComponent(forbiddenSchoolDescriptionLabel).addComponent(forbiddenSchoolDescriptionScroll).addComponent(forbiddenSchoolFeatureLabel).addComponent(forbiddenSchoolFeatureScroll);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(infoGroupV).addComponent(forbiddenSchoolImage);
        mainVerticalGroup.addComponent(forbiddenSchoolOverviewLabel).addComponent(forbiddenSchoolOverviewScroll).addGroup(secondaryVerticalGroup);
        layout.setVerticalGroup(mainVerticalGroup);          
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        forbiddenSchoolSelectionMenu.setActionCommand("Forbidden Arcane School Selection");
        forbiddenSchoolSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        forbiddenSchoolDescription.setText(uiModel.getCurrentInformation("forbiddenArcaneSchool"));
        forbiddenSchoolDescription.setCaretPosition(0);
        forbiddenSchoolFeature.setText(uiModel.getCurrentFeature("forbiddenArcaneSchool"));
        forbiddenSchoolFeature.setCaretPosition(0);
        forbiddenSchoolImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("forbiddenArcaneSchool"))));
    }
    
    public String getSelectedArcaneSchool() {
        return (String) forbiddenSchoolSelectionMenu.getSelectedItem();
    }
    
}
