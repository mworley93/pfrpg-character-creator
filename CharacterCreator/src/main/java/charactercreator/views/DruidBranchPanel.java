/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Brandon
 */
public class DruidBranchPanel extends ContentPanel {
    private UIModel uiModel;
    private JComboBox druidBranchSelectionMenu;
    private JTextArea druidBranchOverview;
    private JTextArea druidBranchDescription;
    private JLabel druidBranchImage;
    private JLabel druidBranchOverviewLabel;
    private JLabel druidBranchSelectionLabel;
    private JLabel druidBranchDescriptionLabel;
    private JScrollPane druidBranchOverviewScroll;
    private JScrollPane druidBranchScrollDescription;    
    
    public DruidBranchPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        druidBranchOverview = guiMethods.createPlainTextArea(uiModel.getDruidBranchOverview(), false, null);
        druidBranchOverviewScroll = new TranslucentScrollPane(druidBranchOverview, new Dimension(970, 150));
        druidBranchDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("druidBranch"), false, null);
        druidBranchScrollDescription = new TranslucentScrollPane(druidBranchDescription, new Dimension (555, 200));
        druidBranchImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("druidBranch"));
        druidBranchImage.setMinimumSize(new Dimension(400, 302));
        String[] items = {"Animal Companion", "Domain"};
        druidBranchSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        druidBranchSelectionLabel = new PathfinderLabel("Choose your Druid's Nature Bond: ");
        druidBranchDescriptionLabel = new PathfinderLabel("Feature Description");
        druidBranchOverviewLabel = new PathfinderLabel("Druid Nature Bond");        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align the components along the horizontal axis
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup selectionGroupH = layout.createSequentialGroup().addComponent(druidBranchSelectionLabel).
                addComponent(druidBranchSelectionMenu);
        GroupLayout.ParallelGroup infoGroupH = layout.createParallelGroup().addGroup(selectionGroupH)
                .addComponent(druidBranchDescriptionLabel).addComponent(druidBranchScrollDescription);
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(druidBranchImage).addGroup(infoGroupH);
        mainHorizontalGroup.addComponent(druidBranchOverviewLabel).addComponent(druidBranchOverviewScroll).addGroup(secondaryHorizontalGroup);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup selectionGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(druidBranchSelectionLabel).addComponent(druidBranchSelectionMenu);
        GroupLayout.SequentialGroup infoGroupV = layout.createSequentialGroup().addGroup(selectionGroupV).
                addComponent(druidBranchDescriptionLabel).addComponent(druidBranchScrollDescription);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(druidBranchImage).addGroup(infoGroupV);
        mainVerticalGroup.addComponent(druidBranchOverviewLabel).addComponent(druidBranchOverviewScroll).addGroup(secondaryVerticalGroup);
        layout.setVerticalGroup(mainVerticalGroup);         
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        druidBranchSelectionMenu.setActionCommand("Druid Branch Selection");
        druidBranchSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        druidBranchDescription.setText(uiModel.getCurrentInformation("druidBranch"));
        druidBranchDescription.setCaretPosition(0);
        druidBranchImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("druidBranch"))));
    }
    
    public String getSelectedDruidBranch() {
        return (String) druidBranchSelectionMenu.getSelectedItem();
    }
}
