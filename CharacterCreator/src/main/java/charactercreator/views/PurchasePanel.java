/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.components.TranslucentTextPane;
import charactercreator.models.AbilityScore;
import charactercreator.models.AbilityScoreGenerator;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.text.StyleConstants;

/**
 * Implements the panel containing the user interface for the Pathfinder "Purchase" method of generating ability scores. See ContentPanel.java
 * for documentation on overridden methods.
 * @author Megan Worley, Brandon Sharp
 */
public class PurchasePanel extends AbilityScoreSubPanel {
    
    private PathfinderLabel pointsLabel;
    private TranslucentTextPane pointsBank;
    private TranslucentScrollPane pointsTable;
    private PathfinderLabel tableLabel;
    private TranslucentTextPane strPoints;
    private TranslucentTextPane dexPoints;
    private TranslucentTextPane conPoints;
    private TranslucentTextPane intPoints;
    private TranslucentTextPane wisPoints;
    private TranslucentTextPane chaPoints;
    private PathfinderButton resetButton;
    private PathfinderButton strMinus;
    private PathfinderButton strPlus;
    private PathfinderButton dexMinus;
    private PathfinderButton dexPlus;
    private PathfinderButton conMinus;
    private PathfinderButton conPlus;
    private PathfinderButton intMinus;
    private PathfinderButton intPlus;
    private PathfinderButton wisMinus;
    private PathfinderButton wisPlus;
    private PathfinderButton chaMinus;
    private PathfinderButton chaPlus;
    
    public PurchasePanel() {
        super();
    }
    
    
    /**
     * Initializes this JPanel.
     */
    @Override
    public void initializePanel(AbilityScoreGenerator scoreGen) {
        this.scoreGen = scoreGen;
        this.scoreGen.setTotalPoints(15);
        this.scoreGen.resetPointsAndPurchaseScores();
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        this.setOpaque(false);
        super.createLabels();
        // Point displays and buttons.
        Dimension pointsDisplay = new Dimension(50,25);
        pointsLabel = new PathfinderLabel("Remaining points: ", 19);
        pointsBank = new TranslucentTextPane("15", pointsDisplay, StyleConstants.ALIGN_CENTER);
        strPoints = new TranslucentTextPane("10", pointsDisplay, StyleConstants.ALIGN_CENTER);
        dexPoints = new TranslucentTextPane("10", pointsDisplay, StyleConstants.ALIGN_CENTER);
        conPoints = new TranslucentTextPane("10", pointsDisplay, StyleConstants.ALIGN_CENTER);
        intPoints = new TranslucentTextPane("10", pointsDisplay, StyleConstants.ALIGN_CENTER);
        wisPoints = new TranslucentTextPane("10", pointsDisplay, StyleConstants.ALIGN_CENTER);
        chaPoints = new TranslucentTextPane("10", pointsDisplay, StyleConstants.ALIGN_CENTER);
        resetButton = new PathfinderButton("Reset");
        strMinus = new PathfinderButton("-");
        strPlus = new PathfinderButton("+");
        dexMinus = new PathfinderButton("-");
        dexPlus = new PathfinderButton("+");
        conMinus = new PathfinderButton("-");
        conPlus = new PathfinderButton("+");
        intMinus = new PathfinderButton("-");
        intPlus = new PathfinderButton("+");
        wisMinus = new PathfinderButton("-");
        wisPlus = new PathfinderButton("+");
        chaMinus = new PathfinderButton("-");
        chaPlus = new PathfinderButton("+");
        tableLabel = new PathfinderLabel("Table: Ability Score Costs");
        String[] tableHeaders = { "Score", "Points" };
        pointsTable = guiMethods.createScrollingTable(scoreGen.getPointsTableData(), tableHeaders, new Dimension(280,200)); 
    }
    
    @Override
    protected void initializeLayout() {
        
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        int gap = 32;
        // Align components along the horizontal axis.
        GroupLayout.SequentialGroup mainHorizontalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup abilityLabelGroupH = layout.createParallelGroup().addComponent(strLabel).addComponent(dexLabel).addComponent(conLabel).
                addComponent(intLabel).addComponent(wisLabel).addComponent(chaLabel);
        GroupLayout.ParallelGroup minusGroupH = layout.createParallelGroup().addComponent(strMinus).addComponent(dexMinus).addComponent(conMinus).
                addComponent(intMinus).addComponent(wisMinus).addComponent(chaMinus);
        GroupLayout.ParallelGroup pointsGroupH = layout.createParallelGroup().addComponent(strPoints).addComponent(dexPoints).addComponent(conPoints).
                addComponent(intPoints).addComponent(wisPoints).addComponent(chaPoints);
        GroupLayout.ParallelGroup plusGroupH = layout.createParallelGroup().addComponent(strPlus).addComponent(dexPlus).addComponent(conPlus).
                addComponent(intPlus).addComponent(wisPlus).addComponent(chaPlus);
        GroupLayout.SequentialGroup assignGroupH = layout.createSequentialGroup().addGroup(abilityLabelGroupH).addGap(gap).addGroup(minusGroupH).
                addGap(gap).addGroup(pointsGroupH).addGap(gap).addGroup(plusGroupH);
        GroupLayout.SequentialGroup pointsLeftH = layout.createSequentialGroup().addComponent(pointsLabel).addComponent(pointsBank).
                addComponent(resetButton);
        GroupLayout.ParallelGroup pointsSectionH = layout.createParallelGroup().addGroup(pointsLeftH).addGroup(assignGroupH);
        GroupLayout.ParallelGroup tableGroupH = layout.createParallelGroup().addComponent(tableLabel).addComponent(pointsTable);
        mainHorizontalGroup.addGroup(pointsSectionH).addGroup(tableGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.ParallelGroup mainVerticalGroup = layout.createParallelGroup();
        GroupLayout.ParallelGroup strGroupV = layout.createParallelGroup().addComponent(strLabel).addGap(gap).addComponent(strMinus).
                addGap(gap).addComponent(strPoints).addGap(gap).addComponent(strPlus);
        GroupLayout.ParallelGroup dexGroupV = layout.createParallelGroup().addComponent(dexLabel).addGap(gap).addComponent(dexMinus).
                addGap(gap).addComponent(dexPoints).addGap(gap).addComponent(dexPlus);
        GroupLayout.ParallelGroup conGroupV = layout.createParallelGroup().addComponent(conLabel).addGap(gap).addComponent(conMinus).
                addGap(gap).addComponent(conPoints).addGap(gap).addComponent(conPlus);
        GroupLayout.ParallelGroup intGroupV = layout.createParallelGroup().addComponent(intLabel).addGap(gap).addComponent(intMinus).
                addGap(gap).addComponent(intPoints).addGap(gap).addComponent(intPlus);
        GroupLayout.ParallelGroup wisGroupV = layout.createParallelGroup().addComponent(wisLabel).addGap(gap).addComponent(wisMinus).
                addGap(gap).addComponent(wisPoints).addGap(gap).addComponent(wisPlus);
        GroupLayout.ParallelGroup chaGroupV = layout.createParallelGroup().addComponent(chaLabel).addGap(gap).addComponent(chaMinus).
                addGap(gap).addComponent(chaPoints).addGap(gap).addComponent(chaPlus);
        GroupLayout.SequentialGroup assignGroupV = layout.createSequentialGroup().addGroup(strGroupV).addGroup(dexGroupV).addGroup(conGroupV).
                addGroup(intGroupV).addGroup(wisGroupV).addGroup(chaGroupV);
        GroupLayout.ParallelGroup pointsLeftV = layout.createParallelGroup().addComponent(pointsLabel).addComponent(pointsBank).addComponent(resetButton);
        GroupLayout.SequentialGroup pointsSectionV = layout.createSequentialGroup().addGroup(pointsLeftV).addGroup(assignGroupV);
        GroupLayout.SequentialGroup tableGroupV = layout.createSequentialGroup().addComponent(tableLabel).addComponent(pointsTable);
        mainVerticalGroup.addGroup(pointsSectionV).addGroup(tableGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        resetButton.setActionCommand("Reset");
        strPlus.setActionCommand("STR+");
        strMinus.setActionCommand("STR-");
        dexPlus.setActionCommand("DEX+");
        dexMinus.setActionCommand("DEX-");
        conPlus.setActionCommand("CON+");
        conMinus.setActionCommand("CON-");
        intPlus.setActionCommand("INT+");
        intMinus.setActionCommand("INT-");
        wisPlus.setActionCommand("WIS+");
        wisMinus.setActionCommand("WIS-");
        chaPlus.setActionCommand("CHA+");
        chaMinus.setActionCommand("CHA-");
        
        resetButton.addActionListener(listener);
        strPlus.addActionListener(listener);
        strMinus.addActionListener(listener);
        dexPlus.addActionListener(listener);
        dexMinus.addActionListener(listener);
        conPlus.addActionListener(listener);
        conMinus.addActionListener(listener);
        intPlus.addActionListener(listener);
        intMinus.addActionListener(listener);
        wisPlus.addActionListener(listener);
        wisMinus.addActionListener(listener);
        chaPlus.addActionListener(listener);
        chaMinus.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        pointsBank.setText(Integer.toString(scoreGen.getPoints()));
        strPoints.setText(scoreGen.getPurchaseScore(AbilityScore.STR));
        dexPoints.setText(scoreGen.getPurchaseScore(AbilityScore.DEX));
        conPoints.setText(scoreGen.getPurchaseScore(AbilityScore.CON));
        intPoints.setText(scoreGen.getPurchaseScore(AbilityScore.INT));
        wisPoints.setText(scoreGen.getPurchaseScore(AbilityScore.WIS));
        chaPoints.setText(scoreGen.getPurchaseScore(AbilityScore.CHA));   
    }
}
