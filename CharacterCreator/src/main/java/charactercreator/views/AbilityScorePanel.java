/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.PathfinderTabbedPane;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.util.HashMap;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.GroupLayout;

/**
 * This custom JPanel provides the general user interface for the ability score generation screen.  It constructs the labels and descriptive components
 * that the user will always need access to, regardless of which method they choose to generate their ability scores.  It also holds an object that 
 * constructs the panels for the specific techniques of score generation, and displays the appropriate sub-user interface based on their selection.
 * See ContentPanel for documentation on overridden methods.
 * @author Megan Worley, Brandon Sharp
 */
public class AbilityScorePanel extends ContentPanel {
    
    private UIModel contentModel;
    private HashMap<String, AbilityScoreSubPanel> subPanels;
    private JLabel overviewTitle;
    private PathfinderButton scoreInfoButton;
    private JScrollPane overviewScroll;
    private JLabel abilityDescriptionTitle;
    private JTabbedPane abilityDescriptionTabs;
    private JLabel methodTitle;
    private PathfinderComboBox methodSelection;
    private JTextArea currentMethodText;
    private JScrollPane methodDescription;
    private JPanel currentMethodPanel;
    
    /**
     * Main constructor
     * @param model The UIModel that holds the information that needs to be displayed to the ability score screen.
     */
    public AbilityScorePanel(UIModel model) {
        super();
        contentModel = model;
        subPanels = new HashMap();
        initializePanel();
    }
    
        /**
     * Creates the panels pertaining to the methods of generating ability scores and stores them in a HashMap.
     */
    private void createSubPanels() {
        AbilityScoreSubPanel standardPanel = new RollPanel();
        AbilityScoreSubPanel purchasePanel = new PurchasePanel();
        subPanels.put("Standard", standardPanel);
        subPanels.put("Purchase", purchasePanel);
    }
    
    /**
     * Initializes this JPanel.
     */
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    /**
     * Initializes the main elements to the screen that will not be removed, such as ability score descriptions and titles.  These components provide 
     * information the user will need regardless of which method they choose to generate their ability scores.
     */
    @Override
    protected void initializeComponents() {
        // Create the screen components.
        overviewTitle = new PathfinderLabel("Overview");
        JTextArea overviewText = guiMethods.createPlainTextArea(contentModel.getAbilityScoreOverview(), false, null);
        overviewScroll = new TranslucentScrollPane(overviewText, new Dimension(400, 250));
        abilityDescriptionTitle = new PathfinderLabel("Select an ability for a summary and recommendation:");
        scoreInfoButton = new PathfinderButton("?");
        createTabbedAbilityPanel();
        methodTitle = new PathfinderLabel("Select a method to generate your ability scores:");
        String[] generationMethods = { "Standard", "Purchase" };
        methodSelection = new PathfinderComboBox(generationMethods, new Dimension(100, 25));
        currentMethodText = guiMethods.createPlainTextArea(contentModel.getCurrentInformation("methodDescription"), false, null);
        methodDescription = new TranslucentScrollPane(currentMethodText, new Dimension(350, 250));
        createSubPanels();
        currentMethodPanel = subPanels.get(contentModel.getCurrentInformation("abilityScoreMethod"));
    }
    
    /**
     * Creates the tabbed panel that holds all of the ability score descriptions for the user to view.
     */
    private void createTabbedAbilityPanel() {
        abilityDescriptionTabs = new PathfinderTabbedPane();
        
        Dimension size = new Dimension(350, 200);

        JTextArea strengthText = guiMethods.createPlainTextArea(contentModel.getAbilityScoreDescription("STR"), false, null);
        JTextArea dexterityText = guiMethods.createPlainTextArea(contentModel.getAbilityScoreDescription("DEX"), false, null);
        JTextArea constitutionText = guiMethods.createPlainTextArea(contentModel.getAbilityScoreDescription("CON"), false, null);
        JTextArea intelligenceText = guiMethods.createPlainTextArea(contentModel.getAbilityScoreDescription("INT"), false, null);
        JTextArea wisdomText = guiMethods.createPlainTextArea(contentModel.getAbilityScoreDescription("WIS"), false, null);
        JTextArea charismaText = guiMethods.createPlainTextArea(contentModel.getAbilityScoreDescription("CHA"), false, null);
        
        JScrollPane strengthTab = new TranslucentScrollPane(strengthText, size);
        JScrollPane dexterityTab = new TranslucentScrollPane(dexterityText, size);
        JScrollPane constitutionTab = new TranslucentScrollPane(constitutionText, size);
        JScrollPane intelligenceTab = new TranslucentScrollPane(intelligenceText, size);
        JScrollPane wisdomTab = new TranslucentScrollPane(wisdomText, size);
        JScrollPane charismaTab = new TranslucentScrollPane(charismaText, size);
        
        abilityDescriptionTabs.addTab("STR", strengthTab);
        abilityDescriptionTabs.addTab("DEX", dexterityTab);
        abilityDescriptionTabs.addTab("CON", constitutionTab);
        abilityDescriptionTabs.addTab("INT", intelligenceTab);
        abilityDescriptionTabs.addTab("WIS", wisdomTab);
        abilityDescriptionTabs.addTab("CHA", charismaTab);
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
        GroupLayout.SequentialGroup overviewLabelGroupH = layout.createSequentialGroup().addComponent(overviewTitle)
                .addComponent(scoreInfoButton);
        GroupLayout.ParallelGroup overviewGroupH = layout.createParallelGroup().addGroup(overviewLabelGroupH).addComponent(overviewScroll);
        GroupLayout.ParallelGroup tabGroup = layout.createParallelGroup().addComponent(abilityDescriptionTitle).
                addComponent(abilityDescriptionTabs);
        GroupLayout.SequentialGroup overviewTabGroup = layout.createSequentialGroup().addGroup(overviewGroupH).addGroup(tabGroup);
        GroupLayout.SequentialGroup methodSelectGroupH = layout.createSequentialGroup().addComponent(methodTitle).addComponent(methodSelection);
        GroupLayout.SequentialGroup abilityScoreGenGroupH = layout.createSequentialGroup().addComponent(methodDescription).addComponent(currentMethodPanel);
        mainHorizontalGroup.addGroup(overviewTabGroup).addGroup(methodSelectGroupH).addGroup(abilityScoreGenGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);

        // Align components along the vertical axis.
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup overviewLabelGroupV = layout.createParallelGroup().addComponent(overviewTitle)
                .addComponent(scoreInfoButton);
        GroupLayout.SequentialGroup overviewGroupV = layout.createSequentialGroup().addGroup(overviewLabelGroupV).addComponent(overviewScroll);
        GroupLayout.SequentialGroup summaryGroup = layout.createSequentialGroup().addComponent(abilityDescriptionTitle).
                addComponent(abilityDescriptionTabs);
        GroupLayout.ParallelGroup overviewSummaryGroup = layout.createParallelGroup().addGroup(overviewGroupV).
                addGroup(summaryGroup);
        GroupLayout.ParallelGroup methodSelectGroupV = layout.createParallelGroup().addComponent(methodTitle).addComponent(methodSelection);
        GroupLayout.ParallelGroup abilityScoreGenGroupV = layout.createParallelGroup().addComponent(methodDescription).addComponent(currentMethodPanel);
        mainVerticalGroup.addGroup(overviewSummaryGroup).addGroup(methodSelectGroupV).addGroup(abilityScoreGenGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener abilityScoreListener) {
        methodSelection.setActionCommand("Ability Score Method Selection");       
        methodSelection.addActionListener(abilityScoreListener);
        scoreInfoButton.setActionCommand("More Info - Ability Scores"); 
        scoreInfoButton.addActionListener(abilityScoreListener);
    }
    
    @Override
    public void updateView() {
        currentMethodText.setText(contentModel.getCurrentInformation("methodDescription"));
        currentMethodText.setCaretPosition(0);
        this.removeAll();
        currentMethodPanel = subPanels.get(contentModel.getCurrentInformation("abilityScoreMethod"));
        initializeLayout();
    }
    
    /**
     * Returns the HashMap containing the JPanels that hold the UI for the methods of generating ability scores.
     * @return The collection of sub panels. 
     */
    public HashMap<String, AbilityScoreSubPanel> getSubPanels() {
        return subPanels;
    }
    
    public String getCurrentMethod() {
        return (String)methodSelection.getSelectedItem();
    }
}
