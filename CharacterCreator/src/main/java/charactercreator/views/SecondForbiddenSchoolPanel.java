/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

/**
 *
 * @author Brandon
 */


import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class SecondForbiddenSchoolPanel extends ContentPanel{
    
    private UIModel uiModel;
    private JComboBox secondForbiddenSchoolSelectionMenu;
    private JTextArea secondForbiddenSchoolOverview;
    private JTextArea secondForbiddenSchoolDescription;
    private JLabel secondForbiddenSchoolImage;
    private JLabel secondForbiddenSchoolSelectionLabel;
    private JLabel secondForbiddenSchoolDescriptionLabel;
    private JScrollPane secondForbiddenSchoolOverviewScroll;
    private JScrollPane secondForbiddenSchoolDescriptionScroll;
    private JLabel secondForbiddenSchoolOverviewLabel;
    private JLabel secondForbiddenSchoolFeatureLabel;
    private JScrollPane secondForbiddenSchoolFeatureScroll;
    private JTextArea secondForbiddenSchoolFeature;    
    
    public SecondForbiddenSchoolPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    public void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        
        secondForbiddenSchoolOverview = guiMethods.createPlainTextArea("A wizard that chooses to specialize in one school of magic must select two other schools as his opposition schools, representing knowledge sacrificed in one area of arcane lore to gain mastery in another. A wizard who prepares spells from his opposition schools must use two spell slots of that level to prepare the spell. For example, a wizard with evocation as an opposition school must expend two of his available 3rd-level spell slots to prepare a fireball.", false, null);
        secondForbiddenSchoolOverviewScroll = new TranslucentScrollPane(secondForbiddenSchoolOverview, new Dimension(970, 120));
        secondForbiddenSchoolDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("secondForbiddenArcaneSchool"), false, null);
        secondForbiddenSchoolDescriptionScroll = new TranslucentScrollPane(secondForbiddenSchoolDescription, new Dimension (710, 100));
        secondForbiddenSchoolFeature = guiMethods.createPlainTextArea(uiModel.getCurrentFeature("secondForbiddenArcaneSchool"), false, null);
        secondForbiddenSchoolFeatureScroll = new TranslucentScrollPane(secondForbiddenSchoolFeature, new Dimension(710, 220));
        secondForbiddenSchoolImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("secondForbiddenArcaneSchool"));
        secondForbiddenSchoolImage.setMinimumSize(new Dimension(245, 354));
        String[] items = {"Abjuration", "Conjuration", "Divination", "Enchantment", "Evocation", "Illusion", "Necromancy", "Transmutation", "Universalist"};
        secondForbiddenSchoolSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        secondForbiddenSchoolSelectionLabel = new PathfinderLabel("Choose your Second Forbidden Arcane School: ");
        secondForbiddenSchoolFeatureLabel = new PathfinderLabel("School Features");
        secondForbiddenSchoolDescriptionLabel = new PathfinderLabel("Arcane Schools");
        secondForbiddenSchoolOverviewLabel = new PathfinderLabel("Forbidden Arcane School Overview");
        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align the components along the horizontal axis
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup selectionGroupH = layout.createSequentialGroup().addComponent(secondForbiddenSchoolSelectionLabel).
                addComponent(secondForbiddenSchoolSelectionMenu);
        GroupLayout.ParallelGroup infoGroupH = layout.createParallelGroup().addGroup(selectionGroupH)
                .addComponent(secondForbiddenSchoolDescriptionLabel).addComponent(secondForbiddenSchoolDescriptionScroll).addComponent(secondForbiddenSchoolFeatureLabel).addComponent(secondForbiddenSchoolFeatureScroll);
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(secondForbiddenSchoolImage).addGroup(infoGroupH);
        mainHorizontalGroup.addComponent(secondForbiddenSchoolOverviewLabel).addComponent(secondForbiddenSchoolOverviewScroll).addGroup(secondaryHorizontalGroup);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup selectionGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(secondForbiddenSchoolSelectionLabel).addComponent(secondForbiddenSchoolSelectionMenu);
        GroupLayout.SequentialGroup infoGroupV = layout.createSequentialGroup().addGroup(selectionGroupV).
                addComponent(secondForbiddenSchoolDescriptionLabel).addComponent(secondForbiddenSchoolDescriptionScroll).addComponent(secondForbiddenSchoolFeatureLabel).addComponent(secondForbiddenSchoolFeatureScroll);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(secondForbiddenSchoolImage).addGroup(infoGroupV);
        mainVerticalGroup.addComponent(secondForbiddenSchoolOverviewLabel).addComponent(secondForbiddenSchoolOverviewScroll).addGroup(secondaryVerticalGroup);
        layout.setVerticalGroup(mainVerticalGroup);        
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        secondForbiddenSchoolSelectionMenu.setActionCommand("Second Forbidden Arcane School Selection");
        secondForbiddenSchoolSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        secondForbiddenSchoolDescription.setText(uiModel.getCurrentInformation("secondForbiddenArcaneSchool"));
        secondForbiddenSchoolDescription.setCaretPosition(0);
        secondForbiddenSchoolFeature.setText(uiModel.getCurrentFeature("secondForbiddenArcaneSchool"));
        secondForbiddenSchoolFeature.setCaretPosition(0);
        secondForbiddenSchoolImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("secondForbiddenArcaneSchool"))));
    }
    
    public String getSelectedArcaneSchool() {
        return (String) secondForbiddenSchoolSelectionMenu.getSelectedItem();
    }
        
}
