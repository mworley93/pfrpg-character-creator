/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.ColoredCellRenderer;
import charactercreator.components.PathfinderButton;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.PathfinderPopup;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.components.PathfinderTabbedPane;
import charactercreator.components.TranslucentTextPane;
import charactercreator.models.BaseCharacter;
import charactercreator.models.EquipmentGenerator;
import charactercreator.models.UIModel;
import charactercreator.models.equipment.Equipment;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Megan
 */
public class EquipmentPanel extends ContentPanel {
    // Data instances
    private UIModel uiModel;
    private EquipmentGenerator equipGen;
    private BaseCharacter baseCharacter;
    
    // Overview & how-to components
    private PathfinderLabel overviewLabel;
    private TranslucentScrollPane overviewPane;
    private PathfinderLabel howToLabel;
    private TranslucentScrollPane howToPane;
    
    // Gold components
    private TranslucentTextPane goldBox;
    private JLabel goldImage;
    
    // Store components
    private PathfinderLabel storeLabel;
    private PathfinderTabbedPane storeCategories;
    private JList armorStoreList;
    private JList weaponStoreList;
    
    // Inventory components
    private PathfinderLabel inventoryLabel;
    private PathfinderTabbedPane inventoryCategories;
    private JList armorInventoryList;
    private JList weaponInventoryList;
    
    // Transaction components
    private JPanel buttonPanel;
    private PathfinderButton addButton;
    private PathfinderButton removeButton;
    
    // Item information
    private PathfinderLabel statsLabel;
    private JTextArea statsText;
    private TranslucentScrollPane statsPane;
    private JLabel itemImage;
    
    // Constants for specifying layout.
    private final static int TABLE_GAP = 20;
    
    // Constants that define tab indices.
    private final static int TAB_ARMOR = 0;
    private final static int TAB_WEAPON = 1;
    
    /**
     * Default constructor.
     * @param uiModel The UIModel that contains the overview description.
     */
    public EquipmentPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    /**
     * Initializes the components for this FeatsPanel.
     */
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        overviewLabel = new PathfinderLabel("Overview");
        JTextArea overviewText = guiMethods.createPlainTextArea(uiModel.getEquipmentOverview(), false, null);
        overviewPane = new TranslucentScrollPane(overviewText, new Dimension(480, 125));
        
        howToLabel = new PathfinderLabel("How To Use the Equipment Store");
        JTextArea howToText = guiMethods.createPlainTextArea(uiModel.getEquipmentHowTo(), false, null);
        howToPane = new TranslucentScrollPane(howToText, new Dimension(480, 125));
        
        goldImage = guiMethods.constructImageLabel(uiModel.getGoldImage());
        goldBox = new TranslucentTextPane("0", new Dimension(60, 25), StyleConstants.ALIGN_CENTER);
        
        storeLabel = new PathfinderLabel("Store: ");
        Object[] initData = {};
        armorStoreList = initializeEquipmentList(initData);
        weaponStoreList = initializeEquipmentList(initData);
        createStore();
        
        inventoryLabel = new PathfinderLabel("Your Inventory: ");
        armorInventoryList = initializeEquipmentList(initData);
        weaponInventoryList = initializeEquipmentList(initData);
        createInventory();
        
        initializeButtons();
        
        statsLabel = new PathfinderLabel("Item Stats");
        statsText = guiMethods.createPlainTextArea("", false, null);
        statsPane = new TranslucentScrollPane(statsText, new Dimension(480, 130));
        itemImage = guiMethods.constructImageLabel(uiModel.getItemsImage());
        itemImage.setMinimumSize(new Dimension(480, 130));
    }
    
    /**
     * This method initializes a new JList that will contain equipment information.
     * @param data An array full of items.
     * @return The new JList.
     */
    private JList initializeEquipmentList(Object[] data) {
        JList list = guiMethods.createTransparentList(data, ListSelectionModel.SINGLE_SELECTION, JList.VERTICAL);
        list.setBackground(new Color(0, 0, 0, 0));
        list.setCellRenderer(new ColoredCellRenderer());
        list.getSelectionModel().addListSelectionListener(new EquipmentSelectionHandler());
        return list;
    }
    
    /**
     * Creates the store widget that will display all of the items that the user can buy.
     */
    private void createStore() {
        storeCategories = new PathfinderTabbedPane();
        storeCategories.setMaximumSize(new Dimension(400,200));
        TranslucentScrollPane storeArmorPane = new TranslucentScrollPane(armorStoreList, new Dimension(400, 200));
        TranslucentScrollPane storeWeaponPane = new TranslucentScrollPane(weaponStoreList, new Dimension(400, 200));
        storeCategories.addTab("Armor", storeArmorPane);
        storeCategories.addTab("Weapons", storeWeaponPane);
    }
    
    /**
     * Creates the inventory widget that will display all of the items that the user has purchased.
     */
    private void createInventory() {
        inventoryCategories = new PathfinderTabbedPane();
        inventoryCategories.setMaximumSize(new Dimension(400,200));
        TranslucentScrollPane inventoryArmorPane = new TranslucentScrollPane(armorInventoryList, new Dimension(400, 200));
        TranslucentScrollPane inventoryWeaponPane = new TranslucentScrollPane(weaponInventoryList, new Dimension(400, 200));
        inventoryCategories.addTab("Armor", inventoryArmorPane);
        inventoryCategories.addTab("Weapons", inventoryWeaponPane);
    }
    
    /**
     * This method initializes the components and layout for the add and remove buttons in between the store and inventory.
     */
    private void initializeButtons() {
        // The upper limit on both buttons is set to the highest possible value.  By 
        // doing this, the buttons will completely fill in the container that is the BoxLayout JPanel that 
        // they are placed inside.
        
        // Purchase button.
        addButton = new PathfinderButton("Purchase >> ");
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setMinimumSize(new Dimension(50, 25));
        addButton.setPreferredSize(new Dimension(50, 25));
        addButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        
        // Sell button.
        removeButton = new PathfinderButton("<< Sell");       
        removeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        removeButton.setMinimumSize(new Dimension(50, 25));
        removeButton.setPreferredSize(new Dimension(50, 25));
        removeButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        
        // Add the two buttons to a BoxLayout.  This aligns them to be the same size and allows us to easily 
        // specify a gap in between them.
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        buttonPanel.setMaximumSize(new Dimension(120, 75));
        buttonPanel.setOpaque(false);
        buttonPanel.add(addButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonPanel.add(removeButton);
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align components along the horizontal axis.
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
        GroupLayout.ParallelGroup overviewGroupH = layout.createParallelGroup().addComponent(overviewLabel).addComponent(overviewPane);
        GroupLayout.ParallelGroup howToGroupH = layout.createParallelGroup().addComponent(howToLabel).addComponent(howToPane);
        GroupLayout.SequentialGroup introGroupH = layout.createSequentialGroup().addGroup(overviewGroupH).addGroup(howToGroupH);
        GroupLayout.ParallelGroup storeGroupH = layout.createParallelGroup().addComponent(storeLabel).addComponent(storeCategories);
        GroupLayout.SequentialGroup goldGroupH = layout.createSequentialGroup().addComponent(goldImage).addComponent(goldBox);
        GroupLayout.ParallelGroup inventoryGroupH = layout.createParallelGroup().addComponent(inventoryLabel).addComponent(inventoryCategories);
        GroupLayout.ParallelGroup buttonGroupH = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(goldGroupH).addComponent(buttonPanel);
        GroupLayout.SequentialGroup equipmentGroupH = layout.createSequentialGroup().addGroup(storeGroupH).addGap(TABLE_GAP).
                addGroup(buttonGroupH).addGap(TABLE_GAP).addGroup(inventoryGroupH);
        GroupLayout.ParallelGroup itemStatsGroupH = layout.createParallelGroup().addComponent(statsLabel).addComponent(statsPane);
        GroupLayout.SequentialGroup itemGroupH = layout.createSequentialGroup().addGroup(itemStatsGroupH).addComponent(itemImage);
        mainHorizontalGroup.addGroup(introGroupH).addGroup(equipmentGroupH).addGroup(itemGroupH);
        layout.setHorizontalGroup(mainHorizontalGroup);
           
        // Align components along the vertical axis.
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.SequentialGroup overviewGroupV = layout.createSequentialGroup().addComponent(overviewLabel).addComponent(overviewPane);
        GroupLayout.SequentialGroup howToGroupV = layout.createSequentialGroup().addComponent(howToLabel).addComponent(howToPane);
        GroupLayout.ParallelGroup introGroupV = layout.createParallelGroup().addGroup(overviewGroupV).addGroup(howToGroupV);
        GroupLayout.SequentialGroup storeGroupV = layout.createSequentialGroup().addComponent(storeLabel).addComponent(storeCategories);
        GroupLayout.SequentialGroup inventoryGroupV = layout.createSequentialGroup().addComponent(inventoryLabel).addComponent(inventoryCategories);
        GroupLayout.ParallelGroup goldGroupV = layout.createParallelGroup().addComponent(goldImage).addComponent(goldBox);
        GroupLayout.SequentialGroup buttonGroupV = layout.createSequentialGroup().addGroup(goldGroupV).addComponent(buttonPanel);
        GroupLayout.ParallelGroup equipmentGroupV= layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(storeGroupV).addGap(TABLE_GAP).
                addGroup(buttonGroupV).addGap(TABLE_GAP).addGroup(inventoryGroupV);
        GroupLayout.SequentialGroup itemStatsGroupV = layout.createSequentialGroup().addComponent(statsLabel).addComponent(statsPane);
        GroupLayout.ParallelGroup itemGroupV = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addGroup(itemStatsGroupV).addComponent(itemImage);
        mainVerticalGroup.addGroup(introGroupV).addGroup(equipmentGroupV).addGroup(itemGroupV);
        layout.setVerticalGroup(mainVerticalGroup);
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        addButton.setActionCommand("Purchase Item");
        addButton.addActionListener(listener);
        removeButton.setActionCommand("Sell Item");
        removeButton.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        // Updates the armor in the user's inventory.
        ArrayList<Equipment> armor = equipGen.getSelectedArmor();
        armorInventoryList.setListData(equipGen.convertNamesToReadable(armor));
        
        // Updates the displayed weapons in the user's inventory.
        ArrayList<Equipment> weapons = equipGen.getSelectedWeapons();
        weaponInventoryList.setListData(equipGen.convertNamesToReadable(weapons));
        
        // Updates the displayed gold that the user owns.
        goldBox.setText(Integer.toString(equipGen.getGold()));
    }
    
    
    /**
     * Adds a new item to the user's inventory if possible.
     */
    public void addItem() {
        Equipment newItem = equipGen.getEquipment(determineStoreSelection());
        if (newItem != null) {
            if (newItem.getCost() > equipGen.getGold()) {
                // Informs the user that they do not have enough gold to purchase this equipment.
                JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(this);
                PathfinderPopup warning = new PathfinderPopup("Sorry, but you don't have enough gold to buy this!", 400, 150, frame);
                warning.setLocationRelativeTo(this.getParent());
            }
            else if (!equipGen.getFilteredList().contains(newItem)) {
                // Informs the user that they do not meet the item prerequisite.
                JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(this);
                PathfinderPopup warning = new PathfinderPopup("Sorry, but you don't meet the prerequisite for this item.", 400, 150, frame);
                warning.setLocationRelativeTo(this.getParent());
            }
            else {
                equipGen.buyEquipment(newItem);
            }
        }
    }
    
    /**
     * Removes a given item from the inventory if possible.
     */
    public void removeItem() {
       Equipment newItem = equipGen.getEquipment(determineInventorySelection());
        if (newItem != null) {
            equipGen.sellEquipment(newItem);
        }
    }
    
    /**
     * This method sets the BaseCharacter for this class to use.
     * @param baseCharacter The BaseCharacter to set.
     */
    public void setBaseCharacter(BaseCharacter baseCharacter) {
        this.baseCharacter = baseCharacter;
    }
    
    /**
     * This method sets the EquipmentGenerator for this class and updates the display to reflect its information.
     * @param equipGen The EquipmentGenerator to set.
     */
    public void setEquipmentGenerator(EquipmentGenerator equipGen) {
        this.equipGen = equipGen;
        
        // Set the total available weapons to choose from.
        equipGen.fillWeaponlist();
        ArrayList<Equipment> weaponList = equipGen.getWeaponList();
        weaponStoreList.setListData(equipGen.convertNamesToReadable(weaponList));
        
        // Set the available armor to choose from.
        equipGen.fillArmorList();
        ArrayList<Equipment> armorList = equipGen.getArmorList();
        armorStoreList.setListData(equipGen.convertNamesToReadable(armorList));

        // Sets the number of feats available.
        goldBox.setText(Integer.toString(equipGen.getGold())); 
        
        // Updates the armor in the user's inventory.
        ArrayList<Equipment> armorInv = equipGen.getSelectedArmor();
        armorInventoryList.setListData(equipGen.convertNamesToReadable(armorInv));
        
        // Updates the displayed weapons in the user's inventory.
        ArrayList<Equipment> weaponsInv = equipGen.getSelectedWeapons();
        weaponInventoryList.setListData(equipGen.convertNamesToReadable(weaponsInv));
        
    }
    
    /**
     * This method determines which item is selected in the store.  It first finds which tab is selected, and then 
     * checks if any of the items are selected in that tab. 
     * @return The item name, if one is selected, or an empty string.
     */
    private String determineStoreSelection() {
        int tab = storeCategories.getSelectedIndex();
        String selection = "";
        if (tab == TAB_ARMOR) {
            if (armorStoreList.getSelectedIndex() >= 0) {
                selection = (String)armorStoreList.getSelectedValue();
            }
        }
        else if (tab == TAB_WEAPON) {
            if (weaponStoreList.getSelectedIndex() >= 0) {
                selection = (String)weaponStoreList.getSelectedValue();
            }
        }
        return selection;
    }
    
    /**
     * This method determines which item is selected in the inventory.  It first finds which tab is selected, and then 
     * checks if any of the items are selected in that tab. 
     * @return The item name, if one is selected, or an empty string.
     */
    private String determineInventorySelection() {
        int tab = inventoryCategories.getSelectedIndex();
        String selection = " ";
        if (tab == TAB_ARMOR) {
            if (armorInventoryList.getSelectedIndex() >= 0) {
                selection = (String)armorInventoryList.getSelectedValue();
            }
        }
        else if (tab == TAB_WEAPON) {
            if (weaponInventoryList.getSelectedIndex() >= 0) {
                selection = (String)weaponInventoryList.getSelectedValue();
            }
        }
        return selection;
    }
    
    /**
     * This private class defines the behavior for the JLists containing the available equipment to choose from 
     * and the user-selected equipment.  It primarily updates the displayed equipment description as a new piece is 
     * selected in the lists.  It handles new mouse selections, as well as changes due to keyboard arrow keys.
     */
    private class EquipmentSelectionHandler implements ListSelectionListener {    
        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            
            // Update whichever list had a new selection.  Deselect the selection in the other lists if a different list was selected.
            if (!lsm.isSelectionEmpty()) {
                if (armorStoreList.getSelectionModel() == lsm) {
                    String newText = "Prerequisite(s): " + equipGen.getEquipment((String)armorStoreList.getSelectedValue()).getProficiencyRequired();
                    newText = newText + "\n\nGold cost: " + equipGen.getEquipment((String)armorStoreList.getSelectedValue()).getCost();
                    newText = newText + "\n\nDescription: " + equipGen.getEquipment((String)armorStoreList.getSelectedValue()).getEquipmentDescription();
                    statsText.setText(newText);
                    statsText.setCaretPosition(0);
                    armorInventoryList.removeSelectionInterval(armorInventoryList.getSelectedIndex(), armorInventoryList.getSelectedIndex());
                    weaponInventoryList.removeSelectionInterval(weaponInventoryList.getSelectedIndex(), weaponInventoryList.getSelectedIndex());
                    weaponStoreList.removeSelectionInterval(weaponStoreList.getSelectedIndex(), weaponStoreList.getSelectedIndex());
                } 
                else if (weaponStoreList.getSelectionModel() == lsm) {
                   String newText = "Prerequisite(s): " + equipGen.getEquipment((String)weaponStoreList.getSelectedValue()).getProficiencyRequired();
                   newText = newText + "\n\nGold cost: " + equipGen.getEquipment((String)weaponStoreList.getSelectedValue()).getCost();
                   newText = newText + "\n\nDescription: " + equipGen.getEquipment((String)weaponStoreList.getSelectedValue()).getEquipmentDescription();
                    statsText.setText(newText);
                    statsText.setCaretPosition(0);
                    armorInventoryList.removeSelectionInterval(armorInventoryList.getSelectedIndex(), armorInventoryList.getSelectedIndex());
                    weaponInventoryList.removeSelectionInterval(weaponInventoryList.getSelectedIndex(), weaponInventoryList.getSelectedIndex());
                    armorStoreList.removeSelectionInterval(armorStoreList.getSelectedIndex(), armorStoreList.getSelectedIndex());
                }
                else if (armorInventoryList.getSelectionModel() == lsm) {
                    String newText = "Prerequisite(s): " + equipGen.getEquipment((String)armorInventoryList.getSelectedValue()).getProficiencyRequired();
                    newText = newText + "\n\nGold cost: " + equipGen.getEquipment((String)armorInventoryList.getSelectedValue()).getCost();
                    newText = newText + "\n\nDescription: " + equipGen.getEquipment((String)armorInventoryList.getSelectedValue()).getEquipmentDescription();
                    statsText.setText(newText);
                    statsText.setCaretPosition(0);
                    armorStoreList.removeSelectionInterval(armorStoreList.getSelectedIndex(), armorStoreList.getSelectedIndex());
                    weaponInventoryList.removeSelectionInterval(weaponInventoryList.getSelectedIndex(), weaponInventoryList.getSelectedIndex());
                    weaponStoreList.removeSelectionInterval(weaponStoreList.getSelectedIndex(), weaponStoreList.getSelectedIndex());
                }
                else if (weaponInventoryList.getSelectionModel() == lsm) {
                    String newText = "Prerequisite(s): " + equipGen.getEquipment((String)weaponInventoryList.getSelectedValue()).getProficiencyRequired();
                    newText = newText + "\n\nGold cost: " + equipGen.getEquipment((String)weaponInventoryList.getSelectedValue()).getCost();
                    newText = newText + "\n\nDescription: " + equipGen.getEquipment((String)weaponInventoryList.getSelectedValue()).getEquipmentDescription();
                    statsText.setText(newText);
                    statsText.setCaretPosition(0);
                    armorInventoryList.removeSelectionInterval(armorInventoryList.getSelectedIndex(), armorInventoryList.getSelectedIndex());
                    armorStoreList.removeSelectionInterval(armorStoreList.getSelectedIndex(), armorStoreList.getSelectedIndex());
                    weaponStoreList.removeSelectionInterval(weaponStoreList.getSelectedIndex(), weaponStoreList.getSelectedIndex());
                }
            }
        }
    }
}
