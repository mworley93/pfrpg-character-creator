/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderComboBox;
import charactercreator.components.PathfinderLabel;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.models.UIModel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Brandon
 */
public class ArcaneBondPanel extends ContentPanel {
    
    private UIModel uiModel;
    private JComboBox arcaneBondSelectionMenu;
    private JTextArea arcaneBondOverview;
    private JTextArea arcaneBondDescription;
    private JLabel arcaneBondImage;
    private JLabel arcaneBondOverviewLabel;
    private JLabel arcaneBondSelectionLabel;
    private JLabel arcaneBondDescriptionLabel;
    private JScrollPane arcaneBondOverviewScroll;
    private JScrollPane arcaneBondDescriptionScroll;
    
    public ArcaneBondPanel(UIModel uiModel) {
        this.uiModel = uiModel;
        initializePanel();
    }
    
    private void initializePanel() {
        initializeComponents();
        initializeLayout();
    }
    
    @Override
    protected void initializeComponents() {
        arcaneBondOverview = guiMethods.createPlainTextArea(uiModel.getArcaneBondOverview(), false, null);
        arcaneBondOverviewScroll = new TranslucentScrollPane(arcaneBondOverview, new Dimension(970, 120));
        arcaneBondDescription = guiMethods.createPlainTextArea(uiModel.getCurrentInformation("arcaneBond"), false, null);
        arcaneBondDescriptionScroll = new TranslucentScrollPane(arcaneBondDescription, new Dimension (570, 220));
        arcaneBondImage = guiMethods.constructImageLabel(uiModel.getCurrentImagePath("arcaneBond"));
        arcaneBondImage.setMinimumSize(new Dimension(390, 334));
        arcaneBondImage.setMaximumSize(new Dimension(390, 334));
        String[] items = {"Bonded Object", "Familiar"};
        arcaneBondSelectionMenu = new PathfinderComboBox(items, new Dimension(100, 25));
        arcaneBondSelectionLabel = new PathfinderLabel("Choose your Wizard's Arcane Bond: ");
        arcaneBondDescriptionLabel = new PathfinderLabel("Feature Description");
        arcaneBondOverviewLabel = new PathfinderLabel("Wizard Arcane Bond Overview");        
    }
    
    @Override
    protected void initializeLayout() {
        GroupLayout layout = guiMethods.constructGroupLayout(this);
        
        // Align the components along the horizontal axis
        GroupLayout.ParallelGroup mainHorizontalGroup = layout.createParallelGroup();
        GroupLayout.SequentialGroup selectionGroupH = layout.createSequentialGroup().addComponent(arcaneBondSelectionLabel).
                addComponent(arcaneBondSelectionMenu);
        GroupLayout.ParallelGroup infoGroupH = layout.createParallelGroup().addGroup(selectionGroupH)
                .addComponent(arcaneBondDescriptionLabel).addComponent(arcaneBondDescriptionScroll);
        GroupLayout.SequentialGroup secondaryHorizontalGroup = layout.createSequentialGroup().addComponent(arcaneBondImage).addGroup(infoGroupH);
        mainHorizontalGroup.addComponent(arcaneBondOverviewLabel).addComponent(arcaneBondOverviewScroll).addGroup(secondaryHorizontalGroup);
        layout.setHorizontalGroup(mainHorizontalGroup);
        
        // Align components along the vertical axis
        GroupLayout.SequentialGroup mainVerticalGroup = layout.createSequentialGroup();
        GroupLayout.ParallelGroup selectionGroupV = layout.createParallelGroup().
                addComponent(arcaneBondSelectionLabel).addComponent(arcaneBondSelectionMenu);
        GroupLayout.SequentialGroup infoGroupV = layout.createSequentialGroup().addGroup(selectionGroupV).
                addComponent(arcaneBondDescriptionLabel).addComponent(arcaneBondDescriptionScroll);
        GroupLayout.ParallelGroup secondaryVerticalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(arcaneBondImage).addGroup(infoGroupV);
        mainVerticalGroup.addComponent(arcaneBondOverviewLabel).addComponent(arcaneBondOverviewScroll).addGroup(secondaryVerticalGroup).addGap(50);
        layout.setVerticalGroup(mainVerticalGroup);    
    }
    
    @Override
    public void attachActionListener(ActionListener listener) {
        arcaneBondSelectionMenu.setActionCommand("Arcane Bond Selection");
        arcaneBondSelectionMenu.addActionListener(listener);
    }
    
    @Override
    public void updateView() {
        arcaneBondDescription.setText(uiModel.getCurrentInformation("arcaneBond"));
        arcaneBondDescription.setCaretPosition(0);
        arcaneBondImage.setIcon(new ImageIcon(getClass().getResource(uiModel.getCurrentImagePath("arcaneBond"))));
    }
    
    public String getSelectedArcaneBond() {
        return (String) arcaneBondSelectionMenu.getSelectedItem();
    }
    
}
