/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.ImageButton;
import charactercreator.components.PathfinderLabel;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;

/**
 * This class is responsible for the construction of the character creation screens.It has three panels: a title bar at the top, a content panel 
 * in the middle, and a navigation bar to switch between screens at the bottom.  
 * @author Megan Worley, Brandon Sharp
 */
public class CreationPanel extends JPanel 
{   
    private String title; 
    private JPanel navigationBar;
    private JPanel contentPanel;
    private JPanel titleBar;
    private ImageButton prevButton;
    private ImageButton nextButton;
    private ImageButton decisionsButton;
    private ImageIcon borderImage = new ImageIcon(getClass().getResource("/images/ui_elements/border.png"));
    
    /**
     * Main constructor.
     * @param panelName The name of the creation panel, e.g. Class Selection Screen.
     * @param titleFile The file containing the image for the title.
     */
    public CreationPanel(String panelName, String title, JPanel content) {
        this.setLayout(new BorderLayout());    
        this.setOpaque(false);
        this.setName(panelName);
        this.title = title;
        
        // Constructs the three panels.  The contentPanel is a placeholder for now since the implementation of the selection screens is not complete. 
        buildTitleBar();
        buildNavigationBar();
        contentPanel = content;
        contentPanel.setOpaque(false);
        
        addPanels();
    }
    
    /**
     * Constructs the title for the screen based on the file given for the image.
     */
    private void buildTitleBar()
    {
        titleBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        titleBar.setLayout(new BoxLayout(titleBar, BoxLayout.X_AXIS));  
        titleBar.setOpaque(false);
        
        // Set the title to have a drop shadow.
        PathfinderLabel titleLabel = new PathfinderLabel(title, 35);
        titleLabel.drawShadow(true);
        titleBar.add(titleLabel);
        
         // The border on the nav bar should be on the bottom of the panel.
        MatteBorder border = BorderFactory.createMatteBorder(0, 0, 7, 0, borderImage);
        titleBar.setBorder(border);
        
        ImageIcon decisionButtonImage = new ImageIcon(getClass().getResource("/images/ui_elements/decisionbutton.png"));
        decisionsButton = new ImageButton(null, decisionButtonImage);
        decisionsButton = new ImageButton(null, decisionButtonImage);
        
        titleBar.add(Box.createHorizontalGlue());
        titleBar.add(new PathfinderLabel("Decisions Thus Far:", 20));
        titleBar.add(decisionsButton);
        titleBar.add(Box.createVerticalBox());
    }
    
    /**
     * Constructs the bar that holds the buttons the user will press to move back and forth between phases of character creation.
     */
    private void buildNavigationBar()
    {
        navigationBar = new JPanel();
        navigationBar.setLayout(new BoxLayout(navigationBar, BoxLayout.X_AXIS));        
        navigationBar.setOpaque(false);
        
        createButtons();
        
        // Add the two buttons to the panel on opposite sides.
        navigationBar.add(prevButton);
        navigationBar.add(new PathfinderLabel("Previous", 20));
        navigationBar.add(Box.createHorizontalGlue());
        navigationBar.add(new PathfinderLabel("Next", 20));
        navigationBar.add(nextButton);   
        navigationBar.add(Box.createVerticalBox());
        
        // The border on the nav bar should be on the top of the panel.
        MatteBorder border = BorderFactory.createMatteBorder(7, 0, 0, 0, borderImage);
        navigationBar.setBorder(border);
    }
    
    /**
     * Handles the creation of the custom image buttons at the bottom of the screen.
     */
    private void createButtons()
    {
        ImageIcon backButtonImage = new ImageIcon(getClass().getResource("/images/ui_elements/backArrow.png"));
        prevButton = new ImageButton(null, backButtonImage);
        
        ImageIcon forwardButtonImage = new ImageIcon(getClass().getResource("/images/ui_elements/forwardArrow.png"));
        nextButton = new ImageButton(null, forwardButtonImage);
        nextButton.setHorizontalTextPosition(SwingConstants.LEADING);
       
    }
    
    /**
     * Adds the three panels to the creation panel.
     */
    private void addPanels() {
        // Title bar.
        this.add(titleBar, BorderLayout.NORTH);
        // Used to center the content panel, so that when the app is fullscreened, all 
        // of the content appears directly in the middle and not skewed to a side.
        JPanel centerHolder = new JPanel();
        centerHolder.setOpaque(false);
        centerHolder.setLayout(new GridBagLayout());
        centerHolder.add(contentPanel, new GridBagConstraints());
        JScrollPane scroller = new JScrollPane(centerHolder);
        scroller.setOpaque(false);
        scroller.getViewport().setOpaque(false);
        scroller.setBorder(null);
        this.add(scroller, BorderLayout.CENTER);
        // Nav bar.
        this.add(navigationBar, BorderLayout.SOUTH);
    }
    
    /**
     * Adds an action listener to the previous and next buttons.
     * @param listener 
     */
    public void attachActionListener(ActionListener listener) {
        prevButton.setActionCommand("Previous");
        prevButton.addActionListener(listener);
        nextButton.setActionCommand("Next");
        nextButton.addActionListener(listener);
        decisionsButton.setActionCommand("Decision");
        decisionsButton.addActionListener(listener);
    }
    
    /**
     * Gets the button that goes to the previous card.
     * @return The "previous" JButton.
     */
    public ImageButton getPreviousButton() {
        return prevButton;
    }
    
    /**
     * Gets the button that goes to the next card.
     * @return The next JButton.
     */
    public ImageButton getNextButton() {
        return nextButton;
    }
    
    /**
     * Gets the button that shows the decisions made thus far.
     * @return The decisions JButton.
     */
    public ImageButton getDecisionsButton() {
        return decisionsButton;
    }
}
