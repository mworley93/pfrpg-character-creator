/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.views;

import charactercreator.components.PathfinderMessagePopup;
import charactercreator.helpers.GUIHelper;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * This abstract class is responsible provides an outline for a JPanel that contains custom graphics and actions.  It is largely responsible for acting
 * as the base to the majority of the panels in the program (the "content" panels).
 * @author Megan Worley, Brandon Sharp
 */
public abstract class ContentPanel extends JPanel {
      
    protected GUIHelper guiMethods;
    protected JScrollPane scrollBackground;
    
    /**
     * Main constructor.
     * @param model The AbilityScoreGenerator that holds the needed data.
     */
    public ContentPanel() {
        guiMethods = new GUIHelper();
    }
    
    /**
     * Constructs the components for this JPanel.
     */
    protected abstract void initializeComponents();
    
    /**
     * Arranges the components in this JPanel to a layout.
     */
    protected abstract void initializeLayout();
    
    /**
     * Attaches an ActionListener to this JPanel's components.
     * @param panelListener The ActionListener to attach.
     */
    public abstract void attachActionListener(ActionListener panelListener);    
    
    /**
     * Updates the view displayed by this JPanel.
     */
    public abstract void updateView();
    
    /**
     * Causes a pop up to display when an info button is pressed.
     * @param title The title of the pop up.
     * @param text The descriptive text that is displayed.
     */
    public void displayPopup(String title, String text) {
        PathfinderMessagePopup popup = new PathfinderMessagePopup(title);
        popup.setString(text);
        popup.setLocationRelativeTo(this.getParent());
    }
}


