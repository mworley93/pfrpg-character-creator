/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.helpers;

import charactercreator.models.AbilityScore;
import charactercreator.models.BaseCharacter;
import charactercreator.models.Feat;
import charactercreator.models.Skill;
import charactercreator.models.Spell;
import charactercreator.models.equipment.Armor;
import charactercreator.models.equipment.Weapon;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

/**
 * This class is responsible for loading a PDF character sheet with forms, filling the 
 * forms with data corresponding to the user's character, and saving it to a file specified 
 * on their computer.
 * @author Megan Worley
 */
public class PDFCreator {
    private static final String CHARACTER_SHEET_LOCATION = "/pdf/PathfinderRPGCharacterSheet.pdf";
    private PDDocument pdfDocument;
    
    public PDFCreator() {
    }

    /**
     * This method loads the character sheet PDF from the resources.
     * @throws IOException
     * @throws COSVisitorException 
     */
    public void loadCharacterSheet() throws IOException, COSVisitorException {
        java.net.URL characterSheetURL = getClass().getResource(CHARACTER_SHEET_LOCATION);
        pdfDocument = PDDocument.load(characterSheetURL);
    }
    
    /**
     * This method will be used to set all of the fields in the character sheet with the values 
     * found in BaseCharacter.
     */
    public void fillCharacterSheet(BaseCharacter character) throws IOException {
        setField("Character_Name", character.getName());
        setField("Alignment", character.getAlignment());
        setField("Player", character.getPlayerName());
        setField("Character_Level", character.getCharacterClass().getClassName() + " Level 1");
        setField("Race", character.getCharacterRace().getRaceName());
        setField("Size", character.getCharacterRace().getSize());
        setField("Gender", character.getGender());
        setField("Height", Integer.toString(character.getHeight()));
        setField("Weight", Integer.toString(character.getWeight()));
        setField("Age", Integer.toString(character.getAge()));
        fillAbilityScores(character);
        setField("Hit_Points", Integer.toString(character.getCharacterClass().getHitDie() + character.getAbilityScoreModifier(AbilityScore.CON)));
        setField("Speed_Feet", Integer.toString(character.getCharacterRace().getSpeed()));
        setField("Speed_Squares", Integer.toString((character.getCharacterRace().getSpeed())/5));
        setField("Initiative", Integer.toString((character.getAbilityScoreModifier(AbilityScore.DEX))));
        setField("Initiative_DEX_Mod", Integer.toString((character.getAbilityScoreModifier(AbilityScore.DEX))));
        fillArmorClass(character);
        fillDefenses(character);
        setField("Base_Attack_Bonus", Integer.toString(character.getCharacterClass().getBaseAttackBonus()));
        fillCombatManeuvers(character);
        fillWeapons(character);
        fillSkills(character);
        fillArmor(character);
        fillGear(character);
        fillMoney(character);
        fillFeats(character);
        fillAbilities(character);
        fillDomainSchool(character);
        fillSpells(character);
        fillSpellStats(character);
        setField("Languages_1", Arrays.toString(character.getCharacterRace().getLanguages()));
        setField("Background", character.getBiography());
    }
    
    private void fillSpells(BaseCharacter character) {
        if(character.getCharacterClass().getClassName().matches("Wizard")) {
            int counter = 1;
            for (Spell s : character.getSpellList()) {
                String scounter = Integer.toString(counter);
                setField("Spell1_" + scounter, s.getSpellName());
                counter++;
            }
        }
        else {
        }
    }
    
    private void fillSpellStats(BaseCharacter character) {
        setField("Spells_Per_Day0", "3");
        setField("Spells_Per_Day1", "1");
        setField("Bonus_Spells1", "1");
        
        if(character.getCharacterClass().getClassName().matches("Wizard")) {
            setField("Save_DC0", Integer.toString(10 + character.getAbilityScoreModifier(AbilityScore.INT)));
            setField("Save_DC1", Integer.toString(11 + character.getAbilityScoreModifier(AbilityScore.INT)));
            setField("Spells_Known1", Integer.toString(3 + character.getAbilityScoreModifier(AbilityScore.INT)));
            
        }
        
        if(character.getCharacterClass().getClassName().matches("Druid")) {
            setField("Save_DC0", Integer.toString(10 + character.getAbilityScoreModifier(AbilityScore.WIS)));
            setField("Save_DC1", Integer.toString(11 + character.getAbilityScoreModifier(AbilityScore.WIS)));
        }
        
        if(character.getCharacterClass().getClassName().matches("Cleric")) {
            setField("Save_DC0", Integer.toString(10 + character.getAbilityScoreModifier(AbilityScore.WIS)));
            setField("Save_DC1", Integer.toString(11 + character.getAbilityScoreModifier(AbilityScore.WIS)));
        }
        
        if(character.getCharacterClass().getClassName().matches("Sorcerer")) {
            setField("Save_DC0", Integer.toString(10 + character.getAbilityScoreModifier(AbilityScore.CHA)));
            setField("Save_DC1", Integer.toString(11 + character.getAbilityScoreModifier(AbilityScore.CHA)));
            setField("Spells_Known0", Integer.toString(4));
            setField("Spells_Known1", Integer.toString(2));
        }
        
    }
    
    private void fillDomainSchool(BaseCharacter character) {
        if(character.getCharacterClass().getClassName().matches("Wizard")) {
            setField("Domain_School_1", character.getFirstDomain());
        }
        if(character.getCharacterClass().getClassName().matches("Sorcerer")) {
            setField("Domain_School_1", character.getBloodline());
        }
        if(character.getCharacterClass().getClassName().matches("Druid")) {
            setField("Domain_School_1", character.getFirstDomain());
        }
        if(character.getCharacterClass().getClassName().matches("Cleric")) {
            String tempString = "";
            tempString = character.getFirstDomain() + character.getSecondDomain();
        }
    }
    
    private void fillAbilities(BaseCharacter character) {
        int counter = 1;
        for (int i = 0; i < character.getCharacterRace().getNumClassFeatures(); i++) {
            String scounter = Integer.toString(counter);
            String tempString = character.getCharacterRace().getRaceFeatures()[i];
            setField("SPECIAL ABILITIES " + scounter, tempString);
            counter += 1;
        }
        for (String s : character.getCharacterClass().getClassFeatures()) {
            if (counter > 20) break;
            String scounter = Integer.toString(counter);
            setField("SPECIAL ABILITIES " + scounter, s);
            counter += 1;
        }
    }
    
    
    private void fillFeats(BaseCharacter character) {
        int counter = 1;
        for (Feat f : character.getFeatList()) {
            String scounter = Integer.toString(counter);
            setField("FEATS " + scounter, f.getFeatName());
            counter += 1;
        }
    }
    
    private void fillMoney(BaseCharacter character) {
        setField("CP", "0");
        setField("SP", "0");
        setField("PP", "0");
        setField("GP", "0");
    }
    
    private void fillGear(BaseCharacter character) {
        int counter = 1;
        for (Weapon w : character.getWeaponsList()) {
            String scounter = Integer.toString(counter);
            setField("Gear" + scounter, w.getName());
            setField("Gear" + scounter + "_Weight", Double.toString(w.getWeight()));
            counter += 1;
        }
        for (Armor a : character.getArmorList()) { 
            String scounter = Integer.toString(counter);
            setField("Gear" + scounter, a.getName());
            setField("Gear" + scounter + "_Weight", Double.toString(a.getWeight()));
            counter += 1;
        }
    }
    
    private void fillArmor(BaseCharacter character) {
        int counter = 1;
        for (Armor a : character.getArmorList()) {
            String scounter = Integer.toString(counter);
            if (counter > 5) break;
            setField("Armor" + scounter, a.getName());
            setField("Armor" + scounter + "_Bonus", Integer.toString(a.getArmorBonus()));
            setField("Armor" + scounter + "_Check_Penalty", Integer.toString(a.getArmorCheckPenalty()));
            String failure = Double.toString(a.getArcaneFailureChance() * 100);
            setField("Armor" + scounter + "_Spell_Failure", failure);
            setField("Armor" + scounter + "_Weight", Double.toString(a.getWeight()));
            counter += 1;
        }
    }
    
    private void fillSkills(BaseCharacter character) {
        setField("Acrobatics_Total", Integer.toString(character.getFinalSkillMap().get(Skill.ACROBATICS)));
        setField("Acrobatics_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Acrobatics_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.ACROBATICS)));
        if((character.getRankSkillMap().get(Skill.ACROBATICS) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.ACROBATICS)) {
            setField("Acrobatics_Misc", "3");
        }
        else setField("Acrobatics_Misc", "0");
        
        setField("Appraise_Total", Integer.toString(character.getFinalSkillMap().get(Skill.APPRAISE)));
        setField("Appraise_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Appraise_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.APPRAISE)));
        if((character.getRankSkillMap().get(Skill.APPRAISE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.APPRAISE)) {
            setField("Appraise_Misc", "3");
        }
        else setField("Appraise_Misc", "0");
        
        setField("Bluff_Total", Integer.toString(character.getFinalSkillMap().get(Skill.BLUFF)));
        setField("Bluff_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CHA)));
        setField("Bluff_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.BLUFF)));
        if((character.getRankSkillMap().get(Skill.BLUFF) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.BLUFF)) {
            setField("Bluff_Misc", "3");
        }
        else setField("Bluff_Misc", "0");
        
        setField("Climb_Total", Integer.toString(character.getFinalSkillMap().get(Skill.CLIMB)));
        setField("Climb_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.STR)));
        setField("Climb_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.CLIMB)));
        if((character.getRankSkillMap().get(Skill.CLIMB) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.CLIMB)) {
            setField("Climb_Misc", "3");
        }
        else setField("Climb_Misc", "0");
        
        setField("Craft1_Total", Integer.toString(character.getFinalSkillMap().get(Skill.CRAFT)));
        setField("Craft1_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.STR)));
        setField("Craft_Ranks1", Integer.toString(character.getRankSkillMap().get(Skill.CRAFT)));
        if((character.getRankSkillMap().get(Skill.CRAFT) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.CRAFT)) {
            setField("Craft1_Misc", "3");
        }
        else setField("Craft1_Misc", "0");
        
        setField("Diplomacy_Total", Integer.toString(character.getFinalSkillMap().get(Skill.DIPLOMACY)));
        setField("Diplomacy_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CHA)));
        setField("Diplomacy_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.DIPLOMACY)));
        if((character.getRankSkillMap().get(Skill.DIPLOMACY) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.DIPLOMACY)) {
            setField("Diplomacy_Misc", "3");
        }
        else setField("Diplomacy_Misc", "0");
        
        setField("Disable_Device_Total", Integer.toString(character.getFinalSkillMap().get(Skill.DISABLEDEVICE)));
        setField("Disable_Device_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Disable_Device_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.DISABLEDEVICE)));
        if((character.getRankSkillMap().get(Skill.DISABLEDEVICE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.DISABLEDEVICE)) {
            setField("Disable_Device_Misc", "3");
        }
        else setField("Disable_Device_Misc", "0");
        
        setField("Disguise_Total", Integer.toString(character.getFinalSkillMap().get(Skill.DISGUISE)));
        setField("Disguise_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CHA)));
        setField("Disguise_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.DISGUISE)));
        if((character.getRankSkillMap().get(Skill.DISGUISE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.DISGUISE)) {
            setField("Disguise_Misc", "3");
        }
        else setField("Disguise_Misc", "0");
        
        setField("Escape_Artist_Total", Integer.toString(character.getFinalSkillMap().get(Skill.ESCAPEARTIST)));
        setField("Escape_Artist_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Escape_Artist_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.ESCAPEARTIST)));
        if((character.getRankSkillMap().get(Skill.ESCAPEARTIST) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.ESCAPEARTIST)) {
            setField("Escape_Artist_Misc", "3");
        }
        else setField("Escape_Artist_Misc", "0");
        
        setField("Fly_Total", Integer.toString(character.getFinalSkillMap().get(Skill.FLY)));
        setField("Fly_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Fly_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.FLY)));
        if((character.getRankSkillMap().get(Skill.FLY) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.FLY)) {
            setField("Fly_Misc", "3");
        }
        else setField("Fly_Misc", "0");
        
        setField("Handle_Animal_Total", Integer.toString(character.getFinalSkillMap().get(Skill.HANDLEANIMAL)));
        setField("Handle_Animal_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CHA)));
        setField("Handle_Animal_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.HANDLEANIMAL)));
        if((character.getRankSkillMap().get(Skill.HANDLEANIMAL) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.HANDLEANIMAL)) {
            setField("Handle_Animal_Misc", "3");
        }
        else setField("Handle_Animal_Misc", "0");
        
        setField("Heal_Total", Integer.toString(character.getFinalSkillMap().get(Skill.HEAL)));
        setField("Heal_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.WIS)));
        setField("Heal_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.HEAL)));
        if((character.getRankSkillMap().get(Skill.HEAL) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.HEAL)) {
            setField("Heal_Misc", "3");
        }
        else setField("Heal_Misc", "0");
        
        setField("Intimidate_Total", Integer.toString(character.getFinalSkillMap().get(Skill.INTIMIDATE)));
        setField("Intimidate_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CHA)));
        setField("Intimidate_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.INTIMIDATE)));
        if((character.getRankSkillMap().get(Skill.INTIMIDATE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.INTIMIDATE)) {
            setField("Intimidate_Misc", "3");
        }
        else setField("Intimidate_Misc", "0");
        
        setField("Knowledge_Arcana_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGEARCANA)));
        setField("Knowledge_Arcana_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Arcana_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGEARCANA)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGEARCANA) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGEARCANA)) {
            setField("Knowledge_Arcana_Misc", "3");
        }
        else setField("Knowledge_Arcana_Misc", "0");
        
        setField("Knowledge_Dungeoneering_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGEDUNGEONEERING)));
        setField("Knowledge_Dungeoneering_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Dungeoneering_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGEDUNGEONEERING)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGEDUNGEONEERING) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGEDUNGEONEERING)) {
            setField("Knowledge_Dungeoneering_Misc", "3");
        }
        else setField("Knowledge_Dungeoneering_Misc", "0");
        
        setField("Knowledge_Engineering_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGEENGINEERING)));
        setField("Knowledge_Engineering_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Engineering_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGEENGINEERING)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGEENGINEERING) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGEENGINEERING)) {
            setField("Knowledge_Engineering_Misc", "3");
        }
        else setField("Knowledge_Engineering_Misc", "0");
        
        setField("Knowledge_Geography_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGEGEOGRAPHY)));
        setField("Knowledge_Geography_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Geography_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGEGEOGRAPHY)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGEGEOGRAPHY) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGEGEOGRAPHY)) {
            setField("Knowledge_Geography_Misc", "3");
        }
        else setField("Knowledge_Geography_Misc", "0");
        
        setField("Knowledge_History_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGEHISTORY)));
        setField("Knowledge_History_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_History_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGEHISTORY)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGEHISTORY) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGEHISTORY)) {
            setField("Knowledge_History_Misc", "3");
        }
        else setField("Knowledge_History_Misc", "0");
        
        setField("Knowledge_Local_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGELOCAL)));
        setField("Knowledge_Local_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Local_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGELOCAL)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGELOCAL) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGELOCAL)) {
            setField("Knowledge_Local_Misc", "3");
        }
        else setField("Knowledge_Local_Misc", "0");
        
        setField("Knowledge_Nature_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGENATURE)));
        setField("Knowledge_Nature_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Nature_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGENATURE)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGENATURE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGENATURE)) {
            setField("Knowledge_Nature_Misc", "3");
        }
        else setField("Knowledge_Nature_Misc", "0");
        
        setField("Knowledge_Nobility_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGENOBILITY)));
        setField("Knowledge_Nobility_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Nobility_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGENOBILITY)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGENOBILITY) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGENOBILITY)) {
            setField("Knowledge_Nobility_Misc", "3");
        }
        else setField("Knowledge_Nobility_Misc", "0");
        
        setField("Knowledge_Planes_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGEPLANES)));
        setField("Knowledge_Planes_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Planes_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGEPLANES)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGEPLANES) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGEPLANES)) {
            setField("Knowledge_Planes_Misc", "3");
        }
        else setField("Knowledge_Planes_Misc", "0");
        
        setField("Knowledge_Religion_Total", Integer.toString(character.getFinalSkillMap().get(Skill.KNOWLEDGERELIGION)));
        setField("Knowledge_Religion_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Knowledge_Religion_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.KNOWLEDGERELIGION)));
        if((character.getRankSkillMap().get(Skill.KNOWLEDGERELIGION) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.KNOWLEDGERELIGION)) {
            setField("Knowledge_Religion_Misc", "3");
        }
        else setField("Knowledge_Religion_Misc", "0");
        
        setField("Linguistics_Total", Integer.toString(character.getFinalSkillMap().get(Skill.LINGUISTICS)));
        setField("Linguistics_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Linguistics_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.LINGUISTICS)));
        if((character.getRankSkillMap().get(Skill.LINGUISTICS) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.LINGUISTICS)) {
            setField("Linguistics_Misc", "3");
        }
        else setField("Linguistics_Misc", "0");
        
        setField("Perception_Total", Integer.toString(character.getFinalSkillMap().get(Skill.PERCEPTION)));
        setField("Perception_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.WIS)));
        setField("Perception_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.PERCEPTION)));
        if((character.getRankSkillMap().get(Skill.PERCEPTION) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.PERCEPTION)) {
            setField("Perception_Misc", "3");
        }
        else setField("Perception_Misc", "0");
        
        setField("Perform1_Total", Integer.toString(character.getFinalSkillMap().get(Skill.PERFORM)));
        setField("Perform1_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CHA)));
        setField("Perform1_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.PERFORM)));
        if((character.getRankSkillMap().get(Skill.PERFORM) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.PERFORM)) {
            setField("Perform1_Misc", "3");
        }
        else setField("Perform1_Misc", "0");
        
        setField("Profession1_Total", Integer.toString(character.getFinalSkillMap().get(Skill.PROFESSION)));
        setField("Profession1_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.WIS)));
        setField("Profession1_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.PROFESSION)));
        if((character.getRankSkillMap().get(Skill.PROFESSION) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.PROFESSION)) {
            setField("Profession1_Misc", "3");
        }
        else setField("Profession1_Misc", "0");
        
        setField("Ride_Total", Integer.toString(character.getFinalSkillMap().get(Skill.RIDE)));
        setField("Ride_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Ride_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.RIDE)));
        if((character.getRankSkillMap().get(Skill.RIDE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.RIDE)) {
            setField("Ride_Misc", "3");
        }
        else setField("Ride_Misc", "0");
        
        setField("Sense_Motive_Total", Integer.toString(character.getFinalSkillMap().get(Skill.SENSEMOTIVE)));
        setField("Sense_Motive_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.WIS)));
        setField("Sense_Motive_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.SENSEMOTIVE)));
        if((character.getRankSkillMap().get(Skill.SENSEMOTIVE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.SENSEMOTIVE)) {
            setField("Sense_Motive_Misc", "3");
        }
        else setField("Sense_Motive_Misc", "0");
        
        setField("Sleight_Of_Hand_Total", Integer.toString(character.getFinalSkillMap().get(Skill.SLEIGHTOFHAND)));
        setField("Sleight_Of_Hand_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Sleight_Of_Hand_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.SLEIGHTOFHAND)));
        if((character.getRankSkillMap().get(Skill.SLEIGHTOFHAND) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.SLEIGHTOFHAND)) {
            setField("Sleight_Of_Hand_Misc", "3");
        }
        else setField("Sleight_Of_Hand_Misc", "0");
        
        setField("Spellcraft_Total", Integer.toString(character.getFinalSkillMap().get(Skill.SPELLCRAFT)));
        setField("Spellcraft_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.INT)));
        setField("Spellcraft_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.SPELLCRAFT)));
        if((character.getRankSkillMap().get(Skill.SPELLCRAFT) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.SPELLCRAFT)) {
            setField("Spellcraft_Misc", "3");
        }
        else setField("Spellcraft_Misc", "0");
        
        setField("Stealth_Total", Integer.toString(character.getFinalSkillMap().get(Skill.STEALTH)));
        setField("Stealth_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Stealth_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.STEALTH)));
        if((character.getRankSkillMap().get(Skill.STEALTH) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.STEALTH)) {
            setField("Stealth_Misc", "3");
        }
        else setField("Stealth_Misc", "0");
        
        setField("Survival_Total", Integer.toString(character.getFinalSkillMap().get(Skill.SURVIVAL)));
        setField("Survival_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.WIS)));
        setField("Survival_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.SURVIVAL)));
        if((character.getRankSkillMap().get(Skill.SURVIVAL) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.SURVIVAL)) {
            setField("Survival_Misc", "3");
        }
        else setField("Survival_Misc", "0");
        
        setField("Swim_Total", Integer.toString(character.getFinalSkillMap().get(Skill.SWIM)));
        setField("Swim_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.STR)));
        setField("Swim_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.SWIM)));
        if((character.getRankSkillMap().get(Skill.SWIM) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.SWIM)) {
            setField("Swim_Misc", "3");
        }
        else setField("Swim_Misc", "0");
        
        setField("Use_Magic_Device_Total", Integer.toString(character.getFinalSkillMap().get(Skill.USEMAGICDEVICE)));
        setField("Use_Magic_Device_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CHA)));
        setField("Use_Magic_Device_Ranks", Integer.toString(character.getRankSkillMap().get(Skill.USEMAGICDEVICE)));
        if((character.getRankSkillMap().get(Skill.USEMAGICDEVICE) > 0) && character.getCharacterClass().getClassSkillsMap().get(Skill.USEMAGICDEVICE)) {
            setField("Use_Magic_Device_Misc", "3");
        }
        else setField("Use_Magic_Device_Misc", "0");
    }
    
    private void fillWeapons(BaseCharacter character) {
        ArrayList<String> rangedWeapons = new ArrayList<>();
        rangedWeapons.add("Longbow");
        rangedWeapons.add("Composite Longbow");
        rangedWeapons.add("Shortbow");
        rangedWeapons.add("Composite Shortbow");
        int counter = 1;
        for (Weapon w : character.getWeaponsList()) {
            String scounter = Integer.toString(counter);
            if (counter > 5) break;
            setField("Weapon" + scounter, w.getName());
            if(rangedWeapons.contains(w.getName())) {
                setField("Weapon" + scounter + "_Attack_Bonus", Integer.toString(character.getCharacterClass().getBaseAttackBonus() + character.getAbilityScoreModifier(AbilityScore.DEX)));
            }
            else {
                setField("Weapon" + scounter + "_Attack_Bonus", Integer.toString(character.getCharacterClass().getBaseAttackBonus() + character.getAbilityScoreModifier(AbilityScore.STR)));
            }
            setField("Weapon" + scounter + "_Critical", w.getCritical());
            setField("Weapon" + scounter + "_Type", w.getDamageType());
            setField("Weapon" + scounter + "_Range", Integer.toString(w.getRange()));
            if(character.getCharacterRace().getSize().matches("Small")) {
                String damage;
                if (rangedWeapons.contains(w.getName())) damage = " + " + Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX));
                else damage = " + " + Integer.toString(character.getAbilityScoreModifier(AbilityScore.STR));
                setField("Weapon" + scounter + "_Damage", damage);
            }
            else {
                String damage;
                if (rangedWeapons.contains(w.getName())) damage = " + " + Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX));
                else damage = " + " + Integer.toString(character.getAbilityScoreModifier(AbilityScore.STR));
                setField("Weapon" + scounter + "_Damage", damage);
            }
            counter += 1;
        }
    }
    private void fillCombatManeuvers(BaseCharacter character) {
        int cmbTotal = 0;
        setField("CMB_Base_Attack_Bonus", Integer.toString(character.getCharacterClass().getBaseAttackBonus()));
        cmbTotal += character.getCharacterClass().getBaseAttackBonus();
        setField("CMB_STR_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.STR)));
        cmbTotal += character.getAbilityScoreModifier(AbilityScore.STR);
        if (character.getCharacterRace().getSize().matches("Small")) {
            cmbTotal -= 1;
            setField("CMB_Size_Mod", "-1");
        }
        else setField("CMB_Size_Mod", "0");
        setField("CMB", Integer.toString(cmbTotal));
        
        int cmdTotal = 10;
        setField("CMD_Base_Attack_Bonus", Integer.toString(character.getCharacterClass().getBaseAttackBonus()));
        cmdTotal += character.getCharacterClass().getBaseAttackBonus();
        setField("CMD_STR_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.STR)));
        cmdTotal += character.getAbilityScoreModifier(AbilityScore.STR);
        setField("CMD_DEX_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        cmdTotal += character.getAbilityScoreModifier(AbilityScore.DEX);
        if (character.getCharacterRace().getSize().matches("Small")) {
            cmdTotal -= 1;
            setField("CMD_Size_Mod", "-1");
        }
        else setField("CMD_Size_Mod", "0");
        setField("CMD", Integer.toString(cmdTotal));
        
    }
    private void fillAbilityScores(BaseCharacter character) {
        Iterator<AbilityScore> enumKeySet = character.getBaseAbilityScores().keySet().iterator();
        while(enumKeySet.hasNext()) {
            AbilityScore currentScore = enumKeySet.next();
            String baseField = currentScore.getString();
            setField(baseField, Integer.toString(character.getFinalAbilityScores().get(currentScore)));
            String modField = currentScore.getString() + "_Mod";
            setField(modField, Integer.toString(character.getAbilityScoreModifier(currentScore)));
        }
    }
    
    private void fillArmorClass(BaseCharacter character) {
        int totalAC = 10;
        Armor bestArmor = new Armor();
        bestArmor.setArmorBonus(0);
        Armor bestShield = new Armor();
        bestShield.setArmorBonus(0);
        for (Armor a : character.getArmorList()) {
            if (a.getProficiencyRequired().matches("Shield Proficiency")) {
                if (a.getArmorBonus() > bestShield.getArmorBonus()) bestShield = a;
            }
            else {
                if (a.getArmorBonus() > bestArmor.getArmorBonus()) bestArmor = a;
            }
        }
        setField("Armor_Bonus", Integer.toString(bestArmor.getArmorBonus()));
        totalAC += bestArmor.getArmorBonus();
        setField("Shield_Bonus", Integer.toString(bestShield.getArmorBonus()));
        totalAC += bestShield.getArmorBonus();
        setField("AC_DEX_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        totalAC += character.getAbilityScoreModifier(AbilityScore.DEX);
        if(character.getCharacterRace().getSize().matches("Small")) {
            setField("Size_Mod", "1");
            totalAC += 1;
        }
        else setField("Size_Mod", "0");
        setField("Armor_Class", Integer.toString(totalAC));
        setField("Touch_Armor_Class", Integer.toString(10 + character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Flat_Armor_Class", Integer.toString(totalAC - character.getAbilityScoreModifier(AbilityScore.DEX)));
    }
    
    private void fillDefenses(BaseCharacter character) {
        setField("Fortitude", Integer.toString(character.getCharacterClass().getFortitude() + character.getAbilityScoreModifier(AbilityScore.CON)));
        setField("Fort_Base_Save", Integer.toString(character.getCharacterClass().getFortitude()));
        setField("Fort_Ability_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.CON)));
        
        setField("Reflex", Integer.toString(character.getCharacterClass().getReflex() + character.getAbilityScoreModifier(AbilityScore.DEX)));
        setField("Reflex_Base_Save", Integer.toString(character.getCharacterClass().getReflex()));
        setField("Reflex_Ability_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.DEX)));
        
        setField("Will", Integer.toString(character.getCharacterClass().getWill() + character.getAbilityScoreModifier(AbilityScore.WIS)));
        setField("Will_Base_Save", Integer.toString(character.getCharacterClass().getWill()));
        setField("Will_Ability_Mod", Integer.toString(character.getAbilityScoreModifier(AbilityScore.WIS)));
        
    }
	
    /**
     * This method is used to set a form field in a loaded PDF document.
     * Assistance from: http://www.daedtech.com/programatically-filling-out-pdfs-in-java
     * @param name The name of the field.
     * @param value The value that should be set.
     * @throws IOException 
     */
    public void setField(String name, String value ) {
        if (value == null) {
            System.err.println("Potential value for field " + name + " is null.");
            return;
        }
        PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();
        PDField field = null;
        try {
            field = acroForm.getField( name );
        }
        catch (Exception e) {
            System.err.println("Error retrieving field from PDAcroForm.");
            return;
        }
        // Try to set the value of the given field.
        try {
            if( field != null) {
                field.setValue(value);        
            }
            else {
                System.err.println( "No field found with name:" + name );
            }
            return;
        }
        catch(Exception e) {
            System.err.println("Parent field could not be set.  Searching for child field candidates...");
        }
        // If the above fails, attempt to look for children that can be set.
        try {
            PDField child = findChild(field, name);
            if (child != null) {
                child.setValue(value);
            }
            else {
                System.err.println("No child field with name " + name + " found.");
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * This method is used to find a child PDField that matches the given name.  Used if the value of the 
     * top level field cannot be set.
     * @param field
     * @param name
     * @return
     * @throws IOException 
     */
    private PDField findChild(PDField field, String name) throws IOException {
        List kids = field.getKids();
        if(kids != null) {
            Iterator kidsIter = kids.iterator();
            while(kidsIter.hasNext()) {
               Object pdfObj = kidsIter.next();
               if(pdfObj instanceof PDField) {
                   PDField kid = (PDField)pdfObj;
                   if (kid.getAcroForm() != null && name.equals(field.getPartialName())) { 
                       return kid;
                   }
               }
            }
         }
        return null;
    }

        
    /**
     * This method prints the names of all of the form fields of a loaded PDF.
     * Assistance from: http://www.daedtech.com/programatically-filling-out-pdfs-in-java
     * @throws IOException 
     */
    public void printFields() throws IOException {
        PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();
        List fields = acroForm.getFields();
        Iterator fieldsIter = fields.iterator();

        System.out.println(new Integer(fields.size()).toString() + " top-level fields were found on the form");

        while( fieldsIter.hasNext()) {
            PDField field = (PDField)fieldsIter.next();
               processField(field, "|--", field.getPartialName());
        }
    }
    private static void processField(PDField field, String sLevel, String sParent) throws IOException {
        List kids = field.getKids();
        if(kids != null) {
            Iterator kidsIter = kids.iterator();
            if(!sParent.equals(field.getPartialName())) {
               sParent = sParent + "." + field.getPartialName();
            }
            
            System.out.println(sLevel + sParent);
            
            while(kidsIter.hasNext()) {
               Object pdfObj = kidsIter.next();
               if(pdfObj instanceof PDField) {
                   PDField kid = (PDField)pdfObj;
                   processField(kid, "|  " + sLevel, sParent);
               }
            }
         }
         else {
             String outputString = sLevel + sParent + "." + field.getPartialName() + ",  type=" + field.getClass().getName();
             System.out.println(outputString);
         }
    }
    
    /**
     * This method saves a PDF to a given file.
     * @throws COSVisitorException
     * @throws IOException 
     */
    public void savePDF(String targetFile) throws COSVisitorException, IOException {
        pdfDocument.save(targetFile);
        pdfDocument.close();
    }
    
    /**
     * This method saves a PDF to a given file.
     */
    public void savePDF(File targetFile) throws COSVisitorException, IOException {
        pdfDocument.save(targetFile);
        pdfDocument.close();
    }
}
