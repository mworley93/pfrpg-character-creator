/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.helpers;

import charactercreator.models.CharacterClass;
import charactercreator.models.CharacterRace;
import charactercreator.models.Feat;
import charactercreator.models.Skill;
import charactercreator.models.Spell;
import charactercreator.models.equipment.*;
import java.io.InputStream;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;

/**
 * This class constructs an XML reader that can be used to read in information from a file.  
 * @author Megan Worley, Brandon Sharp
 */
public class XMLReader {
    
    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    InputStream iStream;
    
    public XMLReader() 
    {
        try {
            dbFactory = DocumentBuilderFactory.newInstance();
        }
        catch (Exception e) {
            //e.printStackTrace();
        }              
    }
            
    
        /**
     * Used to read in descriptions of items in an XML file and store them in a
     * HashMap.  A file name and two tags are given and the reader returns all of the items containing that content.  For example, if the 
     * descriptions for the character classes were desired, the file name, keyword "class", and keyword "descriptions" would return a HashMap 
     * with key-value pairs such as <"Barbarian", String with description>.
     * @param fileName The XML file.
     * @param outerTagName The outer tag in the XML file associated with the
     * category, e.g. class, race
     * @param innerTagName The particular information that needs to be
     * retrieved, e.g. description or features.
     * @return A HashMap containing the data.
     */
    public HashMap<String, String> readDescriptiveFile(String fileName, String outerTagName, String innerTagName) 
    {
        HashMap<String, String> dataMap = new HashMap();
        try {     
            dBuilder = dbFactory.newDocumentBuilder();
            iStream = this.getClass().getResourceAsStream(fileName);
            Document doc = dBuilder.parse(iStream);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName(outerTagName);
            // Get all of the items sharing the given properties.
            for (int i = 0; i < nodes.getLength(); i++) {
                Node nNode = nodes.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getAttribute("id");
                    String text = eElement.getElementsByTagName(innerTagName).item(0).getTextContent();
                    dataMap.put(name, text);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }     
        return dataMap;
    }
    
    // This function takes a paragraph, and given a BreakIterator object can go through and separate the paragraph into sentences.
    // This is used to parse XML files for multiple class or race features on a single line.
    // The code here is taken from http://www.javaiq.net/javacoderanch/how-to-breaks-a-paragraph-into-sentences.html for reference
    
    public static String[] paragraphSplit(String source) {
        
        BreakIterator bi = BreakIterator.getSentenceInstance(Locale.US);
        
        int counter = 0;
        String[] stringArray = new String[100];
        bi.setText(source);
        
        int lastIndex = bi.first();
        while (lastIndex != BreakIterator.DONE) {
            int firstIndex = lastIndex;
            lastIndex = bi.next();
            
            if (lastIndex != BreakIterator.DONE) {
                String sentence = source.substring(firstIndex, lastIndex);
                stringArray[counter] = sentence;
                counter++;
            }
        }
        return stringArray;
    }
    
        /**
     * This function is designed to access an XML file that represents a Pathfinder class and all its relevant information.
     * It will then put this information into the CharacterClass that is passed (classToFill), giving it all the information it needs to be attached
     * to a BaseCharacter.
     */
    public void fillCharacterClass(String fileName, String className, CharacterClass classToFill) 
    {
        try{
            dBuilder = dbFactory.newDocumentBuilder();
            iStream = this.getClass().getResourceAsStream(fileName);
            Document doc = dBuilder.parse(iStream);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("class");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node nNode = nodes.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getAttribute("id");
                    if(name.compareTo(className) == 0) {
                        
                        classToFill.setClassName(name);
                        
                        String tempString = eElement.getElementsByTagName("classFeatures").item(0).getTextContent();
                        String[] tempStringArray = paragraphSplit(tempString);
                        classToFill.setClassFeatures(tempStringArray);
                        
                        int tempHitDie = Integer.parseInt(eElement.getElementsByTagName("hitDie").item(0).getTextContent());
                        classToFill.setHitDie(tempHitDie);
                        
                        int tempGold = Integer.parseInt(eElement.getElementsByTagName("gold").item(0).getTextContent());
                        classToFill.setStartingGold(tempGold);
                        
                        int tempSkillPoints = Integer.parseInt(eElement.getElementsByTagName("skillPoints").item(0).getTextContent());
                        classToFill.setSkillPoints(tempSkillPoints);
                        
                        int tempBaseAttackBonus = Integer.parseInt(eElement.getElementsByTagName("baseAttackBonus").item(0).getTextContent());
                        classToFill.setBaseAttackBonus(tempBaseAttackBonus);
                        
                        int tempFortitude = Integer.parseInt(eElement.getElementsByTagName("fortitude").item(0).getTextContent());
                        classToFill.setFortitude(tempFortitude);
                        
                        int tempReflex = Integer.parseInt(eElement.getElementsByTagName("reflex").item(0).getTextContent());
                        classToFill.setReflex(tempReflex);
                        
                        int tempWill = Integer.parseInt(eElement.getElementsByTagName("will").item(0).getTextContent());
                        classToFill.setWill(tempWill);
                        
                        int tempNumberOfFeats = Integer.parseInt(eElement.getElementsByTagName("numberOfFeats").item(0).getTextContent());
                        classToFill.setNumberOfFeats(tempNumberOfFeats);
                        
                        int tempBonusCombatFeats = Integer.parseInt(eElement.getElementsByTagName("bonusCombatFeats").item(0).getTextContent());
                        classToFill.setBonusCombatFeats(tempBonusCombatFeats);
                        
                        
                        // This portion of the function is dedicated to making an Array of the names of feats that the class begins with. The only possible feats here
                        // are potential proficiencies. These are represented as booleans in the XML file, and so are parsed as Booleans and if they are true,
                        // then the name of that particular proficiency feat is added to the array. This array will then be passed to FeatGenerator so that all these feats
                        // are added to BaseCharacter.
                        
                        String[] tempClassFeats = new String[10];
                        int j = 0;
                        
                        Boolean tempSimpleWeapon = Boolean.parseBoolean(eElement.getElementsByTagName("simpleWeapon").item(0).getTextContent());
                        if(tempSimpleWeapon) {
                           tempClassFeats[j] = "Simple Weapon Proficiency";
                           j++;
                        }
                        
                        Boolean tempMartialWeapon = Boolean.parseBoolean(eElement.getElementsByTagName("martialWeapon").item(0).getTextContent());
                        if(tempMartialWeapon) {
                           tempClassFeats[j] = "Martial Weapon Proficiency";
                           j++;
                        }
                     
                        Boolean tempLightArmor = Boolean.parseBoolean(eElement.getElementsByTagName("lightArmor").item(0).getTextContent());
                        if(tempLightArmor) {
                           tempClassFeats[j] = "Light Armor Proficiency";
                           j++;
                        }                        
                        
                        Boolean tempMediumArmor = Boolean.parseBoolean(eElement.getElementsByTagName("mediumArmor").item(0).getTextContent());
                        if(tempMediumArmor) {
                           tempClassFeats[j] = "Medium Armor Proficiency";
                           j++;
                        } 
                        
                        Boolean tempHeavyArmor = Boolean.parseBoolean(eElement.getElementsByTagName("heavyArmor").item(0).getTextContent());
                        if(tempHeavyArmor) {
                           tempClassFeats[j] = "Heavy Armor Proficiency";
                           j++;
                        } 
                        
                        Boolean tempShield = Boolean.parseBoolean(eElement.getElementsByTagName("shield").item(0).getTextContent());
                        if(tempShield) {
                           tempClassFeats[j] = "Shield Proficiency";
                           j++;
                        }
                        
                        classToFill.addClassFeats(tempClassFeats);
                        
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    // This function takes an XML file full of booleans declaring whether or not a skill is a specific class skill or not. This fuction reads those into an EnumMap
    // and will set that to the passed CharacterClass to be used on the Skills page.
    
    public void fillCharacterClassSkills(String fileName, CharacterClass classToFill) {
        try{
            dBuilder = dbFactory.newDocumentBuilder();
            iStream = this.getClass().getResourceAsStream(fileName);
            Document doc = dBuilder.parse(iStream);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("class");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node nNode = nodes.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getAttribute("id");
                    if(name.matches(classToFill.getClassName())) {
                        EnumMap<Skill, Boolean> tempSkillMap = new EnumMap<>(Skill.class);
                        
                        Boolean acrobatics = Boolean.parseBoolean(eElement.getElementsByTagName("acrobatics").item(0).getTextContent());
                        tempSkillMap.put(Skill.ACROBATICS, acrobatics);
                        
                        Boolean appraise = Boolean.parseBoolean(eElement.getElementsByTagName("appraise").item(0).getTextContent());
                        tempSkillMap.put(Skill.APPRAISE, appraise);
                        
                        Boolean bluff = Boolean.parseBoolean(eElement.getElementsByTagName("bluff").item(0).getTextContent());
                        tempSkillMap.put(Skill.BLUFF, bluff);
                        
                        Boolean climb = Boolean.parseBoolean(eElement.getElementsByTagName("climb").item(0).getTextContent());
                        tempSkillMap.put(Skill.CLIMB, climb);
                        
                        Boolean craft = Boolean.parseBoolean(eElement.getElementsByTagName("craft").item(0).getTextContent());
                        tempSkillMap.put(Skill.CRAFT, craft);   
                        
                        Boolean diplomacy = Boolean.parseBoolean(eElement.getElementsByTagName("diplomacy").item(0).getTextContent());
                        tempSkillMap.put(Skill.DIPLOMACY, diplomacy);
                        
                        Boolean disableDevice = Boolean.parseBoolean(eElement.getElementsByTagName("disabledevice").item(0).getTextContent());
                        tempSkillMap.put(Skill.DISABLEDEVICE, disableDevice);
                        
                        Boolean disguise = Boolean.parseBoolean(eElement.getElementsByTagName("disguise").item(0).getTextContent());
                        tempSkillMap.put(Skill.DISGUISE, disguise);
                        
                        Boolean escapeArtist = Boolean.parseBoolean(eElement.getElementsByTagName("escapeartist").item(0).getTextContent());
                        tempSkillMap.put(Skill.ESCAPEARTIST, escapeArtist);
                        
                        Boolean fly = Boolean.parseBoolean(eElement.getElementsByTagName("fly").item(0).getTextContent());
                        tempSkillMap.put(Skill.FLY, fly);
                        
                        Boolean handleAnimal = Boolean.parseBoolean(eElement.getElementsByTagName("handleanimal").item(0).getTextContent());
                        tempSkillMap.put(Skill.HANDLEANIMAL, handleAnimal);
                        
                        Boolean heal = Boolean.parseBoolean(eElement.getElementsByTagName("heal").item(0).getTextContent());
                        tempSkillMap.put(Skill.HEAL, heal);
                        
                        Boolean intimidate = Boolean.parseBoolean(eElement.getElementsByTagName("intimidate").item(0).getTextContent());
                        tempSkillMap.put(Skill.INTIMIDATE, intimidate);
                        
                        Boolean knowledgeArcana = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgearcana").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGEARCANA, knowledgeArcana);
                        
                        Boolean knowledgeDungeoneering = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgedungeoneering").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGEDUNGEONEERING, knowledgeDungeoneering);
                        
                        Boolean knowledgeEngineering = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgeengineering").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGEENGINEERING, knowledgeEngineering);
                        
                        Boolean knowledgeGeography = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgegeography").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGEGEOGRAPHY, knowledgeGeography);
                        
                        Boolean knowledgeHistory = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgehistory").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGEHISTORY, knowledgeHistory);
                        
                        Boolean knowledgeLocal = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgelocal").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGELOCAL, knowledgeLocal);
                        
                        Boolean knowledgeNature = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgenature").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGENATURE, knowledgeNature);
                        
                        Boolean knowledgeNobility = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgenobility").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGENOBILITY, knowledgeNobility);
                        
                        Boolean knowledgePlanes = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgeplanes").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGEPLANES, knowledgePlanes);      
                        
                        Boolean knowledgeReligion = Boolean.parseBoolean(eElement.getElementsByTagName("knowledgereligion").item(0).getTextContent());
                        tempSkillMap.put(Skill.KNOWLEDGERELIGION, knowledgeReligion);
                        
                        Boolean linguistics = Boolean.parseBoolean(eElement.getElementsByTagName("linguistics").item(0).getTextContent());
                        tempSkillMap.put(Skill.LINGUISTICS, linguistics);
                        
                        Boolean perception = Boolean.parseBoolean(eElement.getElementsByTagName("perception").item(0).getTextContent());
                        tempSkillMap.put(Skill.PERCEPTION, perception);
                        
                        Boolean perform = Boolean.parseBoolean(eElement.getElementsByTagName("perform").item(0).getTextContent());
                        tempSkillMap.put(Skill.PERFORM, perform);
                        
                        Boolean profession = Boolean.parseBoolean(eElement.getElementsByTagName("profession").item(0).getTextContent());
                        tempSkillMap.put(Skill.PROFESSION, profession);
                        
                        Boolean ride = Boolean.parseBoolean(eElement.getElementsByTagName("ride").item(0).getTextContent());
                        tempSkillMap.put(Skill.RIDE, ride);
                        
                        Boolean senseMotive = Boolean.parseBoolean(eElement.getElementsByTagName("sensemotive").item(0).getTextContent());
                        tempSkillMap.put(Skill.SENSEMOTIVE, senseMotive);
                        
                        Boolean sleightOfHand = Boolean.parseBoolean(eElement.getElementsByTagName("sleightofhand").item(0).getTextContent());
                        tempSkillMap.put(Skill.SLEIGHTOFHAND, sleightOfHand);
                        
                        Boolean spellcraft = Boolean.parseBoolean(eElement.getElementsByTagName("spellcraft").item(0).getTextContent());
                        tempSkillMap.put(Skill.SPELLCRAFT, spellcraft);
                        
                        Boolean stealth = Boolean.parseBoolean(eElement.getElementsByTagName("stealth").item(0).getTextContent());
                        tempSkillMap.put(Skill.STEALTH, stealth);                        
                        
                        Boolean survival = Boolean.parseBoolean(eElement.getElementsByTagName("survival").item(0).getTextContent());
                        tempSkillMap.put(Skill.SURVIVAL, survival);
                        
                        Boolean swim = Boolean.parseBoolean(eElement.getElementsByTagName("swim").item(0).getTextContent());
                        tempSkillMap.put(Skill.SWIM, swim);
                        
                        Boolean useMagicDevice = Boolean.parseBoolean(eElement.getElementsByTagName("usemagicdevice").item(0).getTextContent());
                        tempSkillMap.put(Skill.USEMAGICDEVICE, useMagicDevice);                        
                        
                        classToFill.setClassSkills(tempSkillMap);
                    }


                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
            
    }

    public void fillRaceClass(String fileName, String raceName, CharacterRace raceToFill) {
         try{
            dBuilder = dbFactory.newDocumentBuilder();
            iStream = this.getClass().getResourceAsStream(fileName);
            Document doc = dBuilder.parse(iStream);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("race");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node nNode = nodes.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getAttribute("id");
                    if(name.compareTo(raceName) == 0) {
                        
                        raceToFill.setRaceName(raceName);
                        
                        String tempString = eElement.getElementsByTagName("raceFeatures").item(0).getTextContent();
                        String[] tempStringArray = paragraphSplit(tempString);
                        raceToFill.setRaceFeatures(tempStringArray);
                        
                        String tempString2 = eElement.getElementsByTagName("racefeatures").item(0).getTextContent();
                        String[] tempStringArray2 = paragraphSplit(tempString2);
                        raceToFill.setRaceFeatures(tempStringArray2);
                        
                        String tempSize = eElement.getElementsByTagName("size").item(0).getTextContent();
                        raceToFill.setSize(tempSize);
                        
                        int tempSpeed = Integer.parseInt(eElement.getElementsByTagName("speed").item(0).getTextContent());
                        raceToFill.setSpeed(tempSpeed);
                        
                        tempString = eElement.getElementsByTagName("languages").item(0).getTextContent();
                        tempStringArray = paragraphSplit(tempString);
                        raceToFill.setLanguages(tempStringArray);
                                               
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
               
    }
 
    public void readEquipment(String fileName, List<Equipment> equipmentList) {
        
        readArmor(fileName, equipmentList);
        readWeapons(fileName, equipmentList);
        
    }
    
    public void readArmor(String fileName, List<Equipment> equipmentList) {
         try{
            dBuilder = dbFactory.newDocumentBuilder();
            iStream = this.getClass().getResourceAsStream(fileName);
            Document doc = dBuilder.parse(iStream);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("armor");
            
            for (int i = 0; i < nodes.getLength(); i++) {
                Node nNode = nodes.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getAttribute("id");
                    
                    Armor newArmor = new Armor();
                    
                    newArmor.setName(name);
                    
                    String tempDescription = eElement.getElementsByTagName("description").item(0).getTextContent();
                    newArmor.setEquipmentDescription(tempDescription);
                    
                    double tempGold = Double.parseDouble(eElement.getElementsByTagName("gold").item(0).getTextContent());
                    newArmor.setCost(tempGold);
                    
                    double tempWeight = Double.parseDouble(eElement.getElementsByTagName("weight").item(0).getTextContent());
                    newArmor.setWeight(tempWeight);
                    
                    int tempArmorBonus = Integer.parseInt(eElement.getElementsByTagName("armorBonus").item(0).getTextContent());
                    newArmor.setArmorBonus(tempArmorBonus);
                    
                    int tempDexBonus = Integer.parseInt(eElement.getElementsByTagName("maxDexBonus").item(0).getTextContent());
                    newArmor.setMaxDexBonus(tempDexBonus);
                    
                    int tempArmorCheckPenalty = Integer.parseInt(eElement.getElementsByTagName("armorCheckPenalty").item(0).getTextContent());
                    newArmor.setArmorCheckPenalty(tempArmorCheckPenalty);
                    
                    double tempArcaneFailureChance = Double.parseDouble(eElement.getElementsByTagName("arcaneFailureChance").item(0).getTextContent());
                    newArmor.setArcaneFailureChance(tempArcaneFailureChance);
                    
                    String tempProficiency = eElement.getElementsByTagName("proficiency").item(0).getTextContent();
                    newArmor.setProficiencyRequired(tempProficiency);
                    
                    String tempEquipmentType = eElement.getElementsByTagName("type").item(0).getTextContent();
                    newArmor.setEquipmentType(tempEquipmentType);
                    
                    equipmentList.add(newArmor);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }        
    }
    
    public void readWeapons(String fileName, List<Equipment> equipmentList) {
        try{
           dBuilder = dbFactory.newDocumentBuilder();
           iStream = this.getClass().getResourceAsStream(fileName);
           Document doc = dBuilder.parse(iStream);
           doc.getDocumentElement().normalize();
           NodeList nodes = doc.getElementsByTagName("weapon");

           for (int i = 0; i < nodes.getLength(); i++) {
               Node nNode = nodes.item(i);
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                   Element eElement = (Element) nNode;
                   String name = eElement.getAttribute("id");

                   Weapon newWeapon = new Weapon();

                   newWeapon.setName(name);

                   String tempDescription = eElement.getElementsByTagName("description").item(0).getTextContent();
                   newWeapon.setEquipmentDescription(tempDescription);
                    
                   double tempGold = Double.parseDouble(eElement.getElementsByTagName("gold").item(0).getTextContent());
                   newWeapon.setCost(tempGold);

                   double tempWeight = Double.parseDouble(eElement.getElementsByTagName("weight").item(0).getTextContent());
                   newWeapon.setWeight(tempWeight);

                   String tempSmallDamage = eElement.getElementsByTagName("smallDamage").item(0).getTextContent();
                   newWeapon.setSmallDamage(tempSmallDamage);
                   
                   String tempMediumDamage = eElement.getElementsByTagName("mediumDamage").item(0).getTextContent();
                   newWeapon.setMediumDamage(tempMediumDamage);
                   
                   String tempCritical = eElement.getElementsByTagName("critical").item(0).getTextContent();
                   newWeapon.setCritical(tempCritical);
                   
                   int tempRange = Integer.parseInt(eElement.getElementsByTagName("range").item(0).getTextContent());
                   newWeapon.setRange(tempRange);
                   
                   String tempDamageType = eElement.getElementsByTagName("damageType").item(0).getTextContent();
                   newWeapon.setDamageType(tempDamageType);
                   
                   String tempProficiency = eElement.getElementsByTagName("proficiency").item(0).getTextContent();
                   newWeapon.setProficiencyRequired(tempProficiency);  
                   
                    String tempEquipmentType = eElement.getElementsByTagName("type").item(0).getTextContent();
                    newWeapon.setEquipmentType(tempEquipmentType);                   
                   
                   equipmentList.add(newWeapon);
               }
           }
       }
       catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
    public void readFeats(String fileName, ArrayList<Feat> featList) {
        try{           
           dBuilder = dbFactory.newDocumentBuilder();
           iStream = this.getClass().getResourceAsStream(fileName);
           Document doc = dBuilder.parse(iStream);
           doc.getDocumentElement().normalize();
           NodeList nodes = doc.getElementsByTagName("feat");
           for (int i = 0; i < nodes.getLength(); i++) {
               Node nNode = nodes.item(i);
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                   Element eElement = (Element) nNode;
                   
                   Feat newFeat = new Feat();

                   String tempName = eElement.getElementsByTagName("name").item(0).getTextContent();
                   newFeat.setFeatName(tempName);
                   
                   String tempDescription = eElement.getElementsByTagName("description").item(0).getTextContent();
                   newFeat.setDescription(tempDescription);
                   
                   String tempPrerequisites = eElement.getElementsByTagName("prerequisite").item(0).getTextContent();
                   newFeat.setPrerequisite(tempPrerequisites);
                   
                   Boolean tempCombatFeat = Boolean.parseBoolean(eElement.getElementsByTagName("combatfeat").item(0).getTextContent());
                   newFeat.setIsCombatFeat(tempCombatFeat);
                   
                   featList.add(newFeat);
                   
               }
           }
       }
       catch (Exception e) {
           e.printStackTrace();
       }        
    }    
    

    
    public HashMap readAges(String fileName) {
        HashMap<String, Range> ages = new HashMap<String, Range>();
        try{           
           dBuilder = dbFactory.newDocumentBuilder();
           iStream = this.getClass().getResourceAsStream(fileName);
           Document doc = dBuilder.parse(iStream);
           doc.getDocumentElement().normalize();
           Node ageNode = doc.getElementsByTagName("age").item(0);
           NodeList raceAges = ageNode.getChildNodes();
           for (int i = 0; i < raceAges.getLength(); i++) {
               Node nNode = raceAges.item(i);
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                   Element eElement = (Element) nNode;
                   String max = eElement.getElementsByTagName("max").item(0).getTextContent();
                   String min = eElement.getElementsByTagName("min").item(0).getTextContent();
                   Range ageRange = new Range(Integer.parseInt(min), Integer.parseInt(max));
                   ages.put(eElement.getAttribute("id"), ageRange);
               }
           }
        }
        catch (Exception e) {
           e.printStackTrace();
        }
        return ages;
    }

    
        public HashMap[] readHeights(String fileName) {
            HashMap[] heights = new HashMap[4];
            HashMap<String, Integer> maleHeightBase = new HashMap<String, Integer>();
            HashMap<String, Integer> femaleHeightBase = new HashMap<String, Integer>();
            HashMap<String, Integer> maleHeightMod = new HashMap<String, Integer>();
            HashMap<String, Integer> femaleHeightMod = new HashMap<String, Integer>();
            try{           
                dBuilder = dbFactory.newDocumentBuilder();
                iStream = this.getClass().getResourceAsStream(fileName);
                Document doc = dBuilder.parse(iStream);
                doc.getDocumentElement().normalize();
                Node heightNode = doc.getElementsByTagName("height").item(0);
                NodeList raceHeights = heightNode.getChildNodes();
                for (int i = 0; i < raceHeights.getLength(); i++) {
                    Node nNode = raceHeights.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        NodeList genderNodes = nNode.getChildNodes();
                        Node maleNode = genderNodes.item(1);
                        if (maleNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element maleElement = (Element)maleNode;
                            String maleBase = maleElement.getElementsByTagName("base").item(0).getTextContent();
                            String maleMod = maleElement.getElementsByTagName("modifier").item(0).getTextContent();
                            maleHeightBase.put(eElement.getAttribute("id"), Integer.parseInt(maleBase));
                            maleHeightMod.put(eElement.getAttribute("id"), Integer.parseInt(maleMod));
                        }
                        Node femaleNode = genderNodes.item(3);
                        if (femaleNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element femaleElement = (Element)femaleNode;
                            String femaleBase = femaleElement.getElementsByTagName("base").item(0).getTextContent();
                            String femaleMod = femaleElement.getElementsByTagName("modifier").item(0).getTextContent();
                            femaleHeightBase.put(eElement.getAttribute("id"), Integer.parseInt(femaleBase));
                            femaleHeightMod.put(eElement.getAttribute("id"), Integer.parseInt(femaleMod));
                        }
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }   
            heights[0] = maleHeightBase;
            heights[1] = maleHeightMod;
            heights[2] = femaleHeightBase;
            heights[3] = femaleHeightMod;
            return heights;
    }
        
         public HashMap[] readWeights(String fileName) {
            HashMap[] weights = new HashMap[4];
            HashMap<String, Integer> maleWeightBase = new HashMap<String, Integer>();
            HashMap<String, Integer> femaleWeightBase = new HashMap<String, Integer>();
            HashMap<String, Integer> maleWeightMod = new HashMap<String, Integer>();
            HashMap<String, Integer> femaleWeightMod = new HashMap<String, Integer>();
            try{           
                dBuilder = dbFactory.newDocumentBuilder();
                iStream = this.getClass().getResourceAsStream(fileName);
                Document doc = dBuilder.parse(iStream);
                doc.getDocumentElement().normalize();
                Node weightNode = doc.getElementsByTagName("weight").item(0);
                NodeList raceWeights = weightNode.getChildNodes();
                for (int i = 0; i < raceWeights.getLength(); i++) {
                    Node nNode = raceWeights.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        NodeList genderNodes = nNode.getChildNodes();
                        Node maleNode = genderNodes.item(1);
                        if (maleNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element maleElement = (Element)maleNode;
                            String maleBase = maleElement.getElementsByTagName("base").item(0).getTextContent();
                            String maleMod = maleElement.getElementsByTagName("modifier").item(0).getTextContent();
                            maleWeightBase.put(eElement.getAttribute("id"), Integer.parseInt(maleBase));
                            maleWeightMod.put(eElement.getAttribute("id"), Integer.parseInt(maleMod));
                        }
                        Node femaleNode = genderNodes.item(3);
                        if (femaleNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element femaleElement = (Element)femaleNode;
                            String femaleBase = femaleElement.getElementsByTagName("base").item(0).getTextContent();
                            String femaleMod = femaleElement.getElementsByTagName("modifier").item(0).getTextContent();
                            femaleWeightBase.put(eElement.getAttribute("id"), Integer.parseInt(femaleBase));
                            femaleWeightMod.put(eElement.getAttribute("id"), Integer.parseInt(femaleMod));
                        }
                    }
                }
                }
                catch (Exception e) {
               e.printStackTrace();
            }   
            weights[0] = maleWeightBase;
            weights[1] = maleWeightMod;
            weights[2] = femaleWeightBase;
            weights[3] = femaleWeightMod;
            return weights;
    }

    public void readSpells(String fileName, ArrayList<Spell> spellList) {
         try{
           dBuilder = dbFactory.newDocumentBuilder();
           iStream = this.getClass().getResourceAsStream(fileName);
           Document doc = dBuilder.parse(iStream);
           doc.getDocumentElement().normalize();        
           NodeList nodes = doc.getElementsByTagName("spell");
           for (int i = 0; i < nodes.getLength(); i++) {
               Node nNode = nodes.item(i);
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                   Element eElement = (Element) nNode;
                   
                   Spell newSpell = new Spell();

                   String tempName = eElement.getElementsByTagName("name").item(0).getTextContent();
                   newSpell.setSpellName(tempName);
                   
                   String tempDescription = eElement.getElementsByTagName("description").item(0).getTextContent();
                   newSpell.setDescription(tempDescription);
                   
                   String tempLongDescription = eElement.getElementsByTagName("longdescription").item(0).getTextContent();
                   newSpell.setLongDescription(tempLongDescription);
                   
                   String tempSchool = eElement.getElementsByTagName("school").item(0).getTextContent();
                   newSpell.setSchool(tempSchool);
                   
                   int tempSpellLevel = Integer.parseInt(eElement.getElementsByTagName("level").item(0).getTextContent());
                   newSpell.setSpellLevel(tempSpellLevel);
                   
                   spellList.add(newSpell);
                   
               }
           }
       }
       catch (Exception e) {
           e.printStackTrace();
       }
    }
}
