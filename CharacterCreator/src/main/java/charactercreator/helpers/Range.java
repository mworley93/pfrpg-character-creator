/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.helpers;

import java.util.Random;

/**
 * This class acts a a range between a minimum given number and a maximum given number.
 * @author Megan
 */
public class Range {
    private int min;
    private int max;
    
    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }
    
    public void setMin(int min) {
        this.min = min;
    }
    
    public int getMin() {
        return min;
    }
    
    public void setMax(int max) {
        this.max = max;
    }
    
    public int getMax() {
        return max;
    }
    
    /**
     * Returns a random number within this range.
     * @return 
     */
    public int getRandom() {
        Random rand = new Random();
        int random = min + rand.nextInt((max - min) + 1);
        return random;
    }
}
