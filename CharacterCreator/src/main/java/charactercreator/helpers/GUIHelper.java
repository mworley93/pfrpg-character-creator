/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package charactercreator.helpers;

import charactercreator.components.PathfinderColor;
import charactercreator.components.TranslucentScrollPane;
import charactercreator.components.PathfinderScrollTable;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * This class holds reusable methods that are useful for creating Swing components.  Note: A lot of the resizing for now relies on setting the maximum
 * and the minimum sizes, so the sizes of the components are "hardcoded" in rather than being relative.  Eventually the preferred size will just
 * be set and the the .pack(true) function will be called on the JFrame, so the components will size themselves.
 * @author Megan Worley, Brandon Sharp
 */
public class GUIHelper {
    
    /**
     * Main constructor.
     */
    public GUIHelper() { }
    
    /**
     * Creates a Group Layout with gaps and attaches it to a JPanel.
     *
     * @param contentPanel The panel that needs the group layout.
     * @return
     */
    public GroupLayout constructGroupLayout(JPanel contentPanel) {
        GroupLayout layout = new GroupLayout(contentPanel);
        contentPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        return layout;
    }

    /**
     * Creates a JLabel with an image.
     *
     * @param imagePath The file path of the image.
     * @return JLabel
     */
    public JLabel constructImageLabel(String imagePath) {
        ImageIcon icon;
        JLabel imageLabel;
        java.net.URL imgURL = getClass().getResource(imagePath);
        if (imgURL != null) {
            icon = new ImageIcon(imgURL, null);
            imageLabel = new JLabel(icon);
        } else {
            System.err.println("Couldn't find file: " + imagePath);
            return null;
        }
        return imageLabel;
    }
    
    /**
     * Adds a raised beveled border to an image on a JLabel.  (This is the standard image border for this app).
     * @param image The JLabel with the image icon.
     */
    public void addBeveledImageBorder(JLabel image) {
        LineBorder line = (LineBorder)BorderFactory.createLineBorder(Color.getHSBColor(33f, .67f, .17f), 3); 
        BevelBorder raisedBevel = (BevelBorder)BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.getHSBColor(.17f, .1f, .65f), PathfinderColor.BROWN_HIGHLIGHT.value());
        CompoundBorder raisedVisibleBorder = (CompoundBorder)BorderFactory.createCompoundBorder(raisedBevel, line);
        image.setBorder(raisedVisibleBorder);   
    }

    /**
     * This function creates a custom text area meant to hold plain text with no formatting.
     *
     * @param topic The text to be displayed.
     * @param opaque Whether the area should be opaque or not. Transparent backgrounds are good for placing on scroll panes, while opaque backgrounds are
     *               good for standalone text boxes.
     * @param size An optional size, primarily recommended for standalone text boxes.  null is recommended if placing inside of a scroll pane or tabbed 
     *             pane, as it will just fill up that component's size.
     * @return the created text area.
     */
    public JTextArea createPlainTextArea(String topic, boolean opaque, Dimension size) {      
        JTextArea text = new JTextArea(topic);
        text.setEditable(false);
        text.setFont(new Font("Serif", Font.BOLD, 16));
        text.setLineWrap(true);
        text.setWrapStyleWord(true);
        text.setBorder(BorderFactory.createEmptyBorder(0,8,0,8));
        text.setOpaque(opaque);
        if (size != null) {
            text.setMaximumSize(size);
            text.setMinimumSize(size);
            text.setPreferredSize(size);
        }
        return text;
    }
    
    /**
     * Creates a new JTable with a non-editable, custom display.
     * @param data The data to be displayed in rows.
     * @param columnNames The name of the columns.
     * @return The new JTable.
     */
    public JTable createTable(Object[][] data, String[] columnNames) {      
        JTable table = new JTable(data, columnNames) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return table;
    }
    
    /**
     * Creates a new table with a non-editable, custom display with a scrolling interface.  Implementation is achieved by combining the PathfinderScrollTable and
     * TranslucentScrollPane implementations.
     * @param data The data to be displayed in rows.
     * @param columnNames The name of the columns.
     * @return The new TranslucentScrollPane.
     */
    public TranslucentScrollPane createScrollingTable(Object[][] data, String[] columnNames, Dimension size) {     
        PathfinderScrollTable table = new PathfinderScrollTable(data, columnNames, size);
        TranslucentScrollPane scrollPane = new TranslucentScrollPane(table, size);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        return scrollPane;
    }
    
    /**
    * This function creates a simple scrolling text area.
    * @param text The text that needs to be added.
    * @return 
    */
    public JScrollPane createScrollableText(JTextArea text, Dimension size) {
        JScrollPane scrollPanel = new JScrollPane(text);
        scrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPanel.setMaximumSize(size);
        scrollPanel.setMinimumSize(size);
        scrollPanel.setPreferredSize(size);
        scrollPanel.setOpaque(false);
        scrollPanel.getViewport().setOpaque(false);
        return scrollPanel;       

    }
    
   /**
    * Creates a new JList that is meant to be placed upon a JScrollPane.
    * @param data The items in the list.
    * @param selectionMode The way items can be selected, e.g. ListSelectionModel.SINGLE_INTERVAL_SELECTION 
    * @param orientation The way the list is displayed, e.g. JList.VERTICAL_WRAP.
    * @return The new JList.
    */
    public JList createTransparentList(Object[] data, int selectionMode, int orientation) {
        JList list = new JList(data); 
        list.setSelectionMode(selectionMode);
        list.setLayoutOrientation(orientation);
        list.setVisibleRowCount(-1);
        list.setOpaque(false);
        return list;
    }
}
